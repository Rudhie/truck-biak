-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 04, 2021 at 05:05 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `truck_biak`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_general_email`
--

CREATE TABLE `tbl_general_email` (
  `id_email` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `is_active` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_general_email`
--

INSERT INTO `tbl_general_email` (`id_email`, `email`, `is_active`) VALUES
(1, 'muh.rudi.hariyanto@gmail.com', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_log_reminder`
--

CREATE TABLE `tbl_log_reminder` (
  `id_log` int(11) NOT NULL,
  `email_terkirim` varchar(200) DEFAULT NULL,
  `tanggal_log` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_log_reminder`
--

INSERT INTO `tbl_log_reminder` (`id_log`, `email_terkirim`, `tanggal_log`) VALUES
(12, 'robby_biak@yahoo.com', '2020-08-27 07:00:13'),
(13, 'robby_biak@yahoo.com', '2020-08-27 07:00:14'),
(14, 'manisaprop@dibiak.com', '2020-09-01 07:00:12'),
(15, 'mahkotadewabiak@gmail.com', '2020-09-01 07:00:13'),
(16, 'gasirja@dibiak.com', '2020-09-01 07:00:13'),
(17, 'papuamajumandiri@yahoo.com', '2020-09-01 07:00:19'),
(18, 'robby_biak@yahoo.com', '2020-09-12 07:00:13'),
(19, 'gasirja@dibiak.com', '2020-09-12 07:00:14'),
(20, 'robby_biak@yahoo.com', '2020-09-12 07:00:14'),
(21, 'robby_biak@yahoo.com', '2020-09-15 18:02:25'),
(22, 'gasirja@dibiak.com', '2020-09-15 18:02:25'),
(23, 'robby_biak@yahoo.com', '2020-09-15 18:02:31'),
(24, 'manisaprop@dibiak.com', '2020-09-16 07:27:46'),
(25, 'robby_biak@yahoo.com', '2020-09-16 07:27:46'),
(26, 'mahkotadewabiak@gmail.com', '2020-09-16 07:27:52'),
(27, 'robby_biak@yahoo.com', '2020-09-19 07:00:13'),
(28, 'robby_biak@yahoo.com', '2020-09-19 07:00:15'),
(29, 'papuamajumandiri@yahoo.com', '2020-09-21 07:00:13'),
(30, 'robby_biak@yahoo.com', '2020-09-25 07:00:12'),
(31, 'robby_biak@yahoo.com', '2020-09-25 07:00:14'),
(32, 'robby_biak@yahoo.com', '2020-09-26 07:00:13'),
(33, 'robby_biak@yahoo.com', '2020-09-26 07:00:14'),
(34, 'gasirja@dibiak.com', '2020-10-01 07:00:13'),
(35, 'robby_biak@yahoo.com', '2020-10-02 07:00:13'),
(36, 'manisaprop@dibiak.com', '2020-10-02 07:00:13'),
(37, 'robby_biak@yahoo.com', '2020-10-02 07:00:14'),
(38, 'gasirja@dibiak.com', '2020-10-02 07:00:14'),
(39, 'waiting ...', '2020-10-03 07:00:03'),
(40, 'robby_biak@yahoo.com', '2020-10-03 07:00:13'),
(41, 'suyat9056@gmail.com', '2020-10-03 07:00:14'),
(42, 'robby_biak@yahoo.com', '2020-10-03 07:00:14'),
(43, 'papuamajumandiri@yahoo.com', '2020-10-05 07:00:13'),
(44, 'robby_biak@yahoo.com', '2020-10-07 07:00:12'),
(45, 'robby_biak@yahoo.com', '2020-10-09 07:00:13'),
(46, 'robby_biak@yahoo.com', '2020-10-09 07:00:14'),
(47, 'robby_biak@yahoo.com', '2020-10-10 07:00:13'),
(48, 'robby_biak@yahoo.com', '2020-10-16 07:00:13'),
(49, 'robby_biak@yahoo.com', '2020-10-16 07:00:15'),
(50, 'papuamajumandiri@yahoo.com', '2020-10-21 07:00:12'),
(51, 'robby_biak@yahoo.com', '2020-10-23 07:00:13'),
(52, 'gasirja@dibiak.com', '2020-10-23 07:00:14'),
(53, 'papuamajumandiri@yahoo.com', '2020-10-23 07:00:14'),
(54, 'robby_biak@yahoo.com', '2020-10-23 07:00:20'),
(55, 'manisaprop@dibiak.com', '2020-10-24 07:00:13'),
(56, 'gasirja@dibiak.com', '2020-10-24 07:00:15'),
(57, 'papuamajumandiri@yahoo.com', '2020-10-28 07:00:10'),
(58, 'gasirja@dibiak.com', '2020-10-31 07:00:12'),
(59, 'manisaprop@dibiak.com', '2020-11-01 07:00:13'),
(60, 'gasirja@dibiak.com', '2020-11-01 07:00:14'),
(61, 'waiting ...', '2020-11-02 07:00:02'),
(62, 'suyat9056@gmail.com', '2020-11-02 07:00:12'),
(63, 'mahkotadewabiak@gmail.com', '2020-11-02 07:00:14'),
(64, 'gasirja@dibiak.com', '2020-11-02 07:00:14'),
(65, 'papuamajumandiri@yahoo.com', '2020-11-02 07:00:20'),
(66, 'suyat9056@gmail.com', '2020-11-03 07:00:13'),
(67, 'papuamajumandiri@yahoo.com', '2020-11-04 07:00:13'),
(68, 'robby_biak@yahoo.com', '2020-11-06 07:00:13'),
(69, 'papuamajumandiri@yahoo.com', '2020-11-11 07:00:15'),
(70, 'suyat9056@gmail.com', '2020-11-14 07:00:13'),
(71, 'gasirja@dibiak.com', '2020-11-14 07:00:13'),
(72, 'manisaprop@dibiak.com', '2020-11-15 07:00:12'),
(73, 'suyat9056@gmail.com', '2020-11-15 07:00:14'),
(74, 'gasirja@dibiak.com', '2020-11-15 07:00:14'),
(75, 'waiting ...', '2020-11-16 07:00:03'),
(76, 'suyat9056@gmail.com', '2020-11-16 07:00:13'),
(77, 'waiting ...', '2020-11-16 13:57:08'),
(78, 'manisaprop@dibiak.com', '2020-11-16 13:57:14'),
(79, 'robby_biak@yahoo.com', '2020-11-16 13:57:14'),
(80, 'suyat9056@gmail.com', '2020-11-16 13:57:20'),
(81, 'mahkotadewabiak@gmail.com', '2020-11-16 13:57:26'),
(82, 'gasirja@dibiak.com', '2020-11-16 13:57:32'),
(83, 'papuamajumandiri@yahoo.com', '2020-11-16 13:57:38'),
(84, 'robby_biak@yahoo.com', '2020-11-16 13:57:44'),
(85, 'manisaprop@dibiak.com', '2020-11-17 09:23:22'),
(86, 'robby_biak@yahoo.com', '2020-11-17 09:23:22'),
(87, 'suyat9056@gmail.com', '2020-11-17 09:23:28'),
(88, 'gasirja@dibiak.com', '2020-11-17 09:23:34'),
(89, 'robby_biak@yahoo.com', '2020-11-17 09:23:40'),
(90, 'manisaprop@dibiak.com', '2020-11-17 09:24:58'),
(91, 'robby_biak@yahoo.com', '2020-11-17 09:24:58'),
(92, 'suyat9056@gmail.com', '2020-11-17 09:25:04'),
(93, 'gasirja@dibiak.com', '2020-11-17 09:25:10'),
(94, 'robby_biak@yahoo.com', '2020-11-17 09:25:16'),
(95, 'manisaprop@dibiak.com', '2020-11-17 09:28:26'),
(96, 'robby_biak@yahoo.com', '2020-11-17 09:28:26'),
(97, 'suyat9056@gmail.com', '2020-11-17 09:28:32'),
(98, 'gasirja@dibiak.com', '2020-11-17 09:28:38'),
(99, 'robby_biak@yahoo.com', '2020-11-17 09:28:44'),
(100, 'manisaprop@dibiak.com', '2020-11-17 09:29:35'),
(101, 'robby_biak@yahoo.com', '2020-11-17 09:29:35'),
(102, 'suyat9056@gmail.com', '2020-11-17 09:29:41'),
(103, 'gasirja@dibiak.com', '2020-11-17 09:29:47'),
(104, 'robby_biak@yahoo.com', '2020-11-17 09:29:53'),
(105, 'manisaprop@dibiak.com', '2020-11-17 09:30:54'),
(106, 'robby_biak@yahoo.com', '2020-11-17 09:30:54'),
(107, 'suyat9056@gmail.com', '2020-11-17 09:31:00'),
(108, 'gasirja@dibiak.com', '2020-11-17 09:31:06'),
(109, 'robby_biak@yahoo.com', '2020-11-17 09:31:12'),
(110, 'manisaprop@dibiak.com', '2020-11-17 09:34:04'),
(111, 'robby_biak@yahoo.com', '2020-11-17 09:34:04'),
(112, 'suyat9056@gmail.com', '2020-11-17 09:34:10'),
(113, 'gasirja@dibiak.com', '2020-11-17 09:34:16'),
(114, 'robby_biak@yahoo.com', '2020-11-17 09:34:22'),
(115, 'manisaprop@dibiak.com', '2020-11-17 09:38:17'),
(116, 'robby_biak@yahoo.com', '2020-11-17 09:38:17'),
(117, 'suyat9056@gmail.com', '2020-11-17 09:38:23'),
(118, 'gasirja@dibiak.com', '2020-11-17 09:38:29'),
(119, 'robby_biak@yahoo.com', '2020-11-17 09:38:35'),
(120, 'manisaprop@dibiak.com', '2020-11-17 09:39:35'),
(121, 'robby_biak@yahoo.com', '2020-11-17 09:39:35'),
(122, 'suyat9056@gmail.com', '2020-11-17 09:39:41'),
(123, 'gasirja@dibiak.com', '2020-11-17 09:39:47'),
(124, 'robby_biak@yahoo.com', '2020-11-17 09:39:53'),
(125, 'manisaprop@dibiak.com', '2020-11-17 09:42:17'),
(126, 'robby_biak@yahoo.com', '2020-11-17 09:42:17'),
(127, 'suyat9056@gmail.com', '2020-11-17 09:42:23'),
(128, 'gasirja@dibiak.com', '2020-11-17 09:42:29'),
(129, 'robby_biak@yahoo.com', '2020-11-17 09:42:35'),
(130, 'manisaprop@dibiak.com', '2020-11-17 09:43:35'),
(131, 'robby_biak@yahoo.com', '2020-11-17 09:43:35'),
(132, 'suyat9056@gmail.com', '2020-11-17 09:43:41'),
(133, 'gasirja@dibiak.com', '2020-11-17 09:43:47'),
(134, 'robby_biak@yahoo.com', '2020-11-17 09:43:53'),
(135, 'manisaprop@dibiak.com', '2020-11-17 09:47:23'),
(136, 'robby_biak@yahoo.com', '2020-11-17 09:47:23'),
(137, 'suyat9056@gmail.com', '2020-11-17 09:47:29'),
(138, 'gasirja@dibiak.com', '2020-11-17 09:47:35'),
(139, 'robby_biak@yahoo.com', '2020-11-17 09:47:41'),
(140, 'manisaprop@dibiak.com', '2020-11-17 09:55:11'),
(141, 'robby_biak@yahoo.com', '2020-11-17 09:55:11'),
(142, 'suyat9056@gmail.com', '2020-11-17 09:55:17'),
(143, 'gasirja@dibiak.com', '2020-11-17 09:55:23'),
(144, 'robby_biak@yahoo.com', '2020-11-17 09:55:29'),
(145, 'manisaprop@dibiak.com', '2020-11-17 09:58:14'),
(146, 'robby_biak@yahoo.com', '2020-11-17 09:58:14'),
(147, 'suyat9056@gmail.com', '2020-11-17 09:58:20'),
(148, 'gasirja@dibiak.com', '2020-11-17 09:58:26'),
(149, 'robby_biak@yahoo.com', '2020-11-17 09:58:32'),
(150, 'papuamajumandiri@yahoo.com', '2020-11-18 07:00:13'),
(151, 'robby_biak@yahoo.com', '2020-11-20 07:00:13'),
(152, 'gasirja@dibiak.com', '2020-11-22 07:00:13'),
(153, 'papuamajumandiri@yahoo.com', '2020-11-22 07:00:14'),
(154, 'manisaprop@dibiak.com', '2020-11-23 07:00:13'),
(155, 'gasirja@dibiak.com', '2020-11-23 07:00:14'),
(156, 'gasirja@dibiak.com', '2020-11-30 07:00:13'),
(157, 'manisaprop@dibiak.com', '2020-12-01 07:00:12'),
(158, 'suyat9056@gmail.com', '2020-12-01 07:00:13'),
(159, 'gasirja@dibiak.com', '2020-12-01 07:00:13'),
(160, 'waiting ...', '2020-12-02 07:00:03'),
(161, 'suyat9056@gmail.com', '2020-12-02 07:00:13'),
(162, 'mahkotadewabiak@gmail.com', '2020-12-02 07:00:14'),
(163, 'gasirja@dibiak.com', '2020-12-02 07:00:14'),
(164, 'papuamajumandiri@yahoo.com', '2020-12-02 07:00:20'),
(165, 'suyat9056@gmail.com', '2020-12-03 07:00:13'),
(166, 'gasirja@dibiak.com', '2020-12-06 07:00:13'),
(167, 'papuamajumandiri@yahoo.com', '2020-12-06 07:00:14'),
(168, 'robby_biak@yahoo.com', '2020-12-06 07:00:14'),
(169, 'manisaprop@dibiak.com', '2020-12-07 07:00:13'),
(170, 'gasirja@dibiak.com', '2020-12-07 07:00:13'),
(171, 'gasirja@dibiak.com', '2020-12-07 07:00:14'),
(172, 'manisaprop@dibiak.com', '2020-12-08 07:00:13'),
(173, 'suyat9056@gmail.com', '2020-12-08 07:00:14'),
(174, 'gasirja@dibiak.com', '2020-12-08 07:00:14'),
(175, 'waiting ...', '2020-12-09 07:00:03'),
(176, 'suyat9056@gmail.com', '2020-12-09 07:00:13'),
(177, 'robby_biak@yahoo.com', '2020-12-13 07:00:13'),
(178, 'gasirja@dibiak.com', '2020-12-14 07:00:13'),
(179, 'suyat9056@gmail.com', '2020-12-14 07:00:13'),
(180, 'manisaprop@dibiak.com', '2020-12-15 07:00:13'),
(181, 'suyat9056@gmail.com', '2020-12-15 07:00:14'),
(182, 'gasirja@dibiak.com', '2020-12-15 07:00:14'),
(183, 'waiting ...', '2020-12-16 07:00:03'),
(184, 'suyat9056@gmail.com', '2020-12-16 07:00:13'),
(185, 'mahkotadewabiak@gmail.com', '2020-12-16 07:00:14'),
(186, 'gasirja@dibiak.com', '2020-12-16 07:00:14'),
(187, 'papuamajumandiri@yahoo.com', '2020-12-16 07:00:20'),
(188, 'suyat9056@gmail.com', '2020-12-17 07:00:12'),
(189, 'robby_biak@yahoo.com', '2020-12-20 07:00:13'),
(190, 'gasirja@dibiak.com', '2020-12-21 07:00:13'),
(191, 'manisaprop@dibiak.com', '2020-12-22 07:00:09'),
(192, 'suyat9056@gmail.com', '2020-12-22 07:00:09'),
(193, 'gasirja@dibiak.com', '2020-12-22 07:00:13'),
(194, 'papuamajumandiri@yahoo.com', '2020-12-22 07:00:14'),
(195, 'gasirja@dibiak.com', '2020-12-22 07:00:15'),
(196, 'waiting ...', '2020-12-23 07:00:03'),
(197, 'manisaprop@dibiak.com', '2020-12-23 07:00:11'),
(198, 'suyat9056@gmail.com', '2020-12-23 07:00:11'),
(199, 'gasirja@dibiak.com', '2020-12-23 07:00:17'),
(200, 'robby_biak@yahoo.com', '2020-12-27 07:00:12'),
(201, 'suyat9056@gmail.com', '2020-12-28 07:00:13'),
(202, 'gasirja@dibiak.com', '2020-12-29 07:00:13'),
(203, 'manisaprop@dibiak.com', '2020-12-29 07:00:13'),
(204, 'suyat9056@gmail.com', '2020-12-29 07:00:14'),
(205, 'papuamajumandiri@yahoo.com', '2020-12-29 07:00:14'),
(206, 'gasirja@dibiak.com', '2020-12-29 07:00:14'),
(207, 'waiting ...', '2020-12-30 07:00:03'),
(208, 'manisaprop@dibiak.com', '2020-12-30 07:00:13'),
(209, 'suyat9056@gmail.com', '2020-12-30 07:00:14'),
(210, 'gasirja@dibiak.com', '2020-12-30 07:00:14'),
(211, 'mahkotadewabiak@gmail.com', '2021-01-01 07:00:13'),
(212, 'gasirja@dibiak.com', '2021-01-01 07:00:14'),
(213, 'papuamajumandiri@yahoo.com', '2021-01-01 07:00:14'),
(214, 'suyat9056@gmail.com', '2021-01-02 07:00:13'),
(215, 'robby_biak@yahoo.com', '2021-01-03 07:00:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_awak`
--

CREATE TABLE `tbl_m_awak` (
  `id_awak` int(11) NOT NULL,
  `id_truck` int(11) DEFAULT NULL,
  `nip_awak` varchar(20) DEFAULT NULL,
  `foto_awak` varchar(255) NOT NULL,
  `nama_awak` varchar(200) DEFAULT NULL,
  `tgl_lahir_awak` date DEFAULT NULL,
  `masa_berlaku_sim_awak` date DEFAULT NULL,
  `dokumen_sim_awak` text DEFAULT NULL,
  `masa_berlaku_idcard_awak` date DEFAULT NULL,
  `dokumen_idcard_awak` text DEFAULT NULL,
  `bagian` varchar(100) DEFAULT NULL,
  `masa_berlaku_kim_awak` varchar(255) DEFAULT NULL,
  `dokumen_kim_awak` varchar(255) DEFAULT NULL,
  `dokumen_ktp_awak` varchar(255) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_m_awak`
--

INSERT INTO `tbl_m_awak` (`id_awak`, `id_truck`, `nip_awak`, `foto_awak`, `nama_awak`, `tgl_lahir_awak`, `masa_berlaku_sim_awak`, `dokumen_sim_awak`, `masa_berlaku_idcard_awak`, `dokumen_idcard_awak`, `bagian`, `masa_berlaku_kim_awak`, `dokumen_kim_awak`, `dokumen_ktp_awak`, `updated_date`) VALUES
(18, 172, '01', 'profil_AMT_01.jpeg', 'Irfandi Gahung', '1986-06-13', '2024-06-13', 'AMT_01.pdf', '1970-01-01', 'AMT_011.pdf', 'AMT 1', NULL, NULL, 'AMT_012.pdf', NULL),
(19, 172, '02', 'profil_AMT_02.jpeg', 'Yulian Rumwaropen', '1986-02-26', '2020-12-31', 'AMT_02.pdf', '1970-01-01', 'AMT_021.pdf', 'AMT 2', NULL, NULL, 'KTP_AMT_02.pdf', NULL),
(20, 178, '01', 'SI_20200807_123154.jpg', 'PHETER WARPUR ', '1976-07-19', '2023-07-19', 'SIM_OM_WARPUR1.pdf', '2020-12-31', 'SI_20200807_1231541.pdf', 'AMT 1', NULL, NULL, '3__KTP_AMT_01.pdf', NULL),
(21, 188, '02', 'Profil_AMT_021.jpeg', 'Nasir Muhammad', '1966-08-05', '2022-08-05', '2__ID_card_KTP_SIM_AMT_02.pdf', '2020-12-31', '2__ID_card_KTP_SIM_AMT_021.pdf', 'AMT 2', NULL, NULL, '4__KTP_AMT_02.pdf', NULL),
(22, 179, '01', 'Profil_AMT_01.jpeg', 'Santoso', '1981-08-25', '2023-08-25', 'SIM_AMT_01.pdf', '1970-01-01', 'Id_Card_AMT_01.pdf', 'AMT 1', NULL, NULL, 'KTP_AMT_01.pdf', NULL),
(23, 179, '02', 'Profil_AMT_02.jpeg', 'Azis Najamudin', '1988-02-23', '2020-07-11', 'Id_Card_AMT_02.pdf', '1970-01-01', 'Id_Card_AMT_021.pdf', 'AMT 2', NULL, NULL, 'Id_Card_AMT_022.pdf', NULL),
(24, 180, '01', 'Foto_profil_AMT_01.jpeg', 'Maikel Manufandu', '1988-07-17', '2025-07-13', 'pdfresizer_com-pdf-resize_(20).pdf', '1970-01-01', 'SIM_AMT_012.pdf', 'AMT 1', NULL, NULL, 'KTP_AMT_011.pdf', NULL),
(25, 180, '02', 'Foto_profil_AMT_02.jpeg', 'Sukardin', '1994-02-10', '2025-02-18', 'SIM_AMT_013.pdf', '2020-12-31', 'SIM_AMT_02.pdf', 'AMT 2', NULL, NULL, 'KTP_AMT_021.pdf', NULL),
(26, 181, '01', 'Profil_AMT_012.jpeg', 'Yohantyo B.B.G Mallow', '1995-11-28', '2024-11-28', 'Id_Card,_KTP_SIM_AMT_01.pdf', '1970-01-01', 'Id_Card,_KTP_SIM_AMT_011.pdf', 'AMT 1', NULL, NULL, 'Id_Card,_KTP_SIM_AMT_012.pdf', NULL),
(27, 181, '02', 'Profil_AMT_022.jpeg', 'Daniel M. Adidondifu', '1994-04-23', '2025-07-06', 'Id_Card_AMT_023.pdf', '1970-01-01', 'Id_Card_AMT_024.pdf', 'AMT 2', NULL, NULL, 'Id_Card_AMT_025.pdf', NULL),
(28, 175, '12', '60e1cf31687e7d1c1fdc2bef44dcaa41.jpeg', 'Riki Afnaldi', '1991-10-19', '2024-10-19', 'Data_Mobil_Tangki_PA_9568_C_13.pdf', '2021-07-17', 'Data_Mobil_Tangki_PA_9568_C_131.pdf', 'AMT 1', NULL, NULL, 'Data_Mobil_Tangki_PA_9568_C_132.pdf', NULL),
(29, 175, '12/20', '43ccad8ab27578588e45736dc89c17fc.jpeg', 'Yansen Tony Sawor', '1987-09-16', '2024-09-16', 'Data_Mobil_Tangki_PA_9568_C_133.pdf', '2021-07-25', 'Data_Mobil_Tangki_PA_9568_C_134.pdf', 'AMT 2', NULL, NULL, 'Data_Mobil_Tangki_PA_9568_C_135.pdf', NULL),
(30, 182, '01', 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_5208013112980033_2020_10_13_13_43_23-1.jpg', 'Luis Mamoribo', '1984-03-23', '2025-07-16', 'SIM_Luis.pdf', '1970-01-01', 'SIM_Id_Card_AMT_011.pdf', 'AMT 1', NULL, NULL, 'KTP_AMT_012.pdf', NULL),
(31, 182, '02', '7bca7bc41b06ff8bdbf6be2074939c81.jpeg', 'Acan Saman', '1994-02-10', '2020-12-31', 'KTP_AMT_022.pdf', '1970-01-01', 'Id_Card_AMT_026.pdf', 'AMT 2', NULL, NULL, 'KTP_AMT_023.pdf', NULL),
(32, 174, '12/20', '074b6214fddad5c41d59226df97e8df7.jpeg', 'M.Tuwi', '1965-01-06', '2024-12-31', 'ID_dan_KTP.pdf', '2021-07-17', 'ID_dan_KTP1.pdf', 'AMT 2', NULL, NULL, 'ID_dan_KTP2.pdf', NULL),
(33, 174, '12/20', 'c34a317c45aeba9d357b119aba9463e8.jpeg', 'Harends Menufandu', '1981-06-13', '2023-06-13', 'ID_dan_KTP3.pdf', '2021-07-17', 'ID_dan_KTP4.pdf', 'AMT 1', NULL, NULL, 'ID_dan_KTP5.pdf', NULL),
(34, 173, '12/20', 'b705eda74bbdbf57d28730d303f1f48c.jpeg', 'Irwanto Sumargono', '1983-07-17', '2023-07-17', 'Data_Mobil_Tangki_PA_9696_CB_9.pdf', '2021-07-18', 'Data_Mobil_Tangki_PA_9696_CB_91.pdf', 'AMT 1', NULL, NULL, 'Data_Mobil_Tangki_PA_9696_CB_92.pdf', NULL),
(35, 173, '12/20', '52214188355fe4dde9db6c5ca23c6524.jpeg', 'Pilatus Ronsumbre', '1986-03-28', '2020-07-27', 'Data_Mobil_Tangki_PA_9699_C.pdf', '2021-07-27', 'Data_Mobil_Tangki_PA_9699_C1.pdf', 'AMT 2', NULL, NULL, 'Data_Mobil_Tangki_PA_9699_C2.pdf', NULL),
(36, 177, '12/20', '49a6ef806ac2d43414781ce1f640ffa3.jpeg', 'Naftali Rejauw', '1969-11-01', '2022-11-01', 'Data_Mobil_Tangki_DS_9586_C.pdf', '2020-07-27', 'Data_Mobil_Tangki_DS_9586_C1.pdf', 'AMT 1', NULL, NULL, 'Data_Mobil_Tangki_DS_9586_C2.pdf', NULL),
(37, 177, '12/20', '94c9bf7c3798e7c5d09d0ccba2e4b7b1.jpeg', 'Richard Yarangga', '2020-07-27', '2023-12-30', 'DOKUMEN_PERPANJANGAN_SIM_2_9106010501750002_2020_11_17_20_45_09.pdf', '2021-07-27', 'Data_Mobil_Tangki_DS_9586_C4.pdf', 'AMT 2', NULL, NULL, 'Data_Mobil_Tangki_DS_9586_C5.pdf', '2020-11-17 20:47:49'),
(38, 178, '02', 'IMG-20200826-WA0010.jpg', 'MAYOB EL FIOSOS', '1984-04-18', '2025-05-07', 'SIM_MAYOB.pdf', '2020-12-31', 'IMG-20200826-WA0010.pdf', 'AMT 2', NULL, NULL, 'KTP_WANDA.pdf', NULL),
(39, 188, '01', 'SI_20200807_123206.jpg', 'STENLY WANDAYKUS KORWA', '1992-10-30', '2022-10-30', 'SIM_WANDA1.pdf', '2020-12-31', 'SI_20200807_1232061.pdf', 'AMT 1', NULL, NULL, 'KTP_OM_WARPUR.pdf', NULL),
(40, 189, '02', 'bb1c48d8a23219796f00b66953ba446a.jpg', 'EDGAR RUMWAROPEN', '1987-09-13', '2020-12-31', '20200812_135100_(1).pdf', '1970-01-01', 'ID_Card_Edgar_(1).pdf', 'AMT 2', NULL, NULL, 'KTP_Edgar.pdf', NULL),
(41, 189, '01', 'f751a10a341cf983a4bad81c3260b041.jpg', 'OLVIANTO SYAHPUTRA', '1997-10-21', '2024-10-21', 'SIM_OLVIN.pdf', '1970-01-01', '20200812_135319.pdf', 'AMT 1', NULL, NULL, 'KTP_Olvinato.pdf', NULL),
(43, 175, '5208013112980033', '24fc4ab54a6b65deaa94ecc20c38d382.jpg', 'Onaka Metta Dharma', '1998-12-31', '2024-12-31', 'DOKUMEN_PERPANJANGAN_SIM_2_9106010501750002_2020_11_17_20_44_13.pdf', '2021-10-05', 'permohonan1.pdf', 'AMT 2', NULL, NULL, 'KTP_Onaka3.pdf', '2020-11-17 20:47:36'),
(44, 193, '9106120606820002', 'e243621f7ea966861ee784fcd6f658ce.jpg', 'Muhgimin M. Sula', '1982-06-06', '2025-11-04', 'muhgimin_sula.pdf', '2020-12-31', 'muhgimin_sula1.pdf', 'AMT 1', NULL, NULL, 'muhgimin_sula2.pdf', NULL),
(45, 193, '9106011210750004', '0d09b71072b55593849e9e4ed5c35795.jpg', 'Midto', '1975-10-12', '2023-10-12', 'midto.pdf', '2020-12-31', 'midto1.pdf', 'AMT 2', NULL, NULL, 'midto2.pdf', NULL),
(46, 191, '9106010507720001', 'dc11746decbb9aae0e860fe0101f9cfe.jpg', 'Yulius Rejauw', '1972-07-05', '2024-07-05', 'yulius_rejauw.pdf', '2020-12-31', 'yulius_rejauw1.pdf', 'AMT 1', NULL, NULL, 'yulius_rejauw2.pdf', NULL),
(47, 192, '9106010501750002', 'b006e0debda48a496a830222a4fc2dde.jpg', 'Jumran', '1977-10-05', '2024-09-30', 'Jumran.pdf', '2021-12-31', 'Jumran1.pdf', 'AMT 1', NULL, NULL, 'Jumran2.pdf', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_keperluan`
--

CREATE TABLE `tbl_m_keperluan` (
  `id_keperluan` int(15) NOT NULL,
  `nm_keperluan` varchar(200) DEFAULT NULL,
  `type_keperluan` varchar(100) DEFAULT NULL,
  `masa_berlaku` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_m_keperluan`
--

INSERT INTO `tbl_m_keperluan` (`id_keperluan`, `nm_keperluan`, `type_keperluan`, `masa_berlaku`) VALUES
(1, 'Perpanjangan STNK', 'truck', '5'),
(2, 'Perpanjangan SIM', 'amt', '5'),
(3, 'Perpanjangan SKT', 'truck', '5'),
(4, 'Perpanjangan Kartu KIM', 'truck', '5'),
(5, 'Perpanjangan Keur / KIR', 'truck', '5'),
(6, 'Perpanjangan Metrologi Tera', 'truck', '5'),
(7, 'Pengajuan Baru ID CARD', 'amt', '1'),
(8, 'Perpanjangan ID CARD', 'amt', '1'),
(9, 'Penggantian Awak Mobil Tangki', 'amt', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_merek`
--

CREATE TABLE `tbl_m_merek` (
  `id_merek` int(11) NOT NULL,
  `nm_merek` varchar(255) DEFAULT NULL,
  `tipe` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_m_merek`
--

INSERT INTO `tbl_m_merek` (`id_merek`, `nm_merek`, `tipe`, `keterangan`) VALUES
(1, 'MITSUBISHI', NULL, NULL),
(2, 'NISSAN', NULL, NULL),
(3, 'HINO', NULL, NULL),
(4, 'ISUZU', NULL, NULL),
(5, 'TOYOTA', 'L500i', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_pemilik`
--

CREATE TABLE `tbl_m_pemilik` (
  `id_pemilik` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_pemilik` varchar(200) DEFAULT NULL,
  `nama_perusahaan` varchar(200) DEFAULT NULL,
  `email_aktif` varchar(200) DEFAULT NULL,
  `no_telfon` varchar(20) DEFAULT NULL,
  `no_telfon_kedua` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_m_pemilik`
--

INSERT INTO `tbl_m_pemilik` (`id_pemilik`, `id_user`, `nama_pemilik`, `nama_perusahaan`, `email_aktif`, `no_telfon`, `no_telfon_kedua`) VALUES
(52, 77, 'PT Irja Mandiri', 'PT Irja Mandiri', 'robby_biak@yahoo.com', '085244112203', '0811487933'),
(53, 78, 'PT. Papua Maju Mandiri', 'PT. Papua Maju Mandiri', 'papuamajumandiri@yahoo.com', '082347491825', '081243922778'),
(54, 79, 'PT Gasirja Utama', 'PT Gasirja Utama', 'gasirja@dibiak.com', '085243896287', '081344623668'),
(55, 80, 'PT. Makotadewa Araima Supiori', 'PT. Makotadewa Araima Supiori', 'mahkotadewabiak@gmail.com', '082211777714', '081344240621'),
(56, 81, 'PT. Restu Baru Jaya', 'PT. Restu Baru Jaya', 'suyat9056@gmail.com', '082290767907', '082290767907'),
(57, 82, 'PT. Jackson Pratama', 'PT. Jackson Pratama', 'robby_biak@yahoo.com', '085244112203', '082198939999'),
(58, 83, 'CV. Manisaprop', 'CV. Manisaprop', 'manisaprop@dibiak.com', '081344839669', '08124892021'),
(59, 86, 'PT. Wapoga Mutiara Industries', 'PT. Wapoga Mutiara Industries', 'waiting ...', '085254061077', '085254061077');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_syarat_perpanjangan`
--

CREATE TABLE `tbl_m_syarat_perpanjangan` (
  `id_syarat` int(11) NOT NULL,
  `id_keperluan` int(11) DEFAULT NULL,
  `nm_syarat` varchar(255) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `tipe_dokumen` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_m_syarat_perpanjangan`
--

INSERT INTO `tbl_m_syarat_perpanjangan` (`id_syarat`, `id_keperluan`, `nm_syarat`, `keterangan`, `tipe_dokumen`) VALUES
(1, 1, 'Dokumen STNK Baru', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(2, 2, 'Dokumen SIM Baru', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(3, 4, 'Dokumen KIM Lama', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(4, 4, 'Surat Permohonan Perpanjangan KIM', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(5, 4, 'KTP Awak Mobil Tangki', 'Dokumen yang diupload harus berupa file pdf / jpg', 'pdf, jpg'),
(6, 4, 'Surat Keterangan Sehat', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(7, 4, 'Foto Setengah Badan AMT', 'Dokumen yang diupload harus berupa file jpg', 'jpg'),
(8, 5, 'Dokumen KEUR Baru', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(9, 6, 'Dokumen Sertifikat Tera Baru', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(10, 7, 'Surat Permohonan Penerbitan ID CARD', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(11, 7, 'Pas Foto 3x4', '<strong class=\"text-danger\">Dokumen yang diupload harus berupa file jpg</strong>', 'jpg'),
(12, 7, 'Foto Copy KTP', 'Dokumen yang diupload harus berupa file pdf / jpg', 'pdf,jpg'),
(13, 7, 'Bukti Transfer Biaya Pembuatan ID CARD', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(14, 8, 'Surat Permohonan Penerbitan ID CARD', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(15, 8, 'Pas Foto 3x4', 'Dokumen yang diupload harus berupa file jpg', 'jpg'),
(16, 8, 'Foto Copy KTP', 'Dokumen yang diupload harus berupa file pdf / jpg', 'pdf,jpg'),
(17, 8, 'Bukti Transfer Biaya Pembuatan ID CARD', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(18, 3, 'Dokumen SKT Baru', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(19, 9, 'Surat Permohonan Penggantian Awak Mobil Tangki', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(20, 9, 'Foto Copy KTP', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(21, 9, 'Foto Copy SIM B1 Umum', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(22, 9, 'Foto Copy Surat Keterangan Bebas Narkoba', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(23, 9, 'Foto Copy SKCK', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(24, 8, 'Berkas ID CARD AMT Lama', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(25, 7, 'Dokumen SKCK', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(26, 8, 'Dokumen SKCK', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(27, 9, 'Dokumen SKCK', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(28, 4, 'Dokumen SIM AMT', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(29, 7, 'Surat Bebas Narkoba', 'Dokumen yang diupload harus berupa file pdf', 'pdf'),
(30, 8, 'Surat Bebas Narkoba', 'Dokumen yang diupload harus berupa file pdf', 'pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_truck`
--

CREATE TABLE `tbl_m_truck` (
  `id_truck` int(11) NOT NULL,
  `id_pemilik` int(11) DEFAULT NULL,
  `no_polisi` varchar(20) DEFAULT NULL,
  `no_rangka` varchar(100) DEFAULT NULL,
  `no_mesin` varchar(100) DEFAULT NULL,
  `id_merek` int(11) DEFAULT NULL,
  `tahun_pembuatan` varchar(5) DEFAULT NULL,
  `masa_berlaku_stnk` date DEFAULT NULL,
  `masa_berlaku_skt` date DEFAULT NULL,
  `isi_silinder` int(10) DEFAULT NULL,
  `produk` varchar(100) DEFAULT NULL,
  `keur` date DEFAULT NULL,
  `uji_emisi` varchar(10) DEFAULT NULL,
  `metrologi_tera` date DEFAULT NULL,
  `t2` varchar(100) DEFAULT NULL,
  `vol_kl` int(11) DEFAULT NULL,
  `masa_berlaku_kim` date NOT NULL,
  `dokumen_kim` varchar(255) NOT NULL,
  `dokumen_stnk` text DEFAULT NULL,
  `dokumen_kir` text DEFAULT NULL,
  `dokumen_skt` varchar(255) NOT NULL,
  `dokumen_metrologi_tera` varchar(255) NOT NULL,
  `foto_truck` varchar(255) DEFAULT NULL,
  `foto_depan_truck` varchar(255) NOT NULL,
  `foto_belakang_truck` varchar(255) NOT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_m_truck`
--

INSERT INTO `tbl_m_truck` (`id_truck`, `id_pemilik`, `no_polisi`, `no_rangka`, `no_mesin`, `id_merek`, `tahun_pembuatan`, `masa_berlaku_stnk`, `masa_berlaku_skt`, `isi_silinder`, `produk`, `keur`, `uji_emisi`, `metrologi_tera`, `t2`, `vol_kl`, `masa_berlaku_kim`, `dokumen_kim`, `dokumen_stnk`, `dokumen_kir`, `dokumen_skt`, `dokumen_metrologi_tera`, `foto_truck`, `foto_depan_truck`, `foto_belakang_truck`, `updated_date`) VALUES
(172, 58, 'PA 9928 CB', 'MHMFE74P5CK082518', '4D34TH01179', 1, '2012', '2021-04-18', '2021-04-18', 3908, 'Kerosene', '2021-01-22', '0000', '2021-11-30', '985 mm', 5000, '2021-06-01', 'Data_Mobil_Tangki_PA_9928_CB_11.pdf', 'STNK.pdf', 'Keur_PA_9928_CB.pdf', 'Data_Mobil_Tangki_PA_9928_CB.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9928_CB_2020_09_16_11_58_20.pdf', 'Mobil_samping.jpeg', 'Mobil_Depan.jpeg', 'Mobil_belakang.jpeg', '2020-09-19 09:50:07'),
(173, 52, 'PA 9699 C', 'MHMF517BBK003048', 'GD 16-G98348', 1, '2011', '2022-06-02', '2021-10-20', 3908, 'Solar', '2021-04-05', '0000', '2021-11-30', '1265 mm', 10, '2021-06-30', 'Data_Mobil_Tangki_PA_9696_CB_111.pdf', 'Data_Mobil_Tangki_PA_9696_CB_STNK.pdf', 'Data_Mobil_Tangki_PA_9696_CB_STNK1.pdf', 'Data_Mobil_Tangki_PA_9696_CB_11.pdf', 'Data_Mobil_Tangki_PA_9696_CB_5.pdf', 'Samping.jpeg', 'Depan.jpeg', 'Belakang.jpeg', NULL),
(174, 52, 'PA 9663 C', 'MHMFM517AJK012368', '6D16-558841', 1, '2018', '2023-11-12', '2021-10-20', 7545, 'BIO SOLAR B30', '2021-04-05', '0000', '2021-11-30', '1186 mm / 1184 mm', 10, '2021-06-30', 'Data_Mobil_Tangki_DS_9663_C1.pdf', 'Data_Mobil_Tangki_DS_9663_C_3.pdf', 'Data_Mobil_Tangki_DS_9663_C_31.pdf', 'Data_Mobil_Tangki_DS_9663_C.pdf', 'Data_Mobil_Tangki_DS_9663_C_1.pdf', 'foto_MT_13.jpg', 'foto_MT_14.jpg', 'foto_MT_15.jpg', NULL),
(175, 52, 'DS 9568 C', 'MHMFE74P4FK082240', '4D34T-L94685', 1, '2015', '2021-01-05', '2021-10-20', 1908, 'BIO SOLAR B30', '2021-04-05', '0000', '2021-11-30', '998mm', 5000, '2020-07-10', 'Data_Mobil_Tangki_PA_9568_C1.pdf', 'Data_Mobil_Tangki_PA_9568_C_3.pdf', 'Data_Mobil_Tangki_PA_9568_C_31.pdf', 'Data_Mobil_Tangki_PA_9568_C.pdf', 'Data_Mobil_Tangki_PA_9568_C_5.pdf', 'foto_MT_16.jpg', 'foto_MT_17.jpg', 'foto_MT_18.jpg', NULL),
(176, 52, 'DS 9567 C', 'MHMFE74P4FK082239', '4D34TL94667', 1, '2015', '2021-01-05', '2021-10-20', 3908, 'PREMIUM.PERTAMAX', '2021-04-05', '0000', '2021-11-30', '988 mm', 5000, '2020-07-10', 'Data_Mobil_Tangki_DS_9567_C1.pdf', 'Data_Mobil_Tangki_DS_9567_C_1.pdf', 'Data_Mobil_Tangki_DS_9567_C_3.pdf', 'Data_Mobil_Tangki_DS_9567_C.pdf', 'Data_Mobil_Tangki_DS_9567_C_11.pdf', 'foto_MT_19.jpg', 'foto_MT_110.jpg', 'foto_MT_111.jpg', NULL),
(177, 57, 'PA 9586 C', 'MHMFE74P5GK159832', '4D34T-P58211', 1, '2016', '2022-01-21', '2021-10-20', 3908, 'KEROSINE', '2021-04-05', '0000', '2021-11-30', '1029 mm', 5000, '2021-06-30', 'Data_Mobil_Tangki_DS_9586_C1.pdf', 'Data_Mobil_Tangki_DS_9586_C_3.pdf', 'Data_Mobil_Tangki_DS_9586_C_31.pdf', 'Data_Mobil_Tangki_DS_9586_C.pdf', 'Data_Mobil_Tangki_DS_9586_C_1.pdf', 'foto_MT_112.jpg', 'foto_MT_113.jpg', 'foto_MT_114.jpg', NULL),
(178, 54, 'PA 9630 C', 'MHMFM5174ADK008798', '6D16J91398', 1, '2013', '2023-02-15', '2021-01-31', 7545, 'Multiproduk (Gasoline & Biosolar)', '2021-01-22', '000', '2021-11-30', '1235/1238', 10, '2021-06-30', '4__KIM1.pdf', '2__STNK.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9630_C_2020_09_16_08_09_13.pdf', '4__KIM.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9630_C_2020_09_16_07_37_17.pdf', 'foto_samping.jpeg', 'foto_depan.jpeg', 'foto_belakang.jpeg', NULL),
(179, 54, 'PA 9650 C', 'MHMFE75P6K035437', '4D34TL81981', 1, '2015', '2023-04-30', '2021-01-31', 3908, 'Multiproduk (Gasoline & Biosolar)', '2021-01-22', '000', '2021-11-30', '975', 5, '2021-06-30', '4__KIM3.pdf', '2__STNK1.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9650_C_2020_09_16_09_18_32.pdf', '4__KIM2.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9650_C_2020_09_16_07_36_41.pdf', 'foto_samping1.jpeg', 'foto_depan1.jpeg', 'foto_belakang1.jpeg', NULL),
(180, 54, 'PA 9505 C', 'MHMFM517ADK08896', '6D16J91489', 1, '2013', '2022-11-11', '2021-01-21', 7645, 'Multiproduk (Gasoline & Biosolar)', '2021-01-22', '000', '2021-11-30', '1229/1224', 10, '2021-07-31', '2__STNK4.pdf', '2__STNK2.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9505_C_2020_09_16_08_19_00.pdf', '2__STNK3.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9505_C_2020_09_16_07_37_49.pdf', 'foto_samping2.jpeg', 'foto_depan2.jpeg', 'foto_belakang2.jpeg', NULL),
(181, 54, 'S 8621 UN', 'MJEFG8JJ1KJB13310', 'J08EUGJ70530', 3, '2019', '2024-07-04', '2021-01-31', 7684, 'Multiproduk (Gasoline & Biosolar)', '2021-01-22', '000', '2021-11-30', '1165/1155', 10, '2021-06-30', '2__STNK7.pdf', '2__STNK5.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_8621_UN_2020_09_16_08_20_22.pdf', '2__STNK6.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_S_8621_UN_2020_09_16_07_27_26.pdf', 'foto_profil_kosong3.jpg', 'foto_profil_kosong4.jpg', 'foto_profil_kosong5.jpg', NULL),
(182, 54, 'PA 9705 C', 'MHMFE74P5HK169160', '4D34TR32653', 1, '2017', '2025-02-26', '2021-01-31', 3908, 'Multiproduk (Gasoline & Biosolar)', '2021-01-22', '000', '2021-11-30', '977', 5, '2021-06-30', '4__KIM4.pdf', '2__STNK8.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9705_C_2020_09_16_08_22_33.pdf', '2__STNK9.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9705_C_2020_09_16_07_38_23.pdf', 'foto_samping3.jpeg', 'foto_depan3.jpeg', 'foto_belakang3.jpeg', NULL),
(183, 53, 'L 9218 AQ', 'MHFC1JU43F5123071', 'W04DTRR19492', 5, '2015', '2021-01-21', '2021-01-31', 4009, 'MULTI PRODUCT', '2021-05-19', '0000', '2021-11-30', '982', 5000, '2021-06-30', 'Data_Mobil_Tangki_L_9218_AQ1.pdf', 'Data_Mobil_Tangki_L_9218_AQ_3.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_L_9218_AQ_2020_12_09_19_12_05.jpeg', 'Data_Mobil_Tangki_L_9218_AQ.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_L_9218_AQ_2020_11_18_07_49_09.pdf', 'foto_MT_23.jpg', 'foto_MT_117.jpg', 'foto_MT_24.jpg', '2021-01-01 21:18:19'),
(184, 53, 'PA 9004 MI', 'MHMFM517AHK011866', '6D16-R84456', 1, '2017', '2024-08-28', '2021-01-31', 7545, 'MULTI PRODUCT', '2020-11-20', '0000', '2021-07-31', '1195 mm / 1218 mm', 5000, '2021-06-30', 'Data_Mobil_Tangki_PA_9004_MI_(L_9307_AY).pdf', 'Data_Mobil_Tangki_PA_9004_MI_(L_9307_AY)_9.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9004_MI_2020_09_16_10_53_00.pdf', 'Data_Mobil_Tangki_PA_9004_MI_(L_9307_AY)_3.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9004_MI_2020_11_17_20_33_49.pdf', 'foto_MT_118.jpg', 'foto_MT_119.jpg', 'foto_MT_120.jpg', '2020-11-17 20:37:20'),
(185, 55, ' PA 9101 U', 'MHMFE74P5HK169274', '4D34T-R32587', 1, '2017', '2022-11-07', '2021-01-31', 3908, 'Multiproduk (Gasoline & Biosolar)', '2021-11-30', '000', '2021-07-31', '1012', 5, '2020-06-30', '1__KIM.pdf', '2__STNK10.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8__PA_9101_U_2020_11_17_20_40_34.pdf', '2__STNK11.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9__PA_9101_U_2020_11_17_20_32_33.pdf', 'foto_samping4.jpeg', 'Foto_depan.jpeg', 'foto_belakang4.jpeg', '2020-11-17 20:41:28'),
(186, 52, 'PA 9696 CB', 'MHMFE74FK082239', '3D34TL94467', 1, '2010', '2021-07-30', '2021-10-20', 7545, 'Solar', '2021-04-05', '0000', '2021-11-30', '1025mm / 1040mm', 5, '2021-06-30', 'Data_Mobil_Tangki_DS_9663_C3.pdf', 'Data_Mobil_Tangki_DS_9663_C_32.pdf', 'Data_Mobil_Tangki_DS_9663_C_33.pdf', 'Data_Mobil_Tangki_DS_9663_C2.pdf', 'Data_Mobil_Tangki_DS_9663_C_11.pdf', 'Mobil_samping1.jpeg', 'Mobil_depan.jpeg', 'Mobil_belakang1.jpeg', NULL),
(187, 55, 'PA 9103 U', 'MHMFE7455GK45983', '4D34T-P58201', 1, '2016', '2022-01-24', '2021-01-31', 3908, 'Multiproduk (Gasoline & Biosolar)', '2021-11-30', '0000', '2021-07-31', '-', 5, '2021-06-30', '2__STNK15.pdf', '2__STNK12.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9103_U_2020_11_17_20_28_58.pdf', '2__STNK13.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9103_U_2020_11_17_20_30_05.pdf', 'mobil_samping.jpeg', 'Mobil_depan1.jpeg', 'mobil_blakang.jpeg', '2020-11-17 20:38:21'),
(188, 54, 'S 9878 UR', 'MJEC1JG43J5172391', 'W04DTRR62672', 3, '2018', '2024-01-24', '2021-01-31', 4009, '2018', '2021-01-22', '000', '2021-11-30', '985 mm', 5, '2020-05-31', '1__STNK2.pdf', '1__STNK.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_9878_UR_2020_09_16_09_13_51.pdf', '1__STNK1.pdf', 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_S_9878_UR_2020_09_16_07_19_38.pdf', 'foto_MT_26.jpg', 'foto_MT_123.jpg', 'foto_MT_124.jpg', NULL),
(189, 54, 'S 9856 UQ', 'MJEC1JG43II5161066', 'WO4DTRR51332', 3, '2017', '2023-07-20', '2021-01-31', 4009, 'Multiproduk (Gasoline & Biosolar)', '2021-01-22', '000', '2021-11-30', '986 mm', 5, '2020-05-31', 'STNK3.pdf', 'STNK1.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_9856_UQ_2020_09_16_09_14_37.pdf', 'STNK2.pdf', 'Tera2.pdf', 'dari_samping.jpeg', 'dari_depan.jpeg', 'dari_belakang.jpeg', NULL),
(190, 59, 'PA 9134 C', 'MHMFE75P6EK031808', '4D34T-K52365', 1, '2014', '2024-10-22', '2021-01-01', 3908, 'Solar', '2021-11-30', '0000', '2021-11-30', '984 MM', 5000, '2020-09-17', 'STNK7.pdf', 'STNK4.pdf', 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_DS_9134_C_2020_11_17_20_27_13.pdf', 'STNK5.pdf', 'STNK6.pdf', 'WhatsApp_Image_2020-08-28_at_15_21_51.jpeg', 'WhatsApp_Image_2020-08-28_at_15_21_50_(1).jpeg', 'WhatsApp_Image_2020-08-28_at_15_21_49.jpeg', '2020-11-17 20:38:36'),
(191, 56, 'PA 9685 C', 'MHMFN517CCK002983', '6D16-H06624', 1, '2012', '2023-04-22', '2021-01-01', 7545, 'Avtur', '2021-02-12', '0000', '2021-11-30', '1340 mm', 16, '2021-06-30', 'KIM_Identitas_AMT_PA_9685_C.pdf', 'STNK_PAJAK_PA_9685_C.pdf', 'KIR_PA_9685_C.pdf', 'Tera_PA_9685_C.pdf', 'KIR_PA_9685_C1.pdf', 'foto_MT_27.jpg', 'foto_MT_125.jpg', 'foto_MT_28.jpg', NULL),
(192, 56, 'PA 9684 C', 'MHMFM517AFK010599', '6D16L78798', 1, '2015', '2022-02-11', '2021-02-01', 7545, 'Avtur', '2021-02-12', '0000', '2021-11-30', '1194 mm', 8000, '2021-06-30', 'KIM_identitas_AMT_PA_9684_C.pdf', 'STNK_PAJAK_PA_9684_C.pdf', 'KIR_PA_9684_C.pdf', 'STNK_PAJAK_PA_9684_C1.pdf', 'Tera_PA_9684_C.pdf', 'foto_MT_29.jpg', 'foto_MT_126.jpg', 'foto_MT_127.jpg', NULL),
(193, 56, 'PA 9554 C', 'MJEC1J643E5102390', 'W04DTRR02371', 3, '2014', '2021-11-30', '2021-01-01', 4009, 'MULTI PRODUCT', '2021-02-12', '0000', '2021-11-30', '992 mm', 5000, '2021-06-30', 'KIM_Identitas_AMT_DS_9554_C.pdf', 'DOKUMEN_PERPANJANGAN_STNK_1_PA_9554_C_2020_11_17_19_58_06.pdf', 'KIR_DS_9554_C.pdf', 'STNK_PAJAK_DS_9554_C1.pdf', 'Tera_PA_9554_C.pdf', 'foto_MT_210.jpg', 'foto_MT_128.jpg', 'foto_MT_129.jpg', '2020-11-17 20:00:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_m_user`
--

CREATE TABLE `tbl_m_user` (
  `id_user` int(11) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `no_telfon` varchar(50) DEFAULT NULL,
  `no_telfon_kedua` varchar(50) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `level_user` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_m_user`
--

INSERT INTO `tbl_m_user` (`id_user`, `fullname`, `email`, `no_telfon`, `no_telfon_kedua`, `username`, `password`, `level_user`) VALUES
(1, 'Admin Sistem', 'erickprasetyo38@gmail.com', '-', '-', 'admin.sistem', '827ccb0eea8a706c4c34a16891f84e7b', 'ADMIN'),
(10, 'Manager Lokasi', 'jefri.manurung@pertamina.com', '-', '-', 'manager.lokasi', '827ccb0eea8a706c4c34a16891f84e7b', 'MANAGER LOKASI'),
(25, 'Manager Region', 'untung.tarsono@pertamina.com', '-', '-', 'manager.region', '827ccb0eea8a706c4c34a16891f84e7b', 'MANAGER REGION'),
(77, 'PT Irja Mandiri', 'robby_biak@yahoo.com', '085244112203', '085244112203', 'pt.irja.mandiri', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(78, 'PT. Papua Maju Mandiri', 'papuamajumandiri@yahoo.com', '082347491825', '082347491825', 'pt.papua.maju.mandiri', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(79, 'PT Gasirja Utama', 'gasirja@dibiak.com', '085243896287', '085243896287', 'pt.gasirja.utama', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(80, 'PT. Makotadewa Araima Supiori', 'mahkotadewabiak@gmail.com', '082211777714', '082211777714', 'pt.makotadewa.araima.supiori', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(81, 'PT. Restu Jaya', 'suyat9056@gmail.com', '082290767907', '082290767907', 'pt.restu.jaya', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(82, 'PT. Jackson Pratama', 'robby_biak@yahoo.com', '085244112203', '085244112203', 'pt.jackson.pratama', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(83, 'CV. Manisaprop', 'manisaprop@dibiak.com', '081344839669', '081344839669', 'cv.manisaprop', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(84, 'admin sistem second', 'adminsistem2@gmail.com', '09999999999', '08888888888', 'admin.opsi', 'c84258e9c39059a89ab77d846ddab909', 'ADMIN'),
(85, 'Patra Niaga ', 'leonard.patraniaga@gmail.com', '082399234949', '082399234949', 'patra.niaga.biak', '827ccb0eea8a706c4c34a16891f84e7b', 'ADMIN'),
(86, 'PT. Wapoga Mutiara Industries', 'waiting ...', '085254061077', '085254061077', 'pt.wapoga.mutiara.industries', '827ccb0eea8a706c4c34a16891f84e7b', 'PEMILIK'),
(87, 'trans', 'trans@gmail.com', '087778887778', '099878888882', 'trans', '4738019ef434f24099319565cd5185e5', 'PEMILIK'),
(88, 'trans', 'trans@gmail.com', '09222211222', '09722222222', 'trans', '4738019ef434f24099319565cd5185e5', 'PEMILIK');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_aktifitas`
--

CREATE TABLE `tbl_t_aktifitas` (
  `id_aktifitas` int(11) NOT NULL,
  `no_aktifitas` varchar(100) DEFAULT NULL,
  `id_pemilik` int(11) DEFAULT NULL,
  `id_truck` int(11) DEFAULT NULL,
  `id_awak` int(11) DEFAULT NULL,
  `id_keperluan` int(11) DEFAULT NULL,
  `detail_keperluan` varchar(255) DEFAULT NULL,
  `tgl_aktifitas` date DEFAULT NULL,
  `tgl_masa_berlaku_baru` date DEFAULT NULL,
  `tingkat_keperluan` varchar(100) DEFAULT NULL,
  `komentar` text DEFAULT NULL,
  `catatan` text DEFAULT NULL,
  `status_aktifitas` varchar(100) DEFAULT NULL,
  `catatan_admin` varchar(255) DEFAULT NULL,
  `catatan_manager_lokasi` varchar(255) DEFAULT NULL,
  `catatan_manager_region` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `id_truck_lama` int(11) DEFAULT NULL,
  `id_truck_baru` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_t_aktifitas`
--

INSERT INTO `tbl_t_aktifitas` (`id_aktifitas`, `no_aktifitas`, `id_pemilik`, `id_truck`, `id_awak`, `id_keperluan`, `detail_keperluan`, `tgl_aktifitas`, `tgl_masa_berlaku_baru`, `tingkat_keperluan`, `komentar`, `catatan`, `status_aktifitas`, `catatan_admin`, `catatan_manager_lokasi`, `catatan_manager_region`, `created_date`, `updated_date`, `id_truck_lama`, `id_truck_baru`) VALUES
(4, '004/TRK/V/2020', 51, 0, 17, 8, 'PERPANJANGAN IDCARD AMT', '2020-05-22', '2020-05-22', 'URGENT', 'PENTING, KARENA UNTUK KELUAR MASUK TERMINAL', NULL, '0', NULL, NULL, NULL, '2020-05-20 13:07:32', NULL, NULL, NULL),
(5, '005/TRK/V/2020', 1, 154, 0, 1, 'Perpanjangan STNK baru', '2020-05-21', '2020-05-21', 'MEDIUM', 'Mohon dapat diproses lebih lanjut', 'Dokumen sudah sesuai (ADMIN)\r\n\r\nOK SESUAI (MANAGER)', '3', NULL, NULL, NULL, '2020-05-21 21:25:48', '2020-05-21 21:40:18', NULL, NULL),
(6, '006/TRK/V/2020', 1, 0, 16, 7, 'tes', '2020-05-21', '2020-05-21', 'MEDIUM', NULL, 'Sudah sesuai', '4', NULL, NULL, NULL, '2020-05-21 21:49:55', '2020-05-21 22:03:59', NULL, NULL),
(7, '007/TRK/V/2020', 1, 0, 16, 7, 'Perpanjangan ID Card, mohon proses lebih lanjut.', '2020-05-21', '2020-05-21', 'MEDIUM', '', 'telah dilakukan reivisi pada redaksi surat.', '1', NULL, NULL, NULL, '2020-05-21 22:10:00', '2020-05-21 22:22:30', NULL, NULL),
(8, '008/TRK/V/2020', 1, 0, 0, 1, 'perpanjangan STNK', '2020-05-22', '2020-05-22', 'MEDIUM', 'tes', NULL, '0', NULL, NULL, NULL, '2020-05-22 08:49:06', NULL, NULL, NULL),
(9, '009/TRK/V/2020', 1, 0, 16, 7, 'tes', '2020-05-22', '2020-05-22', 'MEDIUM', 'tess', 'sudah sesuai', '5', NULL, NULL, NULL, '2020-05-22 08:50:33', '2020-05-22 09:21:04', NULL, NULL),
(10, '010/TRK/V/2020', 1, 0, 16, 7, 'Pengajuan ID Card Baru', '2020-05-22', '2020-05-22', 'MEDIUM', 'MOhon bantuan proses lebih lanjut', 'Dokumen sudah dierifikasi.', '5', NULL, NULL, NULL, '2020-05-22 12:47:43', '2020-05-22 12:51:19', NULL, NULL),
(11, '011/REQ/Q28042/2020', 1, 154, 0, 1, 's', '2020-07-03', '2025-07-03', 'LOW', '', NULL, '3', '', '', NULL, '2020-07-03 20:04:37', '2020-07-03 20:05:50', NULL, NULL),
(13, '013/REQ/Q28042/2020', 53, 183, 0, 6, 'pengajuan perpanjangan tera mobil tangki L 9218 AQ', '2020-08-03', '2020-07-13', 'MEDIUM', '', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-03 22:18:32', '2020-09-16 11:07:26', NULL, NULL),
(14, '014/REQ/Q28042/2020', 53, 184, 0, 6, 'perpanjangan tera mobil tangki PA 9004 MI/ L 9307 AY', '2020-08-03', '2020-07-13', 'MEDIUM', '', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-03 22:20:52', '2020-09-16 11:06:07', NULL, NULL),
(15, '015/REQ/Q28042/2020', 54, 182, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-04', '2021-11-30', 'MEDIUM', 'Sudah Melakukan Perpanjangan Tera', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-04 10:11:39', '2020-09-16 11:25:56', NULL, NULL),
(16, '016/REQ/Q28042/2020', 54, 180, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-04', '2021-11-30', 'MEDIUM', 'Sudah Melakukan Perpanjangan Tera', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-04 10:24:13', '2020-09-16 11:36:08', NULL, NULL),
(17, '017/REQ/Q28042/2020', 54, 181, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-04', '2021-11-30', 'MEDIUM', 'Sudah Melakukan Perpanjangan Tera', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-04 10:54:54', '2020-09-16 11:36:51', NULL, NULL),
(18, '018/REQ/Q28042/2020', 54, 178, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-04', '2021-11-30', 'MEDIUM', 'Sudah Melakukan Perpanjangan Tera', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-04 11:06:01', '2020-09-16 11:37:20', NULL, NULL),
(19, '019/REQ/Q28042/2020', 54, 179, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-04', '2021-11-30', 'MEDIUM', 'Sudah Melakukan Perpanjangan TERA', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-04 11:11:47', '2020-09-16 11:37:56', NULL, NULL),
(20, '020/REQ/Q28042/2020', 58, 172, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-04', '2021-11-30', 'MEDIUM', 'Sudah Melakukan Perpanjangan Tera', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-04 11:19:11', '2020-09-19 09:50:07', NULL, NULL),
(22, '022/REQ/Q28042/2020', 55, 187, 0, 6, 'Permohonan Perpanjangan Tera (telah dilakukan revisi pada lampiran)', '2020-08-04', '2020-08-04', 'URGENT', '(telah dilakukan revisi pada lampiran)', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-04 21:35:21', '2020-09-16 11:51:16', NULL, NULL),
(23, '023/REQ/Q28042/2020', 54, 188, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-06', '2021-11-30', 'MEDIUM', 'Sudah Melakukan Perpanjangan Tera', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-08-06 07:44:27', '2020-09-16 11:38:36', NULL, NULL),
(24, '024/REQ/Q28042/2020', 54, 189, 0, 6, 'Perpanjangan Meteologi Tera', '2020-08-06', '2021-11-30', 'MEDIUM', 'mohon ditindaklanjuti', NULL, '7', 'FILE DOKUMEN YANG DI UPLOAD HARUS DALAM BENTUK PDF', NULL, NULL, '2020-08-06 07:48:40', '2020-12-08 09:53:27', NULL, NULL),
(25, '025/REQ/Q28042/2020', 54, 0, 20, 7, 'Mohon bantuan untuk perpanjangan ID Card ybs.', '2020-08-26', '1970-01-01', 'LOW', 'Mohon bantuan untuk perpanjangan ID Card ybs.', NULL, '5', '', 'lanjutkan sesuai SOP', 'sesuai', '2020-08-26 12:47:26', '2020-08-26 12:51:57', NULL, NULL),
(26, '026/REQ/Q28042/2020', 54, 178, 0, 5, 'Pengajuan Ulang', '2020-09-16', '2021-01-22', 'MEDIUM', 'masa berlaku dokumen sudah di isi', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 08:08:49', '2020-09-16 11:39:37', NULL, NULL),
(27, '027/REQ/Q28042/2020', 54, 179, 0, 5, 'Pengajuan Ulang', '2020-09-16', '2021-01-22', 'MEDIUM', 'masa berlaku dokumen sudah diisi', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 08:11:36', '2020-09-16 11:40:08', NULL, NULL),
(28, '028/REQ/Q28042/2020', 54, 180, 0, 5, 'Pengajuan Ulang', '2020-09-16', '2021-01-22', 'URGENT', 'masa berlaku dokumen sudah diisi', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 08:18:37', '2020-09-16 11:40:31', NULL, NULL),
(29, '029/REQ/Q28042/2020', 54, 181, 0, 5, 'Pengajuan ulang', '2020-09-16', '2021-01-22', 'URGENT', 'masa berlaku dokumen sudah di isi', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 08:20:16', '2020-09-16 11:40:52', NULL, NULL),
(30, '030/REQ/Q28042/2020', 54, 182, 0, 5, 'Pengajuan Ulang', '2020-09-16', '2021-01-22', 'URGENT', 'masa berlaku dokumen sudah diisikan', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 08:21:57', '2020-09-16 11:41:07', NULL, NULL),
(31, '031/REQ/Q28042/2020', 54, 188, 0, 5, 'Pengajuan Ulang', '2020-09-16', '2021-01-22', 'URGENT', 'masa berlaku sudah diisikan', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 08:23:38', '2020-09-16 11:41:22', NULL, NULL),
(32, '032/REQ/Q28042/2020', 54, 189, 0, 5, 'Pengajuan Ulang', '2020-09-16', '2020-09-16', 'URGENT', 'Sudah Melakukan Perpanjangan Keur', NULL, '3', 'Sesuai', 'sesuai', NULL, '2020-09-16 08:25:05', '2020-09-16 11:08:38', NULL, NULL),
(33, '033/REQ/Q28042/2020', 54, 189, 0, 5, 'Perpanjangan masa berlaku KEUR / KIR (TEST)', '2020-09-16', '2021-01-22', 'URGENT', 'ok', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 08:58:17', '2020-09-16 11:08:58', NULL, NULL),
(34, '034/REQ/Q28042/2020', 53, 184, 0, 5, 'perpanjangan keur', '2020-09-16', '2020-11-20', 'URGENT', '', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-09-16 10:52:54', '2020-09-16 11:41:35', NULL, NULL),
(35, '035/REQ/Q28042/2020', 53, 183, 0, 5, 'perpanjangan keur', '2020-09-19', '2020-09-19', 'URGENT', '', NULL, '2', 'Masa berlaku dokumen harus diinput saat proses request', NULL, NULL, '2020-09-19 08:41:01', '2020-09-19 09:49:03', NULL, NULL),
(36, '036/REQ/Q28042/2020', 53, 183, 0, 5, 'perpanjangan keur', '2020-09-21', '2020-11-20', 'URGENT', '', NULL, '1', 'sesuai', NULL, NULL, '2020-09-21 07:24:05', '2020-09-21 09:09:32', NULL, NULL),
(37, '037/REQ/Q28042/2020', 54, 188, 0, 1, 'Perpanjangan masa berlaku STNK', '2020-10-06', '2021-12-31', 'MEDIUM', 'mohon ditindaklanjuti', NULL, '7', '', NULL, NULL, '2020-10-06 08:14:04', '2020-12-08 09:52:33', NULL, NULL),
(40, '038/REQ/Q28042/2020', 52, 0, 43, 7, 'untuk mendapatkan ID CARD Baru', '2020-10-07', '1970-01-01', 'URGENT', '', NULL, '5', 'Mohon bantuan approval pembuatan ID Card baru PT Irja Mandiri ', 'MOHON BANTUAN APPROVAL PEMBUATAN ID CARD BARU PT IRJA MANDIR', 'Persyaratan lengkap.\r\nYbs bisa untuk di cetak ID Card nya. Terima kasih', '2020-10-07 09:10:41', '2020-10-09 14:12:50', NULL, NULL),
(41, '039/REQ/Q28042/2020', 54, 0, 41, 9, 'Pindah Ke Mobil NOPOL PA 9505 C', '2020-10-12', '2020-10-13', 'URGENT', 'Mohon ditindaklanjuti', NULL, '7', NULL, NULL, NULL, '2020-10-12 13:52:51', '2020-12-08 09:49:56', NULL, 0),
(42, '040/REQ/Q28042/2020', 54, 0, 24, 9, 'Pindah Mobil NOPOL S 9856 UQ', '2020-10-12', '2020-10-13', 'URGENT', 'Mohon Ditindaklanjuti', NULL, '7', NULL, NULL, NULL, '2020-10-12 14:13:39', '2020-12-08 09:46:25', NULL, 0),
(43, '041/REQ/Q28042/2020', 54, 0, 20, 7, 'Permohonan Pembuatan ID Card', '2020-10-13', '2020-12-31', 'URGENT', 'mohon ditindaklanjuti', NULL, '7', 'MOHON BANTUAN APPROVAL PEMBUATAN ID CARD BARU PT IRJA MANDIRI', '', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI', '2020-10-13 13:39:15', '2020-12-08 09:31:26', NULL, NULL),
(44, '042/REQ/Q28042/2020', 54, 0, 38, 7, 'Permohonan Pembuatan ID Card', '2020-10-13', '2020-12-31', 'URGENT', 'mohon ditindaklanjuti', NULL, '7', 'Mohon Approval ', '', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI', '2020-10-13 13:41:19', '2020-12-05 08:49:17', NULL, NULL),
(45, '043/REQ/Q28042/2020', 54, 0, 30, 7, 'Permohonan Pembuatan ID Card', '2020-10-13', '1970-01-01', 'URGENT', 'Mohon ditindaklanjuti', NULL, '5', '', '', 'Kekurangan surat Bebas Narkoba', '2020-10-13 13:43:17', '2020-10-14 14:54:45', NULL, NULL),
(46, '044/REQ/Q28042/2020', 54, 0, 39, 7, 'Permohonan Pembuatan ID Card', '2020-10-13', '2020-12-31', 'URGENT', 'mohon ditindaklanjuti', NULL, '7', 'Mohon melampirkan :\r\n1. SKCK\r\n2. Surat bebas nakoba', '', 'Surat Bebas  Narkoba belum ada.', '2020-10-13 13:44:29', '2020-12-05 08:54:46', NULL, NULL),
(47, '045/REQ/Q28042/2020', 54, 0, 21, 9, 'Pergantian Sementara', '2020-10-28', '2020-10-30', 'MEDIUM', 'Mohon Ditindaklanjuti', NULL, '7', NULL, NULL, NULL, '2020-10-28 07:57:42', '2020-12-08 07:25:32', NULL, 0),
(48, '046/REQ/Q28042/2020', 56, 193, 0, 1, 'Perpanjang STNK', '2020-10-30', '2025-01-08', 'URGENT', '', NULL, '1', '', NULL, NULL, '2020-10-30 11:12:21', '2020-10-30 12:02:10', NULL, NULL),
(51, '049/REQ/Q28042/2020', 54, 181, 0, 5, 'Perpanjangan dokumen Sertifikat KEUR', '2020-11-17', '2021-01-22', 'URGENT', 'Perpanjangan dokumen Sertifikat KEUR', NULL, '7', NULL, NULL, NULL, '2020-11-17 14:11:48', '2020-12-08 09:51:23', NULL, NULL),
(52, '050/REQ/Q28042/2020', 56, 193, 0, 1, 'Perpanjangan dokumen STNK', '2020-11-17', '2021-11-30', 'URGENT', 'Perpanjangan dokumen STNK', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 19:58:00', '2020-11-17 20:00:07', NULL, NULL),
(53, '051/REQ/Q28042/2020', 59, 190, 0, 5, 'Perpanjangan dokumen kEUR', '2020-11-17', '2021-11-30', 'URGENT', 'Perpanjangan dokumen kEUR', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:27:07', '2020-11-17 20:38:36', NULL, NULL),
(54, '052/REQ/Q28042/2020', 55, 187, 0, 5, 'Perpanjangan dokumen kEUR', '2020-11-17', '2021-11-30', 'URGENT', 'Perpanjangan dokumen kEUR', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:28:52', '2020-11-17 20:38:21', NULL, NULL),
(55, '053/REQ/Q28042/2020', 55, 187, 0, 6, 'Perpanjangan dokumen TERA', '2020-11-17', '2021-07-31', 'URGENT', 'Perpanjangan dokumen TERA', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:29:58', '2020-11-17 20:38:06', NULL, NULL),
(56, '054/REQ/Q28042/2020', 55, 185, 0, 5, 'Perpanjangan dokumen KEUR', '2020-11-17', '2020-11-30', 'URGENT', 'Perpanjangan dokumen KEUR', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:31:19', '2020-11-17 20:37:52', NULL, NULL),
(57, '055/REQ/Q28042/2020', 55, 185, 0, 6, 'Perpanjangan dokumen TERA', '2020-11-17', '2021-07-31', 'URGENT', 'Perpanjangan dokumen TERA', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:32:27', '2020-11-17 20:37:39', NULL, NULL),
(58, '056/REQ/Q28042/2020', 53, 184, 0, 6, 'Perpanjangan dokumen TERA', '2020-11-17', '2021-07-31', 'URGENT', 'Perpanjangan dokumen TERA', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:33:43', '2020-11-17 20:37:20', NULL, NULL),
(59, '057/REQ/Q28042/2020', 55, 185, 0, 5, 'Perpanjangan dokumen KEUR', '2020-11-17', '2021-11-30', 'URGENT', 'Perpanjangan dokumen KEUR', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:40:28', '2020-11-17 20:41:28', NULL, NULL),
(60, '058/REQ/Q28042/2020', 52, 0, 43, 2, 'Perpanjangan SIM ', '2020-11-17', '2024-12-31', 'URGENT', 'Perpanjangan SIM ', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:44:06', '2020-11-17 20:47:36', NULL, NULL),
(61, '059/REQ/Q28042/2020', 57, 0, 37, 2, 'Perpanjangan SIM ', '2020-11-17', '2023-12-30', 'URGENT', 'Perpanjangan SIM ', NULL, '3', 'sesuai', 'sesuai', NULL, '2020-11-17 20:45:02', '2020-11-17 20:47:49', NULL, NULL),
(62, '060/REQ/Q28042/2020', 53, 183, 0, 6, 'perpanjangan tera industri', '2020-11-18', '2021-11-30', 'URGENT', '', NULL, '3', 'Sesuai', 'Proses', NULL, '2020-11-18 07:49:03', '2021-01-01 21:18:19', NULL, NULL),
(63, '061/REQ/Q28042/2020', 58, 0, 19, 9, 'AMT II Yulian Rumwaropen diganti sementara dengan Sukardin ', '2020-11-21', '2020-11-21', 'URGENT', 'Mohon ditindaklanjuti', NULL, '1', 'sesuai', NULL, NULL, '2020-11-21 08:37:18', '2021-01-04 07:49:21', 172, 172),
(65, '063/REQ/Q28042/2020', 54, 0, 21, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'telah direvisi', NULL, '1', 'sesuai', NULL, NULL, '2020-12-05 11:22:14', '2021-01-04 07:45:18', NULL, NULL),
(66, '064/REQ/Q28042/2020', 54, 0, 22, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'sesuai', 'Silahkan dicetak', '2020-12-05 11:26:54', '2020-12-23 12:33:48', NULL, NULL),
(67, '065/REQ/Q28042/2020', 54, 0, 23, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'sesuai', 'Silahkan dicetak', '2020-12-05 11:32:40', '2020-12-23 12:33:27', NULL, NULL),
(68, '066/REQ/Q28042/2020', 54, 0, 24, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'sesuai', 'Silahkan dicetak', '2020-12-05 11:37:20', '2020-12-23 12:33:09', NULL, NULL),
(69, '067/REQ/Q28042/2020', 54, 0, 25, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '2020-12-31', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'sesuai', 'sesuai', 'Silahkan dicetak', '2020-12-05 11:43:37', '2020-12-23 12:32:27', NULL, NULL),
(70, '068/REQ/Q28042/2020', 54, 0, 26, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'sesuai', 'Silahkan dicetak', '2020-12-05 11:53:24', '2020-12-23 12:32:00', NULL, NULL),
(71, '069/REQ/Q28042/2020', 54, 0, 27, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'sesuai', 'Silahkan dicetak', '2020-12-05 11:58:32', '2020-12-23 12:31:43', NULL, NULL),
(72, '070/REQ/Q28042/2020', 54, 0, 31, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'SKCK sudah yang terbaru. Mohon ditindaklanjuti', NULL, '5', 'sesuai', 'sesuai', 'Makasih', '2020-12-05 12:03:38', '2020-12-23 12:31:22', NULL, NULL),
(73, '071/REQ/Q28042/2020', 54, 0, 40, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'sesuai', 'Makasih', '2020-12-05 12:08:10', '2020-12-23 12:31:05', NULL, NULL),
(74, '072/REQ/Q28042/2020', 54, 0, 41, 8, 'Permohonan Perpanjangan ID Card', '2020-12-05', '1970-01-01', 'URGENT', 'mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'sesuai', '', '2020-12-05 12:11:54', '2020-12-23 12:30:41', NULL, NULL),
(75, '073/REQ/Q28042/2020', 53, 183, 0, 5, 'PERPANJANGAN KEUR', '2020-12-09', '2021-05-19', 'URGENT', '', NULL, '3', 'Sesuai', 'Proses', NULL, '2020-12-09 19:11:58', '2021-01-01 21:18:07', NULL, NULL),
(78, '076/REQ/Q28042/2020', 58, 0, 18, 8, 'Permohonan Perpanjangan ID Card', '2020-12-29', '1970-01-01', 'URGENT', 'Mohon ditindaklanjuti', NULL, '5', 'sesuai', 'Proses', 'Sesuai', '2020-12-29 08:32:24', '2021-01-01 21:21:49', NULL, NULL),
(79, '077/REQ/Q28042/2020', 58, 0, 19, 8, 'Permohonan Perpanjangan ID Card', '2020-12-29', '1970-01-01', 'URGENT', 'Mohon ditindaklanjuti', NULL, '5', 'Sesuai', 'Proses', 'Sesuai', '2020-12-29 08:34:30', '2021-01-01 21:21:35', NULL, NULL),
(80, '078/REQ/Q28042/2020', 56, 0, 44, 8, 'Perpanjangan ID Card', '2020-12-29', '1970-01-01', 'URGENT', 'Permohonan Telah direvisi', NULL, '2', 'format foto salah', NULL, NULL, '2020-12-29 15:18:27', '2021-01-04 07:55:52', NULL, NULL),
(81, '079/REQ/Q28042/2020', 56, 0, 45, 8, 'Perpanjangan ID card', '2020-12-29', '1970-01-01', 'URGENT', '-', NULL, '7', 'format foto salah', NULL, NULL, '2020-12-29 15:20:25', '2021-01-04 10:38:08', NULL, NULL),
(82, '080/REQ/Q28042/2020', 56, 0, 46, 8, 'Perpanjangan ID Card', '2020-12-29', '1970-01-01', 'URGENT', '-', NULL, '7', 'format foto salah', NULL, NULL, '2020-12-29 15:24:07', '2021-01-04 10:35:58', NULL, NULL),
(83, '081/REQ/Q28042/2020', 56, 0, 47, 8, 'Perpanjangan  ID card', '2020-12-29', '2021-12-31', 'URGENT', 'telah direvisi', NULL, '5', 'sesuai', 'sesuai', 'sesuai', '2020-12-29 15:26:04', '2021-01-04 08:16:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_dokumen_aktifitas`
--

CREATE TABLE `tbl_t_dokumen_aktifitas` (
  `id_dokumen` int(11) NOT NULL,
  `id_aktifitas` int(11) DEFAULT NULL,
  `id_syarat` int(11) DEFAULT NULL,
  `dokumen_syarat` text DEFAULT NULL,
  `format_dokumen` varchar(10) DEFAULT NULL,
  `approval_admin` char(1) DEFAULT NULL,
  `tgl_approval_admin` datetime DEFAULT NULL,
  `note_approval_admin` text DEFAULT NULL,
  `approval_manager_lokasi` char(1) DEFAULT NULL,
  `tgl_approval_manager_lokasi` datetime DEFAULT NULL,
  `note_approval_manager_lokasi` text DEFAULT NULL,
  `approval_manager_region` char(1) DEFAULT NULL,
  `tgl_approval_manager_region` datetime DEFAULT NULL,
  `note_approval_manager_region` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_t_dokumen_aktifitas`
--

INSERT INTO `tbl_t_dokumen_aktifitas` (`id_dokumen`, `id_aktifitas`, `id_syarat`, `dokumen_syarat`, `format_dokumen`, `approval_admin`, `tgl_approval_admin`, `note_approval_admin`, `approval_manager_lokasi`, `tgl_approval_manager_lokasi`, `note_approval_manager_lokasi`, `approval_manager_region`, `tgl_approval_manager_region`, `note_approval_manager_region`) VALUES
(1, 1, 1, 'DOKUMEN_PERPANJANGAN_STNK_1_W_8888_KK_2020_05_20_13_03_12.pdf', '.pdf', 'Y', '2020-05-20 13:10:08', '', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 3, 'DOKUMEN_PERPANJANGAN_KARTU_KIM_3_W_8888_KK_2020_05_20_13_05_09.pdf', '.pdf', 'Y', '2020-05-22 09:17:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 2, 4, 'DOKUMEN_PERPANJANGAN_KARTU_KIM_4_W_8888_KK_2020_05_20_13_05_09.pdf', '.pdf', 'Y', '2020-05-22 09:17:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 2, 5, 'DOKUMEN_PERPANJANGAN_KARTU_KIM_5_W_8888_KK_2020_05_20_13_05_09.jpg', '.jpg', 'Y', '2020-05-22 09:17:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 2, 6, 'DOKUMEN_PERPANJANGAN_KARTU_KIM_6_W_8888_KK_2020_05_20_13_05_09.pdf', '.pdf', 'Y', '2020-05-22 09:17:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 2, 7, 'DOKUMEN_PERPANJANGAN_KARTU_KIM_7_W_8888_KK_2020_05_20_13_05_09.jpg', '.jpg', 'Y', '2020-05-22 09:17:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 3, 2, 'DOKUMEN_PERPANJANGAN_SIM_2_762716_2020_05_20_13_06_23.pdf', '.pdf', 'Y', '2020-05-22 12:44:23', '', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 4, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_762716_2020_05_20_13_07_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 4, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_762716_2020_05_20_13_07_32.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 4, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_762716_2020_05_20_13_07_32.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 4, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_762716_2020_05_20_13_07_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 5, 1, 'DOKUMEN_PERPANJANGAN_STNK_1_DS_7884_J_2020_05_21_21_25_48.jpg', '.jpg', 'Y', '2020-05-21 21:35:25', '', 'Y', '2020-05-21 21:40:18', 'Dokumen sudah sesuai (ADMIN)\r\n\r\nOK SESUAI (MANAGER)', NULL, NULL, NULL),
(13, 6, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_762716_2020_05_21_21_49_55.jpg', '.jpg', 'Y', '2020-05-21 21:59:42', '', 'Y', '2020-05-21 22:02:11', 'Sudah sesuai', 'Y', '2020-05-21 22:03:59', 'Sudah sesuai'),
(14, 6, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_762716_2020_05_21_21_49_55.jpg', '.jpg', 'Y', '2020-05-21 21:59:42', '', 'Y', '2020-05-21 22:02:11', 'Sudah sesuai', 'Y', '2020-05-21 22:03:59', 'Sudah sesuai'),
(15, 6, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_762716_2020_05_21_21_49_55.jpg', '.jpg', 'Y', '2020-05-21 21:59:42', '', 'Y', '2020-05-21 22:02:11', 'Sudah sesuai', 'Y', '2020-05-21 22:03:59', 'Sudah sesuai'),
(16, 6, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_762716_2020_05_21_21_49_55.jpg', '.jpg', 'Y', '2020-05-21 21:59:42', '', 'Y', '2020-05-21 22:02:11', 'Sudah sesuai', 'Y', '2020-05-21 22:03:59', 'Sudah sesuai'),
(17, 7, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_762716_2020_05_21_22_19_13.jpg', '.jpg', 'Y', '2020-05-21 22:22:30', 'redaksi surat salah', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 7, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_762716_2020_05_21_22_10_00.jpg', '.jpg', 'Y', '2020-05-21 22:22:30', '', NULL, NULL, NULL, NULL, NULL, NULL),
(19, 7, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_762716_2020_05_21_22_10_00.jpg', '.jpg', 'Y', '2020-05-21 22:22:30', '', NULL, NULL, NULL, NULL, NULL, NULL),
(20, 7, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_762716_2020_05_21_22_10_00.jpg', '.jpg', 'Y', '2020-05-21 22:22:30', '', NULL, NULL, NULL, NULL, NULL, NULL),
(21, 8, 1, 'DOKUMEN_PERPANJANGAN_STNK_1__2020_05_22_08_49_06.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 9, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_762716_2020_05_22_08_50_33.jpg', '.jpg', 'Y', '2020-05-22 09:19:38', '', 'Y', '2020-05-22 09:20:20', 'sudah sesuai', 'Y', '2020-05-22 09:21:04', 'sudah sesuai'),
(23, 9, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_762716_2020_05_22_08_50_33.jpg', '.jpg', 'Y', '2020-05-22 09:19:38', '', 'Y', '2020-05-22 09:20:20', 'sudah sesuai', 'Y', '2020-05-22 09:21:04', 'sudah sesuai'),
(24, 9, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_762716_2020_05_22_08_50_33.jpg', '.jpg', 'Y', '2020-05-22 09:19:38', '', 'Y', '2020-05-22 09:20:20', 'sudah sesuai', 'Y', '2020-05-22 09:21:04', 'sudah sesuai'),
(25, 9, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_762716_2020_05_22_08_50_33.jpg', '.jpg', 'Y', '2020-05-22 09:19:38', '', 'Y', '2020-05-22 09:20:20', 'sudah sesuai', 'Y', '2020-05-22 09:21:04', 'sudah sesuai'),
(26, 10, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_762716_2020_05_22_12_47_43.jpg', '.jpg', 'Y', '2020-05-22 12:48:42', '', 'Y', '2020-05-22 12:50:27', 'Dokumen sudah dierifikasi.', 'Y', '2020-05-22 12:51:19', 'Dokumen sudah dierifikasi.'),
(27, 10, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_762716_2020_05_22_12_47_43.jpg', '.jpg', 'Y', '2020-05-22 12:48:42', '', 'Y', '2020-05-22 12:50:27', 'Dokumen sudah dierifikasi.', 'Y', '2020-05-22 12:51:19', 'Dokumen sudah dierifikasi.'),
(28, 10, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_762716_2020_05_22_12_47_43.jpg', '.jpg', 'Y', '2020-05-22 12:48:42', '', 'Y', '2020-05-22 12:50:27', 'Dokumen sudah dierifikasi.', 'Y', '2020-05-22 12:51:19', 'Dokumen sudah dierifikasi.'),
(29, 10, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_762716_2020_05_22_12_47_43.jpg', '.jpg', 'Y', '2020-05-22 12:48:42', '', 'Y', '2020-05-22 12:50:27', 'Dokumen sudah dierifikasi.', 'Y', '2020-05-22 12:51:19', 'Dokumen sudah dierifikasi.'),
(30, 11, 1, 'DOKUMEN_PERPANJANGAN_STNK_1_DS_7884_J_2020_07_03_20_04_44.pdf', '.pdf', 'Y', '2020-07-03 20:05:21', '', 'Y', '2020-07-03 20:05:49', '', NULL, NULL, NULL),
(31, 12, 2, 'DOKUMEN_PERPANJANGAN_SIM_2_02_2020_07_14_09_47_08.jpeg', '.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 13, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_L_9218_AQ_2020_08_03_22_18_39.pdf', '.pdf', 'Y', '2020-09-15 17:51:18', '', 'Y', '2020-09-16 11:07:26', 'sesuai', NULL, NULL, NULL),
(33, 14, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9004_MI_2020_08_03_22_20_59.pdf', '.pdf', 'Y', '2020-09-15 17:50:49', '', 'Y', '2020-09-16 11:06:07', 'sesuai', NULL, NULL, NULL),
(34, 15, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9705_C_2020_09_16_07_38_23.pdf', '.pdf', 'Y', '2020-09-16 11:10:35', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-16 11:25:56', 'sesuai', NULL, NULL, NULL),
(35, 16, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9505_C_2020_09_16_07_37_49.pdf', '.pdf', 'Y', '2020-09-16 11:11:37', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-16 11:36:08', 'sesuai', NULL, NULL, NULL),
(36, 17, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_S_8621_UN_2020_09_16_07_27_26.pdf', '.pdf', 'Y', '2020-09-16 11:12:27', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-16 11:36:51', 'sesuai', NULL, NULL, NULL),
(37, 18, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9630_C_2020_09_16_07_37_17.pdf', '.pdf', 'Y', '2020-09-16 11:13:46', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-16 11:37:20', 'sesuai', NULL, NULL, NULL),
(38, 19, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9650_C_2020_09_16_07_36_41.pdf', '.pdf', 'Y', '2020-09-16 11:14:38', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-16 11:37:56', 'sesuai', NULL, NULL, NULL),
(39, 20, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9928_CB_2020_09_16_11_58_20.pdf', '.pdf', 'Y', '2020-09-18 12:35:20', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-19 09:50:06', 'sesuai', NULL, NULL, NULL),
(40, 21, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9__PA_9101_U_2020_08_04_12_25_26.pdf', '.pdf', 'N', '2020-09-15 17:46:42', 'File dokumen yang di upload harus dalam bentuk PDF', NULL, NULL, NULL, NULL, NULL, NULL),
(41, 22, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9103_U_2020_08_04_21_35_28.pdf', '.pdf', 'Y', '2020-09-16 11:48:53', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-16 11:51:16', 'sesuai', NULL, NULL, NULL),
(42, 23, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_S_9878_UR_2020_09_16_07_19_38.pdf', '.pdf', 'Y', '2020-09-16 11:16:32', 'File dokumen yang di upload harus dalam bentuk PDF', 'Y', '2020-09-16 11:38:36', 'sesuai', NULL, NULL, NULL),
(43, 24, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_S_9856_UQ_2020_12_08_09_53_27.pdf', '.pdf', 'N', '2020-09-16 11:18:03', 'File dokumen yang di upload harus dalam bentuk PDF', NULL, NULL, NULL, NULL, NULL, NULL),
(44, 25, 1, 'DOKUMEN_PERPANJANGAN_STNK_1_PA_9630_C_2020_08_11_09_38_46.jpeg', '.jpeg', 'Y', '2020-08-26 12:49:22', '', 'Y', '2020-08-26 12:50:09', 'lanjutkan sesuai SOP', 'Y', '2020-08-26 12:51:57', 'sesuai'),
(45, 25, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_02_2020_08_26_12_47_34.jpeg', '.jpeg', 'Y', '2020-08-26 12:49:22', '', 'Y', '2020-08-26 12:50:09', 'lanjutkan sesuai SOP', 'Y', '2020-08-26 12:51:57', 'sesuai'),
(46, 25, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_02_2020_08_26_12_47_35.jpeg', '.jpeg', 'Y', '2020-08-26 12:49:22', '', 'Y', '2020-08-26 12:50:09', 'lanjutkan sesuai SOP', 'Y', '2020-08-26 12:51:57', 'sesuai'),
(47, 25, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_02_2020_08_26_12_47_35.jpeg', '.jpeg', 'Y', '2020-08-26 12:49:22', '', 'Y', '2020-08-26 12:50:09', 'lanjutkan sesuai SOP', 'Y', '2020-08-26 12:51:57', 'sesuai'),
(48, 25, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_02_2020_08_26_12_47_35.jpeg', '.jpeg', 'Y', '2020-08-26 12:49:22', '', 'Y', '2020-08-26 12:50:09', 'lanjutkan sesuai SOP', 'Y', '2020-08-26 12:51:57', 'sesuai'),
(49, 26, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9630_C_2020_09_16_08_09_13.pdf', '.pdf', 'Y', '2020-09-16 11:18:47', 'TOLONG MASA BERLAKU DOKUMEN KEUR DIISIKAN PADA KOLOM REQUEST PENGAJUAN', 'Y', '2020-09-16 11:39:37', 'sesuai', NULL, NULL, NULL),
(50, 27, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9650_C_2020_09_16_09_18_32.pdf', '.pdf', 'Y', '2020-09-16 11:19:26', 'TOLONG MASA BERLAKU DOKUMEN KEUR DIISIKAN PADA KOLOM REQUEST PENGAJUAN', 'Y', '2020-09-16 11:40:08', 'sesuai', NULL, NULL, NULL),
(51, 28, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9505_C_2020_09_16_08_19_00.pdf', '.pdf', 'Y', '2020-09-16 11:20:54', 'TOLONG MASA BERLAKU DOKUMEN KEUR DIISIKAN PADA KOLOM REQUEST PENGAJUAN', 'Y', '2020-09-16 11:40:31', 'sesuai', NULL, NULL, NULL),
(52, 29, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_8621_UN_2020_09_16_08_20_22.pdf', '.pdf', 'Y', '2020-09-16 11:21:39', 'TOLONG MASA BERLAKU DOKUMEN KEUR DIISIKAN PADA KOLOM REQUEST PENGAJUAN', 'Y', '2020-09-16 11:40:52', 'sesuai', NULL, NULL, NULL),
(53, 30, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9705_C_2020_09_16_08_22_33.pdf', '.pdf', 'Y', '2020-09-16 11:22:24', 'TOLONG MASA BERLAKU DOKUMEN KEUR DIISIKAN PADA KOLOM REQUEST PENGAJUAN', 'Y', '2020-09-16 11:41:07', 'sesuai', NULL, NULL, NULL),
(54, 31, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_9878_UR_2020_09_16_09_13_51.pdf', '.pdf', 'Y', '2020-09-16 11:22:51', 'Tolong masa berlaku dokumen KEUR diisikan pada kolom request pengajuan', 'Y', '2020-09-16 11:41:22', 'sesuai', NULL, NULL, NULL),
(55, 32, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_9856_UQ_2020_09_16_08_25_11.pdf', '.pdf', 'Y', '2020-09-16 08:45:16', '', 'Y', '2020-09-16 11:08:38', 'sesuai', NULL, NULL, NULL),
(56, 33, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_9856_UQ_2020_09_16_09_14_37.pdf', '.pdf', 'Y', '2020-09-16 11:01:44', '', 'Y', '2020-09-16 11:08:58', 'sesuai', NULL, NULL, NULL),
(57, 34, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9004_MI_2020_09_16_10_53_00.pdf', '.pdf', 'Y', '2020-09-16 11:23:32', '', 'Y', '2020-09-16 11:41:35', 'sesuai', NULL, NULL, NULL),
(58, 35, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_L_9218_AQ_2020_09_19_08_41_07.pdf', '.pdf', 'N', '2020-09-19 09:49:03', 'Masa berlaku dokumen harus diinput saat proses request', NULL, NULL, NULL, NULL, NULL, NULL),
(59, 36, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_L_9218_AQ_2020_09_21_07_24_11.pdf', '.pdf', 'Y', '2020-09-21 09:09:32', '', NULL, NULL, NULL, NULL, NULL, NULL),
(60, 37, 1, 'DOKUMEN_PERPANJANGAN_STNK_1_S_9878_UR_2020_12_08_09_52_33.pdf', '.jpeg', 'N', '2020-10-14 08:49:57', 'dokumen tidak sesuai (Test)\r\n\r\ncontoh,no response', NULL, NULL, NULL, NULL, NULL, NULL),
(61, 40, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_5208013112980033_2020_10_07_09_10_48.pdf', '.pdf', 'Y', '2020-10-09 09:48:21', '', 'Y', '2020-10-09 09:50:58', 'MOHON BANTUAN APPROVAL PEMBUATAN ID CARD BARU PT IRJA MANDIR', 'Y', '2020-10-09 14:12:50', 'Persyaratan lengkap.\r\nYbs bisa untuk di cetak ID Card nya. Terima kasih'),
(62, 40, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_5208013112980033_2020_10_07_09_10_48.jpg', '.jpg', 'Y', '2020-10-09 09:48:21', '', 'Y', '2020-10-09 09:50:58', 'MOHON BANTUAN APPROVAL PEMBUATAN ID CARD BARU PT IRJA MANDIR', 'Y', '2020-10-09 14:12:50', 'Persyaratan lengkap.\r\nYbs bisa untuk di cetak ID Card nya. Terima kasih'),
(63, 40, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_5208013112980033_2020_10_07_09_10_48.pdf', '.pdf', 'Y', '2020-10-09 09:48:21', '', 'Y', '2020-10-09 09:50:58', 'MOHON BANTUAN APPROVAL PEMBUATAN ID CARD BARU PT IRJA MANDIR', 'Y', '2020-10-09 14:12:50', 'Persyaratan lengkap.\r\nYbs bisa untuk di cetak ID Card nya. Terima kasih'),
(64, 40, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_5208013112980033_2020_10_07_09_10_48.pdf', '.pdf', 'Y', '2020-10-09 09:48:21', '', 'Y', '2020-10-09 09:50:58', 'MOHON BANTUAN APPROVAL PEMBUATAN ID CARD BARU PT IRJA MANDIR', 'Y', '2020-10-09 14:12:50', 'Persyaratan lengkap.\r\nYbs bisa untuk di cetak ID Card nya. Terima kasih'),
(65, 40, 25, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_25_5208013112980033_2020_10_07_09_10_48.pdf', '.pdf', 'Y', '2020-10-09 09:48:21', '', 'Y', '2020-10-09 09:50:58', 'MOHON BANTUAN APPROVAL PEMBUATAN ID CARD BARU PT IRJA MANDIR', 'Y', '2020-10-09 14:12:50', 'Persyaratan lengkap.\r\nYbs bisa untuk di cetak ID Card nya. Terima kasih'),
(66, 41, 19, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_19_5208013112980033_2020_10_12_13_52_58.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 41, 20, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_20_9106010501750002_2020_12_08_09_49_56.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 41, 21, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_21_9106010501750002_2020_12_08_09_49_56.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 41, 22, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_22_9106010501750002_2020_12_08_09_49_56.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 41, 23, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_23_9106010501750002_2020_12_08_09_49_56.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 41, 27, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_27_9106010501750002_2020_12_08_09_49_56.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 42, 19, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_19_5208013112980033_2020_10_12_14_13_45.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 42, 20, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_20_9106010501750002_2020_12_08_09_46_26.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 42, 21, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_21_9106010501750002_2020_12_08_09_46_26.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 42, 22, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_22_9106010501750002_2020_12_08_09_46_26.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 42, 23, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_23_9106010501750002_2020_12_08_09_46_26.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 42, 27, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_27_9106010501750002_2020_12_08_09_46_26.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 43, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_5208013112980033_2020_10_13_13_39_21.pdf', '.pdf', 'Y', '2020-10-14 09:35:27', '', 'Y', '2020-10-15 07:45:46', '', 'N', '2020-10-20 07:39:53', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(79, 43, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_9106010501750002_2020_12_08_09_31_26.jpg', '.pdf', 'Y', '2020-10-14 09:35:27', '', 'Y', '2020-10-15 07:45:46', '', 'N', '2020-10-20 07:39:53', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(80, 43, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_9106010501750002_2020_12_08_09_31_26.pdf', '.pdf', 'Y', '2020-10-14 09:35:27', '', 'Y', '2020-10-15 07:45:46', '', 'N', '2020-10-20 07:39:53', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(81, 43, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_5208013112980033_2020_10_13_13_39_21.pdf', '.pdf', 'Y', '2020-10-14 09:35:27', '', 'Y', '2020-10-15 07:45:46', '', 'N', '2020-10-20 07:39:53', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(82, 43, 25, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_25_9106010501750002_2020_12_08_09_31_26.pdf', '.pdf', 'Y', '2020-10-14 09:35:27', '', 'Y', '2020-10-15 07:45:46', '', 'N', '2020-10-20 07:39:53', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(83, 44, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_9106010501750002_2020_12_05_08_49_17.pdf', '.pdf', 'Y', '2020-10-14 09:34:32', '', 'Y', '2020-10-15 07:45:27', '', 'N', '2020-10-20 07:37:36', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(84, 44, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_9106010501750002_2020_12_05_08_49_17.jpg', '.pdf', 'Y', '2020-10-14 09:34:32', '', 'Y', '2020-10-15 07:45:27', '', 'N', '2020-10-20 07:37:36', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(85, 44, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_9106010501750002_2020_12_05_08_49_17.pdf', '.pdf', 'Y', '2020-10-14 09:34:32', '', 'Y', '2020-10-15 07:45:27', '', 'N', '2020-10-20 07:37:36', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(86, 44, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_9106010501750002_2020_12_05_08_49_17.pdf', '.pdf', 'Y', '2020-10-14 09:34:32', '', 'Y', '2020-10-15 07:45:27', '', 'N', '2020-10-20 07:37:36', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(87, 44, 25, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_25_9106010501750002_2020_12_05_08_49_17.pdf', '.pdf', 'Y', '2020-10-14 09:34:32', '', 'Y', '2020-10-15 07:45:27', '', 'N', '2020-10-20 07:37:36', '1. SURAT BEBAS NARKOBA BELUM ADA\r\n2. LAMPIRAN SKCK TIDAK SESUAI'),
(88, 45, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_5208013112980033_2020_10_13_13_43_23.pdf', '.pdf', 'Y', '2020-10-14 07:21:34', '', 'Y', '2020-10-14 07:30:30', '', 'Y', '2020-10-14 14:54:45', 'Kekurangan surat Bebas Narkoba'),
(89, 45, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_5208013112980033_2020_10_13_13_43_23.jpg', '.jpg', 'Y', '2020-10-14 07:21:34', '', 'Y', '2020-10-14 07:30:30', '', 'Y', '2020-10-14 14:54:45', 'Kekurangan surat Bebas Narkoba'),
(90, 45, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_5208013112980033_2020_10_13_13_43_23.pdf', '.pdf', 'Y', '2020-10-14 07:21:34', '', 'Y', '2020-10-14 07:30:30', '', 'Y', '2020-10-14 14:54:45', 'Kekurangan surat Bebas Narkoba'),
(91, 45, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_5208013112980033_2020_10_13_13_43_23.pdf', '.pdf', 'Y', '2020-10-14 07:21:34', '', 'Y', '2020-10-14 07:30:30', '', 'Y', '2020-10-14 14:54:45', 'Kekurangan surat Bebas Narkoba'),
(92, 45, 25, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_25_5208013112980033_2020_10_13_13_43_23.pdf', '.pdf', 'Y', '2020-10-14 07:21:34', '', 'Y', '2020-10-14 07:30:30', '', 'Y', '2020-10-14 14:54:45', 'Kekurangan surat Bebas Narkoba'),
(93, 46, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_9106010501750002_2020_12_05_08_54_46.pdf', '.pdf', 'Y', '2020-10-14 07:20:47', 'ok', 'Y', '2020-10-14 07:29:59', '', 'N', '2020-10-14 14:54:02', 'Surat Bebas  Narkoba belum ada.'),
(94, 46, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_9106010501750002_2020_12_05_08_54_46.jpg', '.pdf', 'Y', '2020-10-14 07:20:47', 'ok', 'Y', '2020-10-14 07:29:59', '', 'N', '2020-10-14 14:54:02', 'Surat Bebas  Narkoba belum ada.'),
(95, 46, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_9106010501750002_2020_12_05_08_54_46.pdf', '.pdf', 'Y', '2020-10-14 07:20:47', 'ok', 'Y', '2020-10-14 07:29:59', '', 'N', '2020-10-14 14:54:02', 'Surat Bebas  Narkoba belum ada.'),
(96, 46, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_9106010501750002_2020_12_05_08_54_46.pdf', '.pdf', 'Y', '2020-10-14 07:20:47', 'ok', 'Y', '2020-10-14 07:29:59', '', 'N', '2020-10-14 14:54:02', 'Surat Bebas  Narkoba belum ada.'),
(97, 46, 25, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_25_9106010501750002_2020_12_05_08_54_46.pdf', '.pdf', 'Y', '2020-10-14 07:20:47', 'belum melampirkan', 'Y', '2020-10-14 07:29:59', '', 'N', '2020-10-14 14:54:02', 'Surat Bebas  Narkoba belum ada.'),
(98, 46, 29, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_29_9106010501750002_2020_12_05_08_54_46.pdf', NULL, NULL, NULL, 'belum melampirkan', NULL, NULL, NULL, NULL, NULL, NULL),
(99, 44, 29, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_29_9106010501750002_2020_12_05_08_49_17.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 43, 29, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_29_9106010501750002_2020_12_08_09_31_26.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 47, 19, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_19_9106010501750002_2020_12_08_07_25_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 47, 20, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_20_9106010501750002_2020_12_08_07_25_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 47, 21, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_21_9106010501750002_2020_12_08_07_25_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 47, 22, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_22_9106010501750002_2020_12_08_07_25_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 47, 23, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_23_9106010501750002_2020_12_08_07_25_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 47, 27, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_27_9106010501750002_2020_12_08_07_25_32.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 48, 1, 'DOKUMEN_PERPANJANGAN_STNK_1_DS_9554_C_2020_10_30_11_12_27.pdf', '.pdf', 'Y', '2020-10-30 12:02:10', '', NULL, NULL, NULL, NULL, NULL, NULL),
(108, 49, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_33113_2020_11_16_23_25_05.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 49, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_33113_2020_11_16_23_25_05.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 49, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_33113_2020_11_16_23_25_05.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 49, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_33113_2020_11_16_23_25_05.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 49, 25, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_25_33113_2020_11_16_23_25_05.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 49, 29, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_29_33113_2020_11_16_23_25_05.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 50, 10, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_10_33113_2020_11_16_23_27_17.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 50, 11, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_11_33113_2020_11_16_23_27_17.jpg', '.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 50, 12, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_12_33113_2020_11_16_23_27_17.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 50, 13, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_13_33113_2020_11_16_23_27_17.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 50, 25, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_25_33113_2020_11_16_23_27_17.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 50, 29, 'DOKUMEN_PENGAJUAN_BARU_ID_CARD_29_33113_2020_11_16_23_27_17.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 51, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_S_8621_UN_2020_12_08_09_51_23.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 52, 1, 'DOKUMEN_PERPANJANGAN_STNK_1_PA_9554_C_2020_11_17_19_58_06.pdf', '.pdf', 'Y', '2020-11-17 19:58:51', '', 'Y', '2020-11-17 20:00:07', 'sesuai', NULL, NULL, NULL),
(122, 53, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_DS_9134_C_2020_11_17_20_27_13.pdf', '.pdf', 'Y', '2020-11-17 20:34:34', '', 'Y', '2020-11-17 20:38:36', 'sesuai', NULL, NULL, NULL),
(123, 54, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_PA_9103_U_2020_11_17_20_28_58.pdf', '.pdf', 'Y', '2020-11-17 20:35:01', '', 'Y', '2020-11-17 20:38:21', 'sesuai', NULL, NULL, NULL),
(124, 55, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9103_U_2020_11_17_20_30_05.pdf', '.pdf', 'Y', '2020-11-17 20:35:23', '', 'Y', '2020-11-17 20:38:06', 'sesuai', NULL, NULL, NULL),
(125, 56, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8__PA_9101_U_2020_11_17_20_31_25.pdf', '.pdf', 'Y', '2020-11-17 20:35:46', '', 'Y', '2020-11-17 20:37:52', 'sesuai', NULL, NULL, NULL),
(126, 57, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9__PA_9101_U_2020_11_17_20_32_33.pdf', '.pdf', 'Y', '2020-11-17 20:36:14', '', 'Y', '2020-11-17 20:37:39', 'sesuai', NULL, NULL, NULL),
(127, 58, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_PA_9004_MI_2020_11_17_20_33_49.pdf', '.pdf', 'Y', '2020-11-17 20:36:30', '', 'Y', '2020-11-17 20:37:20', 'sesuai', NULL, NULL, NULL),
(128, 59, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8__PA_9101_U_2020_11_17_20_40_34.pdf', '.pdf', 'Y', '2020-11-17 20:40:58', '', 'Y', '2020-11-17 20:41:28', 'sesuai', NULL, NULL, NULL),
(129, 60, 2, 'DOKUMEN_PERPANJANGAN_SIM_2_9106010501750002_2020_11_17_20_44_13.pdf', '.pdf', 'Y', '2020-11-17 20:46:04', '', 'Y', '2020-11-17 20:47:36', 'sesuai', NULL, NULL, NULL),
(130, 61, 2, 'DOKUMEN_PERPANJANGAN_SIM_2_9106010501750002_2020_11_17_20_45_09.pdf', '.pdf', 'Y', '2020-11-17 20:46:25', '', 'Y', '2020-11-17 20:47:49', 'sesuai', NULL, NULL, NULL),
(131, 62, 9, 'DOKUMEN_PERPANJANGAN_METROLOGI_TERA_9_L_9218_AQ_2020_11_18_07_49_09.pdf', '.pdf', 'Y', '2021-01-01 21:10:49', '', 'Y', '2021-01-01 21:18:19', 'Proses', NULL, NULL, NULL),
(132, 63, 19, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_19_9106010501750002_2020_11_21_08_37_24.pdf', '.pdf', 'Y', '2021-01-04 07:49:21', '', NULL, NULL, NULL, NULL, NULL, NULL),
(133, 63, 20, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_20_9106010501750002_2020_11_21_08_37_24.jpg', '.jpg', 'Y', '2021-01-04 07:49:21', '', NULL, NULL, NULL, NULL, NULL, NULL),
(134, 63, 21, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_21_9106010501750002_2020_11_21_08_37_24.pdf', '.pdf', 'Y', '2021-01-04 07:49:21', '', NULL, NULL, NULL, NULL, NULL, NULL),
(135, 63, 22, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_22_9106010501750002_2020_11_21_08_37_24.pdf', '.pdf', 'Y', '2021-01-04 07:49:21', '', NULL, NULL, NULL, NULL, NULL, NULL),
(136, 63, 23, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_23_9106010501750002_2020_11_21_08_37_24.pdf', '.pdf', 'Y', '2021-01-04 07:49:21', '', NULL, NULL, NULL, NULL, NULL, NULL),
(137, 63, 27, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_27_9106010501750002_2020_11_21_08_37_24.pdf', '.pdf', 'Y', '2021-01-04 07:49:21', '', NULL, NULL, NULL, NULL, NULL, NULL),
(138, 64, 19, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_19_9106010501750002_2020_12_05_08_24_49.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 64, 20, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_20_9106010501750002_2020_12_05_08_24_49.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, 64, 21, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_21_9106010501750002_2020_12_05_08_24_49.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, 64, 22, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_22_9106010501750002_2020_12_05_08_24_49.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, 64, 23, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_23_9106010501750002_2020_12_05_08_24_49.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, 64, 27, 'DOKUMEN_PENGGANTIAN_AWAK_MOBIL_TANGKI_27_9106010501750002_2020_12_05_08_24_49.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(144, 65, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_18_09_15_04.pdf', '.pdf', 'Y', '2021-01-04 07:45:18', '', NULL, NULL, NULL, NULL, NULL, NULL),
(145, 65, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_18_09_15_04.jpeg', '.jpeg', 'Y', '2021-01-04 07:45:18', 'format tidak terbaca', NULL, NULL, NULL, NULL, NULL, NULL),
(146, 65, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_18_09_15_04.pdf', '.pdf', 'Y', '2021-01-04 07:45:18', '', NULL, NULL, NULL, NULL, NULL, NULL),
(147, 65, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_11_22_21.pdf', '.pdf', 'Y', '2021-01-04 07:45:18', '', NULL, NULL, NULL, NULL, NULL, NULL),
(148, 65, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_08_07_26_33.pdf', '.pdf', 'Y', '2021-01-04 07:45:18', 'Lampirkan ID Card Lama', NULL, NULL, NULL, NULL, NULL, NULL),
(149, 65, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_15_10_07_55.pdf', '.pdf', 'Y', '2021-01-04 07:45:18', 'Masa berlaku SKCK sudah tidak berlaku (2003), tolong di perbaharui kembali', NULL, NULL, NULL, NULL, NULL, NULL),
(150, 65, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_08_07_26_33.pdf', '.pdf', 'Y', '2021-01-04 07:45:18', '', NULL, NULL, NULL, NULL, NULL, NULL),
(151, 66, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_11_27_00.pdf', '.pdf', 'Y', '2020-12-06 06:52:21', '', 'Y', '2020-12-10 15:05:02', 'sesuai', 'Y', '2020-12-23 12:33:48', 'Silahkan dicetak'),
(152, 66, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_11_27_00.jpeg', '.jpeg', 'Y', '2020-12-06 06:52:21', '', 'Y', '2020-12-10 15:05:02', 'sesuai', 'Y', '2020-12-23 12:33:48', 'Silahkan dicetak'),
(153, 66, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_11_27_00.pdf', '.pdf', 'Y', '2020-12-06 06:52:21', '', 'Y', '2020-12-10 15:05:02', 'sesuai', 'Y', '2020-12-23 12:33:48', 'Silahkan dicetak'),
(154, 66, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_11_27_00.pdf', '.pdf', 'Y', '2020-12-06 06:52:21', '', 'Y', '2020-12-10 15:05:02', 'sesuai', 'Y', '2020-12-23 12:33:48', 'Silahkan dicetak'),
(155, 66, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_11_27_00.pdf', '.pdf', 'Y', '2020-12-06 06:52:21', 'Lampirkan ID Card Lama', 'Y', '2020-12-10 15:05:02', 'sesuai', 'Y', '2020-12-23 12:33:48', 'Silahkan dicetak'),
(156, 66, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_11_27_00.pdf', '.pdf', 'Y', '2020-12-06 06:52:21', '', 'Y', '2020-12-10 15:05:02', 'sesuai', 'Y', '2020-12-23 12:33:48', 'Silahkan dicetak'),
(157, 66, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_11_27_00.pdf', '.pdf', 'Y', '2020-12-06 06:52:21', '', 'Y', '2020-12-10 15:05:02', 'sesuai', 'Y', '2020-12-23 12:33:48', 'Silahkan dicetak'),
(158, 67, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_11_32_46.pdf', '.pdf', 'Y', '2020-12-06 06:54:23', '', 'Y', '2020-12-10 15:05:29', 'sesuai', 'Y', '2020-12-23 12:33:27', 'Silahkan dicetak'),
(159, 67, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_11_32_46.jpeg', '.jpeg', 'Y', '2020-12-06 06:54:23', '', 'Y', '2020-12-10 15:05:29', 'sesuai', 'Y', '2020-12-23 12:33:27', 'Silahkan dicetak'),
(160, 67, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_11_32_46.pdf', '.pdf', 'Y', '2020-12-06 06:54:23', '', 'Y', '2020-12-10 15:05:29', 'sesuai', 'Y', '2020-12-23 12:33:27', 'Silahkan dicetak'),
(161, 67, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_11_32_46.pdf', '.pdf', 'Y', '2020-12-06 06:54:23', '', 'Y', '2020-12-10 15:05:29', 'sesuai', 'Y', '2020-12-23 12:33:27', 'Silahkan dicetak'),
(162, 67, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_11_32_46.pdf', '.pdf', 'Y', '2020-12-06 06:54:23', '', 'Y', '2020-12-10 15:05:29', 'sesuai', 'Y', '2020-12-23 12:33:27', 'Silahkan dicetak'),
(163, 67, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_11_32_46.pdf', '.pdf', 'Y', '2020-12-06 06:54:23', '', 'Y', '2020-12-10 15:05:29', 'sesuai', 'Y', '2020-12-23 12:33:27', 'Silahkan dicetak'),
(164, 67, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_11_32_46.pdf', '.pdf', 'Y', '2020-12-06 06:54:23', '', 'Y', '2020-12-10 15:05:29', 'sesuai', 'Y', '2020-12-23 12:33:27', 'Silahkan dicetak'),
(165, 68, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_11_37_27.pdf', '.pdf', 'Y', '2020-12-06 06:59:15', '', 'Y', '2020-12-10 15:06:02', 'sesuai', 'Y', '2020-12-23 12:33:09', 'Silahkan dicetak'),
(166, 68, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_11_37_27.jpeg', '.jpeg', 'Y', '2020-12-06 06:59:15', '', 'Y', '2020-12-10 15:06:02', 'sesuai', 'Y', '2020-12-23 12:33:09', 'Silahkan dicetak'),
(167, 68, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_11_37_27.pdf', '.pdf', 'Y', '2020-12-06 06:59:15', '', 'Y', '2020-12-10 15:06:02', 'sesuai', 'Y', '2020-12-23 12:33:09', 'Silahkan dicetak'),
(168, 68, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_11_37_27.pdf', '.pdf', 'Y', '2020-12-06 06:59:15', '', 'Y', '2020-12-10 15:06:02', 'sesuai', 'Y', '2020-12-23 12:33:09', 'Silahkan dicetak'),
(169, 68, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_11_37_27.pdf', '.pdf', 'Y', '2020-12-06 06:59:15', '', 'Y', '2020-12-10 15:06:02', 'sesuai', 'Y', '2020-12-23 12:33:09', 'Silahkan dicetak'),
(170, 68, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_11_37_27.pdf', '.pdf', 'Y', '2020-12-06 06:59:15', '', 'Y', '2020-12-10 15:06:02', 'sesuai', 'Y', '2020-12-23 12:33:09', 'Silahkan dicetak'),
(171, 68, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_11_37_27.pdf', '.pdf', 'Y', '2020-12-06 06:59:15', '', 'Y', '2020-12-10 15:06:02', 'sesuai', 'Y', '2020-12-23 12:33:09', 'Silahkan dicetak'),
(172, 69, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_11_43_43.pdf', '.pdf', 'Y', '2020-12-18 08:33:11', '', 'Y', '2020-12-18 08:38:38', 'sesuai', 'Y', '2020-12-23 12:32:27', 'Silahkan dicetak'),
(173, 69, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_11_43_43.jpeg', '.jpeg', 'Y', '2020-12-18 08:33:11', '', 'Y', '2020-12-18 08:38:38', 'sesuai', 'Y', '2020-12-23 12:32:27', 'Silahkan dicetak'),
(174, 69, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_11_43_43.pdf', '.pdf', 'Y', '2020-12-18 08:33:11', '', 'Y', '2020-12-18 08:38:38', 'sesuai', 'Y', '2020-12-23 12:32:27', 'Silahkan dicetak'),
(175, 69, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_11_43_43.pdf', '.pdf', 'Y', '2020-12-18 08:33:11', '', 'Y', '2020-12-18 08:38:38', 'sesuai', 'Y', '2020-12-23 12:32:27', 'Silahkan dicetak'),
(176, 69, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_11_43_43.pdf', '.pdf', 'Y', '2020-12-18 08:33:11', '', 'Y', '2020-12-18 08:38:38', 'sesuai', 'Y', '2020-12-23 12:32:27', 'Silahkan dicetak'),
(177, 69, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_11_43_43.pdf', '.pdf', 'Y', '2020-12-18 08:33:11', '', 'Y', '2020-12-18 08:38:38', 'sesuai', 'Y', '2020-12-23 12:32:27', 'Silahkan dicetak'),
(178, 69, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_11_43_43.pdf', '.pdf', 'Y', '2020-12-18 08:33:11', '', 'Y', '2020-12-18 08:38:38', 'sesuai', 'Y', '2020-12-23 12:32:27', 'Silahkan dicetak'),
(179, 70, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_11_53_30.pdf', '.pdf', 'Y', '2020-12-06 07:02:44', '', 'Y', '2020-12-10 15:08:11', 'sesuai', 'Y', '2020-12-23 12:32:00', 'Silahkan dicetak'),
(180, 70, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_11_53_30.jpeg', '.jpeg', 'Y', '2020-12-06 07:02:44', '', 'Y', '2020-12-10 15:08:11', 'sesuai', 'Y', '2020-12-23 12:32:00', 'Silahkan dicetak'),
(181, 70, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_11_53_30.pdf', '.pdf', 'Y', '2020-12-06 07:02:44', '', 'Y', '2020-12-10 15:08:11', 'sesuai', 'Y', '2020-12-23 12:32:00', 'Silahkan dicetak'),
(182, 70, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_11_53_30.pdf', '.pdf', 'Y', '2020-12-06 07:02:44', '', 'Y', '2020-12-10 15:08:11', 'sesuai', 'Y', '2020-12-23 12:32:00', 'Silahkan dicetak'),
(183, 70, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_11_53_30.pdf', '.pdf', 'Y', '2020-12-06 07:02:44', '', 'Y', '2020-12-10 15:08:11', 'sesuai', 'Y', '2020-12-23 12:32:00', 'Silahkan dicetak'),
(184, 70, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_11_53_30.pdf', '.pdf', 'Y', '2020-12-06 07:02:44', '', 'Y', '2020-12-10 15:08:11', 'sesuai', 'Y', '2020-12-23 12:32:00', 'Silahkan dicetak'),
(185, 70, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_11_53_30.pdf', '.pdf', 'Y', '2020-12-06 07:02:44', '', 'Y', '2020-12-10 15:08:11', 'sesuai', 'Y', '2020-12-23 12:32:00', 'Silahkan dicetak'),
(186, 71, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_11_58_38.pdf', '.pdf', 'Y', '2020-12-06 07:04:46', '', 'Y', '2020-12-10 15:09:05', 'sesuai', 'Y', '2020-12-23 12:31:43', 'Silahkan dicetak'),
(187, 71, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_11_58_38.jpeg', '.jpeg', 'Y', '2020-12-06 07:04:46', '', 'Y', '2020-12-10 15:09:05', 'sesuai', 'Y', '2020-12-23 12:31:43', 'Silahkan dicetak'),
(188, 71, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_11_58_38.pdf', '.pdf', 'Y', '2020-12-06 07:04:46', '', 'Y', '2020-12-10 15:09:05', 'sesuai', 'Y', '2020-12-23 12:31:43', 'Silahkan dicetak'),
(189, 71, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_11_58_38.pdf', '.pdf', 'Y', '2020-12-06 07:04:46', '', 'Y', '2020-12-10 15:09:05', 'sesuai', 'Y', '2020-12-23 12:31:43', 'Silahkan dicetak'),
(190, 71, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_11_58_38.pdf', '.pdf', 'Y', '2020-12-06 07:04:46', '', 'Y', '2020-12-10 15:09:05', 'sesuai', 'Y', '2020-12-23 12:31:43', 'Silahkan dicetak'),
(191, 71, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_11_58_38.pdf', '.pdf', 'Y', '2020-12-06 07:04:46', '', 'Y', '2020-12-10 15:09:05', 'sesuai', 'Y', '2020-12-23 12:31:43', 'Silahkan dicetak'),
(192, 71, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_11_58_38.pdf', '.pdf', 'Y', '2020-12-06 07:04:46', '', 'Y', '2020-12-10 15:09:05', 'sesuai', 'Y', '2020-12-23 12:31:43', 'Silahkan dicetak'),
(193, 72, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_12_03_45.pdf', '.pdf', 'Y', '2020-12-18 08:31:41', '', 'Y', '2020-12-18 08:38:06', 'sesuai', 'Y', '2020-12-23 12:31:22', 'Makasih'),
(194, 72, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_12_03_45.jpeg', '.jpeg', 'Y', '2020-12-18 08:31:41', '', 'Y', '2020-12-18 08:38:06', 'sesuai', 'Y', '2020-12-23 12:31:22', 'Makasih'),
(195, 72, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_12_03_45.pdf', '.pdf', 'Y', '2020-12-18 08:31:41', '', 'Y', '2020-12-18 08:38:06', 'sesuai', 'Y', '2020-12-23 12:31:22', 'Makasih'),
(196, 72, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_12_03_45.pdf', '.pdf', 'Y', '2020-12-18 08:31:41', '', 'Y', '2020-12-18 08:38:06', 'sesuai', 'Y', '2020-12-23 12:31:22', 'Makasih'),
(197, 72, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_08_07_22_29.pdf', '.pdf', 'Y', '2020-12-18 08:31:41', '', 'Y', '2020-12-18 08:38:06', 'sesuai', 'Y', '2020-12-23 12:31:22', 'Makasih'),
(198, 72, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_14_07_05_04.pdf', '.pdf', 'Y', '2020-12-18 08:31:41', 'Masa berlaku SKCK sudah habis (2019)', 'Y', '2020-12-18 08:38:06', 'sesuai', 'Y', '2020-12-23 12:31:22', 'Makasih'),
(199, 72, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_08_07_22_29.pdf', '.pdf', 'Y', '2020-12-18 08:31:41', '', 'Y', '2020-12-18 08:38:06', 'sesuai', 'Y', '2020-12-23 12:31:22', 'Makasih'),
(200, 73, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_12_08_16.pdf', '.pdf', 'Y', '2020-12-06 07:43:22', '', 'Y', '2020-12-10 15:09:29', 'sesuai', 'Y', '2020-12-23 12:31:05', 'Makasih'),
(201, 73, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_12_08_16.jpg', '.jpg', 'Y', '2020-12-06 07:43:22', '', 'Y', '2020-12-10 15:09:29', 'sesuai', 'Y', '2020-12-23 12:31:05', 'Makasih'),
(202, 73, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_12_08_16.pdf', '.pdf', 'Y', '2020-12-06 07:43:22', '', 'Y', '2020-12-10 15:09:29', 'sesuai', 'Y', '2020-12-23 12:31:05', 'Makasih'),
(203, 73, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_12_08_16.pdf', '.pdf', 'Y', '2020-12-06 07:43:22', '', 'Y', '2020-12-10 15:09:29', 'sesuai', 'Y', '2020-12-23 12:31:05', 'Makasih'),
(204, 73, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_12_08_16.pdf', '.pdf', 'Y', '2020-12-06 07:43:22', '', 'Y', '2020-12-10 15:09:29', 'sesuai', 'Y', '2020-12-23 12:31:05', 'Makasih'),
(205, 73, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_12_08_16.pdf', '.pdf', 'Y', '2020-12-06 07:43:22', '', 'Y', '2020-12-10 15:09:29', 'sesuai', 'Y', '2020-12-23 12:31:05', 'Makasih'),
(206, 73, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_12_08_16.pdf', '.pdf', 'Y', '2020-12-06 07:43:22', '', 'Y', '2020-12-10 15:09:29', 'sesuai', 'Y', '2020-12-23 12:31:05', 'Makasih'),
(207, 74, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_05_12_12_00.pdf', '.pdf', 'Y', '2020-12-06 07:44:58', '', 'Y', '2020-12-18 08:37:24', 'sesuai', 'Y', '2020-12-23 12:30:41', ''),
(208, 74, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_05_12_12_00.jpg', '.jpg', 'Y', '2020-12-06 07:44:58', '', 'Y', '2020-12-18 08:37:24', 'sesuai', 'Y', '2020-12-23 12:30:41', ''),
(209, 74, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_05_12_12_00.pdf', '.pdf', 'Y', '2020-12-06 07:44:58', '', 'Y', '2020-12-18 08:37:24', 'sesuai', 'Y', '2020-12-23 12:30:41', ''),
(210, 74, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_05_12_12_00.pdf', '.pdf', 'Y', '2020-12-06 07:44:58', '', 'Y', '2020-12-18 08:37:24', 'sesuai', 'Y', '2020-12-23 12:30:41', ''),
(211, 74, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_05_12_12_00.pdf', '.pdf', 'Y', '2020-12-06 07:44:58', '', 'Y', '2020-12-18 08:37:24', 'sesuai', 'Y', '2020-12-23 12:30:41', ''),
(212, 74, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_05_12_12_00.pdf', '.pdf', 'Y', '2020-12-06 07:44:58', '', 'Y', '2020-12-18 08:37:24', 'sesuai', 'Y', '2020-12-23 12:30:41', ''),
(213, 74, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_05_12_12_00.pdf', '.pdf', 'Y', '2020-12-06 07:44:58', '', 'Y', '2020-12-18 08:37:24', 'sesuai', 'Y', '2020-12-23 12:30:41', ''),
(214, 75, 8, 'DOKUMEN_PERPANJANGAN_KEUR___KIR_8_L_9218_AQ_2020_12_09_19_12_05.jpeg', '.jpeg', 'Y', '2021-01-01 21:11:22', '', 'Y', '2021-01-01 21:18:07', 'Proses', NULL, NULL, NULL),
(215, 76, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_29_08_23_27.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(216, 76, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_29_08_23_27.jpeg', '.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(217, 76, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_29_08_23_27.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(218, 76, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_29_08_23_27.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(219, 76, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_29_08_23_27.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(220, 76, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_29_08_23_27.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(221, 76, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_29_08_23_27.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(222, 77, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_29_08_27_04.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(223, 77, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_29_08_27_04.jpeg', '.jpeg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(224, 77, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_29_08_27_04.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(225, 77, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_29_08_27_04.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(226, 77, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_29_08_27_04.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(227, 77, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_29_08_27_04.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(228, 77, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_29_08_27_04.pdf', '.pdf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(229, 78, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_29_08_32_30.pdf', '.pdf', 'Y', '2021-01-01 13:33:10', '', 'Y', '2021-01-01 21:19:40', 'Proses', 'Y', '2021-01-01 21:21:49', 'Sesuai'),
(230, 78, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_29_08_32_30.jpeg', '.jpeg', 'Y', '2021-01-01 13:33:10', '', 'Y', '2021-01-01 21:19:40', 'Proses', 'Y', '2021-01-01 21:21:49', 'Sesuai'),
(231, 78, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_29_08_32_30.pdf', '.pdf', 'Y', '2021-01-01 13:33:10', '', 'Y', '2021-01-01 21:19:40', 'Proses', 'Y', '2021-01-01 21:21:49', 'Sesuai'),
(232, 78, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_29_08_32_30.pdf', '.pdf', 'Y', '2021-01-01 13:33:10', '', 'Y', '2021-01-01 21:19:40', 'Proses', 'Y', '2021-01-01 21:21:49', 'Sesuai'),
(233, 78, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_29_08_32_30.pdf', '.pdf', 'Y', '2021-01-01 13:33:10', '', 'Y', '2021-01-01 21:19:40', 'Proses', 'Y', '2021-01-01 21:21:49', 'Sesuai'),
(234, 78, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_29_08_32_30.pdf', '.pdf', 'Y', '2021-01-01 13:33:10', '', 'Y', '2021-01-01 21:19:40', 'Proses', 'Y', '2021-01-01 21:21:49', 'Sesuai'),
(235, 78, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_29_08_32_30.pdf', '.pdf', 'Y', '2021-01-01 13:33:10', '', 'Y', '2021-01-01 21:19:40', 'Proses', 'Y', '2021-01-01 21:21:49', 'Sesuai'),
(236, 79, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2020_12_29_08_34_36.pdf', '.pdf', 'Y', '2021-01-01 13:35:50', '', 'Y', '2021-01-01 21:20:07', 'Proses', 'Y', '2021-01-01 21:21:35', 'Sesuai'),
(237, 79, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2020_12_29_08_34_36.jpeg', '.jpeg', 'Y', '2021-01-01 13:35:50', '', 'Y', '2021-01-01 21:20:07', 'Proses', 'Y', '2021-01-01 21:21:35', 'Sesuai'),
(238, 79, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2020_12_29_08_34_36.pdf', '.pdf', 'Y', '2021-01-01 13:35:50', '', 'Y', '2021-01-01 21:20:07', 'Proses', 'Y', '2021-01-01 21:21:35', 'Sesuai'),
(239, 79, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2020_12_29_08_34_36.pdf', '.pdf', 'Y', '2021-01-01 13:35:50', '', 'Y', '2021-01-01 21:20:07', 'Proses', 'Y', '2021-01-01 21:21:35', 'Sesuai'),
(240, 79, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2020_12_29_08_34_36.pdf', '.pdf', 'Y', '2021-01-01 13:35:50', '', 'Y', '2021-01-01 21:20:07', 'Proses', 'Y', '2021-01-01 21:21:35', 'Sesuai'),
(241, 79, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2020_12_29_08_34_36.pdf', '.pdf', 'Y', '2021-01-01 13:35:50', '', 'Y', '2021-01-01 21:20:07', 'Proses', 'Y', '2021-01-01 21:21:35', 'Sesuai'),
(242, 79, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2020_12_29_08_34_36.pdf', '.pdf', 'Y', '2021-01-01 13:35:50', '', 'Y', '2021-01-01 21:20:07', 'Proses', 'Y', '2021-01-01 21:21:35', 'Sesuai'),
(243, 80, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2021_01_01_21_45_43.pdf', '.pdf', 'Y', '2021-01-04 07:55:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(244, 80, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2021_01_01_21_45_43.jpg', '.jpg', 'N', '2021-01-04 07:55:52', 'format foto salah', NULL, NULL, NULL, NULL, NULL, NULL),
(245, 80, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2021_01_01_21_45_43.pdf', '.pdf', 'Y', '2021-01-04 07:55:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(246, 80, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2021_01_01_21_45_43.pdf', '.pdf', 'Y', '2021-01-04 07:55:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(247, 80, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2021_01_01_21_45_43.pdf', '.pdf', 'Y', '2021-01-04 07:55:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(248, 80, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2021_01_01_21_45_43.pdf', '.pdf', 'Y', '2021-01-04 07:55:52', 'Lampirkan SKCK', NULL, NULL, NULL, NULL, NULL, NULL),
(249, 80, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2021_01_01_21_45_43.pdf', '.pdf', 'Y', '2021-01-04 07:55:52', '', NULL, NULL, NULL, NULL, NULL, NULL),
(250, 81, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2021_01_01_21_40_50.pdf', '.pdf', 'Y', '2021-01-04 07:55:12', '', NULL, NULL, NULL, NULL, NULL, NULL),
(251, 81, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2021_01_04_10_38_08.jpg', '.jpg', 'N', '2021-01-04 07:55:12', 'format foto salah', NULL, NULL, NULL, NULL, NULL, NULL),
(252, 81, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2021_01_01_21_40_50.pdf', '.pdf', 'Y', '2021-01-04 07:55:12', '', NULL, NULL, NULL, NULL, NULL, NULL),
(253, 81, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2021_01_01_21_40_50.pdf', '.pdf', 'Y', '2021-01-04 07:55:12', '', NULL, NULL, NULL, NULL, NULL, NULL),
(254, 81, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2021_01_01_21_40_50.pdf', '.pdf', 'Y', '2021-01-04 07:55:12', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tbl_t_dokumen_aktifitas` (`id_dokumen`, `id_aktifitas`, `id_syarat`, `dokumen_syarat`, `format_dokumen`, `approval_admin`, `tgl_approval_admin`, `note_approval_admin`, `approval_manager_lokasi`, `tgl_approval_manager_lokasi`, `note_approval_manager_lokasi`, `approval_manager_region`, `tgl_approval_manager_region`, `note_approval_manager_region`) VALUES
(255, 81, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2021_01_01_21_40_50.pdf', '.pdf', 'Y', '2021-01-04 07:55:12', 'Lampirkan SKCK', NULL, NULL, NULL, NULL, NULL, NULL),
(256, 81, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2021_01_01_21_40_50.pdf', '.pdf', 'Y', '2021-01-04 07:55:12', '', NULL, NULL, NULL, NULL, NULL, NULL),
(257, 82, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2021_01_01_21_35_31.pdf', '.pdf', 'Y', '2021-01-04 07:54:41', '', NULL, NULL, NULL, NULL, NULL, NULL),
(258, 82, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2021_01_04_10_35_58.jpg', '.jpg', 'N', '2021-01-04 07:54:41', 'format foto salah', NULL, NULL, NULL, NULL, NULL, NULL),
(259, 82, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2021_01_01_21_35_31.pdf', '.pdf', 'Y', '2021-01-04 07:54:41', '', NULL, NULL, NULL, NULL, NULL, NULL),
(260, 82, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2021_01_01_21_35_31.pdf', '.pdf', 'Y', '2021-01-04 07:54:41', '', NULL, NULL, NULL, NULL, NULL, NULL),
(261, 82, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2021_01_01_21_35_31.pdf', '.pdf', 'Y', '2021-01-04 07:54:41', '', NULL, NULL, NULL, NULL, NULL, NULL),
(262, 82, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2021_01_01_21_35_31.pdf', '.pdf', 'Y', '2021-01-04 07:54:41', 'Lampirkan SKCK', NULL, NULL, NULL, NULL, NULL, NULL),
(263, 82, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2021_01_01_21_35_32.pdf', '.pdf', 'Y', '2021-01-04 07:54:41', '', NULL, NULL, NULL, NULL, NULL, NULL),
(264, 83, 14, 'DOKUMEN_PERPANJANGAN_ID_CARD_14_9106010501750002_2021_01_01_21_27_40.pdf', '.pdf', 'Y', '2021-01-04 08:15:22', '', 'Y', '2021-01-04 08:16:00', 'sesuai', 'Y', '2021-01-04 08:16:33', 'sesuai'),
(265, 83, 15, 'DOKUMEN_PERPANJANGAN_ID_CARD_15_9106010501750002_2021_01_04_08_04_12.jpeg', '.jpg', 'Y', '2021-01-04 08:15:22', 'format foto salah', 'Y', '2021-01-04 08:16:00', 'sesuai', 'Y', '2021-01-04 08:16:33', 'sesuai'),
(266, 83, 16, 'DOKUMEN_PERPANJANGAN_ID_CARD_16_9106010501750002_2021_01_01_21_27_40.pdf', '.pdf', 'Y', '2021-01-04 08:15:22', '', 'Y', '2021-01-04 08:16:00', 'sesuai', 'Y', '2021-01-04 08:16:33', 'sesuai'),
(267, 83, 17, 'DOKUMEN_PERPANJANGAN_ID_CARD_17_9106010501750002_2021_01_01_21_27_40.pdf', '.pdf', 'Y', '2021-01-04 08:15:22', '', 'Y', '2021-01-04 08:16:00', 'sesuai', 'Y', '2021-01-04 08:16:33', 'sesuai'),
(268, 83, 24, 'DOKUMEN_PERPANJANGAN_ID_CARD_24_9106010501750002_2021_01_01_21_27_40.pdf', '.pdf', 'Y', '2021-01-04 08:15:22', '', 'Y', '2021-01-04 08:16:00', 'sesuai', 'Y', '2021-01-04 08:16:33', 'sesuai'),
(269, 83, 26, 'DOKUMEN_PERPANJANGAN_ID_CARD_26_9106010501750002_2021_01_01_21_27_40.pdf', '.pdf', 'Y', '2021-01-04 08:15:22', 'Lampirkan SKCK', 'Y', '2021-01-04 08:16:00', 'sesuai', 'Y', '2021-01-04 08:16:33', 'sesuai'),
(270, 83, 30, 'DOKUMEN_PERPANJANGAN_ID_CARD_30_9106010501750002_2021_01_01_21_27_40.pdf', '.pdf', 'Y', '2021-01-04 08:15:22', '', 'Y', '2021-01-04 08:16:00', 'sesuai', 'Y', '2021-01-04 08:16:33', 'sesuai');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_t_temuan`
--

CREATE TABLE `tbl_t_temuan` (
  `id_temuan` int(11) NOT NULL,
  `id_pemilik` int(11) DEFAULT NULL,
  `id_truck` int(11) DEFAULT NULL,
  `jenis_temuan` varchar(100) DEFAULT NULL,
  `deskripsi_temuan` text DEFAULT NULL,
  `tgl_temuan` date DEFAULT NULL,
  `tindak_lanjut` varchar(100) DEFAULT NULL,
  `tgl_tindaklanjut` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `foto_pertama` varchar(255) DEFAULT NULL,
  `foto_kedua` varchar(255) DEFAULT NULL,
  `foto_ketiga` varchar(255) DEFAULT NULL,
  `tindak_lanjut_pertama` varchar(255) DEFAULT NULL,
  `tindak_lanjut_kedua` varchar(255) DEFAULT NULL,
  `tindak_lanjut_ketiga` varchar(255) DEFAULT NULL,
  `komentar_tindak_lanjut` text DEFAULT NULL,
  `tgl_tindak_lanjut` date DEFAULT NULL,
  `status_tindak_lanjut` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_t_temuan`
--

INSERT INTO `tbl_t_temuan` (`id_temuan`, `id_pemilik`, `id_truck`, `jenis_temuan`, `deskripsi_temuan`, `tgl_temuan`, `tindak_lanjut`, `tgl_tindaklanjut`, `status`, `foto_pertama`, `foto_kedua`, `foto_ketiga`, `tindak_lanjut_pertama`, `tindak_lanjut_kedua`, `tindak_lanjut_ketiga`, `komentar_tindak_lanjut`, `tgl_tindak_lanjut`, `status_tindak_lanjut`) VALUES
(7, 54, 182, 'Ban Belakang Mobil Tangki Gundul ', 'Ban Belakang Mobil Tangki Gundul, berpotensi slip dan tergelincir jika kondisi jalan licin dan berbahaya jika membawa muatan jarak jauh. Mohon segera dilakukan penggantian ban belakang.', '2020-08-10', NULL, NULL, 'URGENT', 'WhatsApp_Image_2020-08-10_at_10_28_49.jpeg', 'WhatsApp_Image_2020-08-10_at_10_28_49_(1).jpeg', 'WhatsApp_Image_2020-08-10_at_10_28_09.jpeg', 'IMG-20200810-WA0013.jpg', 'IMG-20200810-WA0014.jpg', 'IMG-20200810-WA00131.jpg', 'Sudah Melakukan Perbaikan ', '2020-08-11', 'CLOSE'),
(9, 54, 178, 'jok kursi AMT tidak layak', 'Kondisi jok kursi Awak Mobil Tangki sudah tidak layak digunakan, mempengaruhi tingkat konsentrasi AMT saat berkendara.', '2020-08-10', NULL, NULL, 'MEDIUM', 'WhatsApp_Image_2020-08-10_at_13_41_53.jpeg', 'WhatsApp_Image_2020-08-10_at_13_41_54.jpeg', NULL, 'IMG-20200904-WA0013.jpg', 'IMG-20200904-WA0009.jpg', 'IMG-20200904-WA0008.jpg', 'Mohon ditindaklanjuti ', '2020-09-16', 'CLOSE'),
(10, 54, 189, 'Lampu samping rusak', 'Kondisi lampu rem dan ritting bagian samping Mobil Tangki sudah rusak, berpotensi bisa terjadi kecelakaan saat berkendara di jalan umum.', '2020-08-10', NULL, NULL, 'SELESAI', 'WhatsApp_Image_2020-08-10_at_13_58_15.jpeg', 'WhatsApp_Image_2020-08-10_at_13_58_17.jpeg', 'WhatsApp_Image_2020-08-10_at_13_58_16.jpeg', 'IMG-20200815-WA00071.jpg', 'IMG-20200815-WA00111.jpg', 'IMG-20200815-WA00101.jpg', 'Mohon ditindaklanjuti pak', '2020-08-18', 'CLOSE'),
(11, 55, 187, 'Tempat air radiotor rusak', 'Tempat air radiator sudah rusak dan tidak layak digunakan, berpotensi bisa membuat panas mesin berlebihan karena sirkulasi air pendingin kurang', '2020-08-12', NULL, NULL, 'MEDIUM', 'WhatsApp_Image_2020-08-12_at_09_29_09_(1).jpeg', 'WhatsApp_Image_2020-08-12_at_09_29_08.jpeg', 'WhatsApp_Image_2020-08-12_at_09_29_09.jpeg', NULL, NULL, NULL, NULL, NULL, 'OPEN'),
(12, 54, 178, 'Ban cadangan kondisi gundul', 'Kondisi ban cadangan mobil tangki sudah gundul parah (kelihatan benang ban dalam), dalam keadaan darurat ban bocor atau pecah tidak dapat digunakan.', '2020-08-21', NULL, NULL, 'URGENT', 'WhatsApp_Image_2020-08-18_at_10_44_02.jpeg', 'WhatsApp_Image_2020-08-18_at_10_44_03.jpeg', 'WhatsApp_Image_2020-08-18_at_10_44_02_(1).jpeg', 'IMG_20200827_163255.jpg', 'IMG_20200827_163157.jpg', 'IMG_20200827_1632551.jpg', 'Mohon di tindaklanjuti Ban cadangan', '2020-08-28', 'CLOSE'),
(13, 58, 172, 'Ban Gundul', 'Kondisi ban depan bagian kanan dan ban belakang bagian kanan tengah sudah gundul, berpotensi terjadi slip saat pengereman dan pada saat kondisi jalan licin.', '2020-08-21', NULL, NULL, 'SELESAI', 'WhatsApp_Image_2020-08-19_at_13_19_35_(1).jpeg', 'WhatsApp_Image_2020-08-19_at_13_19_35_(2).jpeg', 'WhatsApp_Image_2020-08-19_at_13_19_35.jpeg', 'IMG-20200831-WA0009.jpg', 'IMG-20200831-WA0010.jpg', 'IMG-20200831-WA0011.jpg', 'Mohon Di tindaklanjut Pak', '2020-08-31', 'CLOSE'),
(14, 52, 173, 'Cover Jok Mobil Rusak ', 'Cover Jok Rusak/Robek', '2020-08-29', NULL, NULL, 'SELESAI', 'WhatsApp_Image_2020-08-28_at_03_34_18_(1).jpeg', 'WhatsApp_Image_2020-08-28_at_03_34_18.jpeg', 'WhatsApp_Image_2020-08-28_at_03_34_181.jpeg', 'IMG-20201027-WA0046.jpg', 'IMG-20201027-WA0047.jpg', 'IMG-20201027-WA0048.jpg', 'Telah diperbaiki', '2020-10-28', 'CLOSE'),
(15, 54, 180, 'Cover Jok Mobil Rusak ', 'Cover Jok mobil rusak/robek', '2020-08-29', NULL, NULL, 'LOW', 'WhatsApp_Image_2020-08-28_at_03_34_20.jpeg', 'WhatsApp_Image_2020-08-28_at_03_34_19_(1).jpeg', 'WhatsApp_Image_2020-08-28_at_03_34_19_(1)1.jpeg', 'IMG-20200905-WA0009.jpg', 'IMG-20200905-WA00091.jpg', 'IMG-20200905-WA0008.jpg', 'Mohon ditindaklanjuti', '2020-09-16', 'CLOSE'),
(16, 54, 178, 'Cover Jok Mobil Rusak ', '1. Cover jok Mobil Rusak\r\n2. Kabin tidak rapi \r\n\r\nMohon perhatian rekan - rekan pengawas untuk selalu mengingatkan ke AMT nya untuk selalu menjaga kebersihan kabin Mobil Tangki.\r\nTerima Kasih \r\nSalam', '2020-09-02', NULL, NULL, 'LOW', 'WhatsApp_Image_2020-09-01_at_17_52_49.jpeg', 'WhatsApp_Image_2020-09-01_at_17_52_50.jpeg', 'WhatsApp_Image_2020-09-01_at_17_52_491.jpeg', 'IMG-20200904-WA00131.jpg', 'IMG-20200904-WA00091.jpg', 'IMG-20200904-WA00081.jpg', 'Mohon ditindaklanjuti', '2020-09-16', 'CLOSE'),
(18, 55, 187, 'lampu belakang - kiri putus', 'Lampu belakang belakang - kiri putus\r\nharap direspon demi kenyamanan & keamanan dalam bekerja.\r\n\r\nterima kasih', '2020-09-17', NULL, NULL, 'URGENT', 'WhatsApp_Image_2020-09-16_at_19_07_57_(1)1.jpeg', 'WhatsApp_Image_2020-09-16_at_19_07_572.jpeg', 'WhatsApp_Image_2020-09-16_at_19_07_573.jpeg', NULL, NULL, NULL, NULL, NULL, 'OPEN'),
(19, 54, 181, 'Penahan APAR rusak', 'Penahan APAR rusak (tidak ada pengancingnya / Baut)\r\nBerpotensi APAR jatuh\r\n\r\nMohon untuk merespon demi kenyamanan & keamanan kerja\r\nTerima Kasih', '2020-09-16', NULL, NULL, 'SELESAI', 'WhatsApp_Image_2020-09-13_at_16_40_35.jpeg', 'WhatsApp_Image_2020-09-13_at_16_39_54.jpeg', 'WhatsApp_Image_2020-09-13_at_16_39_47.jpeg', 'IMG-20200918-WA0023.jpg', 'IMG-20200918-WA00231.jpg', 'IMG-20200918-WA00232.jpg', 'Mohon ditindaklanjuti', '2020-09-18', 'CLOSE'),
(20, 59, 190, 'Stiker rambu bahaya sudah rusak', 'Stiker rambu bahaya bagian belakang mobil tangki sudah rusak, mohon segera melakukan penggantian stiker baru', '2020-11-04', NULL, NULL, 'MEDIUM', 'PA_9134_C_stiker_belakang.JPG', 'PA_9134_C_stiker_belakang1.JPG', 'PA_9134_C_stiker_belakang2.JPG', 'WhatsApp_Image_2020-12-29_at_09_28_262.jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_281.jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_34_(1).jpeg', 'telah ditindaklanjuti', '2020-12-29', 'CLOSE'),
(21, 54, 179, 'Tempat APAR patah', 'Tempat penahan APAR mobil tangki patah dan rusak, mohon segera diperbaiki', '2020-11-04', NULL, NULL, 'MEDIUM', 'PA_9650_C_Apar.JPG', 'PA_9650_C_Apar1.JPG', 'PA_9650_C_Apar2.JPG', 'IMG-20201105-WA00133.jpg', 'IMG-20201105-WA00134.jpg', 'IMG-20201105-WA00135.jpg', 'Mohon ditindaklanjuti', '2020-11-07', 'CLOSE'),
(22, 54, 182, 'Rambu Bendera Rusak', 'Rambu Bendera Mobil Tangki kondisinya sudah rusak, mohon segera dilakukan peerbaikan atau penggantian', '2020-11-04', NULL, NULL, 'LOW', 'PA9705C_bendera.JPG', 'PA9705C_bendera1.JPG', 'PA9705C_bendera2.JPG', 'IMG-20201107-WA0008.jpg', 'IMG-20201107-WA0009.jpg', 'IMG-20201107-WA0007.jpg', 'mohon ditindaklanjuti', '2020-11-09', 'CLOSE'),
(23, 54, 189, 'Ban Gundul / Botak', 'Kondisi ban mobil tangki depan dan belakang bagian dalam sudah aus atau gundul, mohon segera dilakukan penggantian', '2020-11-04', NULL, NULL, 'URGENT', 'dpn_new.JPG', 'ban_1_new.JPG', 'ban_3_new.JPG', 'Ban_S_9856_UQ_1.jpg', 'Ban_S_9856_UQ.jpg', 'BAn.jpg', 'mohon ditindaklanjuti', '2020-11-11', 'CLOSE'),
(24, 54, 189, 'Pelindung Panas Mesin', 'Pada mobil tangki tidak terdapat pelindung panas mesin, mohon segera dipasang pelindung mesin', '2020-11-04', NULL, NULL, 'MEDIUM', 'mesin_2_new.JPG', 'mesin_1_new.JPG', 'dpn_new1.JPG', 'IMG-20201205-WA0019.jpg', 'IMG-20201205-WA0017.jpg', 'IMG-20201205-WA0018.jpg', 'mohon ditindaklanjuti', '2020-12-05', 'CLOSE'),
(25, 54, 179, 'APAR Rusak', 'Kondisi APAR belakang mobil tangki kondisi rusak, segera lakukan perbaikan dan penggantian', '2020-11-04', NULL, NULL, 'URGENT', 'depan.JPG', 'apar.JPG', 'apar1.JPG', 'IMG-20201105-WA0013.jpg', 'IMG-20201105-WA00131.jpg', 'IMG-20201105-WA00132.jpg', 'Mohon ditindaklanjuti', '2020-11-07', 'CLOSE'),
(26, 54, 179, 'Kabel grounding putus', 'Kondisi kabel grounding pada mobil tangki putus dan rusak, mohon segera diperbaiki', '2020-11-04', NULL, NULL, 'MEDIUM', 'depan1.JPG', 'bounding_putus.JPG', 'bounding_putus1.JPG', 'IMG-20201105-WA0014.jpg', 'IMG-20201105-WA00141.jpg', 'IMG-20201105-WA00142.jpg', 'Mohon ditindaklanjuti', '2020-11-07', 'CLOSE'),
(27, 54, 179, 'Angka T2 tidak sesuai', 'Angka T2 pada mobil tangki tidak sesuai dengan angka T2 terbaru (sertifikat tera 2020), segera lakukan perbaikan', '2020-11-04', NULL, NULL, 'MEDIUM', 'depan2.JPG', 'T2_NEW.JPG', 'T2_NEW1.JPG', 'IMG-20201105-WA0019_(1).jpg', 'IMG-20201105-WA0019_(1)1.jpg', 'IMG-20201105-WA0019_(1)2.jpg', 'Mohon ditindaklanjuti', '2020-11-07', 'CLOSE'),
(28, 54, 178, 'APAR keropos', 'APAR pada mobil tangki keropos \r\nRekomendasi : Dibuatkan ceklist perawatan agar APAR selalu dalam keaadaan siap pakai.', '2020-11-05', NULL, NULL, 'MEDIUM', 'jsdfgh.JPG', 'jrfkug.JPG', 'jsdfgh1.JPG', 'Penahan_Apar_PA_9630.jpg', 'Penahan_Apar_PA_96301.jpg', 'Penahan_Apar_PA_96302.jpg', 'mohon ditindaklanjuti', '2020-11-11', 'CLOSE'),
(29, 54, 178, 'Lampu Rem Putus', 'Lampu Rem putus / tidak menyala\r\nMohon segera dilakukan perbaikan', '2020-11-05', NULL, NULL, 'MEDIUM', 'aasas.JPG', 'aasas1.JPG', 'aasas2.JPG', 'Lampu_Rem_PA_9630_1.jpg', 'Lampu_Rem_PA_9630_2.jpg', 'Lampu_Rem_PA_9630_11.jpg', 'mohon ditindaklanjuti', '2020-11-11', 'CLOSE'),
(30, 54, 178, 'Penutup Aki', 'Penutup Aki rusak \r\nMohon segera dilakukan perbaikan / penggantian pada penutup aki', '2020-11-05', NULL, NULL, 'MEDIUM', 'jujjj.JPG', 'jujjj1.JPG', 'jujjj2.JPG', 'Penutup_PA_9630_C.jpg', 'Penutup_PA_9630_C1.jpg', 'Penutup_PA_9630_C2.jpg', 'mohon ditindaklanjuti', '2020-11-11', 'CLOSE'),
(31, 54, 178, 'Bottom Loader Mobil Tangki', 'Bottom Loader pada Mobil Tangki Merembes\r\nDiharapkan untuk segera melakukan perbaikan pada Bottom Loader', '2020-11-05', NULL, NULL, 'URGENT', 'uuuuuu.JPG', 'uuuuuu1.JPG', 'uuuuuu2.JPG', 'Penutup_PA_9630.jpg', 'Penutup_PA_96301.jpg', 'Penutup_PA_96302.jpg', 'mohon ditindaklanjuti', '2020-11-11', 'CLOSE'),
(32, 57, 177, 'Bottom Loader', 'Nama produk yang tercantum dibottom loader tidak sesuai dengan nama produk yang diangkut', '2020-11-17', NULL, NULL, 'MEDIUM', 'Nama_Produk_Tidak_Sesuai_belum_diupdate.JPG', 'Nama_Produk_Tidak_Sesuai_belum_diupdate1.JPG', 'Nama_Produk_Tidak_Sesuai_belum_diupdate2.JPG', NULL, NULL, NULL, 'Sdh diset.', '2020-11-26', 'OPEN'),
(33, 57, 177, 'Sticker/keterangan T2', 'Sticker T2 pada leher tangki tidak sesuai/belum diupdate', '2020-11-17', NULL, NULL, 'MEDIUM', 'T2_tida_sesuai.JPG', 'T2_tida_sesuai1.JPG', 'T2_tida_sesuai2.JPG', NULL, NULL, NULL, 'Sdh direvisi.', '2020-11-26', 'OPEN'),
(34, 57, 177, 'APAR Berkarat', 'APAR Berkarat\r\nRekomendasi : Dibuatkan ceklist perawatan APAR', '2020-11-17', NULL, NULL, 'MEDIUM', 'APAR.JPG', 'APAR1.JPG', 'APAR2.JPG', NULL, NULL, NULL, 'Apar sdh diganti baru', '2020-11-25', 'OPEN'),
(35, 57, 177, 'Klakson tidak Standart', 'Klakson pada MT tidak Standart,harap dirubah menggunakan klakson standart yang telah ditentukan ', '2020-11-17', NULL, NULL, 'MEDIUM', 'klakson.JPG', 'klakson1.JPG', 'klakson2.JPG', NULL, NULL, NULL, 'Klakson angin sdh dilepas', '2020-11-25', 'OPEN'),
(36, 57, 177, 'Kabel Bounding Putus', 'Kabel Bounding pada MT putus/rusak', '2020-11-17', NULL, NULL, 'MEDIUM', 'Kabel_Bounding.JPG', 'Kabel_Bounding1.JPG', 'Kabel_Bounding2.JPG', NULL, NULL, NULL, 'Kabel Bounding sdh diperbaiki,jepitannya baru', '2020-11-25', 'OPEN'),
(37, 57, 177, 'Jok Mobil', 'Jok Mobil tangki rusak', '2020-11-17', NULL, NULL, 'MEDIUM', 'Jok.JPG', 'Jok1.JPG', 'Jok2.JPG', NULL, NULL, NULL, 'Sudah diperbaiki', '2020-11-25', 'OPEN'),
(38, 57, 177, 'Sticker batas kecepatan  ', 'Sticker batas kecepatan  & jaga jarak tidak ada\r\nMohon untuk memasang sticker yang dimaksud', '2020-11-17', NULL, NULL, 'MEDIUM', 'Sticker.JPG', 'Sticker1.JPG', 'Sticker2.JPG', NULL, NULL, NULL, 'Sudah dibuat stickernya', '2020-11-25', 'OPEN'),
(39, 59, 190, 'Roller Kabel Bounding Rusak', 'Roller Kabel Bounding Rusak.\r\nDiharapkan segera untuk melakukan perbaikan.', '2020-11-19', NULL, NULL, 'MEDIUM', 'Kabel_Bounding3.JPG', 'Kabel_Bounding4.JPG', 'Kabel_Bounding5.JPG', 'WhatsApp_Image_2020-12-29_at_09_28_29.jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_33.jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_331.jpeg', 'telah ditindaklanjuti', '2020-12-29', 'OPEN'),
(40, 59, 190, 'Lampu Rotari ', 'Lampu Rotari Rusak / Mati', '2020-11-19', NULL, NULL, 'MEDIUM', 'Lampu_Rotari_Rusak.JPG', 'Lampu_Rotari_Rusak1.JPG', 'Lampu_Rotari_Rusak2.JPG', 'WhatsApp_Image_2020-12-29_at_09_28_33_(1).jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_34.jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_341.jpeg', 'telah ditindaklanjuti', '2020-12-29', 'OPEN'),
(41, 59, 190, 'Lampu Rusak / Mati', 'Lampu pada Seluruh bagian belakang mobil,rusak / mati\r\ndiharapkan untuk segera melakukan perbaikan. ', '2020-11-19', NULL, NULL, 'URGENT', 'Lampu_Rusak_Mati.JPG', 'Lampu_Rusak_Mati1.JPG', 'Lampu_Rusak_Mati2.JPG', NULL, NULL, NULL, NULL, NULL, 'OPEN'),
(42, 59, 190, 'Sticker atau tanda peringatan', 'Sticker batas kecepatan Minimal / Maksimal tidak ada', '2020-11-19', NULL, NULL, 'MEDIUM', 'Sticker_Kecepatan_Min_Max.JPG', 'Sticker_Kecepatan_Min_Max1.JPG', 'Sticker_Kecepatan_Min_Max2.JPG', NULL, NULL, NULL, NULL, NULL, 'OPEN'),
(43, 59, 190, 'Sticker T2', 'Sticker T2 Belum Terpasang Sesuai dengan sertifikat TERA', '2020-11-19', NULL, NULL, 'MEDIUM', 'Sticker_T2_Belum_Terpasang.JPG', 'Sticker_T2_Belum_Terpasang1.JPG', 'Sticker_T2_Belum_Terpasang2.JPG', NULL, NULL, NULL, NULL, NULL, 'OPEN'),
(44, 59, 190, 'Sticker Tanda Peringatan ', 'Sticker Peringatan Bagian Belakang Rusak / Tidak jelas.\r\nHarap dilakuan pembaruan.', '2020-11-19', NULL, NULL, 'MEDIUM', 'Sticker_Jaga_Jarak_Buram.JPG', 'Sticker_Jaga_Jarak_Buram1.JPG', 'Sticker_Jaga_Jarak_Buram2.JPG', 'WhatsApp_Image_2020-12-29_at_09_28_26.jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_28.jpeg', 'WhatsApp_Image_2020-12-29_at_09_28_261.jpeg', 'telah ditindaklanjuti', '2020-12-29', 'CLOSE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_general_email`
--
ALTER TABLE `tbl_general_email`
  ADD PRIMARY KEY (`id_email`) USING BTREE;

--
-- Indexes for table `tbl_log_reminder`
--
ALTER TABLE `tbl_log_reminder`
  ADD PRIMARY KEY (`id_log`) USING BTREE;

--
-- Indexes for table `tbl_m_awak`
--
ALTER TABLE `tbl_m_awak`
  ADD PRIMARY KEY (`id_awak`) USING BTREE;

--
-- Indexes for table `tbl_m_keperluan`
--
ALTER TABLE `tbl_m_keperluan`
  ADD PRIMARY KEY (`id_keperluan`) USING BTREE;

--
-- Indexes for table `tbl_m_merek`
--
ALTER TABLE `tbl_m_merek`
  ADD PRIMARY KEY (`id_merek`) USING BTREE;

--
-- Indexes for table `tbl_m_pemilik`
--
ALTER TABLE `tbl_m_pemilik`
  ADD PRIMARY KEY (`id_pemilik`) USING BTREE;

--
-- Indexes for table `tbl_m_syarat_perpanjangan`
--
ALTER TABLE `tbl_m_syarat_perpanjangan`
  ADD PRIMARY KEY (`id_syarat`) USING BTREE;

--
-- Indexes for table `tbl_m_truck`
--
ALTER TABLE `tbl_m_truck`
  ADD PRIMARY KEY (`id_truck`) USING BTREE;

--
-- Indexes for table `tbl_m_user`
--
ALTER TABLE `tbl_m_user`
  ADD PRIMARY KEY (`id_user`) USING BTREE;

--
-- Indexes for table `tbl_t_aktifitas`
--
ALTER TABLE `tbl_t_aktifitas`
  ADD PRIMARY KEY (`id_aktifitas`) USING BTREE;

--
-- Indexes for table `tbl_t_dokumen_aktifitas`
--
ALTER TABLE `tbl_t_dokumen_aktifitas`
  ADD PRIMARY KEY (`id_dokumen`) USING BTREE;

--
-- Indexes for table `tbl_t_temuan`
--
ALTER TABLE `tbl_t_temuan`
  ADD PRIMARY KEY (`id_temuan`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_general_email`
--
ALTER TABLE `tbl_general_email`
  MODIFY `id_email` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_log_reminder`
--
ALTER TABLE `tbl_log_reminder`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `tbl_m_awak`
--
ALTER TABLE `tbl_m_awak`
  MODIFY `id_awak` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tbl_m_keperluan`
--
ALTER TABLE `tbl_m_keperluan`
  MODIFY `id_keperluan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_m_merek`
--
ALTER TABLE `tbl_m_merek`
  MODIFY `id_merek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_m_pemilik`
--
ALTER TABLE `tbl_m_pemilik`
  MODIFY `id_pemilik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tbl_m_syarat_perpanjangan`
--
ALTER TABLE `tbl_m_syarat_perpanjangan`
  MODIFY `id_syarat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_m_truck`
--
ALTER TABLE `tbl_m_truck`
  MODIFY `id_truck` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- AUTO_INCREMENT for table `tbl_m_user`
--
ALTER TABLE `tbl_m_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `tbl_t_aktifitas`
--
ALTER TABLE `tbl_t_aktifitas`
  MODIFY `id_aktifitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `tbl_t_dokumen_aktifitas`
--
ALTER TABLE `tbl_t_dokumen_aktifitas`
  MODIFY `id_dokumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `tbl_t_temuan`
--
ALTER TABLE `tbl_t_temuan`
  MODIFY `id_temuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
