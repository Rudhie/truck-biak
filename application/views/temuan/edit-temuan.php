<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Ubah Temuan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li>Temuan</li>
            <li class="active"><strong>Ubah Temuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                           
                            <form action="<?= base_url('temuan/action_edit/').$this->input->get("id"); ?>"  enctype="multipart/form-data" method="POST">
                                <div class="col-lg-12">
                                     <div class="form-group">
                                        <label>Transportir</label>
                                        <select name="id_pemilik" class="form-control m-b" id="pemilik">
                                            <?php
                                                echo '<option value="">Pilih Transportir</option>';
                                                foreach($pemilik as $p){
                                                    $selected = '';
                                                    if($p->id_pemilik == $temuan[0]->id_pemilik){
                                                        $selected = 'selected';
                                                    }
                                                    echo '<option value="'.$p->id_pemilik.'"'.$selected.'>'.$p->nama_perusahaan.'</option>';
                                                }
                                            ?>
                                        </select>
                                        </div>

                                    <div class="form-group">
                                        <label>No Polisi</label>
                                        <select name="id_truck" class="form-control m-b" id="nopol">
                                            <option value="">Pilih Nopol</option>
                                        </select>
                                        </div>
                                    <div class="form-group">
                                            <label>Jenis Temuan</label>
                                            <input type="text" class="form-control" name="jenis_temuan" value="<?php echo $temuan[0]->jenis_temuan ?>" required="">
                                        </div>
                                   
                                    <div class="form-group">
                                    <label>Deskripsi</label>
                                    <textarea class="form-control" name="deskripsi_temuan" rows="5"><?php echo rtrim($temuan[0]->deskripsi_temuan); ?></textarea>
                                        </div>
                                    
                                    <div class="form-group">
                                        <div class="form-group" id="data_1">
                                            <label>Tanggal Temuan</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="tgl_temuan" class="form-control" value="<?php echo date('d/m/Y', strtotime($temuan[0]->tgl_temuan));?>"required>
                                            </div>
                                        </div>
                                    </div>
                                   
                                   <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" id="selectStatus" name="status" >
                                                <option value="LOW"<?php if($temuan[0]->status=="LOW"){ echo "selected";} ?>>LOW</option>
                                                <option value="MEDIUM"<?php if($temuan[0]->status=="MEDIUM"){ echo "selected";}?>>MEDIUM</option>
                                                <option value="URGENT"<?php if($temuan[0]->status=="URGENT"){ echo "selected";} ?>>URGENT</option>
                                                <option value="SELESAI"<?php if($temuan[0]->status=="SELESAI"){ echo "selected";}?>>SELESAI</option>
                                            </select>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Status Tindak Lanjut</label>
                                            <select class="form-control m-b" id="selectStatusTindakLanjut" name="status_tindak_lanjut">
                                                <option>Pilih Status</option>
                                                <option value="OPEN" <?php if($temuan[0]->status_tindak_lanjut=="OPEN"){ echo "selected";} ?>>OPEN</option>
                                                <option value="ON PROGRESS" <?php if($temuan[0]->status_tindak_lanjut=="ON PROGRESS"){ echo "selected";} ?>>ON PROGRESS</option>
                                                <option value="CLOSE" <?php if($temuan[0]->status_tindak_lanjut=="CLOSE"){ echo "selected";} ?>>CLOSE</option>
                                            </select>
                                        </div>
                                        <div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kerusakan</label>
                                        <img id="prev_foto_pertama" src="<?= base_url($temuan[0]->foto_pertama != null ? 'dokumen/foto-temuan/'.$temuan[0]->foto_pertama : 'asset/img/noimage_336_290.jpg'); ?>" width="70%" />
                                        <input type="file" name="foto_pertama" id="foto_pertama" class="form-control">
                                        <small class="text-danger">(*File format JPG/JPEG/PNG, maksimal ukuran 2mb)</small>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kerusakan</label>
                                        <img id="prev_foto_kedua" src="<?= base_url($temuan[0]->foto_kedua != null ? 'dokumen/foto-temuan/'.$temuan[0]->foto_kedua : 'asset/img/noimage_336_290.jpg'); ?>" width="70%" />
                                        <input type="file" name="foto_kedua" id="foto_kedua" class="form-control">
                                        <small class="text-danger">(*File format JPG/JPEG/PNG, maksimal ukuran 2mb)</small>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kerusakan</label>
                                        <img id="prev_foto_ketiga" src="<?= base_url($temuan[0]->foto_ketiga != null ? 'dokumen/foto-temuan/'.$temuan[0]->foto_ketiga : 'asset/img/noimage_336_290.jpg'); ?>" width="70%" />
                                        <input type="file" name="foto_ketiga" id="foto_ketiga" class="form-control">
                                        <small class="text-danger">(*File format JPG/JPEG/PNG, maksimal ukuran 2mb)</small>
                                    </div>
                                </div>
                                    <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                        <a href="<?php echo base_url('temuan') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
         $("#pemilik").select2();
        $("#nopol").select2();
        $("#selectStatus, #selectStatusTindakLanjut").select2();
         
    

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'
        });
    });

    $("#foto_pertama").change(function() {
        readURL(this, "#prev_foto_pertama", "#foto_pertama");
    });

    $("#foto_kedua").change(function() {
        readURL(this, "#prev_foto_kedua", "#foto_kedua");
    });

    $("#foto_ketiga").change(function() {
        readURL(this, "#prev_foto_ketiga", "#foto_ketiga");
    });

    function readURL(input, id, form) {
        var typeFile = input.files[0].type;
        var size = Math.round(input.files[0].size / 1024);
        if(typeFile == "image/jpeg" || typeFile == "image/png"){
            if(size > 2048){
                swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");
                $(form).val('');
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $(id).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        } else {
            swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
            $(form).val('');
        }
        
    }
</script>
<script>
    $(document).ready(function(){
        var id_truck = "<?php echo $temuan[0]->id_truck?>";
        var id_pemilik = $("#pemilik").val();
            if(id_pemilik != ""){
                $.ajax({
                    url: "<?php echo base_url(); ?>temuan/nopol_pemilik",
                    method: "POST",
                    data: {id_pemilik:id_pemilik, id_truck:id_truck},
                    success: function(data){
                        $("#nopol").html(data);
                    }
                })
            }
        $("#pemilik").change(function(){
            var id_pemilik = $("#pemilik").val();
            if(id_pemilik != ""){
                $.ajax({
                    url: "<?php echo base_url(); ?>temuan/nopol_pemilik",
                    method: "POST",
                    data: {id_pemilik:id_pemilik, id_truck:id_truck},
                    success: function(data){
                        $("#nopol").html(data);
                    }
                })
            }
        });
    });
</script>