<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Temuan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li class="active"><strong>Temuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                 <?php if($this->session->userdata("level_user") == "ADMIN"){ ?>
                    <a href="<?php echo base_url('temuan/tambah'); ?>" class="btn btn-outline btn-success"><i class="fa fa-plus"></i> Tambah Temuan</a>
                        <?php } ?>
                </div>
                <div class="ibox-content">
                    <?php if($this->session->flashdata("success")){ ?>
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("success"));
                                unset($_SESSION["success"]);
                            ?>
                        </div>
                    <?php } else if($this->session->flashdata("error")) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("error"));
                                unset($_SESSION["error"]);
                            ?>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <form method="GET">
                        <?php if($this->session->userdata("level_user") != "PEMILIK"){ ?>
                            
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Transportir</label>
                                <select name="transportir" class="form-control m-b select2">
                                    <option value="">Pilih Transportir</option>
                                    <?php foreach($transportir as $t){ ?>
                                    <option value="<?= $t->id_pemilik ?>" <?= $this->input->get("transportir") == $t->id_pemilik ? 'selected' : '' ?>><?= $t->nama_perusahaan ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Bulan</label>
                                <select name="bulan" class="form-control m-b select2">
                                    <option value="">Pilih Bulan</option>
                                    <?php $listBulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"); ?>
                                    <?php for($bulan = 1; $bulan <= 12; $bulan++){ ?>
                                    <option value="<?= $bulan ?>" <?= $this->input->get("bulan") == $bulan ? 'selected' : '' ?>><?= $listBulan[$bulan - 1] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select name="tahun" class="form-control m-b select2">
                                    <option value="">Pilih Tahun</option>
                                    <?php for($tahun = 2020; $tahun <= date("Y"); $tahun++){ ?>
                                    <option value="<?= $tahun ?>" <?= $this->input->get("tahun") == $tahun ? 'selected' : '' ?>><?= $tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-1" style="margin-left:15px;">
                                <label><br/></label>
                                <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i> TAMPILKAN</button>
                            </div>

                             
                            <div class="col-lg-1" style="margin-left:15px;">
                                <label><br/></label>
                                <?php $link = explode("?",$_SERVER['REQUEST_URI']); ?>
                                <a href="<?php echo base_url('temuan/export?').(count($link) > 1 ? $link[1] : ''); ?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Cetak Data</a>

                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">   

                        <table class="table table-striped table-bordered table-hover" id="table_temuan" width="100%">
                            <thead class="gradient">
                                <tr>
                                    <th>No</th>
                                    <th>Transportir</th>
                                    <th>No Polisi</th>
                                    <th>Jenis Temuan</th>
                                    <th>Deskripsi</th>
                                    <th>Tanggal Temuan</th>
                                    <th>Status</th>
                                    <th>Status Tindak Lanjut</th>
                                    <th width="20%" style="text-align:center;">Aksi</th>                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i=1;
                                    
                                    function tanggal_indo($tanggal){
                                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                                        $split = explode('-', $tanggal);
                                        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                                    }
                                    foreach($temuan as $key => $value){ 
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><b><?= $value->nama_perusahaan ?></b></td>
                                    <td><b><?= $value->no_polisi ?></b></td>
                                    <td><?= $value->jenis_temuan ?></td>
                                    <td><?= $value->deskripsi_temuan ?></td>
                                    <td><?= tanggal_indo($value->tgl_temuan) ?></td>
                                    <td><?= strtoupper($value->status) ?></td>
                                    <?php if($value->status_tindak_lanjut == "CLOSE"){ ?>
                                        <td><?= '<span class="badge badge-primary">'.strtoupper($value->status_tindak_lanjut).'</span>' ?></td>
                                    <?php } else if($value->status_tindak_lanjut == "OPEN") { ?>
                                        <td><?= '<span class="badge badge-danger">'.strtoupper($value->status_tindak_lanjut).'</span>' ?></td>
                                    <?php } else { ?>
                                        <td><?= '<span class="badge badge-warning">'.strtoupper($value->status_tindak_lanjut).'</span>' ?></td>
                                    <?php } ?>
                                    <td style="text-align:left;">
                                        <a href="<?= base_url('temuan/detail').'?id='.$value->id_temuan; ?>" class="btn btn-xs btn-info"><i class="fa fa-info"></i> DETAIL</a>
                                        <?php if($this->session->userdata("level_user") == "PEMILIK" && $value->komentar_tindak_lanjut == null){ ?>
                                        <a href="<?= base_url('temuan/followup').'?id='.$value->id_temuan; ?>" class="btn btn-xs btn-success"><i class="fa fa-legal"></i> TINDAK LANJUT</a>
                                        <?php } else { ?>
<!--                                         <a class="btn btn-xs btn-primary"><i class="fa fa-check-square"></i> DONE</a> -->
                                        <a href="" data-toggle="modal" class="btn btn-xs btn-outline btn-default" data-target="#imageModal<?php echo $value->id_temuan?>"> HASIL PERBAIKAN </a>
                                        <?php } ?>
                                        <?php if($this->session->userdata("level_user") != "PEMILIK"){ ?>
                                        <a href="<?= base_url('temuan/edit').'?id='.$value->id_temuan; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> UBAH</a>
                                        <a onclick="hapusTemuan('<?= $value->id_temuan ?>')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php
    foreach ($temuan as $p) {
        ?>
    <div class="modal fade" id="imageModal<?php echo $p->id_temuan?>" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header gradient" style="background-color: #77aaff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <br/>
                    <h3 class="modal-title" id="imageModalLabel"><center><b>Detail Tindak Lanjut Temuan </b><?php echo $p->nama_perusahaan ?></center></h3>
                </div>
                    <div class="modal-body">
                        <?php if($p->komentar_tindak_lanjut) { ?>
                            <center>
                            <div class="form-group">
                                
                    <div class="widget style1 lazur-bg">
                        <div class="row vertical-align">
                            <div class="col-xs-3">
                                <i class="fa fa-calendar fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <span>Tanggal tindak lanjut</span>
                                <h2 class="font-bold"><?php echo date('j-F-Y', strtotime($p->tgl_tindak_lanjut)) ?></h2>
                            </div>
                        </div>
                    </div>
                
                </div>
                </center>
                        <div class="form-group">
                            <!-- <input class="form-control" readonly="" value="<?php echo $p->komentar_tindak_lanjut ?>" style="height: 95px;"> -->
                            <textarea name="Text1" cols="40" rows="4" class="form-control" readonly><?php echo $p->komentar_tindak_lanjut ?></textarea>
                        </div>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="img-thumbnail" src="<?php echo base_url() ?>./dokumen/foto-tindak-lanjut-temuan/<?php echo $p->tindak_lanjut_pertama; ?>">
                            </div>
                            <div class="carousel-item">
                                <img class="img-thumbnail" src="<?php echo base_url() ?>./dokumen/foto-tindak-lanjut-temuan/<?php echo $p->tindak_lanjut_kedua; ?>">
                            </div>
                            <div class="carousel-item">
                                <img class="img-thumbnail" src="<?php echo base_url() ?>./dokumen/foto-tindak-lanjut-temuan/<?php echo $p->tindak_lanjut_ketiga; ?>">
                            </div>
                        </div>
                        <?php } else { ?>
                            <div class="widget red-bg p-lg text-center">
                            <div class="m-b-md">
                            <i class="fa fa-bell fa-4x"></i>
                            <h3 class="font-bold no-margins"><br/>
                                INFO
                            </h3>
                            <small>Belum ada tindak lanjut.</small>
                            </div>
                            </div>
                        <?php } ?>
                    </div>
                
            </div>
        </div>
    </div>
<?php
}
?>

<script>
    $("document").ready(function(){
        $("#table_temuan").dataTable();

         $('.select2').select2();

    });

    function hapusTemuan(id){
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                window.location = "<?php echo base_url('temuan/action_hapus/') ?>"+id; 
            }
        })
    }
</script>