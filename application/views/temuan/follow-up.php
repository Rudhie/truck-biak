<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Tindak lanjut temuan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li>Temuan</li>
            <li class="active"><strong>Tindak lanjut</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                            <form action="<?= base_url('temuan/action_followup/').$this->input->get("id"); ?>"  enctype="multipart/form-data" method="POST">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Tindak lanjut temuan 1</label>
                                        <input type="hidden" style="background-color: white;" name="tgl_tindak_lanjut" class="form-control" value="<?php echo date('d/m/Y')?>" required="">

                                        <img id="prev_foto_pertama" src="<?= base_url('asset/img/noimage_336_290.jpg') ?>" width="70%" />
                                        <input type="file" name="tindak_lanjut_pertama" id="foto_pertama" class="form-control" />
                                        <small class="text-danger">(*File berformat JPG/JPEG/PNG, maksimal ukuran 2mb)</small>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Tindak lanjut temuan 2</label>
                                        <img id="prev_foto_kedua" src="<?= base_url('asset/img/noimage_336_290.jpg') ?>" width="70%" />
                                        <input type="file" name="tindak_lanjut_kedua" id="foto_kedua" class="form-control" />
                                        <small class="text-danger">(*File berformat JPG/JPEG/PNG, maksimal ukuran 2mb)</small>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Tindak lanjut temuan 3</label>
                                        <img id="prev_foto_ketiga" src="<?= base_url('asset/img/noimage_336_290.jpg') ?>" width="70%" />
                                        <input type="file" name="tindak_lanjut_ketiga" id="foto_ketiga" class="form-control" />
                                        <small class="text-danger">(*File berformat JPG/JPEG/PNG, maksimal ukuran 2mb)</small>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Catatan mengenai tindak lanjut</label>
                                        <textarea name="komentar_tindak_lanjut" row="8" class="form-control" required></textarea>
                                        <!-- <input type="text" name="komentar_tindak_lanjut" class="form-control" required> -->
                                    </div>
                                </div>
                                    <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                        <a href="<?php echo base_url('temuan') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
         $("#selectTransportir").select2();
        $("#selectNopolisi").select2();
        $("#selectStatus").select2();
         
    

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'
        });
    });

    $("#foto_pertama").change(function() {
        readURL(this, "#prev_foto_pertama", "#foto_pertama");
    });

    $("#foto_kedua").change(function() {
        readURL(this, "#prev_foto_kedua", "#foto_kedua");
    });

    $("#foto_ketiga").change(function() {
        readURL(this, "#prev_foto_ketiga", "#foto_ketiga");
    });

    function readURL(input, id, form) {
        var typeFile = input.files[0].type;
        var size = Math.round(input.files[0].size / 1024);
        if(typeFile == "image/jpeg" || typeFile == "image/png"){
            if(size > 2048){
                swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");
                $(form).val('');
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $(id).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        } else {
            swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
            $(form).val('');
        }
        
    }
</script>
<script>
    $(document).ready(function(){
        var id_truck = "<?php echo $temuan[0]->id_truck?>";
        var id_pemilik = $("#pemilik").val();
            if(id_pemilik != ""){
                $.ajax({
                    url: "<?php echo base_url(); ?>temuan/nopol_pemilik",
                    method: "POST",
                    data: {id_pemilik:id_pemilik, id_truck:id_truck},
                    success: function(data){
                        $("#nopol").html(data);
                    }
                })
            }
        $("#pemilik").change(function(){
            var id_pemilik = $("#pemilik").val();
            if(id_pemilik != ""){
                $.ajax({
                    url: "<?php echo base_url(); ?>temuan/nopol_pemilik",
                    method: "POST",
                    data: {id_pemilik:id_pemilik, id_truck:id_truck},
                    success: function(data){
                        $("#nopol").html(data);
                    }
                })
            }
        });
    });

    $("#foto_pertama").change(function() {
        readURL(this, "#prev_foto_pertama", "#foto_pertama");
    });

    $("#foto_kedua").change(function() {
        readURL(this, "#prev_foto_kedua", "#foto_kedua");
    });

    $("#foto_ketiga").change(function() {
        readURL(this, "#prev_foto_ketiga", "#foto_ketiga");
    });
</script>