<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sistem Informasi & Monitoring Truck Tangki</title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('asset/img/truck.png') ?>">
    <link href="<?= base_url('asset/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/fullcalendar/fullcalendar.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/dataTables/datatables.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/sweetalert/sweetalert.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/select2/select2.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/steps/jquery.steps.css') ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/animate.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/style.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') ?>" rel="stylesheet">
    
    <!-- Mainly scripts -->
    <script src="<?= base_url('asset/js/jquery-2.1.1.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/fullcalendar/moment.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/metisMenu/jquery.metisMenu.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/dataTables/datatables.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/sweetalert/sweetalert.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/select2/select2.full.min.js') ?>"></script>
    
    <!-- Custom and plugin javascript -->
    <script src="<?= base_url('asset/js/inspinia.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/pace/pace.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/fullcalendar/fullcalendar.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/staps/jquery.steps.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/validate/jquery.validate.min.js') ?>"></script>
    <script src="<?= base_url('asset/highcharts-8.0.0/code/highcharts.js') ?>"></script>

    <!-- Fancybox -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <style>
        .se-pre-con {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url("<?= base_url('asset/img/Infinity-1.9s-338px.gif') ?>") center no-repeat #fff;
		}

        .gradient {
            color:white;
			background: linear-gradient(135deg, rgba(103,119,239,1) 0%, rgb(0,206,255,1) 100%);
        }

        .field-icon {
			float: right;
			margin-left: -25px;
			margin-right: 10px;
			margin-top: -25px;
			position: relative;
			z-index: 2;
		}

        .dot {
            height: 15px;
            width: 15px;
            border-radius: 50%;
            display: inline-block;
        }

        .dot-primary {
            background-color: #1bb394;
        }

        .dot-warning {
            background-color: #f8ac5a;
        }

        .dot-yellow {
            background-color:#FFFF00;
        }

        .dot-danger {
            background-color: #ed5666;
        }

        .ibox-title {
            border-color: #32a4f7;
        }

        .indicator {
            margin-right:20px;
        }
    </style>

</head>
<body class="">
    <!-- <div class="se-pre-con"></div> -->
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <center>
                    	<span>
                    		<img alt="image" class="img-circle" src="<?= base_url('asset/img/logo_nobg.png') ?>" width="40%" />
                        </span>
                        
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        	<span class="clear"><span class="block m-t-xs"> <strong class="font-bold"><?= $this->session->userdata('fullname'); ?></strong></span> <span class="text-muted text-xs block"><?= $this->session->userdata('level_user') ?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="#" id="change_password">Change Password</a></li>
                            <li><a href="#" id="logout">Logout</a></li>
                        </ul>
                        </center>
                    </div>
                    <div class="logo-element">
                        PT
                    </div>
                </li>

                <?php $menu = $this->uri->segment('1'); $submenu = $this->uri->segment('2');?>
                <li <?php if($menu == "dashboard"){ echo "class='active'";} ?>>
                    <a href="<?= base_url('dashboard') ?>"><i class="fa fa-laptop"></i> <span class="nav-label">Dashboard</span></a>
                </li>
                <li <?php if($menu == "truck" || $menu == "awak" || $menu == "pemilik" || $menu == "merek"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if($this->session->userdata("level_user") == "ADMIN" || $this->session->userdata("level_user") == "MANAGER LOKASI" || $this->session->userdata("level_user") == "MANAGER REGION"){ ?>
                        <li <?php if($menu == "merek"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("merek"); ?>"><i class="fa fa-list-alt"></i>Merek & Tipe Mobil</a>
                        </li>
                        <li <?php if($menu == "pemilik"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("pemilik"); ?>"><i class="fa fa-user"></i>Transportir</a>
                        </li>
                        <?php } ?>
                        <li <?php if($menu == "truck"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("truck?filterTransportir=all"); ?>"><i class="fa fa-truck"></i>Mobil Tangki</a>
                        </li>
                        <li <?php if($menu == "awak"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("awak?filterTransportir=all"); ?>"><i class="fa fa-users"></i>Awak Mobil Tangki</a>
                        </li>
                                                
                        <!-- <li <?php if($menu == "supir"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("supir"); ?>"><i class="fa fa-user"></i><font color="#C999D3">Supir</font></a>
                        </li>
                        <li <?php if($menu == "kernet"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("kernet"); ?>"><i class="fa fa-square-o"></i><font color="#C999D3">Kernet</font></a>
                        </li> -->
                    </ul>
                </li>
                <li <?php if($menu == "reminder"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-calendar"></i> <span class="nav-label">Reminder</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li <?php if($menu == "reminder" && $submenu == "truck"){ echo "class='active'";} ?>>
                            <a href="<?php echo base_url("reminder/truck"); ?>"><i class="fa fa-truck"></i>Mobil Tangki</a>
                        </li>
                        <li <?php if($menu == "reminder" && $submenu == "amt"){ echo "class='active'";} ?>>
                            <a href="<?php echo base_url("reminder/amt"); ?>"><i class="fa fa-users"></i>Awak Mobil Tangki</a>
                        </li>
                    </ul>
                </li>
                <li <?php if($menu == "perpanjangan" && $this->input->get("page") != "idcard"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-envelope"></i> <span class="nav-label">Perpanjangan</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li <?php if($menu == "perpanjangan" && $submenu == "truck"){ echo "class='active'";} ?>>
                            <a href="<?php echo base_url("perpanjangan/truck"); ?>"><i class="fa fa-truck"></i>Mobil Tangki</a>
                        </li>
                        <li <?php if($menu == "perpanjangan" && $submenu == "amt"){ echo "class='active'";} ?>>
                            <a href="<?php echo base_url("perpanjangan/amt"); ?>"><i class="fa fa-users"></i>Awak Mobil Tangki</a>
                        </li>
                    </ul>
                </li>
                <!-- <li <?php if($menu == "idcard" || $this->input->get("page") == "idcard"){ echo "class='active'";} ?>>
                    <a href="<?= base_url('idcard') ?>"><i class="fa fa-credit-card"></i> <span class="nav-label">ID CARD AMT</span></a>
                </li> -->
                <li <?php if($menu == "temuan"){ echo "class='active'";} ?>>
                    <a href="<?= base_url('temuan') ?>"><i class="fa fa-search"></i> <span class="nav-label">Temuan</span></a>
                </li>
                <?php if($this->session->userdata("level_user") == "ADMIN"){ ?>
                <li <?php if($menu == "user"){ echo "class='active'";} ?>>
                    <a href="<?= base_url('user') ?>"><i class="fa fa-users"></i> <span class="nav-label">User Management</span></a>
                </li>
                <?php } ?>

            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-success " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning" id="count-notifikasi"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <div id="list-notifikasi"></div>
                        <li>
                            <div class="text-center link-block">
                                <a href="<?= base_url('perpanjangan/truck') ?>">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#" id="logout">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>
            <!-- content in here -->
            <?= $contents; ?>
            <!-- /content in here -->
            <div class="modal inmodal fade" id="modalChangePassword" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header gradient">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h6 class="modal-title" id="title-peserta">Ubah Password</h6>
                        </div>
                        <div class="modal-body">
                            <form id="formChangePassword" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Password</label>
                                    <div class="col-sm-7">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password Baru">
                                        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        <sup id="note_password"></sup>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Konfirmasi Password</label>
                                    <div class="col-sm-7">
                                        <input type="password" class="form-control" name="konfirmasi_password" id="konfirmasi_password" placeholder="Konfirmasi Password Baru">
                                        <span toggle="#konfirmasi_password" class="fa fa-fw fa-eye field-icon toggle-konfirmasi-password"></span>
                                        <sup id="note_password"></sup>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="pull-right">
                    <strong>Copyright</strong> &copy; PT. Pertamina (persero) Fuel Terminal Biak &copy; <?= date('Y') ?>
                </div>
            </div>
        </div>
        </div>
        <script>
            $("document").ready(function(){
                setTimeout(function(){
                    $(".se-pre-con").fadeOut("slow");
                }, 1000);
                notifikasi();

                // setInterval(function() {
                //     notifikasi();
                // }, 10000);
            });

            function notifikasi(){
                $.ajax({
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('dashboard/notifikasi') ?>",
                    "method": "POST",
                    "data": $(this).serialize(),
                }).done(function (response) {
                    var data = JSON.parse(response);
                    $("#count-notifikasi").html(data.jumlah);
                    var list = data.data;
                    var notif = "";
                    if(list.length > 0){
                        list.forEach(function(item, index){
                            notif += "<li>";
                            notif += "<div class='dropdown-messages-box'>";
                            notif += "<a class='pull-left'>";
                            notif += "<img alt='image' class='img-circle' src='<?= base_url()?>asset/img/pertamina.png' width='10%'>";
                            notif += "</a>";
                            notif += "<div class='media-body'>";
                            if(item.id_awak != "0"){
                                notif += "<strong>"+item.nama_perusahaan+"</strong> mengajukan <strong>"+item.nm_keperluan+"</strong> untuk AMT <strong>"+item.nama_awak+"</strong>. <br>";
                            } else {
                                notif += "<strong>"+item.nama_perusahaan+"</strong> mengajukan <strong>"+item.nm_keperluan+"</strong> untuk truck <strong>"+item.no_polisi+"</strong>. <br>";
                            }
                            notif += "<small class='text-muted'>"+item.tgl_aktifitas+"</small>";
                            notif += "</div></div></li><li class='divider'></li>";
                        });
                    } else {
                        notif += "<li><center>No Notifikasi</center></li>";
                    }
                    

                    $("#list-notifikasi").html(notif);
                });
            }

            $(document).on("click", "#change_password", function(e){
                $('#formChangePassword')[0].reset();
                $("#modalChangePassword").modal("show");
            });

            $(document).on("click", ".toggle-password", function() {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            $(document).on("submit", "#formChangePassword", function(e){
                e.preventDefault();
                var password = $("#password").val();
                var konfirmasi_password = $("#konfirmasi_password").val();
                if(password != konfirmasi_password){
                    swal("Gagal.", "Kombinasi password tidak sama.", "warning");
                } else {
                    
                    $.ajax({
                        "async": true,
                        "crossDomain": true,
                        "url": "<?= base_url('user/change_password') ?>",
                        "method": "POST",
                        "data": $(this).serialize(),
                    }).done(function (response) {
                        var data = JSON.parse(response)
                        var message = data.message;
                        if(data.status == "success"){
                            $("#modalChangePassword").modal("hide");
                            swal({
                                title: "Berhasil",
                                text: message.toUpperCase(),
                                type: "success",
                                confirmButtonColor: "#a5dc86",
                                confirmButtonText: "Close",
                            });
                            
                        } else {
                            swal("Gagal.", message.toUpperCase(), "warning");
                        }
                    });
                }
                
            });

            $(document).on("click", ".toggle-konfirmasi-password", function() {
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            $(document).on("click", "#logout", function(e){
                swal({
                    title: "Yakin ingin keluar ?",
                    text: "Apakah anda yakin ingin keluar dari sistem",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Keluar Sistem",
                    cancelButtonText: "Batal",
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function(isConfirm){
                    if(isConfirm){
                        var settings = {
                            "async": true,
                            "crossDomain": true,
                            "url": "<?= base_url('login/action_logout') ?>",
                            "method": "POST"
                        }
                        
                        $.ajax(settings).done(function (response) {
                            var data = JSON.parse(response)
                            var message = data.message;
                            if(data.status == "success"){
                                swal({
                                    title: "Berhasil",
                                    text: message.toUpperCase(),
                                    type: "success",
                                });
                                setTimeout(() => {
                                    window.location = "<?= base_url('login') ?>"
                                }, 1000);
                            }

                        });   
                    }
                });
            });
        </script>
</body>
</html>
