<style type="text/css">
    #tdInvoice {
        border-bottom: 1px solid #DDDDDD;
        text-align: right;
        width: 15%;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-truck"></i> Detail AMT</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('truck'); ?>">Data Awak Mobil Tangki</a></li>
            <li class="active"><strong><a>Detail Awak Mobil Tanki</a></strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <hr style="border-top: dotted 1px;"/>
                    <div class="row">
                        <div class="col-md-4">
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> NIP AMT</p>
                                <small><strong><?php echo strtoupper($awak[0]->nip_awak); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> Nama AMT</p>
                                <small><strong><?php echo $awak[0]->nama_awak; ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> Fungsi</p>
                                <small><strong><?php echo strtoupper($awak[0]->bagian); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> Tanggal Lahir AMT</p>
                                <small><strong><?php echo date('d-F-Y', strtotime(strtoupper($awak[0]->tgl_lahir_awak))); ?></strong></small>
                            </blockquote>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-heading">Foto AMT</div>
                                    <div class="panel-body">
                                        <center><img class="img-thumbnail" id="imgId" src="<?php echo base_url().'dokumen/foto-awak/'.$awak[0]->foto_awak ?>" width="180" height="250"></center>
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-heading">Detail Dokumen & Masa Berlaku</div>
                                <div class="panel-body" style="height:290px;overflow-y: scroll;">
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-awak/'.$awak[0]->dokumen_ktp_awak ?>">Dokumen KTP AMT</a></p>
                                        <small>Masa Berlaku : <strong>Seumur Hidup</strong></small>
                                    </blockquote>
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-awak/'.$awak[0]->dokumen_idcard_awak ?>">Dokumen ID Card AMT</a></p>
                                        <small>Masa Berlaku : <strong><?php echo date('d-F-Y', strtotime(strtoupper($awak[0]->masa_berlaku_idcard_awak))); ?></strong></small>
                                    </blockquote>
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-awak/'.$awak[0]->dokumen_sim_awak ?>">Dokumen SIM AMT</a></p>
                                        <small>Masa Berlaku : <strong><?php echo date('d-F-Y', strtotime(strtoupper($awak[0]->masa_berlaku_sim_awak))); ?></strong></small>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                        <h1><a style="float: right;" href="<?php echo base_url('awak') ?>" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> KEMBALI</a></h1>
                        </div> 
                </div>
            </div>
        </div>
    </div>
</div>