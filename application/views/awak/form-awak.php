<style>

    .select2-container .select2-selection--single{

        height:34px !important;

    }

    .select2-container--default .select2-selection--single{

     border: 1px solid #ccc !important; 

     border-radius: 0px !important; 

 }

</style>



<div class="row wrapper border-bottom white-bg page-heading">

    <div class="col-lg-10">

        <h2><i class="fa fa-users"></i> Master Data Awak & Kernet</h2>

        <ol class="breadcrumb">

            <li><a href="<?php echo base_url('#'); ?>">Dashboard</a></li>

            <li><a href="<?php echo base_url('awak'); ?>">Data Awak & Kernet</a></li>

            <li class="active"><strong>Tambah Awak</strong></li>

        </ol>

    </div>

</div>



<div class="wrapper wrapper-content animated fadeIn">

    <div class="row">

        <div class="col-lg-12">

            <div class="ibox float-e-margins">

                <div class="ibox-content">

                    <div class="jumbotron">

                        <div class="row">

                            <form action="<?php echo base_url('awak/action_tambah'); ?>" enctype="multipart/form-data" method="POST" role="form">

                                <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">

                                    <center>

                                        <h3>I. Profil AMT </h3>

                                    </center>

                                </div>

                                <br/>

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <center><label>Foto Profil <small class="text-danger">(*File harus berupa gambar: JPG, JPEG, PNG)</small></label></center>

                                            <center>

                                                <input type="file" id="foto_awak" name="foto_awak" accept=".jpg, .png, .jpeg" class="form-control" onchange="readURL(this)" required="">

                                                

                                            </center>

                                            <br/>

                                            <center>

                                                    <img class="img-thumbnail" id="fotoAwak" src='<?php echo base_url() ?>./dokumen/foto-awak/default-image.jpg' width="200px" height="100px">

                                            </center>

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label>Nopol</label>

                                            <select id="idTruck" name="id_truck" class="form-control m-b select2">

                                                <option value="">Pilih Nopol</option>

                                                <?php

                                                foreach($nopol as $p){

                                                    echo '<option value="'.$p->id_truck.'">'.$p->nopol.'</option>';

                                                }

                                                ?>

                                            </select>

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label>NIP Awak</label>

                                            <input type="text" class="form-control" name="nip_awak" required="">

                                        </div>

                                    </div>

                                    <div class="col-md-4">

                                        <div class="form-group">

                                            <label>Nama Awak</label>

                                            <input type="text" class="form-control" name="nama_awak" required="">

                                        </div>

                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-md-6">

                                        <div class="form-group" id="data_1">

                                            <label>Tanggal Lahir</label>

                                            <div class="input-group date">

                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                                <input type="text" name="tgl_lahir_awak" class="form-control" value="<?php echo date('d/m/Y')?>" required="">

                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-6">

                                        <div class="form-group">

                                            <label>Bagian</label>

                                            <select class="form-control m-b select2" id="selectBagian" name="bagian" required>

                                                <option value="">Pilih Bagian</option>

                                                <option value="AMT 1">AMT 1</option>

                                                <option value="AMT 2">AMT 2</option>

                                            </select>

                                        </div>

                                    </div>

                                </div>

                                <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">

                                    <center>

                                        <h3>II. Masa Berlaku Berkas </h3>

                                    </center>

                                </div>

                                <br/>

                                    <div class="row">

                                        <div class="col-md-6">

                                            <div class="form-group" id="data_1">

                                                <label>Masa Berlaku SIM</label>

                                                <div class="input-group date">

                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                                    <input type="text" name="masa_berlaku_sim_awak" class="form-control" value="<?php echo date('d/m/Y')?>" required="">

                                                </div>

                                            </div>

                                        </div>

                                        <div class="col-md-6">

                                            <div class="form-group" id="data_1">

                                                <label>Masa Berlaku Id Card</label>

                                                <div class="input-group date">

                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                                    <input type="text" name="masa_berlaku_idcard_awak" class="form-control" value="<?php echo date('d/m/Y')?>" required="">

                                                </div>

                                            </div>

                                        </div>                                    </div>

                                    <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">

                                    <center>

                                        <h3>III. Dokumen Upload </h3>

                                    </center>

                                </div>

                                <br/>

                                    <div class="row">

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Dokumen SIM</label>

                                                <input type="file" class="form-control" name="dokumen_sim_awak" id="dokumen_sim_awak" accept=".pdf" required>

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Dokumen Id Card</label>

                                                <input type="file" class="form-control" name="dokumen_idcard_awak" id="dokumen_idcard_awak" accept=".pdf" required>

                                            </div>

                                        </div>

                                        <div class="col-md-4">

                                            <div class="form-group">

                                                <label>Dokumen KTP</label>

                                                <input type="file" class="form-control" name="dokumen_ktp_awak" id="dokumen_ktp_awak" accept=".pdf" required>

                                            </div>

                                        </div>

                                    </div>

                                    

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">

                                            <a href="<?php echo base_url('awak') ?>" class="btn btn-sm btn-info">KEMBALI</a>

                                            <input type="reset" value="RESET" class="btn btn-sm btn-warning" />

                                        </div>

                                    </div>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



<script>

    $(document).ready(function(){

        $('#data_1 .input-group.date').datepicker({

            todayBtn: "linked",

            keyboardNavigation: false,

            forceParse: false,

            calendarWeeks: true,

            autoclose: true,



            format:'dd/mm/yyyy'







        });

    });



    $("#dokumen_sim_awak").change(function() {

        readURL(this, "", "#dokumen_sim_awak");

    });

    $("#dokumen_idcard_awak").change(function() {

        readURL(this, "", "#dokumen_idcard_awak");

    });



    $("#foto_awak").change(function() {

        readURL(this, "#fotoAwak", "foto_awak");

    });



    function readURL(input, id, form) {

        var typeFile = input.files[0].type;

        var size = Math.round(input.files[0].size / 1024);

        if(form == "#dokumen_sim_awak" || form == "#dokumen_idcard_awak"){

            if(typeFile != "application/pdf"){

                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");

                $(form).val('');

            }

        } else {

            if(typeFile != "image/jpeg" && typeFile != "image/png"){

                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");

                $(form).val('');

            }

        }

        

        if(size > 2048){

            swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");

            $(form).val('');

        } else {

            if (input.files && input.files[0]) {

                var reader = new FileReader();

                reader.onload = function(e) {

                    $(id).attr('src', e.target.result);

                }

                reader.readAsDataURL(input.files[0]);

            }

        }

    }

</script>

<script>

    $('.select2').select2();

</script>