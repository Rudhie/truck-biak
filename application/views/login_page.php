<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mobil Tangki</title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('asset/img/truck.png') ?>">
	<link href="<?= base_url('asset/css/bootstrapv4.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('asset/css/bootstrap-extend.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('asset/css/site.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/login-v2.min.css'); ?>" rel="stylesheet">
    <link href="<?= base_url('asset/css/plugins/sweetalert/sweetalert.css') ?>" rel="stylesheet">
    <style>
        .field-icon {
			float: right;
			margin-left: -25px;
			margin-right: 10px;
			margin-top: -25px;
			position: relative;
			z-index: 2;
		}
    </style>
</head>
<body class="page-login-v2 layout-full page-dark">
	<div class="page">
        <div class="page-content">
            <div class="page-brand-info">
                <div class="brand">
                    <h2 class="brand-text font-size-40">PT. Pertamina (persero) Fuel Terminal Biak</h2>
                </div>
                <p class="font-size-20">
                    Sistem Informasi dan Management Mobil Tangki PT. Pertamina (persero) Fuel Terminal Biak
                </p>
            </div>
            <div class="page-login-main animation-slide-right animation-duration-1">
                <center>
                    <img class="brand-img" src="<?= base_url('asset/img/logo-pertamina.png') ?>" alt="..." width="200px">
                </center>
                <h3 class="font-size-24">Login</h3>
                <p>Silahkan masuk dengan mengunakan user ID anda.</p>
                <form id="formLogin" autocomplete="off">
                    <div class="form-group">
                        <label class="sr-only" for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" id="validateButton">LOGIN</button>
                </form>
                <center style="margin-top:40px;">
                    <span>Download User Guide / Manual Book <a target="_blank" href="<?= base_url('dokumen/DOKUMEN_USER_GUIDE_PERTAMINA_FUEL_TERMINAL_BIAK.pdf') ?>">disini</a></span>
                </center>
                <footer class="page-copyright">
                    <p>CREATED WITH LOVE &hearts; <?= date('Y') ?></p>
                </footer>
            </div>
        </div>
    </div>
	<!-- Mainly scripts -->
    <script src="<?= base_url('asset/js/jquery-2.1.1.js') ?>"></script>
    <script src="<?= base_url('asset/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('asset/js/plugins/metisMenu/jquery.metisMenu.js') ?>"></script>
	<script src="<?= base_url('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/sweetalert/sweetalert.min.js') ?>"></script>
    <!-- Custom and plugin javascript -->
    <script src="<?= base_url('asset/js/inspinia.js') ?>"></script>
    <script src="<?= base_url('asset/js/plugins/pace/pace.min.js') ?>"></script>
    <script>
        $(document).on("submit", "#formLogin", function(e){
            e.preventDefault();
            var id_user = $("#id_user").val();
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "<?= base_url('Login/action') ?>",
                "method": "POST",
                "data": $(this).serialize(),
            }).done(function (response) {
                var data = JSON.parse(response)
                var message = data.message;
                $("#validateButton").html("<i class='fa fa-spinner fa-spin'></i> Please Wait");
                $("#validateButton").prop("disabled", true);
                if(data.status == "success"){
                    swal({
                        title: "Berhasil",
                        text: message.toUpperCase(),
                        type: "success",
                    });
                    setTimeout(() => {
                        window.location = "<?= base_url('dashboard') ?>"
                    }, 1000);
                    
                } else {
                    $("#validateButton").html("LOGIN");
                    $("#validateButton").prop("disabled", false);
                    swal("Login Gagal.", message.toUpperCase(), "warning");
                }
            });
        });

        $(document).on("click", ".toggle-password", function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
</body>
</html>
