<style>
    .select2-results__options[id*="tingkat_keperluan"] .select2-results__option:nth-child(2) {
        color: green;
    }
    .select2-results__options[id*="tingkat_keperluan"] .select2-results__option:nth-child(3) {
        color: orange;
    }
    .select2-results__options[id*="tingkat_keperluan"] .select2-results__option:nth-child(4) {
        color: red;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Ubah Pengajuan Perpanjangan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li>Perpanjangan</li>
            <li class="active"><strong>Ubah Pengajuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="form" class="wizard-big" action="<?= base_url('perpanjangan/action_edit/').$this->input->get('id') ?>" enctype="multipart/form-data" method="POST">
                        <h1>Data Permohonan</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Jenis Keperluan</label>
                                        <input type="hidden" name="type" value="<?= $this->input->get('page'); ?>" />
                                        <select name="id_keperluan" class="form-control m-b" id="id_keperluan" required>
                                            <option value="">Pilih Permohonan</option>
                                            <?php foreach($keperluan as $k_key => $k_val){ ?>
                                            <option value="<?= $k_val->id_keperluan ?>" data-type="<?= $k_val->type_keperluan ?>" <?= $perpanjangan[0]->id_keperluan == $k_val->id_keperluan ? "selected" : "" ?>><?= $k_val->nm_keperluan;?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group" id="form-truck" style="display:none;">
                                        <label>Mobil Tangki</label>
                                        <select name="id_truck" class="form-control m-b" id="id_truck">
                                            <option value="">Pilih Truck</option>
                                            <?php foreach($truck as $t_key => $t_val){ ?>
                                            <option value="<?= $t_val->id_truck ?>" <?= $perpanjangan[0]->id_truck == $t_val->id_truck ? "selected" : "" ?>><?= $t_val->no_polisi." - ".$t_val->nm_merek; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group" id="form-supir" style="display:none;">
                                        <label>Awak Mobil Tangki</label>
                                        <select name="id_supir" class="form-control m-b" id="id_supir">
                                            <option value="">Pilih Awak</option>
                                            <?php foreach($supir as $s_key => $s_val){ ?>
                                            <option value="<?= $s_val->id_awak ?>" <?= $perpanjangan[0]->id_awak == $s_val->id_awak ? "selected" : "" ?>><?= $s_val->nama_awak." - ".$s_val->bagian; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group" id="form-nopol-baru" style="display:none;">
                                        <label>NoPol Mobil Tangki Baru</label>
                                        <select name="id_truck_baru" class="form-control m-b" id="id_truck_baru">
                                            <option value="">Pilih Mobil Tangki Baru</option>
                                            <?php foreach($truck as $t_key => $t_val){ ?>
                                            <option value="<?= $t_val->id_truck ?>" <?= $perpanjangan[0]->id_truck_baru == $t_val->id_truck ? "selected" : "" ?>><?= $t_val->no_polisi." - ".$t_val->nm_merek; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Detail Keperluan</label>
                                        <textarea  class="form-control" id="detail_keperluan" name="detail_keperluan" placeholder="Detail Keterangan Pengajuan" rows="5" required><?= $perpanjangan[0]->detail_keperluan ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-group" id="data_1">
                                            <label>Tanggal Masa Berlaku Dokumen Baru</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="tgl_masa_berlaku_baru" class="form-control" value="<?php echo date('d/m/Y', strtotime($perpanjangan[0]->tgl_masa_berlaku_baru));?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Tingkat Kepentingan</label>
                                        <select name="tingkat_keperluan" class="form-control m-b" id="tingkat_keperluan" required>
                                            <option value="">Pilih Tingkat</option>
                                            <option value="LOW" <?= $perpanjangan[0]->tingkat_keperluan == "LOW" ? "selected" : "" ?>>LOW</option>
                                            <option value="MEDIUM" <?= $perpanjangan[0]->tingkat_keperluan == "MEDIUM" ? "selected" : "" ?>>MEDIUM</option>
                                            <option value="URGENT" <?= $perpanjangan[0]->tingkat_keperluan == "URGENT" ? "selected" : "" ?>>URGENT</option>
                                        </select>
                                    </div>
                                    <?php if($perpanjangan[0]->status_aktifitas != "2"){ ?>
                                    <div class="form-group">
                                        <label>Catatan / Komentar</label>
                                        <textarea class="form-control" rows="3" name="komentar" placeholder="Catatan / Komentar"><?= $perpanjangan[0]->komentar; ?></textarea>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </fieldset>
                        <h1>Checklist Dokumen</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-12">
                                <?php foreach($dok_perpanjangan as $dokumen){ ?>
                                    <div class="form-group">
                                        <label><?= $dokumen->nm_syarat ?></label>
                                        <sub><?= $dokumen->keterangan ?></sub>
                                        <input type="hidden" value="<?= $dokumen->id_syarat ?>" name="id_syarat[]" />
                                        <input type="hidden" value="<?= $dokumen->id_dokumen ?>" name="id_dokumen[]" />
                                        <input type="file" name="dokumen<?= $dokumen->id_syarat ?>" id="dokumen" class="form-control" />
                                        <ul class="sortable-list connectList agile-list" style="width:50%;margin-top:10px;" id="btn-dokumen" data-title_dokumen="<?= $dokumen->nm_syarat ?>" data-nama_dokumen="<?= $dokumen->dokumen_syarat ?>" data-format_dokumen="<?= $dokumen->format_dokumen ?>">
                                            <li class="<?= $dokumen->approval_admin == "N" ? "danger-element" : "success-element" ?>" style="list-style-type: none;padding:5px;">
                                                <?= $dokumen->approval_admin == "N" ? $dokumen->note_approval_admin : "" ?>
                                                <?= $dokumen->dokumen_syarat ?>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>
                                <?php if($perpanjangan[0]->status_aktifitas == "2"){ ?>
                                    <div class="form-group">
                                        <label>Catatan / Komentar</label>
                                        <textarea class="form-control" rows="3" name="komentar" placeholder="Catatan / Komentar" required></textarea>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="modalDokumen" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-data-dokumen"></h6>
            </div>
            <div class="modal-body">
                <div id="dokumen-perpanjangan-view"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        $("#form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex){
                if (currentIndex > newIndex){
                    return true;
                }
                var form = $(this);
                if (currentIndex < newIndex){
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex){
                var form = $(this);
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex){
                var form = $(this);
                form.submit();
            },
            onCanceled: function(evenet, currentIndex){
                window.location = "<?= base_url('perpanjangan/').$this->input->get('page') ?>";
            },
        }).validate({
            errorPlacement: function (error, element){
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });

        $("#id_keperluan, #tingkat_keperluan").select2();
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format:'dd/mm/yyyy'
        });

        var page = "<?= $this->input->get('page'); ?>";
        if(page == "truck"){
            $("#form-truck").css("display", "block");
            $("#form-supir").css("display", "none");
            $("#id_truck").select2();
        } else {
            $("#form-supir").css("display", "block");
            $("#form-truck").css("display", "none");
            $("#id_supir").select2();
        }

        if($("#id_keperluan").val() == 9){
            $("#form-nopol-baru").css("display", "block");
        }
    });

    $("#foto_pertama").change(function() {
        readURL(this, "#prev_foto_pertama", "#foto_pertama");
    });

    $("#foto_kedua").change(function() {
        readURL(this, "#prev_foto_kedua", "#foto_kedua");
    });

    $("#foto_ketiga").change(function() {
        readURL(this, "#prev_foto_ketiga", "#foto_ketiga");
    });

    $("#dokumen").change(function() {
        readURL(this, "", "#dokumen");
    });

    function readURL(input, id, form) {
        var typeFile = input.files[0].type;
        var size = Math.round(input.files[0].size / 1024);
        if(form == "#dokumen"){
            if(typeFile != "application/pdf"){
                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
                $(form).val('');
            }
        } else {
            if(typeFile != "image/jpeg" && typeFile != "image/png"){
                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
                $(form).val('');
            }
        }
        
        if(size > 2048){
            swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");
            $(form).val('');
        } else {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    }

    $(document).on("click", "#btn-dokumen", function(e){
        var title_dokumen = $(this).data("title_dokumen");
        var nama_dokumen = $(this).data("nama_dokumen");
        var format_dokumen = $(this).data("format_dokumen");
        if(format_dokumen == ".pdf"){
            var view = '<iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="<?= base_url() ?>asset/pdfviewer/web/viewer.html?file=<?= base_url('dokumen/perpanjangan/') ?>' + nama_dokumen+ '" scrolling="no"></iframe>';
        } else {
            var view = '<center><img class="img-thumbnail" src="<?= base_url() ?>dokumen/perpanjangan/'+nama_dokumen+'" /></center>';
        }
        $("#dokumen-perpanjangan-view").html(view);
        $("#title-data-dokumen").html(title_dokumen);
        $("#modalDokumen").modal("show");
    });

    $(document).on("change", "#id_keperluan", function(e){
        if($(this).val() == 7 || $(this).val() == 8){
            $("#label-tanggal-masa-berlaku").html("Tanggal Pengajuan");
        } else {
            $("#label-tanggal-masa-berlaku").html("Tanggal Masa Berlaku Dokumen Baru");
        }

        if($(this).val() == 7){
            $("#form-tingkat-kepentingan").css("display", "none");
        } else {
            $("#form-tingkat-kepentingan").css("display", "block");
        }

        if($(this).val() == 9){
            $("#form-nopol-baru").css("display", "block");
        } else {
            $("#form-nopol-baru").css("display", "none");
        }
        form_persyaratan($(this).val());
        check_masa_berlaku();
    });

    $(document).on("change", "#id_truck", function(e){
        check_masa_berlaku();
    });

    $(document).on("change", "#id_supir", function(e){
        check_masa_berlaku();
    })

    function form_persyaratan(id_keperluan){
        $.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('perpanjangan/persyaratan') ?>/"+id_keperluan,
			"method": "GET",
		}).done(function (response) {
            var data = JSON.parse(response);
            var syarat = data.data;
            var docSyarat = '';
            syarat.forEach(function(i, e){
                docSyarat += '<div class="form-group">';
                docSyarat += '<label>'+i.nm_syarat+'</label>';
                docSyarat += '&nbsp;<sub>'+i.keterangan+'</sub>';
                docSyarat += '<input type="hidden" value="'+i.id_syarat+'" name="id_syarat[]" >';
                docSyarat += '<input type="file" name="dokumen'+i.id_syarat+'" id="dokumen" class="form-control" required>';
                docSyarat += '</div>';
            });
            $("#form_persyaratan").html(docSyarat);
		});
    }

    function check_masa_berlaku(){
        var page = "<?= $this->input->get('page') ?>";
        if(page == "truck"){
            var id = $("#id_truck").val();
        } else {
            var id = $("#id_supir").val();
        }
        $.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('perpanjangan/check_tingkat_masa_berlaku') ?>",
			"method": "POST",
            "data": {
                "type": page,
                "id": id,
                "permohonan": $("#id_keperluan").val()
            }
		}).done(function (response) {
            var data = JSON.parse(response);
            if(id != ""){
                if(data.data < -90){
                    $("#tingkat_keperluan").val("LOW").change();
                } else if(data.data < -90 && data.data > -60){
                    $("#tingkat_keperluan").val("MEDIUM").change();
                } else {
                    $("#tingkat_keperluan").val("URGENT").change();
                }
            }
		});
    }
</script>