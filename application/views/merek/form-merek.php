<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-shield"></i> Master Data Merek Mobil</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('merek'); ?>">Data Merek Mobil</a></li>
            <li class="active"><strong>Tambah Merek Mobil</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                            <form action="<?php echo base_url('merek/action_tambah'); ?>" method="POST" role="form">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Merek</label>
                                            <input type="text" class="form-control" name="nm_merek" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tipe</label>
                                            <input type="text" class="form-control" name="tipe" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <label>Keterangan</label>
                                        <textarea class="form-control" name="keterangan" rows="4" required=""></textarea>
                                        <small class="text-danger">(*Deskripsikan keterangan )</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                            <a href="<?php echo base_url('merek') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                            <input type="reset" value="RESET" class="btn btn-sm btn-warning" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'



        });
        $("#inputmasking1, #inputmasking2, #inputmasking3, #inputmasking4, #inputmasking5, #inputmasking6, #inputmasking7, #inputmasking8").mask("000.000.000.000", {reverse: true});
    });

    $("#percentmasking").mask("##0,00", {reverse: true});
</script>