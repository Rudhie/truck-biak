<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-list-alt"></i> Ubah Master Data Merek Mobil</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('merek'); ?>">Data Merek Mobil</a></li>
            <li class="active"><strong>Ubah Merek Mobil</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                            <form action="<?php echo base_url('merek/action_ubah/').$this->input->get('id'); ?>" method="POST" role="form">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Merek</label>
                                            <input type="text" class="form-control" name="nm_merek" value="<?php echo ucfirst($merek[0]->nm_merek); ?>" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tipe</label>
                                            <input type="text" class="form-control" name="tipe" value="<?php echo $merek[0]->tipe; ?>" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <label>Keterangan</label>
                                        <textarea class="form-control" name="keterangan" rows="4" required=""><?php echo ucfirst($merek[0]->keterangan); ?></textarea>
                                        <small class="text-danger">(*Deskripsikan keterangan )</small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                        <a href="<?php echo base_url('merek') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'
        });
    });
</script>