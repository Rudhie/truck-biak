<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-soccer-ball-o"></i> Data Laporan PO</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Laporan PO</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                   
               <?php $link = explode("?",$_SERVER['REQUEST_URI']); ?>
                    <a href="<?php echo base_url('excel/export?').$link[1]; ?>" target="_blank" class="btn btn-xs btn-primary"><i class="fa fa-print"></i> Cetak Data</a>
        </div>
                <div class="ibox-content">
                <?php if($this->session->flashdata("success")){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("success"));
                            unset($_SESSION["success"]);
                        ?>
                    </div>
                <?php } else if($this->session->flashdata("error")) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("error"));
                            unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php } ?>


                <div class="row">
                        <div class="col-md-3">
                            <?php $param = $this->input->get("param"); ?>
                            <select class="form-control m-b" id="selectType">
                              <option value="">Pilih Filter</option> 
                                <option value="tahun" <?= $param == "tahun" ? "selected" : ""; ?> >Filter Tahun</option>
                            </select>  
                        </div>
                        <div class="col-md-3" id="filter-tanggal" style="display: none;">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" id="selectTanggal" class="form-control" value="<?php echo date('d/m/Y')?>" required>
                            </div>
                        </div>
                        <div class="col-md-3" id="filter-lokasi" style="display: none;">
                            <select class="form-control m-b" id="selectLokasi" name="lokasi" required></select>
                        </div>
                        <div class="col-md-3" id="filter-spk" style="display: none;">
                            <select class="form-control m-b" id="selectSpk" name="spk" required></select>
                        </div>
                        <div class="col-md-3" id="filter-bulan" style="display: none;">
                            <select class="form-control m-b" id="selectBulan" name="bulan" required>
                            
                                <?php 
                                $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                                for($i = 1; $i <= 12; $i++){ ?>
                                    <option value="<?php echo $i; ?>" <?php if(date('m') == $i){echo "selected";} ?>><?php echo $bulan[$i]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-3" id="filter-tahun" style="display: none;">
                            <select class="form-control m-b" id="selectTahun" name="tahun" required>
                                <?php for($y = date('Y')-2; $y <= date('Y'); $y++){ ?>
                                    <option value="<?php echo $y; ?>" <?php if(date('Y') == $y){echo "selected";} ?>><?php echo $y; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                                                
                        <div class="col-md-3" id="filter-3">
                            <button id="filterSpk" class="btn btn-sm btn-info">LIHAT DATA</button>
                        </div>
                    </div>
                    <hr/>

                    <div class="table-responsive">
                    <table id="tableSpk" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Tgl Tagihan</th>
                        <th>User</th>
                        <th>Nomor SPK</th>
                        <th>Keterangan</th>
                        <th>Tgl Faktur</th>
                        <th>No Faktur</th>
                        <th>Nilai O P</th>
                        <th>D P P</th>
                        <th>Jumlah PPn</th>
                        <th>Tgl Di Bayar</th>
                        <th>Yg Di Transfer</th>
                        <th>PPh 2 %</th>
                        <th>PPh 1,5 %</th>
                        <th>Total</th>
                        <th>Selisih</th>
                        <th>Bank</th>
                        <th>Denda</th>
                        <!-- <th width="15%">Aksi</th> -->
                    </tr>
                   </thead>
                    <tbody>
                 
                   <?php $i=1;
                   function tanggal_indo($tanggal){
                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                        $split = explode('-', $tanggal);
                        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                   }
                   foreach($excel as $data){
                    echo'<tr >';
                    echo'<td>'.$i.'</td>';
                    echo'<td>'.tanggal_indo($data->tgl_invoice).'</td>';
                    echo'<td>'.$data->nm_mitra.'</td>';
                    echo'<td>'.$data->no_spk.'</td>';
                    echo'<td>'.$data->pekerjaan_utama.'</td>';
                    echo'<td>'.tanggal_indo($data->tgl_faktur).'</td>';
                    echo'<td>'.$data->no_faktur.'</td>';
                    echo'<td style="text-align: right;">'.str_replace(",", ".", number_format($data->nilai_spk)).'</td>';
                    echo'<td style="text-align: right;">'.str_replace(",", ".", number_format($data->nilai_spk / 1.1)).'</td>';
                    echo'<td style="text-align: right;">'.str_replace(",", ".", number_format($data->nilai_spk / 1.1 *(10 / 100))).'</td>';
                    echo'<td>'.tanggal_indo($data->tanggal_pembayaran).'</td>';
                    echo'<td style="text-align: right;">'.str_replace(",", ".", number_format($data->nilai_pembayaran)).'</td>';
                    echo'<td style="text-align: right;">'.str_replace(",", ".", number_format($data->nilai_spk / 1.1 *(2 / 100))).'</td>';
                    echo'<td style="text-align: right;">'.str_replace(",", ".", number_format($data->nilai_spk / 1.1 *(1.5 / 100))).'</td>';
                    echo'<td>'.$data->total.'</td>';
                    echo'<td>'.$data->selisih.'</td>';
                    echo'<td>'.$data->nm_bank.'</td>';
                    echo'<td>'.$data->denda.'</td>';
                
                    echo'</tr>';
                    $i++;
                   }
                   ?>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function () {
        var param = '<?php echo $this->input->get("param") ?>';
        if(param == "tanggal"){
            var date = '<?php echo $this->input->get("date") ?>';
            $("#filter-tanggal").css("display", "block");
            $("#selectTanggal").val(date);
            $('#filter-tanggal .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        } else if(param == "lokasi"){
            var location = '<?php echo $this->input->get("location") ?>';
            $("#filter-lokasi").css("display", "block");
            selectLokasi("#selectLokasi", location);
        } else if(param == "spk"){
            var spk = '<?php echo $this->input->get("spk") ?>';
            $("#filter-spk").css("display", "block");
            selectSpk("#selectSpk", spk);
        } else if(param == "bulan"){
            $("#filter-bulan").css("display", "block");
            $("#filter-tahun").css("display", "block"); 
        }
    else if(param == "tahun"){
            $("#filter-bulan").css("display", "none");
            $("#filter-tahun").css("display", "block"); 
        }
        $("#tableSpk").dataTable();
        $("#selectType").select2();
    });

    $(document).on("click", "#filterSpk", function(e){
        var filter = $("#selectType").val();
        if(filter == "tanggal"){
            var tanggal = $("#selectTanggal").val();
            window.location = "<?php echo base_url('excel?') ?>param="+filter+"&date="+tanggal;
        } else if(filter == "lokasi"){
            var lokasi = $("#selectLokasi").val();
            if(lokasi == ""){
                alert("Silahkan Pilih Lokasi Terlebih Dulu");
            } else {
                window.location = "<?php echo base_url('excel?') ?>param="+filter+"&location="+lokasi;
            }
            
        } else if(filter == "spk"){
            var spk = $("#selectSpk").val();
            if(spk == ""){
                alert("Silahkan Pilih Spk Terlebih Dulu");
            } else {
                window.location = "<?php echo base_url('excel?') ?>param="+filter+"&spk="+spk;
            }
        } else if(filter == "bulan"){
            var bulan = $("#selectBulan").val();
            var tahun = $("#selectTahun").val();
            window.location = "<?php echo base_url('excel?') ?>param="+filter+"&bulan="+bulan+"&tahun="+tahun;
        }  else if(filter == "tahun"){
           
            var tahun = $("#selectTahun").val();
            window.location = "<?php echo base_url('excel?') ?>param="+filter+"&bulan="+bulan+"&tahun="+tahun;
        }
    });

    $(document).on("change", "#selectType", function(e){
        var type = $("#selectType").val();
        if(type == "tanggal"){
            $("#filter-tanggal").css("display", "block");
            $("#filter-lokasi").css("display", "none");
            $("#filter-spk").css("display", "none");
            $("#filter-bulan").css("display", "none");
            $("#filter-tahun").css("display", "none");
            $('#filter-tanggal .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                format: 'dd/mm/yyyy'
            });
        } else if(type == "lokasi"){
            $("#filter-tanggal").css("display", "none");
            $("#filter-lokasi").css("display", "block");
            $("#filter-spk").css("display", "none");
            $("#filter-bulan").css("display", "none");
            $("#filter-tahun").css("display", "none");
            selectLokasi("#selectLokasi");
        } else if(type == "spk"){
            $("#filter-tanggal").css("display", "none");
            $("#filter-lokasi").css("display", "none");
            $("#filter-spk").css("display", "block");
            $("#filter-bulan").css("display", "none");
            $("#filter-tahun").css("display", "none");
            selectSpk("#selectSpk");
        } else if(type == "bulan"){
            $("#filter-tanggal").css("display", "none");
            $("#filter-lokasi").css("display", "none");
            $("#filter-spk").css("display", "none");
            $("#filter-bulan").css("display", "block");
            $("#filter-tahun").css("display", "block");
            $("#selectBulan").select2();
            $("#selectTahun").select2();
        } else if(type == "tahun"){
            $("#filter-tanggal").css("display", "none");
            $("#filter-lokasi").css("display", "none");
            $("#filter-spk").css("display", "none");
            $("#filter-bulan").css("display", "none");
            $("#filter-tahun").css("display", "block");
            
            $("#selectTahun").select2();
        }
    else {
            $("#filter-tanggal").css("display", "none");
            $("#filter-lokasi").css("display", "none");
            $("#filter-spk").css("display", "none");
            $("#filter-bulan").css("display", "none");
            $("#filter-tahun").css("display", "none");
        }
    });

    function selectLokasi(element, val = null){
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?php echo base_url() ?>spk/lokasiUser/",
            "method": "GET"
        }).done(function (response) {
            var data = JSON.parse(response);
            var dataLokasi = data.data;
            var lokasi = '<option value="">Pilih Lokasi User</option>';
            lokasi += '<option value="00">GAE</option>';
            dataLokasi.forEach(function (data, index){
                lokasi += '<option value="'+data.kode_mitra+'">'+data.nm_mitra+'</option>';
            });
            $(element).html(lokasi);
            $(element).val(val);
            $(element).select2();
        }).fail(function (data){
            alert(data);
        });
    }
    
    function selectSpk(element, val = null){
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?php echo base_url() ?>spk/listSpk/",
            "method": "GET"
        }).done(function (response) {
            var data = JSON.parse(response);
            var dataSpk = data.data;
            var spk = '<option value="">Pilih SPK</option>';
            spk += '<option value="00">Tidak Menggunakan SPK</option>';
            dataSpk.forEach(function (data, index){
                spk += '<option value="'+data.id_spk+"."+data.kode_spk+'">'+data.no_spk+'</option>';
            });
            $(element).html(spk);
            $(element).val(val);
            $(element).select2();
        }).fail(function (data){
            alert(data);
        });
    }
    function hapusSpk(id){
        var hapus = confirm("Apakah anda yakin ingin menghapus?");
        if(hapus){
            window.location = "<?php echo base_url('spk/action_hapus/') ?>"+id;
        }
    }
</script>