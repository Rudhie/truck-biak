<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>User Management</h2>
        <ol class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li class="active"><strong>User Management</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <button class="btn btn-sm btn-success" id="btn_add_data">
                        <i class="fa fa-plus"></i> Tambah User Baru
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table_user" width="100%">
                            <thead class="gradient">
                                <tr>
                                    <th width="7%">No</th>
                                    <th>Nama Lengkap</th>
                                    <th>Email</th>
                                    <th>Nomor Telfon</th>
                                    <th>Username</th>
                                    <th>Level User</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-peserta">Tambah Data User</h6>
            </div>
            <div class="modal-body">
                <form id="formAddUser" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Lengkap</label>
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" name="id_user" id="id_user" placeholder="Nama Lengkap" required>
                            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Nama Lengkap" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-7">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nomor Telfon</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="no_telfon" id="no_telfon" placeholder="Nomor Telfon" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Level User</label>
                        <div class="col-sm-7">
                            <select class="form-control" name="level_user" id="level_user">
                                <option>Pilih Level User</option>
                                <option value="ADMIN">ADMIN</option>
                                <option value="MANAGER LOKASI">MANAGER LOKASI</option>
                                <option value="MANAGER REGION">MANAGER REGION</option>
                                <option value="PEMILIK">PEMILIK</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Username</label>
                        <div class="col-sm-7">
                            <input type="text" minlength="6" class="form-control" name="username" id="username" placeholder="Username" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-7">
                            <input type="password" class="form-control" name="password" id="password-user" placeholder="Password Baru">
                            <span toggle="#password-user" class="fa fa-fw fa-eye field-icon toggle-password-user"></span>
                            <sup id="note_password"></sup>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-7">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        var table_user = $("#table_user").DataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('user/get_data_user') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returnDataUser = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returnDataUser.push({
                                "no" : (i+1),
                                "fullname" : item["fullname"],
                                "email" : item["email"],
                                "no_telfon" : item["no_telfon"],
                                "username" : item["username"],
                                "nm_level_user" : item["level_user"],
                                "action" : "<center><button id='btn_edit' data-id_user='"+item["id_user"]+"' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> Ubah</button>&nbsp;"+
                                            "<button id='btn_delete' data-id_user='"+item["id_user"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Hapus</button></center>"
                            });
                        });
                    }
                    return returnDataUser;
                }
            },
            columns : [
                {data : "no"},
                {data : "fullname"},
                {data : "email"},
                {data : "no_telfon"},
                {data : "username"},
                {data : "nm_level_user"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddUser')[0].reset();
        $("#id_user").val("");
        $("#note_password").html("");
        $("#modalAdd").modal("show");
    });

    $(document).on("click", "#btn_edit", function(e){
        e.preventDefault();
        $('#formAddUser')[0].reset();
        var id_user = $(this).data("id_user");
        $.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('user/get_data_user') ?>/"+id_user,
			"method": "GET",
		}).done(function (response) {
            var data = JSON.parse(response);
            $("#id_user").val(data.data[0].id_user);
            $("#fullname").val(data.data[0].fullname);
            $("#email").val(data.data[0].email);
            $("#no_telfon").val(data.data[0].no_telfon);
            $("#username").val(data.data[0].username);
            $("#level_user").val(data.data[0].level_user).change();
            $("#note_password").html("Jika tidak mengubah password, kolom ini kosongkan saja");
            $("#modalAdd").modal("show");
		});
    });
    

    $(document).on("submit", "#formAddUser", function(e){
        e.preventDefault();
        var id_user = $("#id_user").val();
        var url = (id_user == "" ? "add_data_user" : "update_data_user");
		$.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('user') ?>/"+url,
			"method": "POST",
			"data": $(this).serialize(),
		}).done(function (response) {
            var data = JSON.parse(response)
			var message = data.message;
			if(data.status == "success"){
                $("#modalAdd").modal("hide");
				swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#table_user").DataTable().ajax.reload();
                });
                
			} else {
				swal("Gagal menambahkan.", message.toUpperCase(), "warning");
			}
		});
	});

    $(document).on("click", ".toggle-password-user", function() {
        $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });

    $(document).on("click", "#btn_delete", function(e){
        var id_user = $(this).data("id_user");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data user tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('user/delete_data_user') ?>",
                    "method": "POST",
                    "data": {
                        "id_user": id_user
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#table_user").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>