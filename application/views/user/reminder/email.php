<html>
    <head>
        <meta charset=”utf-8″ />
        <title>Codeigniter email template</title>
        <meta name=”viewport” content=”width=device-width, initial-scale=1.0″ />
    </head>
    <body>
        <div style="background-color:#e8e8e8;padding: 5%;">
            <div style="width: 100%;height: 80px;background: -webkit-linear-gradient(45deg, #008cc9,#009ea5);
                background: -moz-linear-gradient(45deg, #008cc9,#009ea5);
                background: -o-linear-gradient(45deg, #008cc9,#009ea5);
                background: linear-gradient(45deg, #008cc9,#009ea5);
                border-radius: 10px 10px 0px 0px;">
                <center><img src="<?= base_url('asset/img/logo-pertamina.png') ?>" width="200px;" style="margin-top:15px"/></center>
            </div>
            <div style="background-color:#ffffff;padding: 2%;">
            <center>
                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;"><p>Kepada Yth. Bapak/Ibu Pemilik ...........</p>
                <p>Berdasarkan pada data yang ada pada sistem monitoring kami, telah ditemukan beberapa mobil truck yang memperlukan pembaruan data serta dokumen.<br/>
                Segera lakukan pembaruan dokumen truck dan melaporkan hasil pembaruan kedalam sistem monitoring.
                </p>
                <p>Berikut Mobil Truck yang harus dilakukan pembaharuan data : </p>
                </span>
                <table width="50%" border="1">
                    <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="40%">Nomor Polisi</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=1;$i<5;$i++){ ?>
                            <tr>
                                <td style="text-align:center"><?= $i; ?></td>
                                <td style="text-align:center">W <?= rand(1000,9999) ?> DD</td>
                                <td style="text-align:center">Perpanjangan STNK Kendaraan</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;">
                    <p>Mohon untuk Bapak/Ibu Pemilik ...... untuk segera menindaklanjuti pemberitahuan ini.</p>
                </span>
                <span style="font-size: 13px;font-style: normal;font-family: sans-serif;color: #7b7b7b;">Terima Kasih</span>
            </center>
            </div>
            <div style="color: #a6a6a6;font-family: sans-serif;font-size: 12px;">
                <center>
                    <p>&copy; <?= date('Y') ?> Sistem Informasi dan Monitoring Truck Tangki</p>
                    <p>PT. Pertamina (persero) Fuel Terminal Biak</p>
                </center>
            </div>
        </div>
    </body>
</html>