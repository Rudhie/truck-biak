<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins" style="text-align:center;">
                <div class="ibox-title" style="border-color: #76b68e;">Transportir
                </div>
                <div class="ibox-content" style="background-color: #76b68e;color:white;">
                    <h1 class="no-margins"><b><?= $pemilik->jumlah ?></b></h1>
                    <small>Perusahaan Transportir</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins" style="text-align:center;">
                <div class="ibox-title" style="border-color: #79abd5;">Mobil Tangki
                </div>
                <div class="ibox-content" style="background-color: #79abd5;color:white;">
                    <h1 class="no-margins"><b><?= $truck->jumlah ?></b></h1>
                    <small>Total Mobil Tangki</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins" style="text-align:center;">
                <div class="ibox-title" style="border-color: #ef8fab;">Awak Mobil Tangki
                </div>
                <div class="ibox-content" style="background-color: #ef8fab;color:white;">
                    <h1 class="no-margins"><b><?= $supir_kernet->jumlah ?></b></h1>
                    <small>Jumlah Awak Mobil Tangki</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins" style="text-align:center;">
                <div class="ibox-title" style="border-color: #e3a688;">Pengajuan Perpanjangan
                </div>
                <div class="ibox-content" style="background-color: #e3a688;color:white;">
                    <h1 class="no-margins"><b><?= $aktifitas->jumlah ?></b></h1>
                    <small>Pengajuan Perpanjangan Dokumen</small>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="chart_rekap_perpanjangan"></div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="modalDetailInfo" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-peserta">Detail Informasi</h6>
            </div>
            <div class="modal-body">
                <h3><b>KETERANGAN</b> : <span id="title-info"></span></h3>
                <h3><b>MASA BERLAKU</b> : <span id="start-info"></span></h3>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        chart_rekap_perpanjangan();
        kalender_masa_aktif();
    });

    function kalender_masa_aktif(){
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('dashboard/kalender_masa_aktif') ?>",
            "method": "POST",
        }).done(function (response) {
            var data = JSON.parse(response);
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: data.data,
                eventClick: function(info){
                    var date = new Date(info.start);
                    var tgl = ((date.getDate() > 9) ? date.getDate() : ('0' + date.getDate())) + '/' + ((date.getMonth() > 8) ? (date.getMonth() + 1) : ('0' + (date.getMonth() + 1))) + '/' + date.getFullYear();
                    $("#title-info").html("MASA BERLAKU "+info.title);
                    $("#start-info").html(tgl);
                    $("#modalDetailInfo").modal("show");
                }
            });
        });
    }

    function chart_rekap_perpanjangan(){
        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('dashboard/rekap_pengajuan') ?>",
            "method": "POST",
        }).done(function (response) {
            var data = JSON.parse(response);
            Highcharts.chart('chart_rekap_perpanjangan', {
                title: {
                    text: 'Rekap Jumlah Permohonan Perpanjangan'
                },
                subtitle: {
                    text: 'Tahun 2020'
                },
                yAxis: {
                    title: {
                        text: 'Jumlah Permohonan'
                    }
                },
                xAxis: {
                    accessibility: {
                        rangeDescription: 'Range: 2010 to 2017'
                    }
                },
                xAxis: {
                    categories: data.categories
                },
                series: [{
                    name: 'Permohonan Perpanjangan Truck',
                    data: data.truck
                }, {
                    name: 'Permohonan Perpanjangan AMT',
                    data: data.amt
                }],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
        });

        
    }
</script>