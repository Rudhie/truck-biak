<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-truck"></i> Master Data Transportir Truck Tanki</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('#'); ?>">Data Transportir Truck Tanki</a></li>
            <li class="active"><strong>Tambah Transportir</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                            <form action="<?php echo base_url('pemilik/action_tambah'); ?>" method="POST" role="form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Transportir</label>
                                            <input type="text" name="nama_pemilik" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nama Perusahaan</label>
                                            <input type="text" name="nama_perusahaan" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email Aktif</label>
                                            <input type="text" name="email_aktif" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>No telfon</label>
                                            <input type="text" name="no_telfon" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                            <a href="<?php echo base_url('pemilik') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                            <input type="reset" value="RESET" class="btn btn-sm btn-warning" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'



        });
        $("#inputmasking1, #inputmasking2, #inputmasking3, #inputmasking4, #inputmasking5, #inputmasking6, #inputmasking7, #inputmasking8").mask("000.000.000.000", {reverse: true});
    });

    $("#percentmasking").mask("##0,00", {reverse: true});
</script>