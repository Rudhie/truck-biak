<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i> Transportir Truck Tanki </h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Transportir Truck Tanki</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo base_url('pemilik/tambah'); ?>" class="btn btn-outline btn-info dim"><i class="fa fa-plus"></i> Tambah</a>
                    <a href="<?php echo base_url('pemilik/export'); ?>" class="btn btn-outline btn-info dim"><i class="fa fa-print"></i> Export Excel</a>
                </div>
                <div class="ibox-content">
                <?php if($this->session->flashdata("success")){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("success"));
                            unset($_SESSION["success"]);
                        ?>
                    </div>
                <?php } else if($this->session->flashdata("error")) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("error"));
                            unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php } ?>
                    <div class="table-responsive">
                    <table id="tablePemilik" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th><center>Nama Transportir</center></th>
                        <th><center>Nama Perusahaan</center></th>
                        <th><center>Email</center></th>
                        <th><center>No. Telephone</center></th>
                        <th width="15%"><center>Aksi</center></th>
                <!-- <?php if($this->session->userdata('role')=='admin'){?>
                        
                        <th width="15%"><center>Aksi</center></th>
                <?php } ?> -->
                    </tr>
                   </thead>
                    <tbody>
                 
                <?php $i=1;
                   foreach($pemilik as $p){
                    echo'<tr >';
                    echo'<td>'.$i.'</td>';
                    echo'<td>'.'<a>'.ucfirst($p->nama_pemilik).'</a>'.'</td>';
                    echo'<td>'.$p->nama_perusahaan.'</td>';
                    echo'<td>'.$p->email_aktif.'</td>';
                    echo'<td>'.$p->no_telfon.'</td>';

                     // if($this->session->userdata('role') == 'admin'){
                        echo'<td><a href="'.base_url('pemilik/edit').'?id='.$p->id_pemilik.'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> UBAH</a> '.'<a onclick="hapusPemilik('.$p->id_pemilik.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a> </td>';
                    // }
                    echo'</tr>';
                    $i++;

                    }
                    ?>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        $("#tablePemilik").dataTable();
    });

    function hapusPemilik(id){
        var hapus = confirm("Apakah anda yakin ingin menghapus?");
        if(hapus){
            window.location = "<?php echo base_url('pemilik/action_hapus/') ?>"+id;
        }
    }
</script>