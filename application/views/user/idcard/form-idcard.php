<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Tambah ID CARD AMT</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li>ID CARD AMT</li>
            <li class="active"><strong>Tambah Pengajuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="form" class="wizard-big" action="<?= base_url('perpanjangan/action_tambah') ?>" enctype="multipart/form-data" method="POST">
                        <h1>Data Perpanjangan</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Jenis Keperluan</label>
                                        <input type="hidden" name="type" value="<?= $this->input->get('page'); ?>" />
                                        <input type="hidden" name="idcard" value="idcard" />
                                        <select name="id_keperluan" class="form-control m-b" id="id_keperluan" required>
                                            <option value="">Pilih Keperluan</option>
                                            <?php foreach($keperluan as $k_key => $k_val){ ?>
                                            <option value="<?= $k_val->id_keperluan ?>" data-type="<?= $k_val->type_keperluan ?>"><?= $k_val->nm_keperluan;?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group" id="form-truck" style="display:none;">
                                        <label>Mobil Tangki</label>
                                        <select name="id_truck" class="form-control m-b" id="id_truck">
                                            <option value="">Pilih Truck</option>
                                            <?php foreach($truck as $t_key => $t_val){ ?>
                                            <option value="<?= $t_val->id_truck ?>"><?= $t_val->no_polisi." - ".$t_val->nm_merek; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group" id="form-supir" style="display:none;">
                                        <label>Awak Mobil Tangki</label>
                                        <select name="id_supir" class="form-control m-b" id="id_supir">
                                            <option value="">Pilih Awak</option>
                                            <?php foreach($supir as $s_key => $s_val){ ?>
                                            <option value="<?= $s_val->id_awak ?>"><?= $s_val->nama_awak." - ".$s_val->bagian; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Detail Keperluan</label>
                                        <textarea  class="form-control" id="detail_keperluan" name="detail_keperluan" placeholder="Detail Keterangan Pengajuan" rows="5" required></textarea>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-group" id="data_1">
                                            <label>Tanggal Pengajuan</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="tgl_aktifitas" class="form-control" value="<?php echo date('d/m/Y')?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Tingkat Kepentingan</label>
                                        <select name="tingkat_keperluan" class="form-control m-b" id="tingkat_keperluan" required>
                                            <option value="">Pilih Tingkat</option>
                                            <option value="LOW">LOW</option>
                                            <option value="MEDIUM">MEDIUM</option>
                                            <option value="URGENT">URGENT</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Catatan / Komentar</label>
                                        <textarea class="form-control" rows="3" name="komentar" placeholder="Catatan / Komentar"></textarea>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <h1>Checklist Dokumen</h1>
                        <fieldset>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="form_persyaratan"></div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="modalNote" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title">Catatan Tindak Lanjut</h6>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="5"  id="note_reject" placeholder="Pesan atau catatan reject"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-note-reject" class="btn btn-info">Simpan Catatan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        $("#form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex){
                if (currentIndex > newIndex){
                    return true;
                }
                var form = $(this);
                if (currentIndex < newIndex){
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex){
                var form = $(this);
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex){
                var form = $(this);
                form.submit();
            },
            onCanceled: function(evenet, currentIndex){
                window.location = "<?= base_url('idcard/') ?>";
            },
        }).validate({
            errorPlacement: function (error, element){
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });

        $("#id_keperluan, #tingkat_keperluan").select2();
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format:'dd/mm/yyyy'
        });

        var page = "<?= $this->input->get('page'); ?>";
        if(page == "truck"){
            $("#form-truck").css("display", "block");
            $("#form-supir").css("display", "none");
            $("#id_truck").select2();
        } else {
            $("#form-supir").css("display", "block");
            $("#form-truck").css("display", "none");
            $("#id_supir").select2();
        }
    });

    function readURL(input, id, form) {
        var typeFile = input.files[0].type;
        var size = Math.round(input.files[0].size / 1024);
        if(form == "#dokumen"){
            if(typeFile != "application/pdf"){
                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
                $(form).val('');
            }
        } else {
            if(typeFile != "image/jpeg" && typeFile != "image/png"){
                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
                $(form).val('');
            }
        }
        
        if(size > 2048){
            swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");
            $(form).val('');
        } else {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    }

    $(document).on("change", "#id_keperluan", function(e){
        form_persyaratan($(this).val());
    });

    function form_persyaratan(id_keperluan){
        $.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('perpanjangan/persyaratan') ?>/"+id_keperluan,
			"method": "GET",
		}).done(function (response) {
            var data = JSON.parse(response);
            var syarat = data.data;
            var docSyarat = '';
            syarat.forEach(function(i, e){
                docSyarat += '<div class="form-group">';
                docSyarat += '<label>'+i.nm_syarat+'</label>';
                docSyarat += '<input type="hidden" value="'+i.id_syarat+'" name="id_syarat[]" >';
                docSyarat += '&nbsp;<sub>'+i.keterangan+'</sub>';
                docSyarat += '<input type="file" name="dokumen'+i.id_syarat+'" id="dokumen'+i.id_syarat+'" onchange="listDocument(this, '+i.id_syarat+')" class="form-control" required>';
                docSyarat += '<ul class="sortable-list connectList agile-list" style="width:50%;margin-top:5px;display:none;" id="preview-dokumen-'+i.id_syarat+'">';
                docSyarat += '<li class="success-element" style="list-style-type: none;padding:5px;" id="filename'+i.id_syarat+'">';
                docSyarat += '</li></ul></div>';
            });
            $("#form_persyaratan").html(docSyarat);
		});
    }

    function listDocument(input, id){
        var file = input.files[0];
        $("#filename"+id).html("<i class='fa fa-file'></i> "+file.name);
        $("#preview-dokumen-"+id).css("display", "block");
    }
</script>