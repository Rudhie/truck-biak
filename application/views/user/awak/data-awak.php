<style>
    .select2-container .select2-selection--single{
        height:34px !important;
    }
    .select2-container--default .select2-selection--single{
       border: 1px solid #ccc !important; 
       border-radius: 0px !important; 
   }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-list-alt"></i> Awak </h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Awak</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo base_url('awak/tambah'); ?>" class="btn btn-outline btn-info dim"><i class="fa fa-plus"></i>Tambah</a>
                </div>
                <div class="ibox-title">
                <?php if($this->session->userdata("level_user") != "PEMILIK"){ ?>
                    <div class="row">
                        <form method="get" action="">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div id="form-filterTransportir">
                                        <label>Transportir</label>
                                        <select name="filterTransportir" class="form-control m-b select2">
                                            <option value="">Pilih Transportir</option>
                                            <?php
                                            $filterTransportir = "";
                                            if (isset($_GET['filterTransportir']) && ! empty($_GET['filterTransportir'])) {
                                                $filterTransportir = $_GET['filterTransportir'];
                                            }
                                            foreach($transportir as $p){
                                                $selected = '';
                                                if($p->id_pemilik == $filterTransportir){
                                                    $selected = 'selected';
                                                }
                                                echo '<option value="'.$p->id_pemilik.'"'.$selected.'>'.$p->nama.'</option>';
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-primary">Tampilkan</button>
                                    <input type="reset" value="RESET" class="btn btn-sm btn-warning"/>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-2">
                                <?php $link = explode("?",$_SERVER['REQUEST_URI']); ?>
                                <a href="<?php echo base_url('awak/export?').(count($link) > 1 ? $link[1] : ''); ?>" class="btn btn-sm btn-info"><i class="fa fa-print"></i> Export Excel</a>                                            
                            </div>
                            </div>
                        <?php } ?>
                        <div class="ibox-content">
                            <?php if($this->session->flashdata("success")){ ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                    <?php
                                    echo strtoupper($this->session->flashdata("success"));
                                    unset($_SESSION["success"]);
                                    ?>
                                </div>
                            <?php } else if($this->session->flashdata("error")) { ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                    <?php
                                    echo strtoupper($this->session->flashdata("error"));
                                    unset($_SESSION["error"]);
                                    ?>
                                </div>
                            <?php } ?>
                            <div class="table-responsive">
                                <table id="tableAwak" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="gradient">
                                        <tr>
                                            <th width="5%">No</th>
                                            <th><center>NIP AMT</center></th>
                                            <th><center>Nama AMT</center></th>
                                            <th><center>Fungsi</center></th>
                                            <th><center>Transportir</center></th>
                                            <th><center>Nopol Tanki</center></th>
                                            <th><center>Produk</center></th>
                                            <th><center>Tgl Lahir AMT</center></th>
                                            <th><center>Masa Berlaku SIM AMT</center></th>
                                            <th><center>Dokumen SIM AMT</center></th>
                                            <th><center>Masa Berlaku Id Card AMT</center></th>
                                            <th><center>Dokumen Id Card AMT</center></th>
                                            
                                            <th width="15%"><center>Aksi</center></th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php $i=1;
                                        foreach($awak as $p){
                                            echo'<tr >';
                                            echo'<td>'.$i.'</td>';
                                            echo'<td>'.$p->nip_awak.'</td>';
                                            echo'<td>'.$p->nama_awak.'</td>';
                                            echo'<td>'.$p->bagian.'</td>';
                                            echo'<td>'.ucfirst($p->nm_pemilik).'</td>';
                                            echo'<td>'.$p->nomor_polisi.'</td>';
                                            echo'<td>'.$p->list_produk.'</td>';
                                            echo'<td>'.date('d/F/Y', strtotime($p->tgl_lahir_awak)).'</td>';
                                            echo'<td>'.date('d/F/Y', strtotime($p->masa_berlaku_sim_awak)).'</td>';
                                            echo'<td>'.$p->dokumen_sim_awak.'</td>';
                                            echo'<td>'.date('d/F/Y', strtotime($p->masa_berlaku_idcard_awak)).'</td>';
                                            echo'<td>'.$p->dokumen_idcard_awak.'</td>';
                                            
                                            echo'<td><a href="'.base_url('awak/edit').'?id='.$p->id_awak.'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> UBAH</a> '.'<a onclick="hapusAwak('.$p->id_awak.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a> </td>';
                                            echo'</tr>';
                                            $i++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
    $('.select2').select2();
</script>
        <script>
            $(document).ready(function(){
                $("#tableAwak").dataTable({
                    "scrollX": true
                });
            });

            function hapusAwak(id){
                var hapus = confirm("Apakah anda yakin ingin menghapus?");
                if(hapus){
                    window.location = "<?php echo base_url('awak/action_hapus/') ?>"+id;
                }
            }
        </script>