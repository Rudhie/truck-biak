<?php
    function tanggal_indo($tanggal){
        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }
?>
<style>
    blockquote {
        font-size: 14px;
        border-left: 2px solid #33a4f7;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Approve Pengajuan Perpanjangan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li class="active"><strong>Approve Perpanjangan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?= $this->input->get("page") == "idcard" ? base_url('idcard') : base_url('perpanjangan/'.$this->input->get('page')); ?>" class="btn btn-xs btn-primary"><i class="fa fa-arrow-left"></i> KEMBALI</a>
                </div>
                <div class="ibox-content">
                    <?php if($this->session->flashdata("success")){ ?>
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("success"));
                                unset($_SESSION["success"]);
                            ?>
                        </div>
                    <?php } else if($this->session->flashdata("error")) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("error"));
                                unset($_SESSION["error"]);
                            ?>
                        </div>
                    <?php } ?>
                    <table class="table table-bordered" style="width:70%;margin-left:auto;margin-right:auto;">
                        <tr>
                            <th style="text-align:center;" colspan="3">
                                <img src="<?= base_url('asset/img/pertamina.png'); ?>" width="8%" style="float:left"/>
                                <h3>TINDAK LANJUT PENGAJUAN PERPANJANGAN TRUCK</h3>
                                <h3>NO : <?= $perpanjangan[0]->no_aktifitas ?></h3>
                            </th>
                        </tr>
                        <tr><th style="text-align:center;background-color:#6777ef;color:white;" colspan="3">General Information</th></tr>
                        <tr>
                            <th>Transportir</th>
                            <td colspan="2"><?= $perpanjangan[0]->nama_pemilik ?></td>
                        </tr>
                        <tr>
                            <th>Mobil Tangki</th>
                            <td colspan="2"><?= $perpanjangan[0]->no_polisi ?></td>
                        </tr>
                        <tr>
                            <th>Keperluan</th>
                            <td colspan="2"><?= $perpanjangan[0]->nm_keperluan ?></td>
                        </tr>
                        <tr>
                            <th>Detail Keperluan</th>
                            <td colspan="2"><?= strtoupper($perpanjangan[0]->detail_keperluan) ?></td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengajuan</th>
                            <td colspan="2"><?= tanggal_indo($perpanjangan[0]->tgl_aktifitas) ?></td>
                        </tr>
                        <tr>
                            <th>Tingkat Kepentingan</th>
                            <td colspan="2"><?= $perpanjangan[0]->tingkat_keperluan ?></td>
                        </tr>
                        <tr>
                            <th>Komentar</th>
                            <td colspan="2"><?= strtoupper($perpanjangan[0]->komentar) ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td colspan="2">
                                <?php 
                                    $status = $perpanjangan[0]->status_aktifitas;
                                    if($status == "0"){
                                        echo "MENUNGGU KONFIRMASI";
                                    } else if($status == "1"){
                                        echo "APPROVE ADMIN";
                                    } else if($status == "2"){
                                        echo "REJECT ADMIN";
                                    } else if($status == "3"){
                                        echo "APPROVE MANAGER LOKASI";
                                    } else if($status == "4"){
                                        echo "REJECT MANAGER LOKASI";
                                    } else if($status == "5"){
                                        echo "APPROVE MANAGER REGION";
                                    } else if($status == "6"){
                                        echo "REJECT MANAGER REGION";
                                    } else if($status == "7"){
                                        echo "PENGAJUAN ULANG / REVISI";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Catatan</th>
                            <td colspan="2"><?= strtoupper($perpanjangan[0]->catatan) ?></td>
                        </tr>
                        <tr><th style="text-align:center;background-color:#6777ef;color:white;" colspan="3">Checklist Dokumen</th></tr>
                        <form action="<?= base_url('perpanjangan/action_approve/').$this->input->get("id") ?>" method="POST">
                        <?php
                            $level_user = $this->session->userdata('level_user');
                            foreach($dok_perpanjangan as $dokumen){ ?>
                        <tr>
                            <th><?= $dokumen->nm_syarat ?></th>
                            <td style="text-align:center;"><button type="button" id="btn-dokumen" data-title_dokumen="<?= $dokumen->nm_syarat ?>" data-nama_dokumen="<?= $dokumen->dokumen_syarat ?>" data-format_dokumen="<?= $dokumen->format_dokumen ?>" class="btn btn-xs btn-info">View Dokumen</button></td>
                            <td>
                                <?php if($level_user == "ADMIN"){ ?>
                                <div>
                                    <input type="hidden" value="<?= $dokumen->id_dokumen ?>" name="id_dokumen[]" >
                                    <?php if($status == "0" || $status == "7"){ ?>
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" id="inlineRadio" value="Y" name="konfirmasi[<?= $dokumen->id_dokumen ?>]" required>
                                        <label> Approve</label>
                                    </div>
                                    <div class="radio radio-danger radio-inline">
                                        <input type="radio" id="reject-form" data-id_dokumen="<?= $dokumen->id_dokumen ?>" value="N" name="konfirmasi[<?= $dokumen->id_dokumen ?>]" required>
                                        <label> Reject</label>
                                    </div>
                                    <?php } ?>
                                    <input type="<?= ($status == "4" ? "text" : "hidden") ?>" class="form-control" id="form_note_reject<?= $dokumen->id_dokumen ?>" name="note_reject[]" value="<?= $dokumen->note_approval_admin ?>" placeholder="Tulis review dokumen disini"/>
                                    <ul class="sortable-list connectList agile-list" id="todo<?= $dokumen->id_dokumen ?>" style="margin:0;display:none;">
                                        <li class="danger-element" id="note<?= $dokumen->id_dokumen ?>"></li>
                                    </ul>
                                </div>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                        <?php 
                            if((($status == "0" || $status == "4" || $status == "7") && $level_user == "ADMIN") || ($status == "1" && $level_user == "MANAGER LOKASI") || ($status == "3" && $level_user == "MANAGER REGION")){
                                if( ($status == "1" && $level_user == "MANAGER LOKASI") || ($status == "3" && $level_user == "MANAGER REGION")){
                            ?>
                            <tr><th style="text-align:center;background-color:#6777ef;color:white;" colspan="3">Konfirmasi Tindak Lanjut</th></tr>
                            <tr>
                                <td colspan="3" style="text-align:center;">
                                    <div class="radio radio-info radio-inline">
                                        <input type="radio" value="Y" name="konfirmasi" required>
                                        <label> Approve</label>
                                    </div>
                                    <div class="radio radio-danger radio-inline">
                                        <input type="radio" value="N" name="konfirmasi" required>
                                        <label> Reject</label>
                                    </div>
                                </td>
                            </tr>
                            <?php } ?>
                        <tr>
                            <td colspan="3">
                                <textarea class="form-control" name="general_note" rows="3" placeholder="Tulisankan catatan untuk pengajuan tersebut (jika ada)"><?= $perpanjangan[0]->catatan ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <input type="hidden" value="<?= $this->input->get('page') ?>" name="page" >
                                <button type="submit" class="btn btn-sm btn-info btn-block"><i class="fa fa-save"></i> SIMPAN</button>
                            </td>
                        </tr>
                        <?php } ?>
                        </form> 
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="modalDokumen" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-data-dokumen"></h6>
            </div>
            <div class="modal-body">
                <div id="dokumen-perpanjangan-view"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="modalNote" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title">Catatan Tindak Lanjut</h6>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="5"  id="note_reject" placeholder="Pesan atau catatan reject"></textarea>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-note-reject" class="btn btn-info">Simpan Catatan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".alert").delay(4000).slideUp(200);
    });

    $(document).on("click", "#btn-dokumen", function(e){
        var title_dokumen = $(this).data("title_dokumen");
        var nama_dokumen = $(this).data("nama_dokumen");
        var format_dokumen = $(this).data("format_dokumen");
        if(format_dokumen == ".pdf"){
            var view = '<iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="<?= base_url() ?>asset/pdfviewer/web/viewer.html?file=<?= base_url('dokumen/perpanjangan/') ?>' + nama_dokumen+ '" scrolling="no"></iframe>';
        } else {
            var view = '<center><img class="img-thumbnail" src="<?= base_url() ?>dokumen/perpanjangan/'+nama_dokumen+'" /></center>';
        }
        $("#dokumen-perpanjangan-view").html(view);
        $("#title-data-dokumen").html(title_dokumen);
        $("#modalDokumen").modal("show");
    });

    $(document).on("click", "#reject-form", function(e){
        var id = $(this).data("id_dokumen");

        var check = $("#form_note_reject"+id).val();
        if(check != ""){
            $("#note_reject").val(check);
        } else {
            $("#note_reject").val("");
        }
        $("#btn-note-reject").data("id_dokumen", id);
        $("#modalNote").modal("show");
    });

    $(document).on("click", "#btn-note-reject", function(e){
        var id = $(this).data("id_dokumen");
        var note = $("#note_reject").val();
        $("#todo"+id).css("display", "block");
        $("#note"+id).html(note);
        $("#form_note_reject"+id).val(note);
        $("#modalNote").modal("hide");
    });
</script>