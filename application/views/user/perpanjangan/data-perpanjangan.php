<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-6">
        <h2>Pengajuan Perpanjangan <?= $this->uri->segment('2') == 'truck' ? 'Mobil Tangki' : 'Awak'; ?></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li class="active"><strong>Perpanjangan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata("level_user") == "PEMILIK"){ ?>
                        <a href="<?= base_url('perpanjangan/tambah?page=').$this->uri->segment('2'); ?>" class="btn btn-sm btn-outline btn-success" id="btn_send_reminder">
                            <i class="fa fa-plus"></i> Ajukan Perpanjangan
                        </a>
                    <?php } ?>
                     
                </div>
                <div class="ibox-content">
                    <?php if($this->session->flashdata("success")){ ?>
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("success"));
                                unset($_SESSION["success"]);
                            ?>
                        </div>
                    <?php } else if($this->session->flashdata("error")) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("error"));
                                unset($_SESSION["error"]);
                            ?>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <form method="GET">
                        <?php if($this->session->userdata("level_user") != "PEMILIK"){ ?>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Transportir</label>
                                <select name="transportir" class="form-control m-b select2">
                                    <option value="">Pilih Transportir</option>
                                    <?php foreach($transportir as $t){ ?>
                                    <option value="<?= $t->id_pemilik ?>" <?= $this->input->get("transportir") == $t->id_pemilik ? 'selected' : '' ?>><?= $t->nama_perusahaan ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Bulan</label>
                                <select name="bulan" class="form-control m-b select2">
                                    <option value="">Pilih Bulan</option>
                                    <?php $listBulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"); ?>
                                    <?php for($bulan = 1; $bulan <= 12; $bulan++){ ?>
                                    <option value="<?= $bulan ?>" <?= $this->input->get("bulan") == $bulan ? 'selected' : '' ?>><?= $listBulan[$bulan - 1] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select name="tahun" class="form-control m-b select2">
                                    <option value="">Pilih Tahun</option>
                                    <?php for($tahun = 2020; $tahun <= date("Y"); $tahun++){ ?>
                                    <option value="<?= $tahun ?>" <?= $this->input->get("tahun") == $tahun ? 'selected' : '' ?>><?= $tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Status</label>
                                <select name="status" class="form-control m-b select2">
                                    <option value="">Pilih Status</option>
                                    <option value="0" <?= $this->input->get("status") == "0" ? 'selected' : '' ?>>MENUNGGU KONFIRMASI</option>
                                    <option value="1" <?= $this->input->get("status") == "1" ? 'selected' : '' ?>>APPROVE ADMIN</option>
                                    <option value="2" <?= $this->input->get("status") == "2" ? 'selected' : '' ?>>REJECT ADMIN</option>
                                    <option value="3" <?= $this->input->get("status") == "3" ? 'selected' : '' ?>>APPROVE MANAGER</option>
                                    <option value="4" <?= $this->input->get("status") == "4" ? 'selected' : '' ?>>REJECT MANAGER</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-1" style="margin-left:15px;">
                                <label><br/></label>
                                <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i> TAMPILKAN</button>
                            </div>

                             
                            <div class="col-lg-1" style="margin-left:15px;">
                                <label><br/></label>
                                <?php $link = explode("?",$_SERVER['REQUEST_URI']); ?>
                                <a href="<?php echo base_url('perpanjangan/export?').(count($link) > 1 ? $link[1].'&from='.$this->uri->segment(2) : 'from='.$this->uri->segment(2)); ?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Cetak Data</a>
                            </div>
                        </form>
                    </div>
                    
                    <div class="table-responsive">                    
                        <table class="table table-striped table-bordered table-hover" id="table_perpanjangan" width="100%">
                            <thead class="gradient">
                                <tr>
                                    <th>No</th>
                                    <th>Transportir</th>
                                    <th><?= $this->uri->segment('2') == 'truck' ? 'Mobil Tangki' : 'Awak'; ?></th>
                                    <th>Keperluan</th>
                                    <th>Tanggal Pengajuan</th>
                                    <th>Tingkat</th>  
                                    <th>Status</th>
                                    <th>Komentar</th>
                                    <th>Catatan</th>
                                    <th width="20%" style="text-align:center;">Aksi</th>                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i=1;
                                    
                                    function tanggal_indo($tanggal){
                                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                                        $split = explode('-', $tanggal);
                                        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                                    }
                                    foreach($perpanjangan as $key => $value){ 
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $value->nama_pemilik ?></td>
                                    <td><b><?= $this->uri->segment('2') == 'truck' ? $value->no_polisi : $value->nama_awak; ?></b></td>
                                    <td><?= $value->nm_keperluan ?></td>
                                    <td><?= tanggal_indo($value->tgl_aktifitas) ?></td>
                                    <td><?= $value->tingkat_keperluan ?></td>
                                    <td>
                                        <?php
                                            $status = $value->status_aktifitas;
                                            if($status == "0"){
                                                echo "MENUNGGU KONFIRMASI";
                                            } else if($status == "1"){
                                                echo "APPROVE ADMIN";
                                            } else if($status == "2"){
                                                echo "REJECT ADMIN";
                                            } else if($status == "3"){
                                                echo "APPROVE MANAGER LOKASI";
                                            } else if($status == "4"){
                                                echo "REJECT MANAGER LOKASI";
                                            } else if($status == "7"){
                                                echo "PENGAJUAN ULANG";
                                            }
                                        ?></td>
                                    <td style="font-size:10px;"><?= strtoupper($value->komentar) ?></td>
                                    <td style="font-size:10px;"><?= strtoupper($value->catatan) ?></td>
                                    <td style="text-align:center;">
                                        <?php 
                                            $status = $value->status_aktifitas;
                                            echo '<a href="'.base_url('perpanjangan/detail').'?id='.$value->id_aktifitas."&page=".$this->uri->segment('2').'" class="btn btn-xs btn-info"><i class="fa fa-info"></i> DETAIL</a>&nbsp;';
                                            if(($status == "0" || $status == "2" || $status == "4" ) && $this->session->userdata('level_user') == 'PEMILIK'){
                                                echo '<a href="'.base_url('perpanjangan/edit').'?id='.$value->id_aktifitas."&page=".$this->uri->segment('2').'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> '.($status == "0" ? "UBAH" : "REVISI").'</a>&nbsp;';
                                                echo '<a onclick="hapusPerpanjangan('.$value->id_aktifitas.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a>&nbsp;';
                                            }
                                        ?>
                                        <?php if(($status == "0" || $status == "7" || $status == "4") && $this->session->userdata('level_user') == 'ADMIN'){ ?>
                                            <a href="<?= base_url('perpanjangan/approve').'?id='.$value->id_aktifitas."&page=".$this->uri->segment('2'); ?>" class="btn btn-xs btn-primary"><i class="fa fa-legal"></i> <?= $status == "4" ? "REVIEW APPROVE MANAGER" : "TINDAKLANJUT" ?></a>
                                        <?php } ?> 
                                        <?php if($status == '1' && $this->session->userdata('level_user') == 'MANAGER LOKASI'){ ?>
                                            <a href="<?= base_url('perpanjangan/approve').'?id='.$value->id_aktifitas."&page=".$this->uri->segment('2'); ?>" class="btn btn-xs btn-primary"><i class="fa fa-legal"></i> TINDAKLANJUT</a>
                                        <?php } ?>                                
                                    </td>
                                </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        $("#table_perpanjangan").dataTable();
        $('.select2').select2();
    });

    function hapusPerpanjangan(id){
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                window.location = "<?php echo base_url('perpanjangan/action_hapus/') ?>"+id+"/<?= $this->uri->segment('2') ?>"; 
            }
        })
    }
</script>