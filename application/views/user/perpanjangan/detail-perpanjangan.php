<?php
    function tanggal_indo($tanggal){
        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }
?>
<style>
    blockquote {
        font-size: 14px;
        border-left: 2px solid #33a4f7;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Detail Pengajuan Perpanjangan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li class="active"><strong>Detail Perpanjangan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?= $this->input->get("page") == "idcard" ? base_url('idcard') : base_url('perpanjangan/'.$this->input->get('page')); ?>" class="btn btn-xs btn-primary"><i class="fa fa-arrow-left"></i> KEMBALI</a>
                </div>
                <div class="ibox-content">
                    <table class="table table-bordered" style="width:50%;margin-left:auto;margin-right:auto;">
                        <tr>
                            <th style="text-align:center;" colspan="3">
                                <h3>DETAIL PENGAJUAN PERPANJANGAN TRUCK</h3>
                                <h3>No : <?= $perpanjangan[0]->no_aktifitas ?></h3>
                            </th>
                        </tr>
                        <tr><th style="text-align:center;background-color:#6777ef;color:white;" colspan="3">General Information</th></tr>
                        <tr>
                            <th>Transportir</th>
                            <td colspan="2"><?= $perpanjangan[0]->nama_pemilik ?></td>
                        </tr>
                        <tr>
                            <th>Mobil Tangki</th>
                            <td colspan="2"><?= $perpanjangan[0]->no_polisi ?></td>
                        </tr>
                        <tr>
                            <th>Keperluan</th>
                            <td colspan="2"><?= $perpanjangan[0]->nm_keperluan ?></td>
                        </tr>
                        <tr>
                            <th>Detail Keperluan</th>
                            <td colspan="2"><?= strtoupper($perpanjangan[0]->detail_keperluan) ?></td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengajuan</th>
                            <td colspan="2"><?= tanggal_indo($perpanjangan[0]->tgl_aktifitas) ?></td>
                        </tr>
                        <tr>
                            <th>Tingkat Kepentingan</th>
                            <td colspan="2"><?= $perpanjangan[0]->tingkat_keperluan ?></td>
                        </tr>
                        <tr>
                            <th>Komentar</th>
                            <td colspan="2"><?= strtoupper($perpanjangan[0]->komentar) ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td colspan="2">
                                <?php 
                                    $status = $perpanjangan[0]->status_aktifitas;
                                    if($status == "0"){
                                        echo "MENUNGGU KONFIRMASI";
                                    } else if($status == "1"){
                                        echo "APPROVE ADMIN";
                                    } else if($status == "2"){
                                        echo "REJECT ADMIN";
                                    } else if($status == "3"){
                                        echo "APPROVE MANAGER LOKASI";
                                    } else if($status == "4"){
                                        echo "REJECT MANAGER LOKASI";
                                    } else if($status == "7"){
                                        echo "PENGAJUAN ULANG";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Catatan</th>
                            <td colspan="2"><?= strtoupper($perpanjangan[0]->catatan) ?></td>
                        </tr>
                        <tr><th style="text-align:center;background-color:#6777ef;color:white;" colspan="3">Checklist Dokumen</th></tr>
                        <?php foreach($dok_perpanjangan as $dokumen){ ?>
                        <tr>
                            <th><?= $dokumen->nm_syarat ?></th>
                            <td><button type="button" id="btn-dokumen" data-title_dokumen="<?= $dokumen->nm_syarat ?>" data-nama_dokumen="<?= $dokumen->dokumen_syarat ?>" data-format_dokumen="<?= $dokumen->format_dokumen ?>" class="btn btn-xs btn-info">View Dokumen</button></td>
                            <td>
                                <?php
                                    if($status == "1" || $status == "2"){
                                        echo ($dokumen->approval_admin == 'Y' ? "<i class='fa fa-check' style='color:green;'></i>" : "<i class='fa fa-close' style='color:red;'></i>")."<br/>".$dokumen->note_approval_admin;
                                    } else if($status == "3" || $status == "4"){

                                    }
                                ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="modalDokumen" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-data-dokumen"></h6>
            </div>
            <div class="modal-body">
                <div id="dokumen-perpanjangan-view"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-xs btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click", "#btn-dokumen", function(e){
        var title_dokumen = $(this).data("title_dokumen");
        var nama_dokumen = $(this).data("nama_dokumen");
        var format_dokumen = $(this).data("format_dokumen");
        if(format_dokumen == ".pdf"){
            var view = '<iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="<?= base_url() ?>asset/pdfviewer/web/viewer.html?file=<?= base_url('dokumen/perpanjangan/') ?>' + nama_dokumen+ '" scrolling="no"></iframe>';
        } else {
            var view = '<center><img class="img-thumbnail" src="<?= base_url() ?>dokumen/perpanjangan/'+nama_dokumen+'" /></center>';
        }
        $("#dokumen-perpanjangan-view").html(view);
        $("#title-data-dokumen").html(title_dokumen);
        $("#modalDokumen").modal("show");
    });
</script>