<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Tambah Temuan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li>Temuan</li>
            <li class="active"><strong>Tambah Temuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                    <?php if($this->session->flashdata("error")) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("error"));
                                unset($_SESSION["error"]);
                            ?>
                        </div>
                    <?php } ?>
                        <div class="row">
                            <form action="<?php echo base_url('temuan/action_tambah'); ?>" enctype="multipart/form-data" method="POST">
                                <div class="col-lg-12">

                                        <div class="form-group">
                                        <label>No Polisi</label>
                                        <select name="id_truck" class="form-control m-b" id="selectNopolisi">
                                            <option value="">Pilih No Polisi</option>
                                            <?php foreach($truck as $t_key => $t_val){ ?>
                                            <option value="<?= $t_val->id_truck ?>"><?= $t_val->no_polisi; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Jenis Temuan</label>
                                            <input type="text" class="form-control" name="jenis_temuan" placeholder="Jenis Temuan Lapangan" required="">
                                        </div>

                                        <div class="form-group">
                                            <label>Deskripsi</label>
                                            <textarea name="deskripsi_temuan" class="form-control" placeholder="Deskripsi Temuan" name="deskripsi_temuan" rows="5" required=""></textarea>
                                        </div>
                                        <div class="form-group" id="data_1">
                                            <label>Tanggal Temuan</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="tgl_temuan" class="form-control" value="<?php echo date('d/m/Y')?>" required>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label>Tindak Lanjut</label>
                                            <textarea name="tindak_lanjut" class="form-control" placeholder="Tindak Lanjut" name="tindak_lanjut" rows="5" required=""></textarea>
                                        </div>
                                        <div class="form-group" id="data_1">
                                            <label>Tanggal Tindak Lanjut</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="tgl_tindaklanjut" class="form-control" value="<?php echo date('d/m/Y')?>" required>
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control m-b" id="selectStatus" name="status">
                                                <option>Pilih Status</option>
                                                <option value="pending">Pending</option>
                                                <option value="proses perbaikan">Proses Perbaikan</option>
                                                <option value="selesai">Selesai</option>
                                            </select>
                                        </div>
                                        </div>
                                   <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kesalahan</label>
                                        <img id="prev_foto_pertama" src="<?= base_url('asset/img/noimage_336_290.jpg') ?>" width="70%" />
                                        <input type="file" name="foto_pertama" id="foto_pertama" class="form-control" />
                                        <sup>Foto maksimal ukuran 2mb</sup>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kesalahan</label>
                                        <img id="prev_foto_kedua" src="<?= base_url('asset/img/noimage_336_290.jpg') ?>" width="70%" />
                                        <input type="file" name="foto_kedua" id="foto_kedua" class="form-control" />
                                        <sup>Foto maksimal ukuran 2mb</sup>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kesalahan</label>
                                        <img id="prev_foto_ketiga" src="<?= base_url('asset/img/noimage_336_290.jpg') ?>" width="70%" />
                                        <input type="file" name="foto_ketiga" id="foto_ketiga" class="form-control" />
                                        <sup>Foto maksimal ukuran 2mb</sup>
                                    </div>
                                </div>
                                        <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN"  >
                                            <a href="<?php echo base_url('temuan') ?>" class="btn btn-sm btn-info">KEMBALI</a>  
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#selectNopolisi").select2();
        $("#selectStatus").select2();
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format:'dd/mm/yyyy'
        });   
    });
     function readURL(input, id, form) {
        var typeFile = input.files[0].type;
        var size = Math.round(input.files[0].size / 1024);
        if(typeFile == "image/jpeg" || typeFile == "image/png"){
            if(size > 2048){
                swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");
                $(form).val('');
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $(id).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        } else {
            swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
            $(form).val('');
        }
        
    }

    $("#foto_pertama").change(function() {
        readURL(this, "#prev_foto_pertama", "#foto_pertama");
    });

    $("#foto_kedua").change(function() {
        readURL(this, "#prev_foto_kedua", "#foto_kedua");
    });

    $("#foto_ketiga").change(function() {
        readURL(this, "#prev_foto_ketiga", "#foto_ketiga");
    });
</script>