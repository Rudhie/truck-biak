<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Ubah Temuan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li>Temuan</li>
            <li class="active"><strong>Ubah Temuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                            <form action="<?= base_url('temuan/action_edit/').$this->input->get("id"); ?>"  enctype="multipart/form-data" method="POST">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>No Polisi</label>
                                        <select name="id_truck" class="form-control m-b" id="selectNopolisi">
                                            <option value="">Pilih No Polisi</option>
                                            <?php foreach($truck as $t_key => $t_val){ ?>
                                            <option value="<?= $t_val->id_truck ?>" <?= $t_val->id_truck == $temuan[0]->id_truck ? 'selected' : '' ?>><?= $t_val->no_polisi; ?></option>
                                            <?php } ?>
                                        </select>
                                        </div>
                                    <div class="form-group">
                                            <label>Jenis Temuan</label>
                                            <input type="text" class="form-control" name="jenis_temuan" value="<?php echo $temuan[0]->jenis_temuan ?>" required="">
                                        </div>
                                   
                                    <div class="form-group">
                                    <label>Deskripsi</label>
                                    <textarea class="form-control" name="deskripsi_temuan" rows="5"><?php echo rtrim($temuan[0]->deskripsi_temuan); ?></textarea>
                                        </div>
                                    
                                    <div class="form-group">
                                        <div class="form-group" id="data_1">
                                            <label>Tanggal Temuan</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="tgl_temuan" class="form-control" value="<?php echo date('d/m/Y', strtotime($temuan[0]->tgl_temuan));?>"required>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                    <label>Tindak Lanjut</label>
                                    <textarea class="form-control" name="tindak_lanjut" rows="5"><?php echo rtrim($temuan[0]->tindak_lanjut); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                        <div class="form-group" id="data_1">
                                            <label>Tanggal Tindak Lanjut</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="tgl_tindaklanjut" class="form-control" value="<?php echo date('d/m/Y', strtotime($temuan[0]->tgl_tindaklanjut));?>"required>
                                            </div>
                                        </div>
                                    </div>
                                   <div class="form-group">
                                            <label>Status</label>
                                            <select class="form-control" id="selectStatus" name="status" >
                                                <option value="Pending"<?php if($temuan[0]->status=="Pending"){ echo "selected";} ?>>Pending</option>
                                                <option value="Proses"<?php if($temuan[0]->status=="Proses"){ echo "selected";}?>>Proses</option>
                                                <option value="Perbaikan"<?php if($temuan[0]->status=="Perbaikan"){ echo "selected";} ?>>Perbaikan</option>
                                                <option value="Selesai"<?php if($temuan[0]->status=="Selesai"){ echo "selected";}?>>Selesai</option>
                                            </select>
                                            
                                        </div>
                                        <div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kerusakan</label>
                                        <img id="prev_foto_pertama" src="<?= base_url($temuan[0]->foto_pertama != null ? 'dokumen/foto-temuan/'.$temuan[0]->foto_pertama : 'asset/img/noimage_336_290.jpg'); ?>" width="70%" />
                                        <input type="file" name="foto_pertama" id="foto_pertama" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kerusakan</label>
                                        <img id="prev_foto_kedua" src="<?= base_url($temuan[0]->foto_kedua != null ? 'dokumen/foto-temuan/'.$temuan[0]->foto_kedua : 'asset/img/noimage_336_290.jpg'); ?>" width="70%" />
                                        <input type="file" name="foto_kedua" id="foto_kedua" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>Foto Temuan Kerusakan</label>
                                        <img id="prev_foto_ketiga" src="<?= base_url($temuan[0]->foto_ketiga != null ? 'dokumen/foto-temuan/'.$temuan[0]->foto_ketiga : 'asset/img/noimage_336_290.jpg'); ?>" width="70%" />
                                        <input type="file" name="foto_ketiga" id="foto_ketiga" class="form-control">
                                    </div>
                                </div>
                                    <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                        <a href="<?php echo base_url('temuan') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        $("#selectNopolisi").select2();
        $("#selectStatus").select2();
         
    

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'
        });
    });

    $("#foto_pertama").change(function() {
        readURL(this, "#prev_foto_pertama", "#foto_pertama");
    });

    $("#foto_kedua").change(function() {
        readURL(this, "#prev_foto_kedua", "#foto_kedua");
    });

    $("#foto_ketiga").change(function() {
        readURL(this, "#prev_foto_ketiga", "#foto_ketiga");
    });

    function readURL(input, id, form) {
        var typeFile = input.files[0].type;
        var size = Math.round(input.files[0].size / 1024);
        if(typeFile == "image/jpeg" || typeFile == "image/png"){
            if(size > 2048){
                swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");
                $(form).val('');
            } else {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $(id).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        } else {
            swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
            $(form).val('');
        }
        
    }
</script>