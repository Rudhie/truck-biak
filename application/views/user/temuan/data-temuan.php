<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Temuan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li class="active"><strong>Temuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-title">
                 <?php if($this->session->userdata("level_user") == "PEMILIK"){ ?>
                    <a href="<?php echo base_url('temuan/tambah'); ?>" class="btn btn-outline btn-success"><i class="fa fa-plus"></i> Tambah Temuan</a>
                        <?php } ?>
                </div>
                <div class="ibox-content">
                    <?php if($this->session->flashdata("success")){ ?>
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("success"));
                                unset($_SESSION["success"]);
                            ?>
                        </div>
                    <?php } else if($this->session->flashdata("error")) { ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                            <?php
                                echo strtoupper($this->session->flashdata("error"));
                                unset($_SESSION["error"]);
                            ?>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <form method="GET">
                        <?php if($this->session->userdata("level_user") != "PEMILIK"){ ?>

                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Transportir</label>
                                <select name="transportir" class="form-control m-b select2">
                                    <option value="">Pilih Transportir</option>
                                    <?php foreach($transportir as $t){ ?>
                                    <option value="<?= $t->id_pemilik ?>" <?= $this->input->get("transportir") == $t->id_pemilik ? 'selected' : '' ?>><?= $t->nama_perusahaan ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Bulan</label>
                                <select name="bulan" class="form-control m-b select2">
                                    <option value="">Pilih Bulan</option>
                                    <?php $listBulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"); ?>
                                    <?php for($bulan = 1; $bulan <= 12; $bulan++){ ?>
                                    <option value="<?= $bulan ?>" <?= $this->input->get("bulan") == $bulan ? 'selected' : '' ?>><?= $listBulan[$bulan - 1] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select name="tahun" class="form-control m-b select2">
                                    <option value="">Pilih Tahun</option>
                                    <?php for($tahun = 2020; $tahun <= date("Y"); $tahun++){ ?>
                                    <option value="<?= $tahun ?>" <?= $this->input->get("tahun") == $tahun ? 'selected' : '' ?>><?= $tahun ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-1" style="margin-left:15px;">
                                <label><br/></label>
                                <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i> TAMPILKAN</button>
                            </div>

                             
                            <div class="col-lg-1" style="margin-left:15px;">
                                <label><br/></label>
                                <?php $link = explode("?",$_SERVER['REQUEST_URI']); ?>
                                <a href="<?php echo base_url('temuan/export?').(count($link) > 1 ? $link[1] : ''); ?>" target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Cetak Data</a>

                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">                    
                        <table class="table table-striped table-bordered table-hover" id="table_temuan" width="100%">
                            <thead class="gradient">
                                <tr>
                                    <th>No</th>
                                    <th>Transportir</th>
                                    <th>No Polisi</th>
                                    <th>Jenis Temuan</th>
                                    <th>Deskripsi</th>
                                    <th>Tanggal Temuan</th>
                                    <th>Tindak Lanjut</th>
                                    <th>Tanggal Tindak Lanjut</th>
                                    <th>Status</th>
                                    <th width="20%" style="text-align:center;">Aksi</th>                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i=1;
                                    
                                    function tanggal_indo($tanggal){
                                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                                        $split = explode('-', $tanggal);
                                        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                                    }
                                    foreach($temuan as $key => $value){ 
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><b><?= $value->nama_perusahaan ?></b></td>
                                    <td><b><?= $value->no_polisi ?></b></td>
                                    <td><?= $value->jenis_temuan ?></td>
                                    <td><?= $value->deskripsi_temuan ?></td>
                                    <td><?= tanggal_indo($value->tgl_temuan) ?></td>
                                    <td><?= $value->tindak_lanjut ?></td>
                                    <td><?= tanggal_indo($value->tgl_tindaklanjut) ?></td>
                                    <td><?= strtoupper($value->status) ?></td>
                                    <td style="text-align:left;">
                                        <a href="<?= base_url('temuan/detail').'?id='.$value->id_temuan; ?>" class="btn btn-xs btn-info"><i class="fa fa-info"></i> DETAIL</a>
                                        <a href="<?= base_url('temuan/edit').'?id='.$value->id_temuan; ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> UBAH</a>
                                        <a onclick="hapusTemuan('<?= $value->id_temuan ?>')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a>
                                    </td>
                                </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        $("#table_temuan").dataTable();

         $('.select2').select2();

    });

    function hapusTemuan(id){
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data tersebut",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                window.location = "<?php echo base_url('temuan/action_hapus/') ?>"+id; 
            }
        })
    }
</script>