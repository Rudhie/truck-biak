<style>
    blockquote {
        font-size: 14px;
        border-left: 2px solid #33a4f7;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Detail Temuan</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li class="active"><strong>Detail Temuan</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Detail Temuan</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-6">
                            <blockquote>
                                <p><i class="fa fa-user"></i> No Polisi</p>
                                <small><strong><?= $temuan[0]->no_polisi ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-tags"></i> Jenis Temuan</p>
                                <small><strong><?= $temuan[0]->jenis_temuan ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-align-justify"></i> Deskripsi Temuan</p>
                                <small><strong><?= $temuan[0]->deskripsi_temuan ?></strong></small>
                            </blockquote>
                        </div>
                        <div class="col-md-6">
                            <blockquote>
                                <p><i class="fa fa-calendar"></i> Tanggal Temuan</p>
                                <small><strong><?= $temuan[0]->tgl_temuan ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-gavel"></i> Tindak Lanjut</p>
                                <small><strong><?= $temuan[0]->tindak_lanjut ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-ticket"></i> Status</p>
                                <small><strong><?= $temuan[0]->status ?></strong></small>
                            </blockquote>
                        </div>
                    </div>

                    <a href="<?php echo base_url('temuan'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> KEMBALI</a>
                                </div>
                            </div>
                        </div>
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                        <div class="ibox-title">
                                    <h5>Dokumen Temuan Kesalahan</h5>
                        </div>
                    <div class="ibox-content">
                    <div class="photos">
                                        <?php if($temuan[0]->foto_pertama != "" && $temuan[0]->foto_pertama != null){ ?>
                                            <a target="_blank" href="<?= base_url().'dokumen/foto-temuan/'.$temuan[0]->foto_pertama; ?>"> <img alt="image" class="feed-photo" src="<?= base_url().'dokumen/foto-temuan/'.$temuan[0]->foto_pertama ?>"></a>
                                        <?php } ?>
                                        <?php if($temuan[0]->foto_kedua != "" && $temuan[0]->foto_kedua != null){ ?>
                                            <a target="_blank" href="<?= base_url().'dokumen/foto-temuan/'.$temuan[0]->foto_kedua; ?>"> <img alt="image" class="feed-photo" src="<?= base_url().'dokumen/foto-temuan/'.$temuan[0]->foto_kedua ?>"></a>
                                        <?php } ?>
                                        <?php if($temuan[0]->foto_ketiga != "" && $temuan[0]->foto_ketiga != null){ ?>
                                            <a target="_blank" href="<?= base_url().'dokumen/foto-temuan/'.$temuan[0]->foto_tiga; ?>"> <img alt="image" class="feed-photo" src="<?= base_url().'dokumen/foto-temuan/'.$perpanjangan[0]->foto_ketiga ?>"></a>
                                        <?php } ?>
                                    </div>

                </div>
            </div>
        </div>
     </div>
</div> </div>
                        