<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>This is main title</h2>
        <ol class="breadcrumb">
            <li><a href="index.html">This is</a></li>
            <li class="active"><strong>Breadcrumb</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <button class="btn btn-sm btn-primary" id="btn_add_data">
                        <i class="fa fa-plus"></i> Tambah Data
                    </button>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="table_sample" width="100%">
                            <thead>
                                <tr>
                                    <th width="10%">No</th>
                                    <th>Nama</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-users modal-icon"></i>
                <h6 class="modal-title" id="title-peserta">Tambah Data Sample</h6>
            </div>
            <div class="modal-body">
                <form id="formAddSample" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" name="nama" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-7">
                            <input class="btn btn-primary" value="SIMPAN" type="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modalUpdate" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated fadeInDown">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <i class="fa fa-users modal-icon"></i>
                <h6 class="modal-title" id="title-peserta">Ubah Data Sample</h6>
            </div>
            <div class="modal-body">
                <form id="formUpdateSample" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama</label>
                        <div class="col-sm-7">
                            <input type="hidden" class="form-control" id="id_sample" name="id_sample" required>
                            <input type="text" class="form-control" id="nama" name="nama" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-7">
                            <input class="btn btn-primary" value="SIMPAN" type="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){
        var table_sample = $("#table_sample").DataTable({
            processing: true,
            select: false,
            ajax: {
                url : "<?= base_url('welcome/data') ?>",
                dataType : "JSON",
                type : "GET",
                dataSrc : function (data){
                    var returnDataPerusahaan = new Array();
                    if(data.status == "success"){
                        $.each(data["data"], function(i, item){
                            returnDataPerusahaan.push({
                                "no" : (i+1),
                                "nama" : item["nama"],
                                "action" : "<center><button id='btnEdit' data-idsample='"+item["id_sample"]+"' class='btn btn-xs btn-info'><i class='fa fa-edit'></i> Ubah</button>&nbsp;"+
                                            "<button id='btnDelete' data-idsample='"+item["id_sample"]+"' class='btn btn-xs btn-danger'><i class='fa fa-trash'></i> Hapus</button></center>"
                            });
                        });
                    }
                    return returnDataPerusahaan;
                }
            },
            columns : [
                {data : "no"},
                {data : "nama"},
                {data : "action"}
            ]
        });
    });

    $(document).on("click", "#btn_add_data", function(e){
        $('#formAddSample')[0].reset();
        $("#modalAdd").modal("show");
    });

    $(document).on("submit", "#formAddSample", function(e){
		e.preventDefault();
		$.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('welcome/add_data') ?>",
			"method": "POST",
			"data": $(this).serialize(),
		}).done(function (response) {
            var data = JSON.parse(response)
			var message = data.message;
			if(data.status == "success"){
                $("#modalAdd").modal("hide");
				swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#table_sample").DataTable().ajax.reload();
                });
                
			} else {
				swal("Gagal menambahkan.", message.toUpperCase(), "warning");
			}
		});
	});

    $(document).on("click", "#btnEdit", function(e){
        var id = $(this).data("idsample");
        $.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('welcome/data') ?>/"+id,
			"method": "GET",
		}).done(function (response) {
            var data = JSON.parse(response);
            $("#id_sample").val(data.data[0].id_sample);
            $("#nama").val(data.data[0].nama);
            $("#modalUpdate").modal("show");
		});
    });

    $(document).on("submit", "#formUpdateSample", function(e){
		e.preventDefault();
		$.ajax({
			"async": true,
			"crossDomain": true,
			"url": "<?= base_url('welcome/update_data') ?>",
			"method": "POST",
			"data": $(this).serialize(),
		}).done(function (response) {
            var data = JSON.parse(response)
			var message = data.message;
			if(data.status == "success"){
                $("#modalUpdate").modal("hide");
				swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                    confirmButtonColor: "#a5dc86",
                    confirmButtonText: "Close",
                }, function(isConfirm){
                    $("#table_sample").DataTable().ajax.reload();
                });
                
			} else {
				swal("Gagal menambahkan.", message.toUpperCase(), "warning");
			}
		});
	});

    $(document).on("click", "#btnDelete", function(e){
        var idsample = $(this).data("idsample");
        swal({
            title: "Yakin ingin menghapus ?",
            text: "Apakah anda yakin menghapus data",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function(isConfirm){
            if(isConfirm){
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "<?= base_url('welcome/delete_data') ?>",
                    "method": "POST",
                    "data": {
                        "id_sample": idsample
                    }
                }
                
                $.ajax(settings).done(function (response) {
                    var data = JSON.parse(response)
                    var message = data.message;
                    if(data.status == "success"){
                        swal({
                            title: "Berhasil",
                            text: message.toUpperCase(),
                            type: "success",
                            confirmButtonColor: "#a5dc86",
                            confirmButtonText: "Close",
                        }, function(isConfirm){
                            $("#table_sample").DataTable().ajax.reload();
                        });
                    } else {
                        swal("Gagal menghapus data.", message.toUpperCase(), "warning");
                    }
                });   
            }
        });
    });
</script>