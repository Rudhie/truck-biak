<style>
    .select2-container .select2-selection--single{
        height:34px !important;
    }
    .select2-container--default .select2-selection--single{
     border: 1px solid #ccc !important; 
     border-radius: 0px !important; 
 }
</style>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-truck"></i> Master Data Mobil Tangki</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('#'); ?>">Data Mobil Tangki</a></li>
            <li class="active"><strong>Tambah Mobil Tangki</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                            <form action="<?php echo base_url('truck/action_tambah'); ?>" enctype="multipart/form-data" method="POST" role="form">
                                <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">
                                    <center>
                                        <h3>I. Profil Mobil Tangki </h3>
                                    </center>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nama Transportir</label>
                                            <select id="idPemilik" name="id_pemilik" class="form-control m-b select2">
                                                <option value="">Pilih Transportir</option>
                                                <?php
                                                foreach($pemilik as $p){
                                                    echo '<option value="'.$p->id_pemilik.'">'.$p->nama.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nomor Polisi</label>
                                            <input type="text" name="no_polisi" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nomor Rangka</label>
                                            <input type="text" name="no_rangka" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nomor Mesin</label>
                                            <input type="text" name="no_mesin" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Merek</label>
                                            <select id="selectMerek" name="id_merek" class="form-control m-b select2"  required>
                                                <option>Pilih Merek</option>
                                                <?php
                                                foreach($merek as $p){
                                                    echo '<option value="'.$p->id_merek.'">'.$p->nama.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Tahun Pembuatan</label>
                                            <input type="text" name="tahun_pembuatan" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">
                                    <center>
                                        <h3>II. Masa Berlaku Berkas </h3>
                                    </center>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group" id="data_1">
                                                <label>Masa Berlaku STNK</label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="masa_berlaku_stnk" class="form-control" value="<?php echo date('d/m/Y')?>" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" id="data_1">
                                            <label>Masa Berlaku SKT</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="masa_berlaku_skt" class="form-control" value="<?php echo date('d/m/Y')?>" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" id="data_1">
                                            <label>Masa Berlaku Keur</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="keur" class="form-control" value="<?php echo date('d/m/Y')?>" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" id="data_1">
                                            <label>Masa Berlaku Metrologi Tera</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="metrologi_tera" class="form-control" value="<?php echo date('d/m/Y')?>" required="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" id="data_1">
                                            <label>Masa Berlaku KIM</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="masa_berlaku_kim" class="form-control" value="<?php echo date('d/m/Y')?>" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">
                                    <center>
                                        <h3>III. Dokumen Upload </h3>
                                    </center>
                                </div>
                                <br/>
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Dokumen STNK</label>
                                            <!-- <input type="file" name="dokumen_stnk" class="form-control" required=""> -->
                                            <input type="file" name="dokumen_stnk" id="dokumen_stnk" accept=".pdf" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Dokumen SKT</label>
                                            <!-- <input type="file" name="dokumen_kir" class="form-control" required=""> -->
                                            <input type="file" name="dokumen_skt" id="dokumen_skt" accept=".pdf" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Dokumen KEUR</label>
                                            <!-- <input type="file" name="dokumen_kir" class="form-control" required=""> -->
                                            <input type="file" name="dokumen_kir" id="dokumen_kir" accept=".pdf" class="form-control" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Dokumen Metrologi Tera</label>
                                            <!-- <input type="file" name="dokumen_kir" class="form-control" required=""> -->
                                            <input type="file" name="dokumen_metrologi_tera" id="dokumen_metrologi_tera" accept=".pdf" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Dokumen KIM</label>
                                            <!-- <input type="file" name="dokumen_kir" class="form-control" required=""> -->
                                            <input type="file" name="dokumen_kim" id="dokumen_kim" accept=".pdf" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">
                                    <center>
                                        <h3>IV. Deskripsi Mobil Tangki </h3>
                                    </center>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Isi Silinder</label>
                                            <input type="text" name="isi_silinder" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Produk</label>
                                            <input type="text" name="produk" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Uji Emisi</label>
                                            <input type="text" name="uji_emisi" class="form-control" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>T2</label>
                                            <input type="text" name="t2" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Volume Kl</label>
                                            <input type="text" name="vol_kl" class="form-control" required="">
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-content gradient" style="background-color:#0086ad; color:white;">
                                    <center>
                                        <h3>V. Detail Truck </h3>
                                    </center>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-4">
                                            <label>Foto Depan</label>
                                                <input type="file" id="foto_depan_truck" name="foto_depan_truck" accept=".jpg, .png, .jpeg" class="form-control" onchange="readURL(this)" required="">
                                                <img class="img-thumbnail" id="fotoDepantruck" src='<?php echo base_url() ?>./dokumen/foto-truck/default-image.jpg' width="600px" height="300px">
                                    </div>
                                    <div class="col-md-4">
                                            <label>Foto Badan</label>
                                                <input type="file" id="foto_truck" name="foto_truck" accept=".jpg, .png, .jpeg" class="form-control" onchange="readURL(this)" required="">
                                                <img class="img-thumbnail" id="fotoTruck" src='<?php echo base_url() ?>./dokumen/foto-truck/default-image.jpg' width="600px" height="300px">
                                    </div>
                                    <div class="col-md-4">
                                            <label>Foto Belakang</label>
                                                <input type="file" id="foto_belakang_truck" name="foto_belakang_truck" accept=".jpg, .png, .jpeg" class="form-control" onchange="readURL(this)" required="">
                                                <img class="img-thumbnail" id="fotoBelakangtruck" src='<?php echo base_url() ?>./dokumen/foto-truck/default-image.jpg' width="600px" height="300px">
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                            <a href="<?php echo base_url('truck') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                            <input type="reset" value="RESET" class="btn btn-sm btn-warning" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.select2').select2();
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'



        });
    });

    $("#dokumen_kir").change(function() {
        readURL(this, "", "#dokumen_kir");
    });

    $("#dokumen_stnk").change(function() {
        readURL(this, "", "#dokumen_stnk");
    });

    $("#foto_truck").change(function() {
        readURL(this, "#fotoTruck", "foto_truck");
    });
    $("#foto_depan_truck").change(function() {
        readURL(this, "#fotoDepantruck", "foto_depan_truck");
    });
    $("#foto_belakang_truck").change(function() {
        readURL(this, "#fotoBelakangtruck", "foto_belakang_truck");
    });
    function readURL(input, id, form) {
        var typeFile = input.files[0].type;
        var size = Math.round(input.files[0].size / 1024);
        if(form == "#dokumen_kir" || form == "#dokumen_stnk"){
            if(typeFile != "application/pdf"){
                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
                $(form).val('');
            }
        } else {
            if(typeFile != "image/jpeg" && typeFile != "image/png"){
                swal("Error", "FORMAT FILE TIDAK SESUAI", "warning");
                $(form).val('');
            }
        }
        
        if(size > 2048){
            swal("Error", "UKURAN GAMBAR TERLALU BESAR", "warning");
            $(form).val('');
        } else {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(id).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    }
</script>