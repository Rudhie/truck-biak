<style>
    .select2-container .select2-selection--single{
        height:34px !important;
    }
    .select2-container--default .select2-selection--single{
       border: 1px solid #ccc !important; 
       border-radius: 0px !important; 
   }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-truck"></i> Mobil Tangki </h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Mobil Tangki</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                
                    <a href="<?php echo base_url('truck/tambah'); ?>" class="btn btn-outline btn-info dim"><i class="fa fa-plus"></i>Tambah</a>
                
                </div>
                <div class="ibox-content">
                <?php if($this->session->flashdata("success")){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("success"));
                            unset($_SESSION["success"]);
                        ?>
                    </div>
                <?php } else if($this->session->flashdata("error")) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("error"));
                            unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php } ?>
                <?php if($this->session->userdata("level_user") != "PEMILIK"){ ?>
                    <div class="row">
                        <form method="get" action="">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div id="form-filterTransportir">
                                        <label>Transportir</label>
                                        <select name="filterTransportir" class="form-control m-b select2">
                                            <option value="">Pilih Transportir</option>
                                            <?php
                                            $filterTransportir = "";
                                            if (isset($_GET['filterTransportir']) && ! empty($_GET['filterTransportir'])) {
                                                $filterTransportir = $_GET['filterTransportir'];
                                            }
                                            foreach($transportir as $p){
                                                $selected = '';
                                                if($p->id_pemilik == $filterTransportir){
                                                    $selected = 'selected';
                                                }
                                                echo '<option value="'.$p->id_pemilik.'"'.$selected.'>'.$p->nama.'</option>';
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-sm btn-primary">Tampilkan</button>
                                    <input type="reset" value="RESET" class="btn btn-sm btn-warning"/>
                                </form>
                            </div>
                            </div>
                            <div class="col-md-2">
                                <?php $link = explode("?",$_SERVER['REQUEST_URI']); ?>
                                <a href="<?php echo base_url('truck/export?').(count($link) > 1 ? $link[1] : ''); ?>" class="btn btn-sm btn-info"><i class="fa fa-print"></i> Export Excel</a>                                            
                            </div>
                            
                        </div>
                    <?php } ?>
                    <div class="table-responsive">
                    <table id="tableTruck" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead class="gradient">
                        <tr>
                        <th width="5%">No</th>
                        <th><center>Nama Transportir</center></th>
                        <th><center>Nomor Polisi</center></th>
                        <th><center>Merek</center></th>
                        <th><center>Masa Berlaku STNK</center></th>
                        <th><center>Masa Berlaku SKT</center></th>
                        <th><center>Masa Berlaku KEUR</center></th>
                        <th><center>Isi Silinder</center></th>
                        <th><center>Produk</center></th>
                        <th><center>Keur</center></th>
                        <th><center>Metrologi Tera</center></th>
                        <th><center>T2</center></th>
                        <th><center>Vol Kl</center></th>
                        <th><center>Foto Truck</center></th>

                        <th width="15%"><center>Aksi</center></th>

                    </tr>
                   </thead>
                    <tbody>
                 
                <?php $i=1;
                   foreach($truck as $p){
                    echo'<tr >';
                    echo'<td>'.$i.'</td>';
                    echo'<td>'.ucfirst($p->nm_pemilik).'</a>'.'</td>';
                    echo'<td>'.'<a href="'.base_url('truck/detail').'?id='.$p->id_truck.'">'.$p->no_polisi.'</a>'.'</td>';
                    echo'<td>'.ucfirst($p->nama_merek).'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->masa_berlaku_stnk)).'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->masa_berlaku_skt)).'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->keur)).'</td>';                    
                    echo'<td>'.$p->isi_silinder.'</td>';
                    echo'<td>'.$p->produk.'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->keur)).'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->metrologi_tera)).'</td>';
                    echo'<td>'.$p->t2.'</td>';
                    echo'<td>'.$p->vol_kl.'</td>';
                    echo'<td><a href="" data-toggle="modal" class="btn btn-outline btn-info dim" data-target="#imageModal'.$p->id_truck.'"> Show </a></td>';

                    // '<td><a href="" data-toggle="modal" class="btn btn-outline btn-info dim" data-target="#editSlidemodal'.$p->id_slide.'"> Ubah </a> '
                    echo'<td><a href="'.base_url('truck/edit').'?id='.$p->id_truck.'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> UBAH</a> '.'<a onclick="hapusTruck('.$p->id_truck.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a> </td>';
                    
                    echo'</tr>';
                    $i++;

                    }
                    ?>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>


    <?php
    foreach ($truck as $p) {
        ?>
    <div class="modal fade" id="imageModal<?php echo $p->id_truck?>" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header gradient" style="background-color: #77aaff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <br/>
                    <h3 class="modal-title" id="imageModalLabel"><center><b>Detail Foto Truck</b></center></h3>
                </div>
                    <div class="modal-body">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="img-thumbnail" src="<?php echo base_url() ?>./dokumen/foto-truck/<?php echo $p->foto_depan_truck; ?>">
                            </div>
                            <div class="carousel-item">
                                <img class="img-thumbnail" src="<?php echo base_url() ?>./dokumen/foto-truck/<?php echo $p->foto_truck; ?>">
                            </div>
                            <div class="carousel-item">
                                <img class="img-thumbnail" src="<?php echo base_url() ?>./dokumen/foto-truck/<?php echo $p->foto_belakang_truck; ?>">
                            </div>
                        </div>
                    </div>
                
            </div>
        </div>
    </div>
<?php
}
?>

<script>
    $(document).ready(function(){
        $("#tableTruck").dataTable({
            "scrollX": true
        });

        $('.select2').select2();
    });

    function hapusTruck(id){
        var hapus = confirm("Apakah anda yakin ingin menghapus?");
        if(hapus){
            window.location = "<?php echo base_url('truck/action_hapus/') ?>"+id;
        }
    }
</script>