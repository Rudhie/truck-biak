<style type="text/css">
    #tdInvoice {
        border-bottom: 1px solid #DDDDDD;
        text-align: right;
        width: 15%;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-truck"></i> Detail Mobil Tangki</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('truck'); ?>">Data Mobil Tanki</a></li>
            <li class="active"><strong><a>Detail Mobil Tanki</a></strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <hr style="border-top: dotted 1px;"/>
                    <div class="row">
                        <div class="col-md-6">
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> Nama Transportir</p>
                                <small><strong><?php echo strtoupper($truck[0]->nm_pemilik); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> Nomor Polisi</p>
                                <small><strong><?php echo $truck[0]->no_polisi; ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> Merek</p>
                                <small><strong><?php echo strtoupper($truck[0]->nama_merek); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-chevron-circle-right"></i> Tahun Pembuatan</p>
                                <small><strong><?php echo $truck[0]->tahun_pembuatan; ?></strong></small>
                            </blockquote>
                        </div>
                        <div class="row">
                        <div class="col-md-6">
                            <div class="panel panel-success">
                                <div class="panel-heading">Detail Dokumen & Masa Berlaku</div>
                                <div class="panel-body" style="height:300px;overflow-y: scroll;">
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-truck/'.$truck[0]->dokumen_stnk ?>">STNK</a></p>
                                        <small>Masa Berlaku : <strong><?php echo date('d-F-Y', strtotime(strtoupper($truck[0]->masa_berlaku_stnk))); ?></strong></small>
                                    </blockquote>
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-truck/'.$truck[0]->dokumen_skt ?>">SKT</a></p>
                                        <small>Masa Berlaku : <strong><?php echo date('d-F-Y', strtotime(strtoupper($truck[0]->masa_berlaku_skt))); ?></strong></small>
                                    </blockquote>
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-truck/'.$truck[0]->dokumen_kir ?>">Keur</a></p>
                                        <small>Masa Berlaku : <strong><?php echo date('d-F-Y', strtotime(strtoupper($truck[0]->keur))); ?></strong></small>
                                    </blockquote>
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-truck/'.$truck[0]->dokumen_metrologi_tera ?>">Metrologi Tera</a></p>
                                        <small>Masa Berlaku : <strong><?php echo date('d-F-Y', strtotime(strtoupper($truck[0]->metrologi_tera))); ?></strong></small>
                                    </blockquote>
                                    <blockquote>
                                        <p><i class="fa fa-info-circle"></i> <a target="_blank" href="<?php echo base_url().'dokumen/berkas-truck/'.$truck[0]->dokumen_kim ?>">Kartu KIM</a></p>
                                        <small>Masa Berlaku : <strong><?php echo date('d-F-Y', strtotime(strtoupper($truck[0]->masa_berlaku_kim))); ?></strong></small>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                        <div class="col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-heading">Foto Depan</div>
                                    <div class="panel-body">
                                        <img class="img-thumbnail" id="imgId" src="<?php echo base_url().'dokumen/foto-truck/'.$truck[0]->foto_depan_truck ?>" >
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-heading">Foto Badan</div>
                                    <div class="panel-body">
                                        <img class="img-thumbnail" id="imgId" src="<?php echo base_url().'dokumen/foto-truck/'.$truck[0]->foto_truck ?>" >
                                    </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-heading">Foto Belakang</div>
                                    <div class="panel-body">
                                        <img class="img-thumbnail" id="imgId" src="<?php echo base_url().'dokumen/foto-truck/'.$truck[0]->foto_belakang_truck ?>" >
                                    </div>
                            </div>
                        </div>
                    </div>
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">Detail Informasi Truck</div>
                                <div class="panel-body">
                                    <table class="table invoice-table">
                                        <thead>
                                            <tr>
                                                <th>Nomor Rangka</th>
                                                <th>Nomor Mesin</th>
                                                <th style="text-align: center;">Isi Silinder</th>
                                                <th>Produk</th>
                                                <th style="text-align: center;">Uji Emisi</th>
                                                <th style="text-align: right;">T2</th>
                                                <th style="text-align: right;">Volume (Kl)</th>
                                                <th style="text-align: right;">Dokumen STNK</th>
                                                <th style="text-align: right;">Dokumen KIR</th>
                                            </tr>
                                        </thead>
                                        <tbody style="text-align: right;">
                                            <?php foreach($truck as $p){ ?>
                                                <tr>
                                                    <td style="text-align: left;"><div><strong><?php echo $p->no_rangka; ?></strong></div></td>
                                                    <td><?php echo $p->no_mesin; ?></td>
                                                    <td><?php echo $p->isi_silinder; ?></td>
                                                    <td><?php echo $p->produk; ?></td>
                                                    <td><?php echo $p->uji_emisi; ?></td>
                                                    <td><?php echo $p->t2; ?></td>
                                                    <td><?php echo $p->vol_kl; ?></td>
                                                    <td><?php echo $p->dokumen_stnk; ?></td>
                                                    <td><?php echo $p->dokumen_kir; ?></td>
                                                </tr>
                                            <?php 
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                            <h1><a style="float: right;" href="<?php echo base_url('truck') ?>" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> KEMBALI</a></h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>