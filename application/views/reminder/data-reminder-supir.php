<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Reminder Awak Mobil Tangki</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('dashboard') ?>">Dashboard</a></li>
            <li class="active">Data Reminder</li>
            <li class="active"><strong>Awak Mobil Tangki</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata("level_user") == "ADMIN" || $this->session->userdata("level_user") == "MANAGER"){ ?>
                        <button type="button" class="btn btn-sm btn-outline btn-success" id="btn_send_reminder">
                            <i class="fa fa-refresh"></i> Kirim Reminder Manual
                        </button>
                    <?php } ?>
                    <div style="float:right;">
                        <span class="indicator"><span class="dot dot-primary"></span> > 3 bulan</span>
                        <span class="indicator"><span class="dot dot-yellow"></span> < 3 bulan</span>
                        <span class="indicator"><span class="dot dot-warning"></span> < 2 bulan</span>
                        <span class="indicator"><span class="dot dot-danger"></span> < 1 bulan</span>
                    </div>
                </div>
                <div class="ibox-content">
                <div class="row">
                    <form method="GET">
                        <?php if($this->session->userdata("level_user") != "PEMILIK"){ ?>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Transportir</label>
                                <select name="transportir" class="form-control m-b select2">
                                    <option value="">Pilih Transportir</option>
                                    <?php foreach($transportir as $t){ ?>
                                    <option value="<?= $t->id_pemilik ?>" <?= $this->input->get("transportir") == $t->id_pemilik ? 'selected' : '' ?>><?= $t->nama_perusahaan ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-1" style="margin-left:15px;">
                            <label><br/></label>
                            <button type="submit" class="btn btn-sm btn-info"><i class="fa fa-search"></i> TAMPILKAN</button>
                        </div>
                        <?php } ?>

                        <div class="col-lg-1" style="margin-left:15px;">
                            <label><br/></label>
                            <?php $link = explode("?",$_SERVER['REQUEST_URI']); ?>
                                <a href="<?php echo base_url('reminder/export_awak?').(count($link) > 1 ? $link[1] : ''); ?>" class="btn btn-sm btn-info"><i class="fa fa-print"></i> Export Excel</a>
                            
                        </div>
                        </form>
                    </div>
                    <div class="table-responsive">                    
                        <table class="table table-striped table-bordered table-hover" id="table_reminder_supir" width="100%">
                            <thead class="gradient">
                                <tr>
                                    <th width="7%" rowspan="2" style="text-align:center;vertical-align:middle;">No</th>
                                    <th rowspan="2" style="text-align:center;vertical-align:middle;">Perusahaan</th>
                                    <th rowspan="2" style="text-align:center;vertical-align:middle;">Nama AMT</th>
                                    <th rowspan="2" style="text-align:center;vertical-align:middle;">Fungsi</th>
                                    <th rowspan="2" style="text-align:center;vertical-align:middle;">Usia AMT</th>
                                    <th colspan="2" style="text-align:center;">SIM</th>
                                    <th colspan="2" style="text-align:center;">ID CARD</th>
                                </tr>
                                <tr>
                                    <th>Expired Date</th>
                                    <th>Remaining Days</th>
                                    <th>Expired Date</th>
                                    <th>Remaining Days</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $i=1;

                                    function reminder_label($value, $column, $id){
                                        if($value > 0){
                                            $label = '<span id="btn-dokumen" data-id="'.$id.'" data-nm_dokumen="'.$column.'" style="cursor: pointer" class="badge badge-danger">Lewat '.$value.' Hari</span>';
                                        } else if($value > -30){
                                            $label = '<span id="btn-dokumen" data-id="'.$id.'" data-nm_dokumen="'.$column.'" style="cursor: pointer" class="badge badge-danger">'.abs($value).' Hari Lagi</span>';
                                        } else if($value >= -60){
                                            $label = '<span id="btn-dokumen" data-id="'.$id.'" data-nm_dokumen="'.$column.'" class="badge" style="cursor: pointer;background-color: #FFFF00;color: gray;">'.abs($value).' Hari Lagi</span>';
                                        } else if($value >= -90){
                                            $label = '<span id="btn-dokumen" data-id="'.$id.'" data-nm_dokumen="'.$column.'" style="cursor: pointer" class="badge badge-warning">'.abs($value).' Hari Lagi</span>';
                                        } else {
                                            $label = '<span id="btn-dokumen" data-id="'.$id.'" data-nm_dokumen="'.$column.'" style="cursor: pointer" class="badge badge-primary">'.abs($value).' Hari Lagi</span>';
                                        }

                                        return $label;
                                    }
                                    
                                    function tanggal_indo($tanggal){
                                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                                        $split = explode('-', $tanggal);
                                        if($tanggal){
                                            return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                                        } else {
                                            return '-';
                                        }
                                    }
                                    foreach($supir as $key => $value){ 
                                ?>
                                <tr>
                                    <td><?= $i ?></td>
                                    <td><?= $value->nama_perusahaan ?></td>
                                    <td><?= $value->nama_awak ?></td>
                                    <td><?= $value->bagian ?></td>
                                    <td><?= $value->usia." Tahun" ?></td>
                                    <td><?= tanggal_indo($value->masa_berlaku_sim_awak) ?></td>
                                    <td><?= reminder_label($value->reminder_sim_awak, "SIM", $value->id_awak) ?></td>
                                    <td><?= tanggal_indo($value->masa_berlaku_idcard_awak) ?></td>
                                    <td><?= reminder_label($value->reminder_idcard_awak, "IDCARD", $value->id_awak) ?></td>
                                </tr>
                                <?php $i++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="modalDokumen" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header gradient">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal-title" id="title-data-dokumen"></h6>
            </div>
            <div class="modal-body">
                <div id="dokumen-supir-view"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $("document").ready(function(){

        $("#table_reminder_supir").dataTable({
            scrollCollapse: true,
        });
        $('.select2').select2();

    });

    $(document).on("click", "#btn-dokumen", function(e){
        var id = $(this).data("id");
        var nm_dokumen = $(this).data("nm_dokumen");

        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('reminder/dokumen') ?>",
            "method": "POST",
            "data": {
                "nm_dokumen": nm_dokumen,
                "type": "awak",
                "id" : id
            }
        }).done(function (response) {
            var data = JSON.parse(response);
            if(data.data.dokumen == "" || data.data.dokumen == null){
                $("#dokumen-supir-view").html("<center><img class='img-thumbnail' src='<?= base_url() ?>asset/img/noimage_336_290.jpg' width='30%' /></center>");
            } else {
                var view = '<iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="<?= base_url() ?>asset/pdfviewer/web/viewer.html?file=<?= base_url('dokumen/berkas-awak/') ?>' + data.data.dokumen+ '" scrolling="no"></iframe>';
                $("#dokumen-supir-view").html(view);
            }
            
            $("#title-data-dokumen").html("DOKUMEN "+nm_dokumen);
            $("#modalDokumen").modal("show");
        })
    });

    $(document).on("click", "#btn_send_reminder", function(e){
        e.preventDefault();
        swal({
            title: "",
            text: "Mohon Tunggu",
            type: "",
            imageUrl: "<?= base_url('asset/img/loading.gif') ?>",
            showConfirmButton: false
        });

        $.ajax({
            "async": true,
            "crossDomain": true,
            "url": "<?= base_url('reminder/send_mail/supir/manual') ?>",
            "method": "GET"
        }).done(function (response) {
            var data = JSON.parse(response)
            var message = data.message;
            if(data.status == "success"){
                swal({
                    title: "Berhasil",
                    text: message.toUpperCase(),
                    type: "success",
                });
            } else {
                swal("Login Gagal.", message.toUpperCase(), "warning");
            }
        });
    });
</script>
