<?php
    class Model_temuan extends CI_Model {

        public function get_temuan($id = null, $transportir = null, $bulan = null, $tahun = null){

            if(isset($id)){
                $this->db->where("a.id_temuan", $id);
            }
            $this->db->select("a.id_temuan, a.id_pemilik, a.id_truck, a.jenis_temuan, a.deskripsi_temuan, a.tgl_temuan, a.status, a.foto_pertama, a.foto_kedua, a.foto_ketiga, a.tindak_lanjut_pertama, a.tindak_lanjut_kedua, a.tindak_lanjut_ketiga, a.komentar_tindak_lanjut, a.tgl_tindak_lanjut, t.no_polisi, p.nama_perusahaan, p.no_telfon, p.no_telfon_kedua, a.status_tindak_lanjut");
            $this->db->from("tbl_t_temuan a");
            $this->db->join("tbl_m_truck t", "a.id_truck = t.id_truck", "left");
            $this->db->join("tbl_m_pemilik p", "a.id_pemilik = p.id_pemilik", "left");;
            $this->db->order_by("a.id_temuan", "desc");


           // $this->db->join("tbl_m_pemilik p", "t.id_pemilik = p.id_pemilik");

            if(isset($id)){
                $this->db->where("a.id_temuan", $id);
            }

            if(isset($transportir)){
                if($transportir != ""){
                    $this->db->where("p.id_pemilik", $transportir);
                }
            }

            if(isset($bulan)){
                if($bulan != ""){
                    $this->db->where("MONTH(a.tgl_temuan)", $bulan);
                }
            }

            if(isset($tahun)){
                if($tahun != ""){
                    $this->db->where("YEAR(a.tgl_temuan)", $tahun);
                }
            }

            // if(isset($status)){
            //     if($status != ""){
            //         if($status == "menunggu"){
            //             $this->db->where("approve_admin IS NULL");
            //             $this->db->where("approve_manager_lokasi IS NULL");
            //         } else if($status == "approve admin"){
            //             $this->db->where("approve_admin", "Y");
            //             $this->db->where("approve_manager_lokasi IS NULL");
            //         } else if($status == "reject admin"){
            //             $this->db->where("approve_admin", "N");
            //             $this->db->where("approve_manager_lokasi IS NULL");
            //         } else if($status == "approve manager"){
            //             $this->db->where("approve_admin", "Y");
            //             $this->db->where("approve_manager_lokasi", "Y");
            //         } else if($status == "reject manager"){
            //             $this->db->where("approve_admin", "Y");
            //             $this->db->where("approve_manager_lokasi", "N");
            //         }
            //     }
            // }

            if($this->session->userdata("level_user") == "PEMILIK"){
                $this->db->where("p.id_user", $this->session->userdata("id_user"));
            }



            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }

        }

        public function view_nopol($id_pemilik, $idTruck){
            $this->db->where("id_pemilik", $id_pemilik);
            $this->db->order_by("no_polisi", "ASC");
            $query = $this->db->get("tbl_m_truck");
            $output = '<option value="">Pilih Nopol</option>';
            foreach ($query->result() as $row) {
                if($idTruck == $row->id_truck){
                    $selected = "selected";
                }
                else {
                    $selected = "";
                }
                $output .= '<option value="'.$row->id_truck.'"'.$selected.'>'.$row->no_polisi.'</option>';
            }
            return $output;
        }

        // public function viewNopol($idPemilik){
        //     $this->db->select("truck.id_truck, truck.id_pemilik, truck.no_polisi, pemilik.id_pemilik, pemilik.nama_pemilik");
        //     $this->db->from("tbl_m_truck truck");
        //     $this->db->join("tbl_m_pemilikpemilik", "pemilik.id_pemilik=truck.id_pemilik", "left");
        //     $this->db->where("id_pemilik", $idPemilik);
        //     $this->db->order_by("id_pemilik");
        //     return $this->db->get()->result();
        // }

        // public function get_truck($id){
    
        //     $this->db->select("truck.id_truck, truck.no_polisi");
        //     $this->db->from("tbl_m_truck truck");
        //     $this->db->where("id_pemilik=", $id);
        //     // $this->db->join("tbl_m_pemilik pemilik", "pemilik")
        //     $data = $this->db->get();
        //     if($data->num_rows() > 0){
        //         return $data->result();
        //     } else {
        //         return array();
        //     }
        // }

         public function get_pemilik(){
            $this->db->select("pemilik.id_pemilik, pemilik.nama_perusahaan");
            $this->db->from("tbl_m_pemilik pemilik");
            $this->db->join("tbl_m_truck truck", "truck.id_truck=pemilik.id_pemilik", "left");
            return $this->db->get()->result();
            }


        public function tambah_temuan($data){
            $this->db->insert("tbl_t_temuan", $data);
            if($this->db->affected_rows() > 0){
                return $this->db->insert_id();
            } else {
                return false;
            }
        }

        public function edit_temuan($data, $id){
            $this->db->where("id_temuan", $id)->update("tbl_t_temuan", $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function hapus_temuan($id){
            $this->db->where("id_temuan", $id)->delete("tbl_t_temuan");
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

    }
?>