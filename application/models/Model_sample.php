<?php
    class Model_sample extends CI_Model {

        protected $table = "sample_crud";

        public function getData($id = null){
            if(isset($id)){
                $this->db->where("id_sample", $id);
            }

            $this->db->select("id_sample, nama")->from($this->table)->order_by("id_sample", "desc");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
        }

        public function addData($data){
            $this->db->insert($this->table, $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function updateData($data, $id){
            $this->db->where("id_sample", $id)->update($this->table, $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function deleteData($id){
            $this->db->where("id_sample", $id)->delete($this->table);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

    }
?>