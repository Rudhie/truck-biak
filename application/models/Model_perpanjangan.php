<?php
    class Model_perpanjangan extends CI_Model {

        public function get_perpanjangan($type = null, $id = null, $transportir = null, $bulan = null, $tahun = null, $status = null, $is_id_card = null){
            $this->db->select("a.id_aktifitas, a.no_aktifitas, a.id_pemilik, p.nama_pemilik, a.id_truck, t.no_polisi, a.id_awak, s.nama_awak, a.id_keperluan, k.nm_keperluan, a.detail_keperluan, a.tgl_masa_berlaku_baru, a.tgl_aktifitas, a.tingkat_keperluan, a.status_aktifitas, 
            a.komentar, a.no_aktifitas, a.catatan_admin, a.catatan_manager_lokasi, a.catatan_manager_region, t.masa_berlaku_stnk, t.masa_berlaku_skt, t.keur, t.metrologi_tera, s.masa_berlaku_sim_awak, s.masa_berlaku_idcard_awak, s.bagian,
            p.nama_perusahaan");
            $this->db->from("tbl_t_aktifitas a");
            $this->db->join("tbl_m_keperluan k", "a.id_keperluan = k.id_keperluan");
            $this->db->join("tbl_m_awak s", "a.id_awak = s.id_awak", "left");
            $this->db->order_by("a.id_aktifitas", "desc");

            if(isset($type)){
                if($type == "truck"){
                    $this->db->join("tbl_m_truck t", "a.id_truck = t.id_truck", "left");
                    $this->db->where("a.id_awak", "0");
                } else {
                    $this->db->select("a.id_truck_baru, tb.no_polisi as no_polisi_baru");
                    $this->db->join("tbl_m_truck t", "s.id_truck = t.id_truck", "left");
                    $this->db->join("tbl_m_truck tb", "a.id_truck_baru = tb.id_truck", "left");
                    $this->db->where("a.id_truck", "0");
                }
            }

            $this->db->join("tbl_m_pemilik p", "t.id_pemilik = p.id_pemilik", "left");

            if(isset($id)){
                $this->db->where("a.id_aktifitas", $id);
            }

            if(isset($transportir)){
                if($transportir != ""){
                    $this->db->where("p.id_pemilik", $transportir);
                }
            }

            if(isset($bulan)){
                if($bulan != ""){
                    $this->db->where("MONTH(a.tgl_aktifitas)", $bulan);
                }
            }

            if(isset($tahun)){
                if($tahun != ""){
                    $this->db->where("YEAR(a.tgl_aktifitas)", $tahun);
                }
            }

            if(isset($status)){
                if($status != ""){
                    $this->db->where("a.status_aktifitas", $status);
                }
            }

            if($this->session->userdata("level_user") == "PEMILIK"){
                $this->db->where("p.id_user", $this->session->userdata("id_user"));
            }


            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }

        }

        public function get_dokumen_perpanjangan($id = null){
            if(isset($id)){
                $this->db->where("ta.id_aktifitas", $id);
            }

            $this->db->select("ta.id_aktifitas, tda.id_dokumen, msp.keterangan, tda.id_syarat, msp.nm_syarat, tda.dokumen_syarat, tda.format_dokumen, tda.approval_admin, tda.note_approval_admin, tda.approval_manager_lokasi, tda.note_approval_manager_lokasi, tda.approval_manager_region, tda.note_approval_manager_region");
            $this->db->from("tbl_t_aktifitas ta");
            $this->db->join("tbl_t_dokumen_aktifitas tda", "ta.id_aktifitas = tda.id_aktifitas");
            $this->db->join("tbl_m_syarat_perpanjangan msp", "tda.id_syarat = msp.id_syarat");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_keperluan($page = null, $id = null){
            if(isset($page)){
                $this->db->where("type_keperluan", $page);
            }

            if(isset($id)){
                $this->db->where("id_keperluan", $id);
            }
            $this->db->select("id_keperluan, nm_keperluan, type_keperluan");
            $this->db->from("tbl_m_keperluan");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_syarat_keperluan($page = null){
            if(isset($page)){
                $this->db->where("tbl_m_keperluan.type_keperluan", $page);
            }
            $this->db->select("tbl_m_syarat_perpanjangan.id_keperluan, tbl_m_syarat_perpanjangan.nm_syarat, tbl_m_syarat_perpanjangan.keterangan");
            $this->db->from("tbl_m_syarat_perpanjangan");
            $this->db->join("tbl_m_keperluan", "tbl_m_syarat_perpanjangan.id_keperluan = tbl_m_keperluan.id_keperluan");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_keperluan_by_name($name){
            $this->db->select("id_keperluan, nm_keperluan, type_keperluan, masa_berlaku");
            $this->db->from("tbl_m_keperluan");
            $this->db->like("nm_keperluan", $name, 'both');
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_truck(){
            if($this->session->userdata("level_user") == "PEMILIK"){
                $this->db->where("pemilik.id_user", $this->session->userdata("id_user"));
            }
            $this->db->select("truck.id_truck, truck.no_polisi, truck.id_merek, merek.nm_merek");
            $this->db->from("tbl_m_truck truck");
            $this->db->join("tbl_m_pemilik pemilik", "pemilik.id_pemilik = truck.id_pemilik");
            $this->db->join("tbl_m_merek merek", "truck.id_merek = merek.id_merek", "left");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_supir(){
            if($this->session->userdata("level_user") == "PEMILIK"){
                $this->db->where("pemilik.id_user", $this->session->userdata("id_user"));
            }
            $this->db->select("awak.id_awak, awak.nip_awak, awak.nama_awak, awak.bagian");
            $this->db->from("tbl_m_awak awak");
            $this->db->join("tbl_m_truck truck", "awak.id_truck = truck.id_truck");
            $this->db->join("tbl_m_pemilik pemilik", "pemilik.id_pemilik = truck.id_pemilik");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function tambah_perpanjangan($data){
            $this->db->insert("tbl_t_aktifitas", $data);
            if($this->db->affected_rows() > 0){
                return $this->db->insert_id();
            } else {
                return false;
            }
        }

        public function edit_perpanjangan($data, $id){
            $this->db->where("id_aktifitas", $id)->update("tbl_t_aktifitas", $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function ubahAwak($data, $idAwak){
            $this->db->where("id_awak", $idAwak)->update("tbl_m_awak", $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function hapus_perpanjangan($id){
            $this->db->where("id_aktifitas", $id)->delete("tbl_t_aktifitas");
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function get_persyaratan($id_keperluan){
            $data = $this->db->where("id_keperluan", $id_keperluan)->get("tbl_m_syarat_perpanjangan");
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
        }

        public function tambah_dokumen_perpanjangan($data){
            $this->db->insert_batch("tbl_t_dokumen_aktifitas", $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function edit_dokumen_perpanjangan($data){
            $this->db->update_batch("tbl_t_dokumen_aktifitas", $data, "id_dokumen");
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function get_no_aktifitas_akhir($tahun){
            $this->db->select("max(no_aktifitas) as max_no_aktifitas");
            $this->db->from("tbl_t_aktifitas");
            $this->db->where("YEAR(tgl_aktifitas)", $tahun);
            $query = $this->db->get();
            return $query->row();
        }

        public function get_email($level_user, $id = null){
            if(isset($id)){
                $this->db->where("p.id_pemilik", $id);
            }

            $this->db->select("u.email");
            $this->db->from("tbl_m_user u");
            $this->db->join("tbl_m_pemilik p", "u.id_user = p.id_user", "left");
            $this->db->where("u.level_user", $level_user);
            $query = $this->db->get();
            return $query->result_array();
        }

        public function get_email_general(){
            $this->db->select("email");
            $this->db->from("tbl_general_email");
            $this->db->where("is_active", "Y");
            $query = $this->db->get();
            return $query->result_array();
        }

        public function check_masa_berlaku($type, $id){
            if($type == "truck"){
                $sql = "SELECT
                DATEDIFF( now( ), t.masa_berlaku_stnk ) AS reminder_stnk,
                DATEDIFF( now( ), t.masa_berlaku_skt ) AS reminder_skt,
                DATEDIFF( now( ), t.keur ) AS reminder_keur,
                DATEDIFF( now( ), t.metrologi_tera ) AS reminder_tera,
                DATEDIFF( now( ), t.masa_berlaku_kim ) AS reminder_kim
                FROM tbl_m_truck t WHERE id_truck = '$id'";
            } else {
                $sql = "SELECT 
                DATEDIFF( now( ), s.masa_berlaku_sim_awak ) AS reminder_sim_awak, 
                DATEDIFF( now( ), s.masa_berlaku_idcard_awak ) AS reminder_idcard_awak 
                FROM tbl_m_awak s WHERE s.id_awak = '$id'";
            }

            $data = $this->db->query($sql);
            if($data->num_rows() > 0){
                return $data->result_array();
            } else {
                return array();
            }
        }

    }
?>