<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pemilik extends CI_Model {

    private $_tbl_m_pemilik = "tbl_m_pemilik";

    public function getPemilik($id = null){
        if(isset($id)){
        $this->db->where("id_pemilik", $id);
        }
        $this->db->select("*");
        $this->db->from("tbl_m_pemilik");
        $this->db->join("tbl_m_user", "tbl_m_pemilik.id_user = tbl_m_user.id_user");
        $this->db->order_by("id_pemilik", "desc");
        
        return $this->db->get()->result();
        
    }

    public function tambahPemilik($data){
        $this->db->insert("tbl_m_pemilik", $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    public function ubahPemilik($data, $idPemilik){
        $this->db->where("id_pemilik", $idPemilik)->update("tbl_m_pemilik", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function ubahUser($data, $idUser){
        $this->db->where("id_user", $idUser)->update("tbl_m_user", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    public function hapusPemilik($idPemilik){
        $this->db->where("id_pemilik", $idPemilik)->delete("tbl_m_pemilik");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function view(){
        return $this->db->get('tbl_m_pemilik')->result(); // Tampilkan semua data yang ada di tabel
    }

    public function hapusUserPemilik($idUser){
        $this->db->where("id_user", $idUser)->delete("tbl_m_user");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
}
?>