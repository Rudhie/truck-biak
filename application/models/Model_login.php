<?php
    class Model_login extends CI_Model {

        public function action_login($username, $password){

            $this->db->select("tbl_m_user.id_user, tbl_m_user.fullname, tbl_m_user.email, tbl_m_user.no_telfon, tbl_m_user.username, tbl_m_user.level_user");
            $this->db->from("tbl_m_user");
            $this->db->where("tbl_m_user.username", $username);
            $this->db->where("tbl_m_user.password", $password);
            
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row_array();
            } else {
                return false;
            }

        }

        public function get_pemilik($id_user){

            $this->db->select("id_pemilik");
            $this->db->from("tbl_m_pemilik");
            $this->db->where("id_user", $id_user);
            
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row_array();
            } else {
                return false;
            }

        }
    }
?>