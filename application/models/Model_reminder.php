<?php
    class Model_reminder extends CI_Model {

        public function get_data_reminder_truck($transportir = null){
            $level_user = $this->session->userdata("level_user");
            $id_user = $this->session->userdata("id_user");
            $wherePemilik = "";
            if($level_user == "PEMILIK"){
                $wherePemilik = "WHERE id_user = '".$id_user."' ";
            }

            if(isset($transportir)){
                if($transportir != ""){
                    $wherePemilik = "WHERE id_pemilik = '".$transportir."' ";
                }
            }

            $sql = "SELECT
                        * 
                    FROM
                        (
                    SELECT
                        t.id_truck,
                        u.id_user,
                        p.id_pemilik,
                        p.nama_pemilik,
                        p.nama_perusahaan,
                        t.no_polisi,
                        t.masa_berlaku_stnk,
                        DATEDIFF( now( ), t.masa_berlaku_stnk ) AS reminder_stnk,
                        t.masa_berlaku_skt,
                        DATEDIFF( now( ), t.masa_berlaku_skt ) AS reminder_skt,
                        t.keur,
                        DATEDIFF( now( ), t.keur ) AS reminder_keur,
                        t.metrologi_tera,
                        DATEDIFF( now( ), t.metrologi_tera ) AS reminder_tera,
                        t.masa_berlaku_kim,
                        DATEDIFF( now( ), t.masa_berlaku_kim ) AS reminder_kim
                    FROM
                        tbl_m_truck t
                        LEFT JOIN tbl_m_pemilik p ON t.id_pemilik = p.id_pemilik 
                        JOIN tbl_m_user u ON p.id_user = u.id_user
                        ) r 
                    
                        $wherePemilik ORDER BY r.id_truck DESC";
            $data = $this->db->query($sql);
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }

        }

        public function get_data_reminder_supir($transportir = null){
            $level_user = $this->session->userdata("level_user");
            $id_user = $this->session->userdata("id_user");
            $wherePemilik = "";
            if($level_user == "PEMILIK"){
                $wherePemilik = "WHERE id_user = '".$id_user."' ";
            }

            if(isset($transportir)){
                if($transportir != ""){
                    $wherePemilik = "WHERE id_pemilik = '".$transportir."' ";
                }
            }

            $sql = "SELECT
                        * 
                    FROM
                        (
                    SELECT
                        s.id_awak,
                        t.id_truck,
                        t.id_pemilik,
                        p.id_user,
                        p.nama_perusahaan,
                        t.no_polisi,
                        s.nip_awak,
                        s.nama_awak,
                        s.bagian,
                        s.masa_berlaku_sim_awak,
                        abs(TIMESTAMPDIFF( YEAR, NOW( ), s.tgl_lahir_awak )) as usia,
                        DATEDIFF( now( ), s.masa_berlaku_sim_awak ) AS reminder_sim_awak,
                        s.masa_berlaku_idcard_awak,
                        DATEDIFF( now( ), s.masa_berlaku_idcard_awak ) AS reminder_idcard_awak
                    FROM
                        tbl_m_awak s
                        LEFT JOIN tbl_m_truck t ON s.id_truck = t.id_truck
                        LEFT JOIN tbl_m_pemilik p ON t.id_pemilik = p.id_pemilik 
                        JOIN tbl_m_user u ON p.id_user = u.id_user
                        ) r 
                    
                        $wherePemilik ORDER BY r.id_awak DESC";
            $data = $this->db->query($sql);
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }

        }

        public function get_temuan(){
            $this->db->select("a.id_temuan, a.id_pemilik, a.id_truck, a.jenis_temuan, a.deskripsi_temuan, a.tgl_temuan, a.status, a.foto_pertama, a.foto_kedua, a.foto_ketiga, a.tindak_lanjut_pertama, a.tindak_lanjut_kedua, a.tindak_lanjut_ketiga, a.komentar_tindak_lanjut, a.tgl_tindak_lanjut, t.no_polisi, p.nama_perusahaan, p.no_telfon, p.no_telfon_kedua");
            $this->db->from("tbl_t_temuan a");
            $this->db->join("tbl_m_truck t", "a.id_truck = t.id_truck", "left");
            $this->db->join("tbl_m_pemilik p", "a.id_pemilik = p.id_pemilik", "left");
            $this->db->where("(a.tindak_lanjut_pertama IS NULL AND a.tindak_lanjut_kedua IS NULL AND a.tindak_lanjut_ketiga IS NULL)");
            $this->db->order_by("a.id_temuan", "desc");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_pemilik(){
            $this->db->select("");
            $this->db->from("tbl_m_pemilik");
            $this->db->join("tbl_m_user", "tbl_m_pemilik.id_user = tbl_m_user.id_user");
            return $this->db->get()->result();
        }

        public function add_log_reminder($data){
            $this->db->insert("tbl_log_reminder", $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function get_dokumen($type, $column, $id){
            if($type == "awak"){
                $this->db->select("dokumen_".$column."_awak as dokumen");
            } else {
                $this->db->select("dokumen_".$column." as dokumen");
            }
            $this->db->from("tbl_m_".$type);
            $this->db->where("id_".$type, $id);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return false;
            }
        }

    }
?>