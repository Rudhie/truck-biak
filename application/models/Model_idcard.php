<?php
    class Model_idcard extends CI_Model {

        public function get_data_idcard($id = null, $transportir = null, $bulan = null, $tahun = null, $status = null){
            $id_keperluan = $this->keperluan_idcard();

            if(isset($id)){
                $this->db->where("a.id_aktifitas", $id);
            }

            if($this->session->userdata("level_user") == "PEMILIK"){
                $this->db->where("p.id_user", $this->session->userdata("id_user"));
            }

            if(isset($transportir)){
                if($transportir != ""){
                    $this->db->where("p.id_pemilik", $transportir);
                }
            }

            if(isset($bulan)){
                if($bulan != ""){
                    $this->db->where("MONTH(a.tgl_aktifitas)", $bulan);
                }
            }

            if(isset($tahun)){
                if($tahun != ""){
                    $this->db->where("YEAR(a.tgl_aktifitas)", $tahun);
                }
            }

            if(isset($status)){
                if($status != ""){
                    if($status == "menunggu"){
                        $this->db->where("approve_admin IS NULL");
                        $this->db->where("approve_manager_lokasi IS NULL");
                    } else if($status == "approve admin"){
                        $this->db->where("approve_admin", "Y");
                        $this->db->where("approve_manager_lokasi IS NULL");
                    } else if($status == "reject admin"){
                        $this->db->where("approve_admin", "N");
                        $this->db->where("approve_manager_lokasi IS NULL");
                    } else if($status == "approve manager"){
                        $this->db->where("approve_admin", "Y");
                        $this->db->where("approve_manager_lokasi", "Y");
                    } else if($status == "reject manager"){
                        $this->db->where("approve_admin", "Y");
                        $this->db->where("approve_manager_lokasi", "N");
                    }
                }
            }

            $this->db->select("a.id_aktifitas, a.id_pemilik, p.nama_perusahaan, p.nama_pemilik, a.id_truck, t.no_polisi, a.id_awak, s.nama_awak, a.id_keperluan, k.nm_keperluan, a.detail_keperluan, a.tgl_aktifitas, a.tingkat_keperluan, a.status_aktifitas, 
            a.approve_admin, a.note_admin, a.tgl_action_admin, a.approve_manager_lokasi, a.note_manager_lokasi, a.tgl_action_manager_lokasi, 
            a.approve_manager_region, a.note_manager_region, a.tgl_action_manager_region,
            a.komentar, a.dokumen_pengajuan, a.foto_pertama, a.foto_kedua, a.foto_ketiga, s.masa_berlaku_idcard_awak, s.bagian");
            $this->db->from("tbl_t_aktifitas a");
            $this->db->join("tbl_m_keperluan k", "a.id_keperluan = k.id_keperluan");
            $this->db->join("tbl_m_awak s", "a.id_awak = s.id_awak", "left");
            $this->db->join("tbl_m_truck t", "s.id_truck = t.id_truck");
            $this->db->join("tbl_m_pemilik p", "t.id_pemilik = p.id_pemilik");
            $this->db->where("a.id_keperluan", $id_keperluan);
            $this->db->order_by("a.id_aktifitas", "desc");

            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }

        }

        public function keperluan_idcard(){
            $this->db->select("m.id_keperluan")->from("tbl_m_keperluan m")->like("m.nm_keperluan", 'ID CARD', 'both');
            $data = $this->db->get()->row();
            return $data->id_keperluan;
        }

    }
?>