<?php
    class Model_user extends CI_Model {

        public function data_user($id = null){
            if(isset($id)){
                $this->db->where("tbl_m_user.id_user", $id);
            }

            $this->db->select("tbl_m_user.id_user, tbl_m_user.fullname, tbl_m_user.email, tbl_m_user.no_telfon, tbl_m_user.no_telfon_kedua, tbl_m_user.username, tbl_m_user.level_user");
            $this->db->from("tbl_m_user");
            $this->db->order_by("tbl_m_user.id_user", "desc");
            $this->db->where("tbl_m_user.level_user !=", "PEMILIK");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
        }

        public function add_user($data){
            $this->db->insert("tbl_m_user", $data);
            if($this->db->affected_rows() > 0){
                return $this->db->insert_id();
            } else {
                return false;
            }
        }

        public function update_user($data, $id_user){
            $this->db->where("id_user", $id_user)->update("tbl_m_user", $data);
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }

        public function check_existing_user($column, $value){
            $this->db->select("id_user");
            $this->db->from("tbl_m_user");
            $this->db->where($column, $value);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return false;
            }
        }

        public function delete_user($id_user){
            $this->db->where("id_user", $id_user)->delete("tbl_m_user");
            if($this->db->affected_rows() > 0){
                return true;
            } else {
                return false;
            }
        }
    }
?>