<?php
    class Model_dashboard extends CI_Model {

        public function count_pemilik(){
            $this->db->select("count(id_pemilik) as jumlah");
            $this->db->from("tbl_m_pemilik");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_truck($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_truck) as jumlah");
            $this->db->from("tbl_m_truck");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_supir_kernet($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("t.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(a.id_awak) as jumlah");
            $this->db->from("tbl_m_awak a");
            $this->db->join("tbl_m_truck t", "a.id_truck = t.id_truck");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_aktifitas($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_sim($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 2);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_stnk($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 1);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_skt($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 3);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_kim($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 4);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_kir($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 5);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_metrologi($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 6);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_newidcard($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 7);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_oldidcard($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 8);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_gantiawak($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("akt.id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_aktifitas) as jumlah");
            $this->db->from("tbl_t_aktifitas akt");
            $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_aktifitas)", date("Y"));
            $this->db->where("perlu.id_keperluan", 9);
            $this->db->where("akt.status_aktifitas!=", 5);
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_urgent_temuan($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_temuan) as jumlah");
            $this->db->from("tbl_t_temuan");
            // $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_temuan)", date("Y"));
            // $this->db->where("perlu.id_keperluan", 9);
            $this->db->where("status", "URGENT");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_medium_temuan($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("id_pemilik", $id_pemilik);
            }
            $this->db->select("count(id_temuan) as jumlah");
            $this->db->from("tbl_t_temuan");
            // $this->db->join("tbl_m_keperluan perlu", "perlu.id_keperluan=akt.id_keperluan", "left");
            $this->db->where("YEAR(tgl_temuan)", date("Y"));
            // $this->db->where("perlu.id_keperluan", 9);
            $this->db->where("status", "MEDIUM");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->row();
            } else {
                return 0;
            }
        }

        public function count_notifikasi($level_user, $type){
            if($type == "count"){
                $this->db->select("count(a.id_aktifitas) as jumlah");
            } else {
                $this->db->select("a.*, k.nm_keperluan, p.nama_perusahaan, t.no_polisi, aw.nama_awak");
                $this->db->limit(5);
            }

            $this->db->from("tbl_t_aktifitas a");
            $this->db->join("tbl_m_keperluan k", "a.id_keperluan = k.id_keperluan");
            $this->db->join("tbl_m_pemilik p", "a.id_pemilik = p.id_pemilik");
            $this->db->join("tbl_m_truck t", "a.id_truck = t.id_truck", "left");
            $this->db->join("tbl_m_awak aw", "aw.id_awak = a.id_awak", "left");
            if($level_user == "ADMIN"){
                $this->db->where("a.status_aktifitas", "0");
            } else if($level_user == "MANAGER LOKASI"){
                $this->db->where("a.status_aktifitas", "1");
            } else if($level_user == "MANAGER REGION"){
                $this->db->where("a.status_aktifitas", "3");
            } else {
                $this->db->where("a.status_aktifitas", "100");
            }
            $this->db->order_by("a.id_aktifitas", "desc");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                if($type == "count"){
                    return $data->row();
                } else {
                    return $data->result();
                }
                
            } else {
                return 0;
            }
        }

        public function get_rekap_pengajuan($id_pemilik = null){
            if(isset($id_pemilik)){
                $this->db->where("a.id_pemilik", $id_pemilik);
            }
            $this->db->select("k.type_keperluan, month(a.tgl_aktifitas) as bulan, count(a.id_aktifitas) as jml");
            $this->db->from("tbl_t_aktifitas a");
            $this->db->join("tbl_m_keperluan k", "a.id_keperluan = k.id_keperluan");
            $this->db->where("year(a.tgl_aktifitas)", date("Y"));
            $this->db->group_by("k.type_keperluan, month(a.tgl_aktifitas)");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }
    }
?>