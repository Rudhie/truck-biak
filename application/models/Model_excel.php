<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Model_excel extends CI_Model {
 //private $_tbl_bank = "tbl_bank";
	private $_tbl_permintaan = "tbl_permintaan";
	private $_tbl_invoice = "tbl_invoice";
	private $_tbl_spk = "tbl_spk";
	private $_tbl_penawaran = "tbl_penawaran";
	private $_tbl_pembayaran = "tbl_pembayaran";
	private $_tbl_pemesanan_po = "tbl_pemesanan_po";

  	public function view($id = null, $filter = null, $param_1 = null, $param_2 = null){
  	if(isset($id)){
			$this->db->where("tbl_spk.id_spk", $id);
		}
		if(isset($filter)){
			if($filter == "tanggal"){
				$this->db->where("tbl_invoice.tgl_invoice", $param_1);
			} else if($filter == "lokasi"){
				$this->db->where("SUBSTR( tbl_spk.no_spk, 1, 2) = ", $param_1);
			} else if($filter == "spk"){
				$this->db->where("SUBSTR( tbl_spk.no_spk, 4, 2) = ", $param_1);
				if(isset($param_2)){
					$this->db->where("tbl_spk.id_spk", $param_2);
				}
			} else if($filter == "bulan"){
				$this->db->where("month(tbl_invoice.tgl_invoice)", $param_1);
				$this->db->where("year(tbl_invoice.tgl_invoice)", $param_2);
			}
			else if($filter == "tahun"){
				
				$this->db->where("year(tbl_invoice.tgl_invoice)", $param_1);
			}
		}
    // return $this->db->get('tbl_pemesanan_po')->result();
		$this->db->select("tbl_spk.id_spk, tbl_pemesanan_po.desc_po, tbl_spk.id_penawaran, tbl_spk.no_spk, tbl_spk.nilai_spk, tbl_spk.tgl_spk, tbl_spk.pengiriman, tbl_spk.tempat_penyerahan, tbl_permintaan.nm_mitra, tbl_penawaran.no_spph, tbl_pemesanan_po.no_po, tbl_pemesanan_po.desc_po, tbl_pemesanan_po.tgl_pemesanan, tbl_pemesanan_po.tgl_akhir_pemesanan, tbl_penawaran.tgl_pengajuan as tgl_penawaran, tbl_permintaan.no_permintaan, tbl_permintaan.tgl_pengajuan as tgl_permintaan, tbl_permintaan.id_permintaan, tbl_permintaan.tgl_penutupan, tbl_pemesanan_po.no_seri_faktur, tbl_permintaan.pekerjaan_utama, tbl_pembayaran.nilai_pembayaran, tbl_pembayaran.tanggal_pembayaran, tbl_invoice.nm_bank, tbl_invoice.tgl_faktur, tbl_invoice.no_faktur, tbl_invoice.tgl_invoice");
		$this->db->from($this->_tbl_spk);
		$this->db->join($this->_tbl_invoice, "tbl_spk.id_spk = tbl_invoice.id_spk");
		$this->db->join($this->_tbl_penawaran, "tbl_spk.id_penawaran = tbl_penawaran.id_penawaran");
		$this->db->join($this->_tbl_permintaan, "tbl_penawaran.id_permintaan = tbl_permintaan.id_permintaan");
		
		$this->db->join($this->_tbl_pembayaran, "tbl_invoice.id_invoice = tbl_pembayaran.id_invoice", "left");
		$this->db->join($this->_tbl_pemesanan_po, "tbl_spk.id_spk = tbl_pemesanan_po.id_spk", "left");
		
		$this->db->order_by("tbl_invoice.tgl_invoice", "asc");
        return $this->db->get()->result();
 
  }
  public function get_spk($id = null, $filter = null, $param_1 = null, $param_2 = null){
  		if(isset($id)){
			$this->db->where("tbl_spk.id_spk", $id);
		}
		if(isset($filter)){
			if($filter == "tanggal"){
				$this->db->where("tbl_pemesanan_po.tgl_pemesanan", $param_1);
			} else if($filter == "lokasi"){
				$this->db->where("SUBSTR( tbl_spk.no_spk, 1, 2) = ", $param_1);
			} else if($filter == "spk"){
				$this->db->where("SUBSTR( tbl_spk.no_spk, 4, 2) = ", $param_1);
				if(isset($param_2)){
					$this->db->where("tbl_spk.id_spk", $param_2);
				}
			} else if($filter == "bulan"){
				$this->db->where("month(tbl_pemesanan_po.tgl_pemesanan)", $param_1);
				$this->db->where("year(tbl_pemesanan_po.tgl_pemesanan)", $param_2);
			}
			else if($filter == "tahun"){
				
				$this->db->where("year(tbl_pemesanan_po.tgl_pemesanan)", $param_1);
			}
		}
		$this->db->select("sum( nilai_spk ) AS nilai_spk");
		$this->db->from($this->_tbl_spk);
		
		$this->db->order_by("tbl_spk.id_spk", "desc");
	}

  public function getPembayaran(){
		$this->db->select("tbl_pembayaran.id_pembayaran, tbl_pembayaran.nilai_pembayaran, tbl_pembayaran.tanggal_pembayaran");
		$this->db->from($this->_tbl_pembayaran);
		$this->db->join($this->_tbl_invoice, "tbl_pembayaran.id_invoice = tbl_invoice.id_invoice");
		$this->db->order_by("tbl_pembayaran.id_pembayaran", "desc");
        return $this->db->get()->result();
	}
}