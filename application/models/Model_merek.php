<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_merek extends CI_Model {

    private $_tbl_m_merek = "tbl_m_merek";

    public function getMerek($id = null){
        if(isset($id)){
        $this->db->where("id_merek", $id);
        }
        $this->db->select("*");
        $this->db->from("tbl_m_merek");
        $this->db->order_by("id_merek", "desc");
        
        return $this->db->get()->result();
        
    }

    public function tambahMerek($data){
        $this->db->insert("tbl_m_merek", $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    public function ubahMerek($data, $idMerek){
        $this->db->where("id_merek", $idMerek)->update("tbl_m_merek", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    public function hapusMerek($idMerek){
        $this->db->where("id_merek", $idMerek)->delete("tbl_m_merek");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function view(){
        return $this->db->get('tbl_m_merek')->result(); // Tampilkan semua data yang ada di tabel
    }
}
?>