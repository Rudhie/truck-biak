<?php
    class Model_keperluan extends CI_Model {

        public function get_keperluan($id = null){
            if(isset($id)){
                $this->db->where("id_keperluan", $id);
            }
            $this->db->select("id_keperluan, nm_keperluan, type_keperluan");
            $this->db->from("tbl_m_keperluan");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_truck(){
            if($this->session->userdata("level_user") == "PEMILIK"){
                $this->db->where("pemilik.id_user", $this->session->userdata("id_user"));
            }
            $this->db->select("truck.id_truck, truck.no_polisi, truck.id_merek, merek.nm_merek");
            $this->db->from("tbl_m_truck truck");
            $this->db->join("tbl_m_pemilik pemilik", "pemilik.id_pemilik = truck.id_pemilik");
            $this->db->join("tbl_m_merek merek", "truck.id_merek = merek.id_merek", "left");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }

        public function get_supir(){
            if($this->session->userdata("level_user") == "PEMILIK"){
                $this->db->where("pemilik.id_user", $this->session->userdata("id_user"));
            }
            $this->db->select("awak.id_awak, awak.nip_awak, awak.nama_awak, awak.bagian");
            $this->db->from("tbl_m_awak awak");
            $this->db->join("tbl_m_truck truck", "awak.id_truck = truck.id_truck");
            $this->db->join("tbl_m_pemilik pemilik", "pemilik.id_pemilik = truck.id_pemilik");
            $data = $this->db->get();
            if($data->num_rows() > 0){
                return $data->result();
            } else {
                return array();
            }
        }
    }
?>