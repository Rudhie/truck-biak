<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_truck extends CI_Model {

    private $_tbl_truck = "tbl_truck";

    public function getTruck($id = null, $id_pemilik = null){
        if(isset($id)){
            $this->db->where("id_truck", $id);
        }

        if(isset($id_pemilik)){
            if($id_pemilik != "" && $id_pemilik != "all"){
                $this->db->where("truck.id_pemilik", $id_pemilik);
            }
        }
        $this->db->select("truck.*, pemilik.nama_pemilik as nm_pemilik, pemilik.nama_perusahaan, merek.nm_merek as nama_merek");
        $this->db->from("tbl_m_truck truck");
        $this->db->join("tbl_m_pemilik pemilik", "pemilik.id_pemilik = truck.id_pemilik", "left");
        $this->db->join("tbl_m_merek merek", "merek.id_merek = truck.id_merek", "left");
        $this->db->order_by("id_truck", "desc");
        
        return $this->db->get()->result();
        
    }

    public function view_all_pemilik(){
        $this->db->select("id_pemilik, nama_pemilik AS nama");
        $this->db->from("tbl_m_pemilik");
        $this->db->order_by("nama_pemilik"); 
        return $this->db->get()->result();
    }
    public function view_all_merek(){
        $this->db->select("id_merek, nm_merek AS nama");
        $this->db->from("tbl_m_merek");
        $this->db->order_by("nm_merek"); 
        return $this->db->get()->result();
    }

    public function tambahTruck($data){
        $this->db->insert("tbl_m_truck", $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    public function ubahTruck($data, $idTruck){
        $this->db->where("id_truck", $idTruck)->update("tbl_m_truck", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    public function hapusTruck($idTruck){
        $this->db->where("id_truck", $idTruck)->delete("tbl_m_truck");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function view(){
        return $this->db->get('tbl_m_truck')->result(); // Tampilkan semua data yang ada di tabel
    }
}
?>