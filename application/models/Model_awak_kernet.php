<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_awak_kernet extends CI_Model {

    private $_tbl_awak = "tbl_awak";

    public function getAwak($id, $filterTransportir){
        if(isset($id)){
        $this->db->where("id_awak", $id);
        }
        $this->db->select("awak.*, pemilik.nama_pemilik as nm_pemilik, pemilik.nama_perusahaan, truck.no_polisi as nomor_polisi, truck.produk as list_produk, truck.id_pemilik as nama_pemilik");
        $this->db->from("tbl_m_awak awak");
        $this->db->join("tbl_m_truck truck", "truck.id_truck = awak.id_truck", "left");
        $this->db->join("tbl_m_pemilik pemilik", "truck.id_pemilik = pemilik.id_pemilik", "left");
        if($filterTransportir!="all" && $filterTransportir!=""){
            $this->db->where('truck.id_pemilik =', $filterTransportir);
            
        }
        if($id!=""){
            $this->db->where("awak.id_awak", $id);
        }
        $this->db->order_by("id_awak", "desc");
        
        return $this->db->get()->result();
        
    }

    public function view_all_nopol(){
        $this->db->select("id_truck, no_polisi AS nopol");
        $this->db->from("tbl_m_truck");
        $this->db->order_by("no_polisi"); 
        return $this->db->get()->result();
    }
    public function view_all_transportir(){
        $this->db->select("id_pemilik, nama_pemilik AS nama");
        $this->db->from("tbl_m_pemilik");
        $this->db->order_by("nama_pemilik"); 
        return $this->db->get()->result();
    }

    public function tambahAwak($data){
        $this->db->insert("tbl_m_awak", $data);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
    
    public function ubahAwak($data, $idAwak){
        $this->db->where("id_awak", $idAwak)->update("tbl_m_awak", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    public function hapusAwak($idAwak){
        $this->db->where("id_awak", $idAwak)->delete("tbl_m_awak");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function view(){
        return $this->db->get('tbl_m_awak')->result(); // Tampilkan semua data yang ada di tabel
    }
}
?>