<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Merek extends CI_Controller {

	public function __construct(){
		parent::__construct();
        checkSessionUser();
		$this->load->model("Model_merek");
		$this->load->model("Model_user");
	}

	public function index(){
		$data["merek"] = $this->Model_merek->getMerek();
		$this->template->load("template", "merek/data-merek", $data);
	}

	public function tambah(){
		$this->template->load("template", "merek/form-merek");
	}

	public function edit(){
		$id = $this->input->get("id");
		$data["merek"] = $this->Model_merek->getMerek($id);
		$this->template->load("template", "merek/edit-merek", $data);
	}

	public function action_tambah(){
		$nm_merek = $this->input->post("nm_merek");
		$tipe = $this->input->post("tipe");
		$keterangan = $this->input->post("keterangan");

		$dataMerek = array(
		"nm_merek" => $nm_merek,
		"tipe" => $tipe,
		"keterangan" => $keterangan
		);

		$tambahMerek = $this->Model_merek->tambahMerek($dataMerek);
		if($tambahMerek){
			$this->session->set_flashdata("success", "BERHASIL MENYIMPAN");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENYIMPAN");
		}

		redirect("merek");
	}

	public function action_ubah($idMerek){
		$nm_merek = $this->input->post("nm_merek");
		$tipe = $this->input->post("tipe");
		$keterangan = $this->input->post("keterangan");

		$dataMerek = array(
		"nm_merek" => $nm_merek,
		"tipe" => $tipe,
		"keterangan" => $keterangan
		);

		$ubahMerek = $this->Model_merek->ubahMerek($dataMerek, $idMerek);
		if($ubahMerek){
			$this->session->set_flashdata("success", "BERHASIL MENGUBAH");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGUBAH");
		}

		
		redirect("merek");

	}

	public function action_hapus($idMerek){
		$hapusMerek = $this->Model_merek->hapusMerek($idMerek);
		if($hapusMerek){
			$this->session->set_flashdata("success", "BERHASIL MENGHAPUS");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGHAPUS");
		}

		redirect("merek");
	}

	public function export(){
		include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
        $namesheet = array("Master Data Merk Mobil");            
        $jumlah = array(4); 
        $header = array(
            array(
                'NO',
				'MERK',
				'TIPE',
				'KETERANGAN'
            )
        );
        $get_data = $this->Model_merek->getMerek();
        $i=0;
        while ($i < 1) {
            $objWorkSheet = $objPHPExcel->createSheet($i);
            $lastColumn = $objWorkSheet->getHighestColumn();
            for($x = 0; $x < $jumlah[$i]; $x++){                    
                $row = 1;                    
                $objWorkSheet->setCellValue($lastColumn.$row, $header[$i][$x]);
                $objWorkSheet
                    ->getColumnDimension($lastColumn)
                    ->setAutoSize(true);
                $lastColumn++;
            }
            
            $objWorkSheet->setTitle($namesheet[$i]);
            $i++;
        }     
        
        $index=0;
        $rowgabung = 2;
        while ($index < 1) {
            $worksheet = $objPHPExcel->setActiveSheetIndex($index);
            $row = 2;  
            if ($index == 0) {
                $a = 1;
                foreach ($get_data as $data) {       
                    $worksheet->setCellValue('A'.$row, $a); 
					$worksheet->setCellValue('B'.$row, $data->nm_merek);
					$worksheet->setCellValue('C'.$row, $data->tipe);
					$worksheet->setCellValue('D'.$row, $data->keterangan);
                    $row++;
                    $a++;
                }
            }
            $index++;
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $t = date("Y_m_d_H_i_s");
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=MASTER_DATA_MERK_MOBIL_".$t.".xlsx");
        $objWriter->save("php://output");
	}
}
?>