<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Excel extends CI_Controller {
  
  public function __construct(){
    parent::__construct();
    checkSessionUser();
    $this->load->model('Model_excel');
    $this->load->model('Model_perpanjangan');
  }
  
  public function index(){
    $filter = $this->input->get("param");
    if(isset($filter)){
      $param_2 = null;
      if($filter == "tanggal"){
        $param_1 = date("Y-m-d", strtotime(str_replace("/", "-", $this->input->get("date"))));
      } else if ($filter == "lokasi"){
        $param_1 = $this->input->get("location");
      } else if($filter == "spk"){
        if($this->input->get("spk") == "00"){
          $param_1 = "00";
        } else {
          $param = explode(".",$this->input->get("spk"));
          $param_1 = $param[1]; //AMBIL NO JURNAL SPK
          $param_2 = $param[0]; //AMBIL ID SPK
        }
      } else if($filter == "bulan"){
        $param_1 = $this->input->get("bulan");
        $param_2 = $this->input->get("tahun");
      }
      else if($filter == "tahun"){
        $param_1 = $this->input->get("tahun");
      }
      $tbl_pemesanan_po["excel"] = $this->Model_excel->view(null, $filter, $param_1, $param_2);
    } else {
      $tbl_pemesanan_po["excel"] = $this->Model_excel->view(null, "tanggal", date("Y-m-d"));
    }

    $tbl_pemesanan_po["excel"] = $this->Model_excel->view(null, $filter, $param_1, $param_2);
    $this->template->load("template", "excel/data-excel", $tbl_pemesanan_po);
  }
  //   $data['excel'] = $this->Model_excel->view();
  //   $this->load->view('excel/data-excel', $data);
  // }
  
  public function export(){
     $filter = $this->input->get("param");
       if(isset($filter)){
      $param_2 = null;
      if($filter == "tanggal"){
        $param_1 = date("Y-m-d", strtotime(str_replace("/", "-", $this->input->get("date"))));
      } else if ($filter == "lokasi"){
        $param_1 = $this->input->get("location");
      } else if($filter == "spk"){
        if($this->input->get("spk") == "00"){
          $param_1 = "00";
        } else {
          $param = explode(".",$this->input->get("spk"));
          $param_1 = $param[1]; //AMBIL NO JURNAL SPK
          $param_2 = $param[0]; //AMBIL ID SPK
          $tahun = date('Y');
          $bulan = date('m');
        }
      } else if($filter == "bulan"){
        $param_1 = $this->input->get("bulan");
        $param_2 = $this->input->get("tahun");
        $tahun = $this->input->get("tahun");
        $bulan = $this->input->get("bulan");
      }
      else if($filter == "tahun"){
        $param_1 = $this->input->get("tahun");
        $tahun = $this->input->get("tahun");
        $bulan = $this->input->get("bulan");
      }
      $tbl_pemesanan_po = $this->Model_excel->view(null, $filter, $param_1, $param_2);
    } else {
      $tbl_pemesanan_po = $this->Model_excel->view(null, "tanggal", date("Y-m-d"));
      $tahun = date('Y');
      $bulan = date('m');
//     // Load plugin PHPExcel nya $filter = $this->input->get("param");
   }
   
    include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
   
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Team Programmer')
                 ->setLastModifiedBy('Team Programmer')
                 ->setTitle("PT PELANGI LINTAS SEJAHTERA")
                 ->setSubject("Laporan PO ")
                 ->setDescription("Laporan Semua Data PO")
                 ->setKeywords("PT LINTAS PELANGI SEJAHTERA");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      //'color' => array('argb'=> 'FF808080'),
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      ),
      'fill' => array(
        'type' => PHPExcel_Style_Fill :: FILL_GRADIENT_LINEAR,
        'startcolor' => array(
              'argb' => 'FFA0A0A0'
            ),
        'endcolor' => array(
              'argb' => 'FFFFFFFF'
      ))
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      ),
      // 'fill' => array(
      //   'type' => PHPExcel_Style_Fill :: FILL_GRADIENT_LINEAR,
      //   'startcolor' => array(
      //         'argb' => 'FFA0A0A0'
      //       ),
      //   'endcolor' => array(
      //         'argb' => 'FFFFFFFF'
      // ))
    );
        function tanggal_indo($tanggal){
                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                        $split = explode('-', $tanggal);
                        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                   }
                   function bulan_indo($tanggal){
                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                        $split = explode('-', $tanggal);
                        return $bulan[ (int)$split[1] ] ;
                   }
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "PT PELANGI LINTAS SEJAHTERA"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:R1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20); 
    $excel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

    $excel->setActiveSheetIndex(0)->setCellValue('A2', "Laporan Po Tahun ".$tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A2:R2'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); 
    $excel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "Bulan");
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "No Nota"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "Tanggal Tagihan"); // Set kolom B3 dengan tulisan"
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "User"); // Set kolom C3 dengan tulisan "NAMA"
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "Nomor SPK"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "Keterangan"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "Tgl Faktur");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "No Faktur"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "Nilai O P"); // Set kolom B3 dengan tulisan"
    $excel->setActiveSheetIndex(0)->setCellValue('J3', "D P P");
    $excel->setActiveSheetIndex(0)->setCellValue('K3', "Jumlah PPn"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('L3', "Tgl Di Bayar"); // Set kolom B3 dengan tulisan"
    $excel->setActiveSheetIndex(0)->setCellValue('M3', "Yg Di Transfer");
    $excel->setActiveSheetIndex(0)->setCellValue('N3', "PPh 2 %"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('O3', "PPh 1,5 %"); // Set kolom B3 dengan tulisan"
    $excel->setActiveSheetIndex(0)->setCellValue('P3', "Total");
    $excel->setActiveSheetIndex(0)->setCellValue('Q3', "Selisih"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('R3', "Bank"); // Set kolom B3 dengan tulisan"
    $excel->setActiveSheetIndex(0)->setCellValue('S3', "Denda");
   
    // $excel->setActiveSheetIndex(0)->setCellValue('D3', "JENIS KELAMIN"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
    // $excel->setActiveSheetIndex(0)->setCellValue('E3', "ALAMAT"); // Set kolom E3 dengan tulisan "ALAMAT"
    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('S3')->applyFromArray($style_col);
   
    // $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    // $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
  
    //$tbl_pemesanan_po["excel"] = $this->Model_excel->view(null, "tanggal", date("Y-m-d"));
    //$tbl_pemesanan_po = $this->Model_excel->view(null, $filter, $param_1, $param_2);
    //$tbl_pemesanan_po["excel"] = $this->Model_excel->view(null, $filter, $param_1, $param_2);

                    //echo'<td>'.tanggal_indo($data->tgl_pemesanan).'</td>';
    $numrow = 4;
    $no = 0;
    $y = 0;
    $t = 0;
    for($i = 1; $i <= 12; $i++){
      if($i == 1){
        $numrow = $numrow + $no;
      } else {
        $numrow = $numrow + $no - 2;
      }
    
    $tbl_pemesanan_po = $this->Model_excel->view(null, 'bulan', $i, $tahun);
    $no = 1;
    
    if($tbl_pemesanan_po){
      $total_data = count($tbl_pemesanan_po);
      foreach($tbl_pemesanan_po as $data){ // Lakukan looping pada variabel siswa
        //$excel->setActiveSheetIndex(0)->setCellValue('A2', "Laporan Po Tahun ".$tahun);
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, bulan_indo($data->tgl_invoice));
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $no);
        $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, tanggal_indo($data->tgl_invoice));
        $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->nm_mitra);
        $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->no_spk);
        $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->pekerjaan_utama);
        $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, tanggal_indo($data->tgl_faktur));
        $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->no_faktur);
        $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->nilai_spk);
        $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->nilai_spk / 1.1);
        $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $data->nilai_spk / 1.1 * (10 / 100));
        //$ppn = (10 / 100) *  $harga_satuan;
        $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, tanggal_indo($data->tanggal_pembayaran));
        $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $data->nilai_pembayaran);
        $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $data->nilai_spk / 1.1 *(2 / 100));
        $excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $data->nilai_spk / 1.1 *(1.5 / 100));
        $excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, "");
        $excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, "");
        $excel->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $data->nm_bank);
        $excel->setActiveSheetIndex(0)->setCellValue('S'.$numrow, "");
        // $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->jenis_permintaan);
        // $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->alamat);
        
        // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
  
        $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('M'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('N'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('O'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('P'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('Q'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $excel->getActiveSheet()->getStyle('R'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('S'.$numrow)->applyFromArray($style_row);
        // $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        // $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
  
        $no++; // Tambah 1 setiap kali looping
        $numrow++; // Tambah 1 setiap kali looping
      
        $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row)->getNumberFormat()->setFormatCode('Rp, #,##0.00');
        $t++;
      }
      if($i == 1){
        $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow,'=SUM(I4:I'.($numrow + $t - $total_data - 1).')');
      $excel->setActiveSheetIndex(0)->mergeCells('A'.($numrow - ($total_data)).':A'.($numrow + $t - $total_data - 1));

      } else {
        $excel->setActiveSheetIndex(0)->setCellValue('I'.($numrow),'=SUM(I'.($numrow - $total_data).':I'.($numrow + $t - $total_data - 1).')');
        $excel->setActiveSheetIndex(0)->mergeCells('A'.($numrow - $total_data).':A'.($numrow + $t - $total_data - 1));

      }
      $t = 0;
    }
    


  }

    
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(45); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(45); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(45); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(65); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(45); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(25); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(25); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('K')->setWidth(25); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('L')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('M')->setWidth(25); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('N')->setWidth(25); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('O')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('P')->setWidth(25); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('Q')->setWidth(25); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('R')->setWidth(25);
    $excel->getActiveSheet()->getColumnDimension('S')->setWidth(25); // Set width kolom C
    // $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    // $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
  
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);



    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data PO");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Laporan PO.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
  }
}