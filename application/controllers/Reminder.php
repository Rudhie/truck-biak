<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reminder extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // checkSessionUser();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->model("Model_reminder");
        $this->load->model("Model_pemilik");
        $this->load->model("Model_perpanjangan");
    }

    public function index(){
        $this->load->view('reminder/email');
    }

    public function truck(){
        $transportir = $this->input->get("transportir");
        $data["transportir"] = $this->Model_pemilik->getPemilik();
        $data["truck"] = $this->Model_reminder->get_data_reminder_truck($transportir);
        $this->template->load("template", "reminder/data-reminder-truck", $data);
    }

    public function amt(){
        $transportir = $this->input->get("transportir");
        $data["transportir"] = $this->Model_pemilik->getPemilik();
        $data["supir"] = $this->Model_reminder->get_data_reminder_supir($transportir);
        $this->template->load("template", "reminder/data-reminder-supir", $data);
    }

    public function dokumen(){
        $nm_dokumen = str_replace(" ", "_", strtolower($this->input->post("nm_dokumen")) );
        $type = $this->input->post("type");
        $id = $this->input->post("id");

        $dokumen = $this->Model_reminder->get_dokumen($type, $nm_dokumen, $id);
        if($dokumen){
            echo json_encode(array("status" => "success", "data" => $dokumen));
        } else {
            echo json_encode(array("status" => "error", "data" => array()));
        }
    }

    public function temuan(){
        $data = $this->Model_reminder->get_temuan();
        if($data){
            foreach ($data as $key => $value) {
                $this->send_sms_temuan($value);
            }
        }
    }

    public function label_reminder($value){
        if($value > 0){
            $label = 'Urgent (Lewat Masa Berlaku)';
        } else if($value > -30){
            $label = 'Urgent';
        } else if($value >= -60){
            $label = 'High';
        } else if($value >= -90){
            $label = 'Warning';
        } else {
            $label = 'Masa Berlaku Lebih dari 3 Bulan';
        }

        return $label;
    }

    public function tanggal_indo($tanggal){
        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $split = explode('-', $tanggal);
        if($tanggal != ""){
            return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];    
        } else {
            return "-";
        }
        
    }

    public function get_data_supir_reminder($manual = null){
        $pemilik = $this->Model_pemilik->getPemilik();
        $supir = $this->Model_reminder->get_data_reminder_supir();
        foreach ($pemilik as $key => $value) {
            $data["nama_pemilik"] = $value->nama_pemilik;
            $data["email_aktif"] = $value->email_aktif;
            $data["no_telfon"] = $value->no_telfon;
            $data["reminder"] = array();
            foreach($supir as $s_key => $s_val){
                if($value->id_pemilik == $s_val->id_pemilik){

                    if($s_val->masa_berlaku_sim_awak != null){
                        if(isset($manual) || $s_val->reminder_sim_awak == -90 || $s_val->reminder_sim_awak == -60 || $s_val->reminder_sim_awak == -46 || $s_val->reminder_sim_awak == -30 || $s_val->reminder_sim_awak == -23 || $s_val->reminder_sim_awak == -16 || $s_val->reminder_sim_awak == -9 || $s_val->reminder_sim_awak == -2){
                            array_push($data["reminder"], array(
                                "nama" => $s_val->nama_awak,
                                "no_polisi" => $s_val->no_polisi, 
                                "jenis_perpanjangan" => "SIM",
                                "masa_berlaku" => $this->tanggal_indo($s_val->masa_berlaku_sim_awak),
                                "reminder" => abs($s_val->reminder_sim_awak),
                                "tindakan" => "Perpanjangan SIM AWAK",
                                "tingkat" => $this->label_reminder($s_val->reminder_sim_awak)
                            ));
                        }
                    }
                    

                    if($s_val->masa_berlaku_idcard_awak != null){
                        if(isset($manual) || $s_val->reminder_idcard_awak == -90 || $s_val->reminder_idcard_awak == -60 || $s_val->reminder_idcard_awak == -46 || $s_val->reminder_idcard_awak == -30 || $s_val->reminder_idcard_awak == -23 || $s_val->reminder_idcard_awak == -16 || $s_val->reminder_idcard_awak == -9 || $s_val->reminder_idcard_awak == -2){
                            array_push($data["reminder"], array(
                                "nama" => $s_val->nama_awak,
                                "no_polisi" => $s_val->no_polisi, 
                                "jenis_perpanjangan" => "ID CARD",
                                "masa_berlaku" => $this->tanggal_indo($s_val->masa_berlaku_idcard_awak),
                                "reminder" => abs($s_val->reminder_idcard_awak),
                                "tindakan" => "Perpanjangan ID CARD AWAK",
                                "tingkat" => $this->label_reminder($s_val->reminder_idcard_awak)
                            ));
                        }
                    }
                    

                }
            }

            $sendReminder[] = $data;
        }
        return $sendReminder;
    }

    public function get_data_truck_reminder($manual = null){
        $pemilik = $this->Model_pemilik->getPemilik();
        $truck = $this->Model_reminder->get_data_reminder_truck();
        foreach($pemilik as $key => $value){
            $data["nama_pemilik"] = $value->nama_pemilik;
            $data["email_aktif"] = $value->email_aktif;
            $data["no_telfon"] = $value->no_telfon;
            $data["no_telfon_kedua"] = $value->no_telfon_kedua;
            $data["reminder"] = array();
            foreach($truck as $t_key => $t_val){

                if($value->id_pemilik == $t_val->id_pemilik){

                    if($t_val->masa_berlaku_stnk != null){
                        if(isset($manual) || $t_val->reminder_stnk == -90 || $t_val->reminder_stnk == -60 || $t_val->reminder_stnk == -46 || $t_val->reminder_stnk == -30 || $t_val->reminder_stnk == -23 || $t_val->reminder_stnk == -16 || $t_val->reminder_stnk == -9 || $t_val->reminder_stnk == -2){
                            array_push($data["reminder"], array(
                                "no_polisi" => $t_val->no_polisi, 
                                "jenis_perpanjangan" => "STNK",
                                "masa_berlaku" => $this->tanggal_indo($t_val->masa_berlaku_stnk),
                                "reminder" => abs($t_val->reminder_stnk),
                                "tindakan" => "Perpanjangan STNK Kendaraan",
                                "tingkat" => $this->label_reminder($t_val->reminder_stnk)
                            ));
                        }
                    }

                    

                    if($t_val->masa_berlaku_skt != null){
                        if(isset($manual) || $t_val->reminder_skt == -90 || $t_val->reminder_skt == -60 || $t_val->reminder_skt == -46 || $t_val->reminder_skt == -30 || $t_val->reminder_skt == -23 || $t_val->reminder_skt == -16 || $t_val->reminder_skt == -9 || $t_val->reminder_skt == -2){
                            array_push($data["reminder"], array(
                                "no_polisi" => $t_val->no_polisi, 
                                "jenis_perpanjangan" => "SKT",
                                "masa_berlaku" => $this->tanggal_indo($t_val->masa_berlaku_skt),
                                "reminder" => abs($t_val->reminder_skt),
                                "tindakan" => "Perpanjangan SKT Kendaraan",
                                "tingkat" => $this->label_reminder($t_val->reminder_skt)
                            ));
                        }
                    }

                    if($t_val->keur != null){
                        if(isset($manual) || $t_val->reminder_keur == -90 || $t_val->reminder_keur == -60 || $t_val->reminder_keur == -46 || $t_val->reminder_keur == -30 || $t_val->reminder_keur == -23 || $t_val->reminder_keur == -16 || $t_val->reminder_keur == -9 || $t_val->reminder_keur == -2){
                            array_push($data["reminder"], array(
                                "no_polisi" => $t_val->no_polisi, 
                                "jenis_perpanjangan" => "KEUR",
                                "masa_berlaku" => $this->tanggal_indo($t_val->keur),
                                "reminder" => abs($t_val->reminder_keur),
                                "tindakan" => "Perpanjangan KEUR",
                                "tingkat" => $this->label_reminder($t_val->reminder_keur)
                            ));
                        }
                    }
                    

                    if($t_val->metrologi_tera != null){
                        if(isset($manual) || $t_val->reminder_tera == -90 || $t_val->reminder_tera == -60 || $t_val->reminder_tera == -46 || $t_val->reminder_tera == -30 || $t_val->reminder_tera == -23 || $t_val->reminder_tera == -16 || $t_val->reminder_tera == -9 || $t_val->reminder_tera == -2){
                            array_push($data["reminder"], array(
                                "no_polisi" => $t_val->no_polisi, 
                                "jenis_perpanjangan" => "Metrologi Tera",
                                "masa_berlaku" => $this->tanggal_indo($t_val->metrologi_tera),
                                "reminder" => abs($t_val->reminder_tera),
                                "tindakan" => "Perpanjangan Metrologi Tera",
                                "tingkat" => $this->label_reminder($t_val->reminder_tera)
                            ));
                        }
                    }
                    

                    if($t_val->masa_berlaku_kim != null){
                        if(isset($manual) || $t_val->reminder_kim == -90 || $t_val->reminder_kim == -60 || $t_val->reminder_kim == -46 || $t_val->reminder_kim == -30 || $t_val->reminder_kim == -23 || $t_val->reminder_kim == -16 || $t_val->reminder_kim == -9 || $t_val->reminder_kim == -2){
                            array_push($data["reminder"], array(
                                "no_polisi" => $t_val->no_polisi, 
                                "jenis_perpanjangan" => "Kartu Izin Masuk",
                                "masa_berlaku" => $this->tanggal_indo($t_val->masa_berlaku_kim),
                                "reminder" => abs($t_val->reminder_kim),
                                "tindakan" => "Perpanjangan Kartu Izin Masuk",
                                "tingkat" => $this->label_reminder($t_val->reminder_kim)
                            ));
                        }
                    }
                    
                    
                }
            }

            $sendReminder[] = $data;
        }
        return $sendReminder;
    }

    public function send_sms_temuan($temuan){
        $message = "Mobil Tangki ".$temuan->no_polisi.", terdapat temuan aspek safety '".strtoupper($temuan->jenis_temuan)."' pada Mobil Tangki anda . Segera lakukan  tindaklanjut perbaikan dengan cara klik tautan di bawah atau segera koordinasi dengan HSSE Fuel Terminal Biak. Untuk lebih detail dapat klik tautan berikut : https:".base_url('temuan/detail')."?id=".$temuan->id_temuan;
        if($temuan->no_telfon != null){
            $telepon = $temuan->no_telfon;
            $this->sms_config($telepon, $message);
        }

        if($temuan->no_telfon_kedua != null){
            $telepon = $temuan->no_telfon_kedua;
            $this->sms_config($telepon, $message);
        }
    }
    
    public function testing_sms(){
    	$this->sms_config('085158279919','SMS from pertaminabiak.com');
    }

    public function sms_config($telepon, $message){
        $userkey = 'c0027701b6e4';
        $passkey = '7be0a5670850b9b3ac359761';
        
        //$url = 'https://masking.zenziva.net/api/sendsms/'; OLD
        $url = 'https://console.zenziva.net/masking/api/sendsms/';
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => $userkey,
            'passkey' => $passkey,
            'to' => $telepon,
            'message' => $message
        ));
        $results = json_decode(curl_exec($curlHandle), true);
        print_r( $results );
        curl_close($curlHandle);
    }

    public function send_sms_reminder($type, $manual = null){
        if($type == "truck"){
            $sendReminder = $this->get_data_truck_reminder($manual);
        } else {
            $sendReminder = $this->get_data_supir_reminder($manual);
        }

        foreach($sendReminder as $r_key => $r_val){
            if(count($sendReminder[$r_key]['reminder']) > 0){
                $i=1;
                foreach($sendReminder[$r_key]['reminder'] as $s_key => $s_val){
                    $message = strtoupper($type == "truck" ? $s_val['no_polisi'] : $s_val['nama']).", masa berlaku ".strtoupper($s_val['jenis_perpanjangan'])." anda akan berakhir Tgl. ".$s_val['masa_berlaku'].". Segera lakukan pengurusan perpanjangan dan update data di HSSE Fuel Terminal Biak. Untuk lebih detail dapat klik tautan berikut :  https:".base_url('reminder/'.($type == "truck" ? "truck" : "amt"));

                    if($sendReminder[$r_key]['no_telfon'] != null){
                        $this->sms_config($sendReminder[$r_key]['no_telfon'], $message);
                    }

                    if($sendReminder[$r_key]['no_telfon_kedua'] != null){
                        $this->sms_config($sendReminder[$r_key]['no_telfon_kedua'], $message);
                    }
                }
            }
        }

        
    }

    public function send_mail($type, $manual = null){
        if($type == "truck"){
            $sendReminder = $this->get_data_truck_reminder($manual);
        } else {
            $sendReminder = $this->get_data_supir_reminder($manual);
        }
        $general = $this->Model_perpanjangan->get_email_general();
        $email_general = array();

        foreach($general as $gen){
            array_push($email_general, $gen['email']);
        }
        $text = ($type == "truck" ? "Mobil Tangki" : "Awak Mobil Tangki");

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.pertaminabiak.com',
            'smtp_port' => 25,
            'smtp_user' => 'noreply@pertaminabiak.com',
            'smtp_pass' => 'culametanmetmet',
            'mailtype'  => 'html', 
            'charset'  => 'iso-8859-1'
        );

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");

        foreach($sendReminder as $r_key => $r_val){

            if(count($sendReminder[$r_key]['reminder']) > 0){
                $message = '<html><body><div style="background-color:#e8e8e8;padding: 5%;">
                                <div style="width: 100%;height: 80px;background: -webkit-linear-gradient(45deg, #008cc9,#009ea5);
                                    background: -moz-linear-gradient(45deg, #008cc9,#009ea5);
                                    background: -o-linear-gradient(45deg, #008cc9,#009ea5);
                                    background: linear-gradient(45deg, #008cc9,#009ea5);
                                    border-radius: 10px 10px 0px 0px;">
                                    <center><img src="http://pertaminabiak.com/asset/img/logo-pertamina.png" width="200px;" style="margin-top:15px"/></center>
                                </div>
                                <div style="background-color:#ffffff;padding: 2%;">
                                    <center>
                                        <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;"><p>Kepada Yth. Bapak/Ibu '.$sendReminder[$r_key]['nama_pemilik'].'</p>
                                            <p>Berdasarkan pada data yang ada pada sistem monitoring kami, telah ditemukan beberapa '.$text.' yang memperlukan pembaruan data serta dokumen.<br/>
                                                Segera lakukan pembaruan dokumen '.$text.' dan melaporkan hasil pembaruan kedalam sistem monitoring.
                                            </p>
                                            <p>Berikut '.$text.' yang harus dilakukan pembaharuan data : </p>
                                        </span>';

                $message .= '<table width="80%" border="1">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="10%">'.($type == "truck" ? "Nomor Polisi" : "Nama").'</th>
                                        <th width="15%">Dokumen</th>
                                        <th width="15%">Masa Berlaku</th>
                                        <th>Tindakan</th>
                                        <th width="25%">Tingkat Kepentingan</th>
                                    </tr>
                                </thead>
                                <tbody>';
                $i=1;
                foreach($sendReminder[$r_key]['reminder'] as $s_key => $s_val){
                    $message .= '<tr>
                                    <td style="text-align:center">'.$i.'</td>
                                    <td style="text-align:center">'.($type == "truck" ? $s_val['no_polisi'] : $s_val['nama']).'</td>
                                    <td style="text-align:center">'.$s_val['jenis_perpanjangan'].'</td>
                                    <td style="text-align:center">'.$s_val['masa_berlaku'].'</td>
                                    <td style="text-align:center">'.$s_val['tindakan'].'</td>
                                    <td style="text-align:center">'.$s_val['tingkat'].'</td>
                                </tr>';
                    $i++;
                }
                
                $message .= '</tbody>
                                </table>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;">
                                    <p>Mohon untuk Bapak/Ibu '.$sendReminder[$r_key]['nama_pemilik'].' untuk segera menindaklanjuti pemberitahuan ini.</p>
                                </span>
                                <span style="font-size: 13px;font-style: normal;font-family: sans-serif;color: #7b7b7b;">Terima Kasih</span>
                                </center>
                            </div>
                            <div style="color: #a6a6a6;font-family: sans-serif;font-size: 12px;">
                                <center>
                                    <p>&copy;'.date('Y').' Sistem Informasi dan Monitoring Truck Tangki</p>
                                    <p>PT. Pertamina (persero) Fuel Terminal Biak</p>
                                </center>
                            </div>
                        </div></body></html>';
                $this->email->clear();
                $this->email->set_newline("\r\n");
                
                $this->email->from('noreply@pertaminabiak.com', 'PT. Pertamina Fuel Terminal Biak'); 
                $this->email->to($sendReminder[$r_key]['email_aktif']);
                $this->email->cc($email_general);
                $this->email->subject('Reminder Pembaruan Dokumen '.$text." - ".($type == "truck" ? $sendReminder[$r_key]["reminder"][0]["no_polisi"] : $sendReminder[$r_key]["reminder"][0]["nama"])); 
                $this->email->message($message);
                $this->email->send();

                $dataLog = array(
                    "email_terkirim" => $sendReminder[$r_key]['email_aktif'],
                    "tanggal_log" => date('Y-m-d H:i:s')
                );

                $this->Model_reminder->add_log_reminder($dataLog);
            }
        }

        echo json_encode(array("status" => "success", "message" => "Berhasil Mengirim Reminder ".$text." Pemilik"));

    }

    public function export_mobil(){
		include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
        $namesheet = array("Data Reminder Mobil Tangki");            
        $jumlah = array(13); 
        $header = array(
            array(
                'NO',
				'NAMA PERUSAHAAN',
				'NO POLISI',
				'TANGGAL MASA BERLAKU STNK',
				'SISA HARI HABIS STNK',
				'TANGGAL MASA BERLAKU SKT',
				'SISA HARI HABIS SKT',
				'TANGGAL MASA BERLAKU KEUR',
                'SISA HARI HABIS KEUR',
                'TANGGAL MASA BERLAKU METROLOGI TERA',
                'SISA HARI HABIS METROLOGI TERA',
                'TANGGAL MASA BERLAKU KARTU IZIN MASUK',
                'SISA HARI HABUS KARTU IZIN MASUK'
            )
        );
        
        $transportir = $this->input->get("transportir");
		$get_data = $this->Model_reminder->get_data_reminder_truck($transportir);
        $i=0;
        while ($i < 1) {
            $objWorkSheet = $objPHPExcel->createSheet($i);
            $lastColumn = $objWorkSheet->getHighestColumn();
            for($x = 0; $x < $jumlah[$i]; $x++){                    
                $row = 1;                    
                $objWorkSheet->setCellValue($lastColumn.$row, $header[$i][$x]);
                $objWorkSheet
                    ->getColumnDimension($lastColumn)
                    ->setAutoSize(true);
                $lastColumn++;
            }
            
            $objWorkSheet->setTitle($namesheet[$i]);
            $i++;
        }     
        
        $index=0;
        $rowgabung = 2;
        while ($index < 1) {
            $worksheet = $objPHPExcel->setActiveSheetIndex($index);
            $row = 2;  
            if ($index == 0) {
                $a = 1;
                foreach ($get_data as $data) {       
                    $worksheet->setCellValue('A'.$row, $a); 
					$worksheet->setCellValue('B'.$row, $data->nama_perusahaan);
					$worksheet->setCellValue('C'.$row, $data->no_polisi);
					$worksheet->setCellValue('D'.$row, $data->masa_berlaku_stnk);
					$worksheet->setCellValue('E'.$row, $data->reminder_stnk);
					$worksheet->setCellValue('F'.$row, $data->masa_berlaku_skt);
					$worksheet->setCellValue('G'.$row, $data->reminder_skt);
					$worksheet->setCellValue('H'.$row, $data->keur);
					$worksheet->setCellValue('I'.$row, $data->metrologi_tera);
                    $worksheet->setCellValue('J'.$row, $data->reminder_tera);
                    $worksheet->setCellValue('K'.$row, $data->masa_berlaku_kim);
                    $worksheet->setCellValue('L'.$row, $data->reminder_kim);
                    $row++;
                    $a++;
                }
            }
            $index++;
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $t = date("Y_m_d_H_i_s");
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=DATA_REMINDER_MOBIL_TANGKI_".$t.".xlsx");
        $objWriter->save("php://output");
    }
    
    public function export_awak(){
		include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
        $namesheet = array("Data Reminder Awak Mobil Tangki");
        $jumlah = array(9); 
        $header = array(
            array(
                'NO',
                'NAMA PERUSAHAAN',
                'NAMA AWAK',
                'USIA',
                'BAGIAN',
				'TANGGAL MASA BERLAKU SIM',
				'SISA HARI HABIS SIM',
				'TANGGAL MASA BERLAKU IDCARD',
                'SISA HARI HABIS IDCARD',
            )
        );
        
        $transportir = $this->input->get("transportir");
		$get_data = $this->Model_reminder->get_data_reminder_supir($transportir);
        $i=0;
        while ($i < 1) {
            $objWorkSheet = $objPHPExcel->createSheet($i);
            $lastColumn = $objWorkSheet->getHighestColumn(1);
            
            for($x = 0; $x < $jumlah[$i]; $x++){                    
                $row = 1;                    
                $objWorkSheet->setCellValue($lastColumn.$row, $header[$i][$x]);
                $objWorkSheet
                    ->getColumnDimension($lastColumn)
                    ->setAutoSize(true);
                $lastColumn++;
            }

            $objWorkSheet->setTitle($namesheet[$i]);
            $i++;
        }

        
        $index=0;
        $rowgabung = 2;
        while ($index < 1) {
            $worksheet = $objPHPExcel->setActiveSheetIndex($index);      
            $row = 2;  
            if ($index == 0) {
                $a = 1;
                foreach ($get_data as $data) {       
                    $worksheet->setCellValue('A'.$row, $a); 
					$worksheet->setCellValue('B'.$row, $data->nama_perusahaan);
                    $worksheet->setCellValue('C'.$row, $data->nama_awak);
                    $worksheet->setCellValue('D'.$row, $data->usia);
                    $worksheet->setCellValue('E'.$row, $data->bagian);
					$worksheet->setCellValue('F'.$row, $data->masa_berlaku_sim_awak);
					$worksheet->setCellValue('G'.$row, $data->reminder_sim_awak);
					$worksheet->setCellValue('H'.$row, $data->masa_berlaku_idcard_awak);
					$worksheet->setCellValue('I'.$row, $data->reminder_idcard_awak);
                    $row++;
                    $a++;
                }
            }
            $index++;
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $t = date("Y_m_d_H_i_s");
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=DATA_REMINDER_AWAK_MOBIL_TANGKI_".$t.".xlsx");
        $objWriter->save("php://output");
	}

}
