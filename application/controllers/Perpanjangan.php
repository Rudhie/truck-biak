<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perpanjangan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        checkSessionUser();
        $this->load->model("Model_perpanjangan");
        $this->load->model("Model_keperluan");
        $this->load->model("Model_truck");
        $this->load->model("Model_pemilik");
        $this->load->model("Model_awak_kernet");
        date_default_timezone_set('Asia/Jakarta');
	}

	public function truck(){
        $transportir = $this->input->get("transportir");
        $bulan = $this->input->get("bulan");
        $tahun = $this->input->get("tahun");
        $status = $this->input->get("status");
        $data["transportir"] = $this->Model_pemilik->getPemilik();
		$data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan("truck", null, $transportir, $bulan, $tahun, $status);
		$this->template->load("template", "perpanjangan/data-perpanjangan", $data);
    }

    public function amt(){
        $transportir = $this->input->get("transportir");
        $bulan = $this->input->get("bulan");
        $tahun = $this->input->get("tahun");
        $status = $this->input->get("status");
        $data["transportir"] = $this->Model_pemilik->getPemilik();
        $data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan("amt", null, $transportir, $bulan, $tahun, $status);
		$this->template->load("template", "perpanjangan/data-perpanjangan", $data);
    }

    public function persyaratan($id_keperluan){
        $syarat = $this->Model_perpanjangan->get_persyaratan($id_keperluan);
        echo json_encode(array("data" => $syarat));
    }

    public function tambah(){
        $page = $this->input->get("page");
        if($page == "truck"){
            $data["truck"] = $this->Model_perpanjangan->get_truck();
        } else {
            $data["supir"] = $this->Model_perpanjangan->get_supir();
            $data["truck"] = $this->Model_perpanjangan->get_truck();
        }
        
        $data["keperluan"] = $this->Model_perpanjangan->get_keperluan($page);
        $data["syarat_keperluan"] = $this->Model_perpanjangan->get_syarat_keperluan($page);
        $this->template->load("template", "perpanjangan/form-perpanjangan", $data);
    }

    public function edit(){
        $id = $this->input->get("id");
        $page = $this->input->get("page");
        $data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan($page, $id);
        $data["dok_perpanjangan"] = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);
        $data["truck"] = $this->Model_perpanjangan->get_truck();
        $data["keperluan"] = $this->Model_perpanjangan->get_keperluan();
        $data["supir"] = $this->Model_perpanjangan->get_supir();
        $this->template->load("template", "perpanjangan/edit-perpanjangan", $data);
    }

    public function detail(){
        $id = $this->input->get("id");
        $page = $this->input->get("page");
        $data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan($page, $id);
        $data["dok_perpanjangan"] = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);
		$this->template->load("template", "perpanjangan/detail-perpanjangan", $data);
    }

    public function approve(){
        $id = $this->input->get("id");
        $page = $this->input->get("page");
        $data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan($page, $id);
        $data["dok_perpanjangan"] = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);
		$this->template->load("template", "perpanjangan/approve-perpanjangan", $data);
    }

    public function action_tambah(){
        $type = $this->input->post("type");
        $idcard = $this->input->post("idcard");
        $id_pemilik = $this->session->userdata("id_pemilik");
        $keperluan = $this->input->post("id_keperluan");
        $truck = $this->input->post("id_truck");
        $supir = $this->input->post("id_supir");
        $detail_keperluan = $this->input->post("detail_keperluan");
        $tgl_masa_berlaku_baru = $this->input->post("tgl_masa_berlaku_baru");
        $tingkat_keperluan = $this->input->post("tingkat_keperluan");
        $komentar = $this->input->post("komentar");
        $id_truck_baru = $this->input->post("id_truck_baru");

        $no_aktifitas_akhir = $this->Model_perpanjangan->get_no_aktifitas_akhir(date('Y'));
        $no_urut = substr($no_aktifitas_akhir->max_no_aktifitas, 0, 3);
        if($no_urut){
            $next = $no_urut + 1;
            if(strlen($next) == 1){
                $no_aktifitas = "00".$next;
            } else if(strlen($next) == 2){
                $no_aktifitas = "0".$next;
            } else if(strlen($next) == 3){
                $no_aktifitas = $next;
            }
        } else {
            $no_aktifitas = "001";
        }
        

        $no_aktifitas = $no_aktifitas."/REQ/Q28042/".date('Y');

        $data = array(
            "id_pemilik" => $id_pemilik,
            "no_aktifitas" => $no_aktifitas,
            "id_truck" => $truck,
            "id_awak" => $supir,
            "id_keperluan" => $keperluan,
            "detail_keperluan" => $detail_keperluan,
            "tgl_aktifitas" => date("Y-m-d"),
            "tgl_masa_berlaku_baru" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_masa_berlaku_baru))),
            "tingkat_keperluan" => $tingkat_keperluan,
            "komentar" => $komentar,
            "status_aktifitas" => "0",
            "created_date" => date("Y-m-d H:i:s")
        );

        if($keperluan == 9){
            $truck_lama = $this->Model_awak_kernet->getAwak($supir, "");
            $data["id_truck_lama"] = $truck_lama[0]->id_truck;
            $data["id_truck_baru"] = $id_truck_baru;
        }


        $tambahAktifitas = $this->Model_perpanjangan->tambah_perpanjangan($data);
        if($tambahAktifitas){
            $this->send_mail_pengajuan($type, $tambahAktifitas, "ADMIN");
            $id_syarat = $this->input->post("id_syarat");
            $config['upload_path']= './dokumen/perpanjangan/';
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size'] = 2048;

            $i = 0;
            $getKeperluan = $this->Model_perpanjangan->get_keperluan(null, $keperluan);
            foreach ($id_syarat as $syarat) {

                $newNameFile = "DOKUMEN_".str_replace("/", "_", str_replace(" ", "_", strtoupper($getKeperluan[0]->nm_keperluan)));
                $newNameFile .= "_".$syarat;
                if($type == "truck"){
                    $getTruck = $this->Model_truck->getTruck($truck);
                    $newNameFile .= "_".str_replace(" ", "_", strtoupper($getTruck[0]->no_polisi));
                } else {
                    $getAwak = $this->Model_awak_kernet->getAwak($id_awak, "");
                    $newNameFile .= "_".str_replace(" ", "_", strtoupper($getAwak[0]->nip_awak));
                }
                $newNameFile .= "_".date('Y_m_d_H_i_s');

                $config['file_name'] = $newNameFile;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ( ! $this->upload->do_upload('dokumen'.$syarat)){
                    echo $this->upload->display_errors();
                }

                $dataDokumen = array(
                    "id_aktifitas" => $tambahAktifitas,
                    "id_syarat" => $syarat,
                    "dokumen_syarat" => $newNameFile.$this->upload->data("file_ext"),
                    "format_dokumen" => $this->upload->data("file_ext")
                );

                $dokumen[] = $dataDokumen;
                $i++;
            }

            $tambahDokumen = $this->Model_perpanjangan->tambah_dokumen_perpanjangan($dokumen);
            if($tambahDokumen){
                $this->session->set_flashdata("success", "Berhasil Menambahkan Perpanjangan");
            } else {
                $this->session->set_flashdata("error", "Gagal Melakukan Pengajuan Perpanjangan");
            }
        } else {
            $this->session->set_flashdata("error", "Gagal Melakukan Pengajuan Perpanjangan");
        }

        if($idcard){
            redirect("idcard");
        } else {
            redirect("perpanjangan/".$type);
        }
    }

    public function action_edit($id){
        $type = $this->input->post("type");
        $id_pemilik = $this->session->userdata("id_pemilik");
        $keperluan = $this->input->post("id_keperluan");
        $truck = $this->input->post("id_truck");
        $supir = $this->input->post("id_supir");
        $detail_keperluan = $this->input->post("detail_keperluan");
        $tgl_masa_berlaku_baru = $this->input->post("tgl_masa_berlaku_baru");
        $tingkat_keperluan = $this->input->post("tingkat_keperluan");
        $komentar = $this->input->post("komentar");
        $id_truck_baru = $this->input->post("id_truck_baru");

        $data = array(
            "id_pemilik" => $id_pemilik,
            "id_truck" => $truck,
            "id_awak" => $supir,
            "id_keperluan" => $keperluan,
            "detail_keperluan" => $detail_keperluan,
            "tgl_masa_berlaku_baru" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_masa_berlaku_baru))),
            "tingkat_keperluan" => $tingkat_keperluan,
            "komentar" => $komentar,
            "status_aktifitas" => "7",
            "updated_date" => date("Y-m-d H:i:s")
        );

        if($keperluan == 9){
            $data["id_truck_baru"] = $id_truck_baru;
        }

        $ubahAktifitas = $this->Model_perpanjangan->edit_perpanjangan($data, $id);
        
        if($ubahAktifitas){
            $id_syarat = $this->input->post("id_syarat");
            $id_dokumen = $this->input->post("id_dokumen");
            $config['upload_path']= './dokumen/perpanjangan/';
            $config['allowed_types'] = 'pdf|jpg|jpeg';
            $config['max_size'] = 2048;

            $i = 0;
            $getKeperluan = $this->Model_perpanjangan->get_keperluan(null, $keperluan);
            $dokumen = [];
            foreach ($id_syarat as $syarat) {

                if($_FILES['dokumen'.$syarat]['name'] != ""){
                    $newNameFile = "DOKUMEN_".str_replace("/", "_", str_replace(" ", "_", strtoupper($getKeperluan[0]->nm_keperluan)));
                    $newNameFile .= "_".$syarat;
                    if($type == "truck"){
                        $getTruck = $this->Model_truck->getTruck($truck);
                        $newNameFile .= "_".str_replace(" ", "_", strtoupper($getTruck[0]->no_polisi));
                    } else {
                        $getAwak = $this->Model_awak_kernet->getAwak($id_awak, "");
                        $newNameFile .= "_".str_replace(" ", "_", strtoupper($getAwak[0]->nip_awak));
                    }
                    $newNameFile .= "_".date('Y_m_d_H_i_s');

                    $config['file_name'] = $newNameFile;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ( ! $this->upload->do_upload('dokumen'.$syarat)){
                        echo $this->upload->display_errors();
                    }

                    $dataDokumen = array(
                        "id_dokumen" => $id_dokumen[$i],
                        "id_aktifitas" => $id,
                        "id_syarat" => $syarat,
                        "dokumen_syarat" => $newNameFile.$this->upload->data("file_ext")
                    );

                    $dokumen[] = $dataDokumen;
                }
                
                $i++;
            }
            if(count($dokumen) > 0){
                $editDokumen = $this->Model_perpanjangan->edit_dokumen_perpanjangan($dokumen);
                if($editDokumen){
                    $this->session->set_flashdata("success", "Berhasil Mengubah Pengajuan Perpanjangan");
                } else {
                    $this->session->set_flashdata("error", "Gagal Mengubah Pengajuan Perpanjangan");
                }
            } else {
                $this->session->set_flashdata("success", "Berhasil Mengubah Pengajuan Perpanjangan");
            }
        } else {
            $this->session->set_flashdata("error", "Gagal Mengubah Pengajuan Perpanjangan");
        }

        if($type == "idcard"){
            redirect("idcard/");
        } else {
            redirect("perpanjangan/".$type);
        }
    }

    public function action_hapus($id, $page){
        $perpanjangan = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);
        foreach($perpanjangan as $p){
            $file = $p->dokumen_syarat;
            unlink('./dokumen/perpanjangan/'.$file);
        }

        $hapus = $this->Model_perpanjangan->hapus_perpanjangan($id);
        if($hapus){
            $this->session->set_flashdata("success", "Berhasil Menghapus Perpanjangan");
        } else {
            $this->session->set_flashdata("error", "Gagal Menghapus Perpanjangan");
        }
        if($page == "idcard"){
            redirect("idcard/");
        } else {
            redirect("perpanjangan/".$page);
        }
    }

    public function action_approve($id){
        $level_user = $this->session->userdata("level_user");
        $page = $this->input->post("page");
        $id_dokumen = $this->input->post("id_dokumen");
        $konfirmasi = $this->input->post("konfirmasi");
        $note = $this->input->post("note_reject");
        $catatan = $this->input->post("general_note");

        $getPerpanjangan = $this->Model_perpanjangan->get_perpanjangan($page, $id);
        if($level_user == "ADMIN"){
            $status = "1";
            $i = 0;
            foreach ($id_dokumen as $dokumen) {
                
                if($konfirmasi[ $id_dokumen[$i] ]){
                    $dataDokumen[] = array(
                        "id_dokumen" => $id_dokumen[$i],
                        "approval_admin" => $konfirmasi[ $id_dokumen[$i] ],
                        "note_approval_admin" => $note[$i],
                        "tgl_approval_admin" => date("Y-m-d H:i:s")
                    );
                    if($konfirmasi[ $id_dokumen[$i] ] == "N"){
                        $status = "2";
                    }
                } else {
                    $status = "4";
                    $dataDokumen[] = array(
                        "id_dokumen" => $id_dokumen[$i],
                        "note_approval_admin" => $note[$i]
                    );
                }
                
                $i++;
            }

            $data = array(
                "status_aktifitas" => $status,
                "catatan_admin" => $catatan,
                "updated_date" => date("Y-m-d H:i:s")
            );

            if($status == "1"){
                $this->send_mail_pengajuan($page, $id, "MANAGER LOKASI");
            } else if($status == "2"){
                $this->send_mail_reject($page, $id, "PEMILIK");
            }
        } else if($level_user == "MANAGER LOKASI"){
            $status = ($konfirmasi == "Y" ? "3" : "4");
            if($status == "3"){
                if($getPerpanjangan[0]->id_keperluan == 7 || $getPerpanjangan[0]->id_keperluan == 8){
                    $this->send_mail_pengajuan($page, $id, "MANAGER REGION");
                }
            }
            $id_dokumen = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);
            foreach($id_dokumen as $dokumen){
                $dataDokumen[] = array(
                    "id_dokumen" => $dokumen->id_dokumen,
                    "approval_manager_lokasi" => $konfirmasi,
                    "tgl_approval_manager_lokasi" => date("Y-m-d H:i:s"),
                    "note_approval_manager_lokasi" => $catatan
                );
            }

            $getMasaAktif = $this->Model_perpanjangan->get_keperluan_by_name($getPerpanjangan[0]->nm_keperluan);
            if($konfirmasi == "Y" && $getPerpanjangan[0]->id_keperluan != 7  && $getPerpanjangan[0]->id_keperluan != 8){
                $masaAktif = $getPerpanjangan[0]->tgl_masa_berlaku_baru;
                $dokumen = $this->Model_perpanjangan->get_dokumen_perpanjangan($getPerpanjangan[0]->id_aktifitas);
                $berkas = "berkas-truck";
                if(strpos(strtoupper($getMasaAktif[0]->nm_keperluan), "STNK") == true){
                    $kolom = "masa_berlaku_stnk";
                    $kolom_dokumen = "dokumen_stnk";
                    $dokumen = $dokumen[0]->dokumen_syarat;
                } else if(strpos(strtoupper($getMasaAktif[0]->nm_keperluan), "SKT") == true){
                    $kolom = "masa_berlaku_skt";
                    $kolom_dokumen = "dokumen_skt";
                    $dokumen = $dokumen[0]->dokumen_syarat;
                } else if(strpos(strtoupper($getMasaAktif[0]->nm_keperluan), "KEUR") == true){
                    $kolom = "keur";
                    $kolom_dokumen = "dokumen_kir";
                    $dokumen = $dokumen[0]->dokumen_syarat;
                } else if(strpos(strtoupper($getMasaAktif[0]->nm_keperluan), "TERA") == true){
                    $kolom = "metrologi_tera";
                    $kolom_dokumen = "dokumen_metrologi_tera";
                    $dokumen = $dokumen[0]->dokumen_syarat;
                } else if(strpos(strtoupper($getMasaAktif[0]->nm_keperluan), "SIM") == true){
                    $kolom = "masa_berlaku_sim_awak";
                    $kolom_dokumen = "dokumen_sim_awak";
                    $dokumen = $dokumen[0]->dokumen_syarat;
                    $berkas = "berkas-awak";
                } else if(strpos(strtoupper($getMasaAktif[0]->nm_keperluan), "KIM") == true){
                    $kolom = "masa_berlaku_kim";
                    $kolom_dokumen = "dokumen_kim";
                    $dokumen = $dokumen[1]->dokumen_syarat;
                } else if(strpos(strtoupper($getMasaAktif[0]->nm_keperluan), "TANGKI") == true){
                    $kolom_dokumen = "id_truck";
                    $dokumen = $getPerpanjangan[0]->id_truck_baru;
                }

                if($getPerpanjangan[0]->id_keperluan != 9){
                    copy("dokumen/perpanjangan/".$dokumen, "dokumen/".$berkas."/".$dokumen);    
                }
                

                if($konfirmasi == "Y"){
                    if($getPerpanjangan[0]->id_keperluan != 9){
                        $dataPerpanjangan = array(
                            $kolom => $masaAktif,
                            $kolom_dokumen => $dokumen,
                            "updated_date" => date("Y-m-d H:i:s")
                        );
                    } else {
                        $dataPerpanjangan = array(
                            $kolom_dokumen => $dokumen,
                            "updated_date" => date("Y-m-d H:i:s")
                        );
                    }
                    

                    if($page == "truck"){
                        $perpanjangan = $this->Model_truck->ubahTruck($dataPerpanjangan, $getPerpanjangan[0]->id_truck);
                    } else {
                        $perpanjangan = $this->Model_perpanjangan->ubahAwak($dataPerpanjangan, $getPerpanjangan[0]->id_awak);
                    }

                    if($perpanjangan){
                        $this->session->set_flashdata("success", "Berhasil Melakukan Perpanjangan");
                    } else {
                        $this->session->set_flashdata("error", "Gagal Perpanjangan");
                    }
                }
            }

            $data = array(
                "status_aktifitas" => $status,
                "catatan_manager_lokasi" => $catatan,
                "updated_date" => date("Y-m-d H:i:s")
            );
        } else if($level_user == "MANAGER REGION"){
            $status = ($konfirmasi == "Y" ? "5" : "6");
            $id_dokumen = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);
            foreach($id_dokumen as $dokumen){
                $dataDokumen[] = array(
                    "id_dokumen" => $dokumen->id_dokumen,
                    "approval_manager_region" => $konfirmasi,
                    "tgl_approval_manager_region" => date("Y-m-d H:i:s"),
                    "note_approval_manager_region" => $catatan
                );
            }

            if($konfirmasi == "Y"){
                $masaAktif = $getPerpanjangan[0]->tgl_masa_berlaku_baru;

                $dataPerpanjangan = array(
                    "masa_berlaku_idcard_awak" => $masaAktif
                );

                $perpanjangan = $this->Model_perpanjangan->ubahAwak($dataPerpanjangan, $getPerpanjangan[0]->id_awak);
                if($perpanjangan){
                    $this->session->set_flashdata("success", "Berhasil Melakukan Perpanjangan");
                } else {
                    $this->session->set_flashdata("error", "Gagal Perpanjangan");
                }
            }

            $data = array(
                "status_aktifitas" => $status,
                "catatan_manager_region" => $catatan,
                "updated_date" => date("Y-m-d H:i:s")
            );
        }

        $this->Model_perpanjangan->edit_dokumen_perpanjangan($dataDokumen);
        $edit = $this->Model_perpanjangan->edit_perpanjangan($data, $id);
        if($edit){
            $this->session->set_flashdata("success", "Berhasil Melakukan Konfirmasi Perpanjangan");
        } else {
            $this->session->set_flashdata("error", "Gagal Konfirmasi Perpanjangan");
        }

        $halaman = $page == "idcard" ? "&form=idcard" : "";
        redirect("perpanjangan/approve?id=".$id."&page=".$page.$halaman);
    }

    public function check_tingkat_masa_berlaku(){
        $type = $this->input->post("type");
        $id = $this->input->post("id");
        $permohonan = $this->input->post("permohonan");

        if($type != "" && $id != ""){
            $check = $this->Model_perpanjangan->check_masa_berlaku($type, $id);
            if($permohonan == 1){
                $masa_berlaku = $check[0]['reminder_stnk'];
            } else if($permohonan == 2){
                $masa_berlaku = $check[0]['reminder_sim_awak'];
            } else if($permohonan == 3){
                $masa_berlaku = $check[0]['reminder_skt'];
            } else if($permohonan == 4){
                $masa_berlaku = $check[0]['reminder_kim'];
            } else if($permohonan == 5){
                $masa_berlaku = $check[0]['reminder_keur'];
            } else if($permohonan == 6){
                $masa_berlaku = $check[0]['reminder_tera'];
            } else if($permohonan == 7){
                $masa_berlaku = $check[0]['reminder_idcard_awak'];
            } else if($permohonan == 8){
                $masa_berlaku = $check[0]['reminder_idcard_awak'];
            } else if($permohonan == 9){
                $masa_berlaku = 0;
            }
        } else {
            $masa_berlaku = 0;
        }

        echo json_encode(array("data" => (int)$masa_berlaku));
    }

    public function send_mail_pengajuan($type, $id, $level){
        $perpanjangan = $this->Model_perpanjangan->get_perpanjangan($type, $id);
        $to = $this->Model_perpanjangan->get_email($level);
        $general = $this->Model_perpanjangan->get_email_general();

        $email_to = array();
        $email_general = array();
        foreach($to as $tos){
            array_push($email_to, $tos['email']);
        }

        foreach($general as $gen){
            array_push($email_general, $gen['email']);
        }

        $message = '<html><body><div style="background-color:#e8e8e8;padding: 5%;">
                        <div style="width: 100%;height: 80px;background: -webkit-linear-gradient(45deg, #008cc9,#009ea5);
                            background: -moz-linear-gradient(45deg, #008cc9,#009ea5);
                            background: -o-linear-gradient(45deg, #008cc9,#009ea5);
                            background: linear-gradient(45deg, #008cc9,#009ea5);
                            border-radius: 10px 10px 0px 0px;">
                            <center><img src="http://pertaminabiak.com/asset/img/logo-pertamina.png" width="200px;" style="margin-top:15px"/></center>
                        </div>
                        <div style="background-color:#ffffff;padding: 2%;">
                            <center>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;"><p>Kepada Yth. Bapak/Ibu '.$level.'</p>
                                    <p>Pada tanggal '.$this->tanggal_indo($perpanjangan[0]->tgl_aktifitas).' kami telah mengajukan permohonan approval untuk perihal sebagai berikut :</p>
                                </span>
                                <table width="50%" border="1">
                                    <tr>
                                        <th>Nomor Permohonan</th>
                                        <td>'.$perpanjangan[0]->no_aktifitas.'</td>
                                    </tr>
                                    <tr>
                                        <th>Transportir</th>
                                        <td>'.$perpanjangan[0]->nama_perusahaan.'</td>
                                    </tr>
                                    <tr>
                                        <th>Keperluan</th>
                                        <td>'.$perpanjangan[0]->nm_keperluan.'</td>
                                    </tr>
                                    <tr>
                                        <th>Detail Keperluan</th>
                                        <td>'.$perpanjangan[0]->detail_keperluan.'</td>
                                    </tr>
                                    <tr>
                                        <th>'.($type == "truck" ? 'No Polisi' : 'Nama Awak').'</th>
                                        <td>'.($type == "truck" ? $perpanjangan[0]->no_polisi : $perpanjangan[0]->nama_awak).'</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Masa Berlaku Baru</th>
                                        <td>'.$this->tanggal_indo($perpanjangan[0]->tgl_masa_berlaku_baru).'</td>
                                    </tr>
                                    <tr>
                                        <th>Tingkat Keperluan</th>
                                        <td>'.$perpanjangan[0]->tingkat_keperluan.'</td>
                                    </tr>
                                    <tr>
                                        <th>Catatan</th>
                                        <td>'.$perpanjangan[0]->komentar.'</td>
                                    </tr>';
                                    if($level == "MANAGER LOKASI"){
                                        $message .= '<tr>
                                        <th>Status</th>
                                        <td>'.($perpanjangan[0]->status_aktifitas == "1" ? "APPROVE ADMIN" : "").'</td>
                                    </tr>
                                    <tr>
                                        <th>Catatan Admin</th>
                                        <td>'.$perpanjangan[0]->catatan_admin.'</td>
                                    </tr>';
                                    }
            $message .= '</table>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;">
                                    <p>Mohon untuk Bapak/Ibu '.$level.' untuk segera menindaklanjuti permohonan ini dan segera melakukan peroses pengecekan dari data yang telah diinput pada sistem.</p>
                                </span>
                                <span style="font-size: 13px;font-style: normal;font-family: sans-serif;color: #7b7b7b;">Terima Kasih</span>
                                </center>
                            </div>
                            <div style="color: #a6a6a6;font-family: sans-serif;font-size: 12px;">
                                <center>
                                    <p>&copy;'.date('Y').' Sistem Informasi dan Monitoring Truck Tangki</p>
                                    <p>PT. Pertamina (persero) Fuel Terminal Biak</p>
                                </center>
                            </div>
                        </div></body></html>';

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.pertaminabiak.com',
            'smtp_port' => 587,
            'smtp_user' => 'noreply@pertaminabiak.com',
            'smtp_pass' => 'culametanmetmet',
            'mailtype'  => 'html', 
            'charset'  => 'iso-8859-1'
        );

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->from('noreply@pertaminabiak.com', 'PT. Pertamina Fuel Terminal Biak');
        $this->email->to($email_to);
        $this->email->cc($email_general);
        $this->email->subject('Permohonan Approval Dokumen '.$perpanjangan[0]->nm_keperluan." ".$perpanjangan[0]->nama_perusahaan); 
        $this->email->message($message);
        $this->email->send();
        return true;
    }

    public function send_mail_reject($type, $id, $level){
        $perpanjangan = $this->Model_perpanjangan->get_perpanjangan($type, $id);
        if($level == "PEMILIK"){
            $to = $this->Model_perpanjangan->get_email($level, $perpanjangan[0]->id_pemilik);
        }
        $general = $this->Model_perpanjangan->get_email_general();

        $email_to = array();
        $email_general = array();
        foreach($to as $tos){
            array_push($email_to, $tos['email']);
        }

        foreach($general as $gen){
            array_push($email_general, $gen['email']);
        }

        $message = '<html><body><div style="background-color:#e8e8e8;padding: 5%;">
                        <div style="width: 100%;height: 80px;background: -webkit-linear-gradient(45deg, #008cc9,#009ea5);
                            background: -moz-linear-gradient(45deg, #008cc9,#009ea5);
                            background: -o-linear-gradient(45deg, #008cc9,#009ea5);
                            background: linear-gradient(45deg, #008cc9,#009ea5);
                            border-radius: 10px 10px 0px 0px;">
                            <center><img src="http://pertaminabiak.com/asset/img/logo-pertamina.png" width="200px;" style="margin-top:15px"/></center>
                        </div>
                        <div style="background-color:#ffffff;padding: 2%;">
                            <center>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;"><p>Kepada Yth. Bapak/Ibu '.$level." ".strtoupper($perpanjangan[0]->nama_perusahaan).'</p>
                                    <p>Permohonan perpanjangan anda pada tanggal '.$this->tanggal_indo($perpanjangan[0]->tgl_aktifitas).' perihal sebagai berikut :</p>
                                </span>
                                <table width="50%" border="1">
                                    <tr>
                                        <th>Nomor Permohonan</th>
                                        <td>'.$perpanjangan[0]->no_aktifitas.'</td>
                                    </tr>
                                    <tr>
                                        <th>Transportir</th>
                                        <td>'.$perpanjangan[0]->nama_perusahaan.'</td>
                                    </tr>
                                    <tr>
                                        <th>Keperluan</th>
                                        <td>'.$perpanjangan[0]->nm_keperluan.'</td>
                                    </tr>
                                    <tr>
                                        <th>Detail Keperluan</th>
                                        <td>'.$perpanjangan[0]->detail_keperluan.'</td>
                                    </tr>
                                    <tr>
                                        <th>'.($type == "truck" ? 'No Polisi' : 'Nama Awak').'</th>
                                        <td>'.($type == "truck" ? $perpanjangan[0]->no_polisi : $perpanjangan[0]->nama_awak).'</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Masa Berlaku Baru</th>
                                        <td>'.$this->tanggal_indo($perpanjangan[0]->tgl_masa_berlaku_baru).'</td>
                                    </tr>
                                    <tr>
                                        <th>Tingkat Keperluan</th>
                                        <td>'.$perpanjangan[0]->tingkat_keperluan.'</td>
                                    </tr>';
                                    if($level == "PEMILIK"){
                                        $message .= '<tr>
                                        <th>Status</th>
                                        <td>'.($perpanjangan[0]->status_aktifitas == "2" ? "<b>REJECT ADMIN</b>" : "").'</td>
                                    </tr>
                                    <tr>
                                        <th>Catatan Admin</th>
                                        <td>'.$perpanjangan[0]->catatan_admin.'</td>
                                    </tr>';
                                    }
            $message .= '</table>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;">
                                    <p>Mohon untuk Bapak/Ibu '.$level.' untuk segera memperbaiki kesalahan pada dokumen yang dimaksud dan melakukan pengajuan ulang aras permohonan tersebut.</p>
                                </span>
                                <span style="font-size: 13px;font-style: normal;font-family: sans-serif;color: #7b7b7b;">Terima Kasih</span>
                                </center>
                            </div>
                            <div style="color: #a6a6a6;font-family: sans-serif;font-size: 12px;">
                                <center>
                                    <p>&copy;'.date('Y').' Sistem Informasi dan Monitoring Truck Tangki</p>
                                    <p>PT. Pertamina (persero) Fuel Terminal Biak</p>
                                </center>
                            </div>
                        </div></body></html>';

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'mail.pertaminabiak.com',
            'smtp_port' => 587,
            'smtp_user' => 'noreply@pertaminabiak.com',
            'smtp_pass' => 'culametanmetmet',
            'mailtype'  => 'html', 
            'charset'  => 'iso-8859-1'
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->from('noreply@pertaminabiak.com', 'PT. Pertamina Fuel Terminal Biak');
        $this->email->to($email_to);
        $this->email->cc($email_general);
        $this->email->subject('Notifikasi Permohonan Reject'); 
        $this->email->message($message);
        $this->email->send();
        return true;
    }

    public function tanggal_indo($tanggal){
        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

    public function export(){
    
        $transportir = $this->input->get("transportir");
        $bulan = $this->input->get("bulan");
        $tahun = $this->input->get("tahun");
        $status = $this->input->get("status");
        $from = $this->input->get("from");
            //$data["transportir"] = $this->Model_pemilik->getPemilik();
            //$data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan("truck", null, $transportir, $bulan, $tahun, $status);
            //$this->template->load("template", "perpanjangan/data-perpanjangan", $data);
        include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
        
        // Panggil class PHPExcel nya
        $excel = new PHPExcel();
       
        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Team Programmer')
                     ->setLastModifiedBy('Team Programmer')
                     ->setTitle("PT PERTAMINA")
                     ->setSubject("Laporan Perpanjangan ")
                     ->setDescription("Laporan Semua Data Perpanjangan")
                     ->setKeywords("PT PERTAMINA");
        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
          'font' => array('bold' => true), // Set font nya jadi bold
          //'color' => array('argb'=> 'FF808080'),
          'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
          ),
          'borders' => array(
            'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
            'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
            'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
            'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
          ),
          'fill' => array(
            'type' => PHPExcel_Style_Fill :: FILL_SOLID,
            'startcolor' => array(
                  'argb' => 'C0C0C0C0'
                )
          )
        );
       $gdImage = imagecreatefrompng('asset/img/logo-pertamina.png');
    // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    $objDrawing->setName('Sample image');
    $objDrawing->setDescription('Sample image');
    $objDrawing->setImageResource($gdImage);
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setOffsetX(75);//samping kanan kiri
    $objDrawing->setOffsetY(7);
    $objDrawing->setHeight(50);
    $objDrawing->setCoordinates('E1');
    $objDrawing->setWorksheet($excel->getActiveSheet());
    
    $gdImage = imagecreatefrompng('asset/img/truck.png');
    // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    $objDrawing->setName('Sample image');
    $objDrawing->setDescription('Sample image');
    $objDrawing->setImageResource($gdImage);
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setOffsetX(75);
    $objDrawing->setOffsetY(7);
    $objDrawing->setHeight(50);
    //$objDrawing->setWidth(20);
    $objDrawing->setCoordinates('H1');
    $objDrawing->setWorksheet($excel->getActiveSheet());
    
        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
          'alignment' => array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
          ),
          'borders' => array(
            'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
            'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
            'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
            'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
          ),
          // 'fill' => array(
          //   'type' => PHPExcel_Style_Fill :: FILL_GRADIENT_LINEAR,
          //   'startcolor' => array(
          //         'argb' => 'FFA0A0A0'
          //       ),
          //   'endcolor' => array(
          //         'argb' => 'FFFFFFFF'
    
       
          // ))
        );
    
            function tanggal_indo($tanggal){
                            $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            $split = explode('-', $tanggal);
                            return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                       }
                       function bulan_indo($tanggal){
                            $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            $split = explode('-', $tanggal);
                            return $bulan[ (int)$split[1] ] ;
                       }
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "PT PERTAMINA"); // Set kolom A1 dengan tulisan "DATA SISWA"
        $excel->getActiveSheet()->mergeCells('A1:J1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20); 
        $excel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    
        $excel->setActiveSheetIndex(0)->setCellValue('A2', "DATA PERPANJANGAN ".$tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
        $excel->getActiveSheet()->mergeCells('A2:J2'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); 
        $excel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A3', "Bulan");
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "Transportir"); // Set kolom A3 dengan tulisan "NO"
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "Mobil Tangki"); // Set kolom B3 dengan tulisan"
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "Keperluan"); // Set kolom C3 dengan tulisan "NAMA"
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "Tanggal Pengajuan"); // Set kolom A3 dengan tulisan "NO"
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "Tingkat"); // Set kolom B3 dengan tulisan "NIS"
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "Status"); // Set kolom C3 dengan tulisan "NAMA"
        $excel->setActiveSheetIndex(0)->setCellValue('I3', "komentar"); // Set kolom A3 dengan tulisan "NO"
        $excel->setActiveSheetIndex(0)->setCellValue('J3', "Catatan");   
        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
    
        $numrow = 4;
        //$no = 0;
        //$y = 0;
        $t = 0;
        
        $perpanjangan = $this->Model_perpanjangan->get_perpanjangan($from, null, $transportir, $bulan, $tahun, $status);
        $no = 1;
        
        if($perpanjangan){
          $total_data = count($perpanjangan);
          foreach($perpanjangan as $data){ // Lakukan looping pada variabel siswa
            //$excel->setActiveSheetIndex(0)->setCellValue('A2', "Laporan Po Tahun ".$tahun);
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, bulan_indo($data->tgl_aktifitas));
            $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama_perusahaan);
            $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->no_polisi);
            $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->nm_keperluan);
            $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, tanggal_indo($data->tgl_aktifitas));
            $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->tingkat_keperluan);
            $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->status_aktifitas);
            $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->komentar);
            $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->note_admin);
            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      
            $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
    
            // $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
            // $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      
            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
            $t++;
           
          }
    
          $excel->setActiveSheetIndex(0)->mergeCells('A'.($numrow - ($total_data)).':A'.($numrow + $t - $total_data - 1));
          $t = 0;
        
        
        
      }
    
        
        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(10); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(45); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(45); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(35); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(20); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(35); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('I')->setWidth(40); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('J')->setWidth(60); // Set width kolom C
        // $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
        // $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
      
        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    
    
    
        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Laporan Perpanjangan ".$from);
        $excel->setActiveSheetIndex(0);
        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Laporan Perpanjangan '.$from.'.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');
        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
      }
}