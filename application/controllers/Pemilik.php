<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemilik extends CI_Controller {

	public function __construct(){
		parent::__construct();
        checkSessionUser();
		$this->load->model("Model_pemilik");
		$this->load->model("Model_user");
	}

	public function index(){
		$data["pemilik"] = $this->Model_pemilik->getPemilik();
		$this->template->load("template", "pemilik/data-pemilik", $data);
	}

	public function tambah(){
		$this->template->load("template", "pemilik/form-pemilik");
	}

	public function edit(){
		$id = $this->input->get("id");
		$data["pemilik"] = $this->Model_pemilik->getPemilik($id);
		$this->template->load("template", "pemilik/edit-pemilik", $data);
	}

	public function action_tambah(){
		// $id_user = $this->input->post("id_user");
		$nama_pemilik = $this->input->post("nama_pemilik");
		$nama_perusahaan = $this->input->post("nama_perusahaan");
		$email_aktif = $this->input->post("email_aktif");
		$no_telfon = $this->input->post("no_telfon");
		$no_telfon_kedua = $this->input->post("no_telfon_kedua");
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$dataUser = array(
			"fullname" => $nama_pemilik,
			"email" => $email_aktif,
			"no_telfon" => $no_telfon,
			"no_telfon_kedua" => $no_telfon_kedua,
			"username" => $username,
			"password" => md5($password),
			"level_user" => "PEMILIK"
		);

		$tambahUserPemilik = $this->Model_user->add_user($dataUser);
		if($tambahUserPemilik){

			$dataPemilik = array(
				"id_user" => $tambahUserPemilik,
				"nama_pemilik" => $nama_pemilik,
				"nama_perusahaan" => $nama_perusahaan,
				"email_aktif" => $email_aktif,
				"no_telfon" => $no_telfon,
				"no_telfon_kedua" => $no_telfon_kedua
			);
	
			$tambahPemilik = $this->Model_pemilik->tambahPemilik($dataPemilik);
			if($tambahPemilik){
				$this->session->set_flashdata("success", "BERHASIL MENYIMPAN PEMILIK");
			} else {
				$this->session->set_flashdata("error", "GAGAL MENYIMPAN PEMILIK");
			}
		} else {
			$this->session->set_flashdata("error", "GAGAL MENYIMPAN USER PEMILIK");
		}
		redirect("pemilik");
		
	}

	public function action_ubah($idPemilik){
		// $id_user = $this->input->post("id_user");
		$nama_pemilik = $this->input->post("nama_pemilik");
		$nama_perusahaan = $this->input->post("nama_perusahaan");
		$email_aktif = $this->input->post("email_aktif");
		$no_telfon = $this->input->post("no_telfon");
		$no_telfon_kedua = $this->input->post("no_telfon_kedua");
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$dataPemilik = array(
			// "id_user" => $id_user,
			"nama_pemilik" => $nama_pemilik,
			"nama_perusahaan" => $nama_perusahaan,
			"email_aktif" => $email_aktif,
			"no_telfon" => $no_telfon,
			"no_telfon_kedua" => $no_telfon_kedua,
		);

		$dataUser = array(
			"email" => $email_aktif,
			"no_telfon" => $no_telfon,
			"no_telfon_kedua" => $no_telfon_kedua,
			"username" => $username,
		);

		if($password != ""){
            $dataUser["password"] = md5($password);
        }

		$ubahPemilik = $this->Model_pemilik->ubahPemilik($dataPemilik, $idPemilik);
		$ubahUser = $this->Model_pemilik->ubahUser($dataUser, $this->input->post("id_user"));
		if($ubahPemilik || $ubahUser){
			$this->session->set_flashdata("success", "BERHASIL MENGUBAH");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGUBAH");
		}		

		redirect("pemilik");

	}

	public function action_hapus($idPemilik){
		$getIdUser = $this->Model_pemilik->getPemilik($idPemilik);

		$hapusUserPemilik = $this->Model_pemilik->hapusUserPemilik($getIdUser[0]->id_user);
		$hapusPemilik = $this->Model_pemilik->hapusPemilik($idPemilik);
		if($hapusPemilik){
			$this->session->set_flashdata("success", "BERHASIL MENGHAPUS");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGHAPUS");
		}

		redirect("pemilik");
	}

	public function export(){
		include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
        $namesheet = array("Master Data Transportir");            
        $jumlah = array(5); 
        $header = array(
            array(
                'NO',
				'NAMA PEMILIK',
				'NAMA PERUSAHAAN',
				'EMAIL',
				'NO TELFON',
				'NO TELFON KEDUA'
            )
        );
        $get_data = $this->Model_pemilik->getPemilik();
        $i=0;
        while ($i < 1) {
            $objWorkSheet = $objPHPExcel->createSheet($i);
            $lastColumn = $objWorkSheet->getHighestColumn();
            for($x = 0; $x < $jumlah[$i]; $x++){                    
                $row = 1;                    
                $objWorkSheet->setCellValue($lastColumn.$row, $header[$i][$x]);
                $objWorkSheet
                    ->getColumnDimension($lastColumn)
                    ->setAutoSize(true);
                $lastColumn++;
            }
            
            $objWorkSheet->setTitle($namesheet[$i]);
            $i++;
        }     
        
        $index=0;
        $rowgabung = 2;
        while ($index < 1) {
            $worksheet = $objPHPExcel->setActiveSheetIndex($index);
            $row = 2;  
            if ($index == 0) {
                $a = 1;
                foreach ($get_data as $data) {       
                    $worksheet->setCellValue('A'.$row, $a); 
					$worksheet->setCellValue('B'.$row, $data->nama_pemilik);
					$worksheet->setCellValue('C'.$row, $data->nama_perusahaan);
					$worksheet->setCellValue('D'.$row, $data->email_aktif);
					$worksheet->setCellValue('E'.$row, $data->no_telfon);
					$worksheet->setCellValue('F'.$row, $data->no_telfon_kedua);
                    $row++;
                    $a++;
                }
            }
            $index++;
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $t = date("Y_m_d_H_i_s");
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=MASTER_DATA_TRANSPORTIR_".$t.".xlsx");
        $objWriter->save("php://output");
	}
}
?>