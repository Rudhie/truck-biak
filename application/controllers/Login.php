<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Model_login");
		$this->load->model("Model_user");
	}

	public function index(){
		if($this->session->userdata("is_login") == "Y"){
			redirect("dashboard");
		}
		$this->load->view("login_page");
	}
	
	public function action(){
		$username = $this->input->post("username");
		$password = $this->input->post("password");

		$checkUser = $this->Model_user->check_existing_user("username", $username);
		if($checkUser){
			$checkLogin = $this->Model_login->action_login($username, md5($password));
			if($checkLogin){
				if($checkLogin["level_user"] == "PEMILIK"){
					$getPemilik = $this->Model_login->get_pemilik($checkLogin['id_user']);
					$this->session->set_userdata($getPemilik);
				}
				$this->session->set_userdata($checkLogin);
				$this->session->set_userdata("is_login", "Y");
				echo json_encode(array("status" => "success", "message" => "Login Berhasil."));
			} else {
				echo json_encode(array("status" => "error", "message" => "Kombinasi username dan password salah."));
			}
		} else {
			echo json_encode(array("status" => "error", "message" => "Username tidak terdaftar, silahkan hubungi admin."));
		}
	}

	public function action_logout(){
		$this->session->sess_destroy();
		echo json_encode(array("status" => "success", "message" => "Logout Berhasil."));
	}
}