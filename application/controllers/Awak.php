<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Awak extends CI_Controller {

	public function __construct(){
		parent::__construct();
        checkSessionUser();
		$this->load->model("Model_awak_kernet");
	}

	public function index(){
		if($this->session->userdata("level_user") == "PEMILIK"){
			$filterTransportir = $this->session->userdata("id_pemilik");
		} else {
			if(isset($_GET['filterTransportir']) && ! empty($_GET['filterTransportir'])){
				$filterTransportir = $_GET['filterTransportir'];
			}else{
				$filterTransportir = "all";
			}
		}

		$data["awak"] = $this->Model_awak_kernet->getAwak(null, $filterTransportir);
		$data["nopol"] = $this->Model_awak_kernet->view_all_nopol();
		// $data["produk"] = $this->Model_awak_kernet->view_all_produk();
		$data["transportir"] = $this->Model_awak_kernet->view_all_transportir();
		// $data["produk"] = $this->Model_awak_kernet->view_all_produk();
		$this->template->load("template", "awak/data-awak", $data);
	}

	public function detail(){
		$id = $this->input->get("id");
		$data["awak"] = $this->Model_awak_kernet->getAwak($id, "", "");
		$data["nopol"] = $this->Model_awak_kernet->view_all_nopol();
		$this->template->load("template", "awak/detail-awak", $data);
	}

	public function tambah(){
		$data["nopol"] = $this->Model_awak_kernet->view_all_nopol();
		$this->template->load("template", "awak/form-awak", $data);
	}

	public function edit(){
		$id = $this->input->get("id");
		$data["awak"] = $this->Model_awak_kernet->getAwak($id, "", "");
		$data["nopol"] = $this->Model_awak_kernet->view_all_nopol();
		$this->template->load("template", "awak/edit-awak", $data);
	}

	public function action_tambah(){
		$id_truck = $this->input->post("id_truck");
		$nip_awak = $this->input->post("nip_awak");
		$nama_awak = $this->input->post("nama_awak");
		$foto_awak = $this->input->post("foto_awak");
		$tgl_lahir_awak = $this->input->post("tgl_lahir_awak");
		$masa_berlaku_sim_awak = $this->input->post("masa_berlaku_sim_awak");
		$dokumen_sim_awak = $this->input->post("dokumen_sim_awak");
		$masa_berlaku_idcard_awak = $this->input->post("masa_berlaku_idcard_awak");
		$dokumen_idcard_awak = $this->input->post("dokumen_idcard_awak");
		$dokumen_ktp_awak = $this->input->post("dokumen_ktp_awak");
		$bagian = $this->input->post("bagian");

		$dataAwak = array(
			"id_truck" => $id_truck,
			"nip_awak" => $nip_awak,
			"nama_awak" => $nama_awak,
			"tgl_lahir_awak" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_lahir_awak))),
			"masa_berlaku_sim_awak" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_sim_awak))),
			"masa_berlaku_idcard_awak" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_idcard_awak))),
			"bagian" => $bagian
		);

		if(isset($_FILES['dokumen_sim_awak']) || isset($_FILES['dokumen_idcard_awak']) || isset($_FILES['dokumen_ktp_awak'])){
			$config['upload_path']= './dokumen/berkas-awak/';
			$config['allowed_types'] = 'pdf';
			$config['max_size'] = 2048;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if(isset($_FILES['dokumen_sim_awak'])){
				if ($this->upload->do_upload('dokumen_sim_awak')){
					$dokumen_sim_awak = $this->upload->data();
					$dataAwak["dokumen_sim_awak"] = $dokumen_sim_awak["file_name"];
				}                    
			}

			if(isset($_FILES['dokumen_idcard_awak'])){
				if($this->upload->do_upload('dokumen_idcard_awak')){
					$dokumen_idcard_awak = $this->upload->data();
					$dataAwak["dokumen_idcard_awak"] = $dokumen_idcard_awak["file_name"];
				}

			}
			if (isset($_FILES['dokumen_ktp_awak'])){
				if ($this->upload->do_upload('dokumen_ktp_awak')){
					$dokumen_ktp_awak = $this->upload->data();
					$dataAwak["dokumen_ktp_awak"] = $dokumen_ktp_awak["file_name"];
				}
			}
		}

		if(isset($_FILES['foto_awak'])) {
			$configFoto['upload_path']= './dokumen/foto-awak/';
			$configFoto['allowed_types'] = 'jpg|png|jpeg';
			$configFoto['encrypt_name'] = TRUE;
			$configFoto['max_size'] = 2048;
			$this->load->library('upload', $configFoto);
			$this->upload->initialize($configFoto);
			if(isset($_FILES['foto_awak'])){
				if ($this->upload->do_upload('foto_awak')){
					$foto_awak = $this->upload->data();
					$dataAwak["foto_awak"] = $foto_awak["file_name"];
				}                    
			}
		}

		$tambahAwak = $this->Model_awak_kernet->tambahAwak($dataAwak);
		if($tambahAwak){
			$this->session->set_flashdata("success", "BERHASIL MENYIMPAN");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENYIMPAN");
		}

		redirect("awak");
	}

	public function action_ubah($idAwak){
		$id_truck = $this->input->post("id_truck");
		$nip_awak = $this->input->post("nip_awak");
		$nama_awak = $this->input->post("nama_awak");
		$tgl_lahir_awak = $this->input->post("tgl_lahir_awak");
		$masa_berlaku_sim_awak = $this->input->post("masa_berlaku_sim_awak");
		$dokumen_sim_awak = $this->input->post("dokumen_sim_awak");
		$masa_berlaku_idcard_awak = $this->input->post("masa_berlaku_idcard_awak");
		$dokumen_idcard_awak = $this->input->post("dokumen_idcard_awak");
		$dokumen_ktp_awak = $this->input->post("dokumen_ktp_awak");
		$foto_awak = $this->input->post("foto_awak");
		$bagian = $this->input->post("bagian");

		$data = array(
			"id_truck" => $id_truck,
			"nip_awak" => $nip_awak,
			"nama_awak" => $nama_awak,
			"tgl_lahir_awak" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_lahir_awak))),
			"masa_berlaku_sim_awak" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_sim_awak))),
			"masa_berlaku_idcard_awak" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_idcard_awak))),
			"bagian" => $bagian,
			"updated_date" => date("Y-m-d H:i:s")
		);

		$get_old_files = $this->Model_awak_kernet->getAwak($idAwak);

		if($_FILES['dokumen_sim_awak']['name'] != "" || $_FILES['dokumen_idcard_awak']['name'] != "" || $_FILES['dokumen_ktp_awak']['name'] != ""){
			$config['upload_path'] = './dokumen/berkas-awak/';
			$config['allowed_types'] = 'pdf';
			$config['max_size'] = 2048;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($_FILES['dokumen_sim_awak']['name'] != ""){
				if($this->upload->do_upload('dokumen_sim_awak')){
					unlink("./dokumen/berkas-awak/".$get_old_files[0]->dokumen_sim_awak);

					$dokumen_sim_awak = $this->upload->data();
					$data['dokumen_sim_awak'] = $dokumen_sim_awak["file_name"];
				}
			}

			if($_FILES['dokumen_idcard_awak']['name'] != ""){
				if($this->upload->do_upload('dokumen_idcard_awak')){
					unlink("./dokumen/berkas-awak/".$get_old_files[0]->dokumen_idcard_awak);

					$dokumen_idcard_awak = $this->upload->data();
					$data['dokumen_idcard_awak'] = $dokumen_idcard_awak["file_name"];
				}
			}

			if ($_FILES['dokumen_ktp_awak']['name'] != "") {
				if ($this->upload->do_upload('dokumen_ktp_awak')){
					unlink("./dokumen/berkas-awak/".$get_old_files[0]->dokumen_ktp_awak);

					$dokumen_ktp_awak = $this->upload->data();
					$data['dokumen_ktp_awak'] = $dokumen_ktp_awak["file_name"];
				}
			}

		}
		if($_FILES['foto_awak']['name'] != ""){
			$configFoto['upload_path'] = './dokumen/foto-awak/';
			$configFoto['allowed_types'] = 'jpg|png|jpeg';
			$configFoto['max_size'] = 2048;
			$this->load->library('upload', $configFoto);
			$this->upload->initialize($configFoto);

			if($_FILES['foto_awak']['name'] != ""){
				if ($this->upload->do_upload('foto_awak')){
					unlink("./dokumen/foto-awak/".$get_old_files[0]->foto_awak)

					$foto_awak = $this->upload->data();
					$data["foto_awak"] = $foto_awak["file_name"];
				}                    
			}
		}

		$ubahAwak = $this->Model_awak_kernet->ubahAwak($data, $idAwak);
		if($ubahAwak){
			$this->session->set_flashdata("success", "BERHASIL MENGUBAH");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGUBAH");
		}

		
		redirect("awak");

	}

	public function action_hapus($id){
		$_id = $this->db->get_where('tbl_m_awak',['id_awak' => $id])->row();
		$query = $this->db->delete('tbl_m_awak',['id_awak'=>$id]);
		unlink("./dokumen/foto-awak/".$_id->foto_awak);
		unlink("./dokumen/berkas-awak/".$_id->dokumen_sim_awak);
		unlink("./dokumen/berkas-awak/".$_id->dokumen_idcard_awak);

		if ($query) {
			$this->session->set_flashdata('success', 'Berhasil Menghapus');
		} else {
			$this->session->set_flashdata('error', 'Gagal Menghapus');
		}

		redirect("awak");
	}

	public function export(){
		include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
        $namesheet = array("Master Data Mobil Tangki");            
        $jumlah = array(10); 
        $header = array(
            array(
                'NO',
				'NAMA PERUSAHAAN',
				'NO POLISI',
				'NIP',
				'NAMA AWAK',
				'TGL LAHIR AWAK',
				'MASA BERLAKU SIM',
				'MASA BERLAKU ID CARD',
				'BAGIAN'
            )
		);
		
		if($this->session->userdata("level_user") == "PEMILIK"){
			$filterTransportir = $this->session->userdata("id_pemilik");
		} else {
			if(isset($_GET['filterTransportir']) && ! empty($_GET['filterTransportir'])){
				$filterTransportir = $_GET['filterTransportir'];
			}else{
				$filterTransportir = "all";
			}
		}

		
		$get_data = $this->Model_awak_kernet->getAwak(null, $filterTransportir);
        $i=0;
        while ($i < 1) {
            $objWorkSheet = $objPHPExcel->createSheet($i);
            $lastColumn = $objWorkSheet->getHighestColumn();
            for($x = 0; $x < $jumlah[$i]; $x++){                    
                $row = 1;                    
                $objWorkSheet->setCellValue($lastColumn.$row, $header[$i][$x]);
                $objWorkSheet
                    ->getColumnDimension($lastColumn)
                    ->setAutoSize(true);
                $lastColumn++;
            }
            
            $objWorkSheet->setTitle($namesheet[$i]);
            $i++;
        }     
        
        $index=0;
        $rowgabung = 2;
        while ($index < 1) {
            $worksheet = $objPHPExcel->setActiveSheetIndex($index);
            $row = 2;  
            if ($index == 0) {
                $a = 1;
                foreach ($get_data as $data) {       
                    $worksheet->setCellValue('A'.$row, $a); 
					$worksheet->setCellValue('B'.$row, $data->nama_perusahaan);
					$worksheet->setCellValue('C'.$row, $data->nomor_polisi);
					$worksheet->setCellValue('D'.$row, $data->nip_awak);
					$worksheet->setCellValue('E'.$row, strtoupper($data->nama_awak));
					$worksheet->setCellValue('F'.$row, $data->tgl_lahir_awak);
					$worksheet->setCellValue('G'.$row, $data->masa_berlaku_sim_awak);
					$worksheet->setCellValue('h'.$row, $data->masa_berlaku_idcard_awak);
					$worksheet->setCellValue('i'.$row, $data->bagian);
                    $row++;
                    $a++;
                }
            }
            $index++;
        }
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $t = date("Y_m_d_H_i_s");
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=MASTER_DATA_AWAK_".$t.".xlsx");
        $objWriter->save("php://output");
	}
}
?>