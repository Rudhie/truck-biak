<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Truck extends CI_Controller {

	public function __construct(){
		parent::__construct();
		checkSessionUser();
		$this->load->model("Model_truck");
	}

	public function index(){
		if($this->session->userdata("level_user") == "PEMILIK"){
			$filterTransportir = $this->session->userdata("id_pemilik");
		} else {
			if(isset($_GET['filterTransportir']) && ! empty($_GET['filterTransportir'])){
				$filterTransportir = $_GET['filterTransportir'];
			}else{
				$filterTransportir = "all";
			}
		}

		
		$data["truck"] = $this->Model_truck->getTruck(null, $filterTransportir);
		$data["transportir"] = $this->Model_truck->view_all_pemilik();
		$data["merek"] = $this->Model_truck->view_all_merek();
		$this->template->load("template", "truck/data-truck", $data);
	}
	public function detail(){
		$id = $this->input->get("id");
		$data["truck"] = $this->Model_truck->getTruck($id);
		$data["merek"] = $this->Model_truck->getTruck($id);
		$this->template->load("template", "truck/detail-truck", $data);
	}
	public function tambah(){
		$data["truck"] = $this->Model_truck->getTruck();
		$data["pemilik"] = $this->Model_truck->view_all_pemilik();
		$data["merek"] = $this->Model_truck->view_all_merek();
		$this->template->load("template", "truck/form-truck", $data);
	}

	public function edit(){
		$id = $this->input->get("id");
		$data["truck"] = $this->Model_truck->getTruck($id);
		$data["pemilik"] = $this->Model_truck->view_all_pemilik();
		$data["merek"] = $this->Model_truck->view_all_merek();
		$this->template->load("template", "truck/edit-truck", $data);
	}

	public function action_tambah(){
		
		$id_pemilik = $this->input->post("id_pemilik");
		$id_merek = $this->input->post("id_merek");
		$no_polisi = $this->input->post("no_polisi");
		$no_rangka = $this->input->post("no_rangka");
		$no_mesin = $this->input->post("no_mesin");
		$tahun_pembuatan = $this->input->post("tahun_pembuatan");
		$masa_berlaku_stnk = $this->input->post("masa_berlaku_stnk");
		$masa_berlaku_skt = $this->input->post("masa_berlaku_skt");
		$masa_berlaku_kim = $this->input->post("masa_berlaku_kim");
		$isi_silinder = $this->input->post("isi_silinder");
		$produk = $this->input->post("produk");
		$keur = $this->input->post("keur");
		$uji_emisi = $this->input->post("uji_emisi");
		$metrologi_tera = $this->input->post("metrologi_tera");
		$t2 = $this->input->post("t2");
		$vol_kl = $this->input->post("vol_kl");
		$dokumen_stnk = $this->input->post("dokumen_stnk");
		$dokumen_kir = $this->input->post("dokumen_kir");
		$dokumen_skt = $this->input->post("dokumen_skt");
		$dokumen_metrologi_tera = $this->input->post("dokumen_metrologi_tera");
		$dokumen_kim = $this->input->post("dokumen_kim");

		$dataTruck = array(
			"id_pemilik" => $id_pemilik,
			"id_merek" => $id_merek,
			"no_polisi" => $no_polisi,
			"no_rangka" => $no_rangka,
			"no_mesin" => $no_mesin,
			"tahun_pembuatan" => $tahun_pembuatan,
			"masa_berlaku_stnk" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_stnk))),
			"masa_berlaku_skt" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_skt))),
			"masa_berlaku_kim" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_kim))),
			"isi_silinder" => $isi_silinder,
			"produk" => $produk,
			"keur" => date("Y-m-d", strtotime(str_replace("/", "-", $keur))),
			"uji_emisi" => $uji_emisi,
			"metrologi_tera" => date("Y-m-d", strtotime(str_replace("/", "-", $metrologi_tera))),
			"t2" => $t2,
			"vol_kl" => $vol_kl
		);

		if(isset($_FILES['dokumen_stnk']) || isset($_FILES['dokumen_kir']) || isset($_FILES['dokumen_skt']) || isset($_FILES['dokumen_metrologi_tera']) || isset($_FILES['dokumen_kim'])){
			$config['upload_path']= './dokumen/berkas-truck/';
			$config['allowed_types'] = 'pdf';
			$config['max_size'] = 2048;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if(isset($_FILES['dokumen_stnk'])){
				if ($this->upload->do_upload('dokumen_stnk')){
					$dokumen_stnk = $this->upload->data();
					$dataTruck["dokumen_stnk"] = $dokumen_stnk["file_name"];
				}                    
			}

			if(isset($_FILES['dokumen_kir'])){
				if($this->upload->do_upload('dokumen_kir')){
					$dokumen_kir = $this->upload->data();
					$dataTruck["dokumen_kir"] = $dokumen_kir["file_name"];
				}

			}

			if(isset($_FILES['dokumen_skt'])){
				if($this->upload->do_upload('dokumen_skt')){
					$dokumen_skt = $this->upload->data();
					$dataTruck["dokumen_skt"] = $dokumen_skt["file_name"];
				}
			}

			if(isset($_FILES['dokumen_metrologi_tera'])){
				if($this->upload->do_upload('dokumen_metrologi_tera')){
					$dokumen_metrologi_tera = $this->upload->data();
					$dataTruck["dokumen_metrologi_tera"] = $dokumen_metrologi_tera["file_name"];
				}
			}

			if(isset($_FILES['dokumen_kim'])){
				if($this->upload->do_upload('dokumen_kim')){
					$dokumen_kim = $this->upload->data();
					$dataTruck["dokumen_kim"] = $dokumen_kim["file_name"];
				}
			}
		}

		if(isset($_FILES['foto_truck']) || isset($_FILES['foto_depan_truck']) || isset($_FILES['foto_belakang_truck'])) {
			$configFoto['upload_path']= './dokumen/foto-truck/';
			$configFoto['allowed_types'] = 'jpg|png|jpeg|pdf';
			$config['encrypt_name'] = TRUE;
			$configFoto['max_size'] = 2048;
			$this->load->library('upload', $configFoto);
			$this->upload->initialize($configFoto);
			if(isset($_FILES['foto_truck'])){
				if ($this->upload->do_upload('foto_truck')){
					$foto_truck = $this->upload->data();
					$dataTruck["foto_truck"] = $foto_truck["file_name"];
				}                    
			}

			if(isset($_FILES['foto_depan_truck'])){
				if ($this->upload->do_upload('foto_depan_truck')){
					$foto_depan_truck = $this->upload->data();
					$dataTruck["foto_depan_truck"] = $foto_depan_truck["file_name"];
				}                    
			}
			if(isset($_FILES['foto_belakang_truck'])){
				if ($this->upload->do_upload('foto_belakang_truck')){
					$foto_belakang_truck = $this->upload->data();
					$dataTruck["foto_belakang_truck"] = $foto_belakang_truck["file_name"];
				}                    
			}
		}

		$tambahTruck = $this->Model_truck->tambahTruck($dataTruck);
		if($tambahTruck){
			$this->session->set_flashdata("success", "BERHASIL MENYIMPAN");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENYIMPAN");
		}

		redirect("truck");
	}

	public function action_edit($idTruck){

		$id_pemilik = $this->input->post("id_pemilik");
		$id_merek = $this->input->post("id_merek");
		$no_polisi = $this->input->post("no_polisi");
		$no_rangka = $this->input->post("no_rangka");
		$no_mesin = $this->input->post("no_mesin");
		$tahun_pembuatan = $this->input->post("tahun_pembuatan");
		$masa_berlaku_stnk = $this->input->post("masa_berlaku_stnk");
		$masa_berlaku_skt = $this->input->post("masa_berlaku_skt");
		$masa_berlaku_kim = $this->input->post("masa_berlaku_kim");
		$foto_truck = $this->input->post("foto_truck");
		$isi_silinder = $this->input->post("isi_silinder");
		$produk = $this->input->post("produk");
		$keur = $this->input->post("keur");
		$uji_emisi = $this->input->post("uji_emisi");
		$metrologi_tera = $this->input->post("metrologi_tera");
		$t2 = $this->input->post("t2");
		$vol_kl = $this->input->post("vol_kl");
		$dokumen_stnk = $this->input->post("dokumen_stnk");
		$dokumen_kir = $this->input->post("dokumen_kir");

		$data = array(
			"id_pemilik" => $id_pemilik,
			"id_merek" => $id_merek,
			"no_polisi" => $no_polisi,
			"no_rangka" => $no_rangka,
			"no_mesin" => $no_mesin,
			"tahun_pembuatan" => $tahun_pembuatan,
			"masa_berlaku_stnk" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_stnk))),
			"masa_berlaku_skt" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_skt))),
			"metrologi_tera" => date("Y-m-d", strtotime(str_replace("/", "-", $metrologi_tera))),
			"keur" => date("Y-m-d", strtotime(str_replace("/", "-", $keur))),
			"masa_berlaku_kim" => date("Y-m-d", strtotime(str_replace("/", "-", $masa_berlaku_kim))),
			"uji_emisi" => $uji_emisi,
			"t2" => $t2,
			"vol_kl" => $vol_kl,
			"updated_date" => date("Y-m-d H:i:s")

		);

		$get_old_files = $this->Model_truck->getTruck($idTruck);


		if($_FILES['dokumen_stnk']['name'] != "" || $_FILES['dokumen_kir']['name'] != "" || $_FILES['dokumen_skt']['name'] != "" || $_FILES['dokumen_metrologi_tera']['name'] != "" || $_FILES['dokumen_kim'] != ""){
			$config['upload_path'] = './dokumen/berkas-truck/';
			$config['allowed_types'] = 'pdf';
			$config['max_size'] = 1024;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($_FILES['dokumen_stnk']['name'] != ""){
				if($this->upload->do_upload('dokumen_stnk')){
					unlink("./dokumen/berkas-truck/".$get_old_files[0]->dokumen_stnk);

					$dokumen_stnk = $this->upload->data();
					$data['dokumen_stnk'] = $dokumen_stnk["file_name"];
				}
			}

			if($_FILES['dokumen_kir']['name'] != ""){
				if($this->upload->do_upload('dokumen_kir')){
					unlink("./dokumen/berkas-truck/".$get_old_files[0]->dokumen_kir);

					$dokumen_kir = $this->upload->data();
					$data['dokumen_kir'] = $dokumen_kir["file_name"];
				}
			}

			if($_FILES['dokumen_skt']['name'] != ""){
				if($this->upload->do_upload('dokumen_skt')){
					unlink("./dokumen/berkas-truck/".$get_old_files[0]->dokumen_skt);

					$dokumen_skt = $this->upload->data();
					$data['dokumen_skt'] = $dokumen_skt["file_name"];
				}
			}

			if($_FILES['dokumen_metrologi_tera']['name'] != ""){
				if($this->upload->do_upload('dokumen_metrologi_tera')){
					unlink("./dokumen/berkas-truck/".$get_old_files[0]->dokumen_metrologi_tera);

					$dokumen_metrologi_tera = $this->upload->data();
					$data['dokumen_metrologi_tera'] = $dokumen_metrologi_tera["file_name"];
				}
			}
			if($_FILES['dokumen_kim']['name'] != ""){
				if($this->upload->do_upload('dokumen_kim')){
					unlink("./dokumen/berkas-truck/".$get_old_files[0]->dokumen_kim);

					$dokumen_kim = $this->upload->data();
					$data['dokumen_kim'] = $dokumen_kim["file_name"];
				}
			}

		}

		if($_FILES['foto_truck']['name'] != "" || $_FILES['foto_depan_truck']['name'] != "" || $_FILES['foto_belakang_truck']['name'] != ""){
			$configFoto['upload_path'] = './dokumen/foto-truck/';
			$configFoto['allowed_types'] = 'jpg|png|jpeg';
			$configFoto['max_size'] = 1024;
			$this->load->library('upload', $configFoto);
			$this->upload->initialize($configFoto);

			if($_FILES['foto_truck']['name'] != ""){
				if ($this->upload->do_upload('foto_truck')){
					unlink("./dokumen/foto-truck/".$get_old_files[0]->foto_truck);

					$foto_truck = $this->upload->data();
					$data["foto_truck"] = $foto_truck["file_name"];
				}                    
			}

			if($_FILES['foto_depan_truck']['name'] != ""){
				if($this->upload->do_upload('foto_depan_truck')){
					unlink("./dokumen/foto-truck/".$get_old_files[0]->foto_depan_truck);

					$foto_depan_truck = $this->upload->data();
					$data["foto_depan_truck"] = $foto_depan_truck["file_name"];
				}

			}

			if($_FILES['foto_belakang_truck']['name'] != ""){
				if($this->upload->do_upload('foto_belakang_truck')){
					unlink("./dokumen/foto-truck/".$get_old_files[0]->foto_belakang_truck);

					$foto_belakang_truck = $this->upload->data();
					$data["foto_belakang_truck"] = $foto_belakang_truck["file_name"];
				}

			}
		}

		$edit = $this->Model_truck->ubahTruck($data, $idTruck);
		if($edit){
			$this->session->set_flashdata("success", "Berhasil Mengubah Truck");
		} else {
			$this->session->set_flashdata("error", "Gagal Mengubah Truck");
		}
		redirect("truck");
	}

	public function action_hapus($idTruck){
		$hapusTruck = $this->Model_truck->hapusTruck($idTruck);
		if($hapusTruck){
			$this->session->set_flashdata("success", "BERHASIL MENGHAPUS");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGHAPUS");
		}

		redirect("truck");
	}

	public function export(){
		include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
		$objPHPExcel = new PHPExcel();
		$namesheet = array("Master Data Mobil Tangki");            
		$jumlah = array(16); 
		$header = array(
			array(
				'NO',
				'NAMA PERUSAHAAN',
				'NO POLISI',
				'NO RANGKA',
				'NO MESIN',
				'MEREK',
				'TAHUN PEMBUATAN',
				'MASA BERLAKU STNK',
				'MASA BERLAKU SKT',
				'ISI SILINDER',
				'PRODUK',
				'KEUR',
				'UJI EMISI',
				'METROLOGI TERA',
				'T2',
				'VOL KL'
			)
		);
		
		if($this->session->userdata("level_user") == "PEMILIK"){
			$filterTransportir = $this->session->userdata("id_pemilik");
		} else {
			if(isset($_GET['filterTransportir']) && ! empty($_GET['filterTransportir'])){
				$filterTransportir = $_GET['filterTransportir'];
			}else{
				$filterTransportir = "all";
			}
		}

		
		$get_data = $this->Model_truck->getTruck(null, $filterTransportir);
		$i=0;
		while ($i < 1) {
			$objWorkSheet = $objPHPExcel->createSheet($i);
			$lastColumn = $objWorkSheet->getHighestColumn();
			for($x = 0; $x < $jumlah[$i]; $x++){                    
				$row = 1;                    
				$objWorkSheet->setCellValue($lastColumn.$row, $header[$i][$x]);
				$objWorkSheet
				->getColumnDimension($lastColumn)
				->setAutoSize(true);
				$lastColumn++;
			}

			$objWorkSheet->setTitle($namesheet[$i]);
			$i++;
		}     

		$index=0;
		$rowgabung = 2;
		while ($index < 1) {
			$worksheet = $objPHPExcel->setActiveSheetIndex($index);
			$row = 2;  
			if ($index == 0) {
				$a = 1;
				foreach ($get_data as $data) {       
					$worksheet->setCellValue('A'.$row, $a); 
					$worksheet->setCellValue('B'.$row, $data->nama_perusahaan);
					$worksheet->setCellValue('C'.$row, $data->no_polisi);
					$worksheet->setCellValue('D'.$row, $data->no_rangka);
					$worksheet->setCellValue('E'.$row, $data->no_mesin);
					$worksheet->setCellValue('F'.$row, $data->nama_merek);
					$worksheet->setCellValue('G'.$row, $data->tahun_pembuatan);
					$worksheet->setCellValue('H'.$row, $data->masa_berlaku_stnk);
					$worksheet->setCellValue('I'.$row, $data->masa_berlaku_skt);
					$worksheet->setCellValue('J'.$row, $data->isi_silinder);
					$worksheet->setCellValue('K'.$row, strtoupper($data->produk));
					$worksheet->setCellValue('L'.$row, $data->keur);
					$worksheet->setCellValue('M'.$row, $data->uji_emisi);
					$worksheet->setCellValue('N'.$row, $data->metrologi_tera);
					$worksheet->setCellValue('O'.$row, $data->t2);
					$worksheet->setCellValue('P'.$row, $data->vol_kl);
					$row++;
					$a++;
				}
			}
			$index++;
		}

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$t = date("Y_m_d_H_i_s");
		ob_end_clean();
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Disposition: attachment;filename=MASTER_DATA_MOBIL_TANGKI_".$t.".xlsx");
		$objWriter->save("php://output");
	}
}
?>