<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Model_sample");
	}

	public function index(){
		$this->template->load("template", "welcome_message");
	}

	public function data($id = null){
		$getData = $this->Model_sample->getData($id);
		echo json_encode(array("status" => "success", "data" => $getData));
	}

	public function add_data(){
		$nama = $this->input->post("nama");

		$data = array(
			"nama" => $nama
		);

		$add = $this->Model_sample->addData($data);
		if($add){
			echo json_encode(array("status" => "success", "data" => $data, "message" => "Berhasil Menambahkan Data"));
		} else {
			echo json_encode(array("status" => "error", "message" => "Gagal Menambahkan Data"));
		}
	}

	public function update_data(){
		$id_sample = $this->input->post("id_sample");
		$nama = $this->input->post("nama");

		$data = array(
			"nama" => $nama
		);

		$add = $this->Model_sample->updateData($data, $id_sample);
		if($add){
			echo json_encode(array("status" => "success", "data" => $data, "message" => "Berhasil Mengubah Data"));
		} else {
			echo json_encode(array("status" => "error", "message" => "Gagal Mengubah Data"));
		}
	}

	public function delete_data(){
		$id_sample = $this->input->post("id_sample");
		$add = $this->Model_sample->deleteData($id_sample);
		if($add){
			echo json_encode(array("status" => "success", "data" => $id_sample, "message" => "Berhasil Menghapus Data"));
		} else {
			echo json_encode(array("status" => "error", "message" => "Gagal Menghapus Data"));
		}
	}
}
