<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct(){
		parent::__construct();
		checkSessionUser();
		$this->load->model("Model_dashboard");
		$this->load->model("Model_truck");
		$this->load->model("Model_awak_kernet");
	}

	public function index(){
		$id_pemilik = ($this->session->userdata("level_user") == "PEMILIK" ? $this->session->userdata("id_pemilik") : null);
		$data["pemilik"] = $this->Model_dashboard->count_pemilik();
		$data["truck"] = $this->Model_dashboard->count_truck($id_pemilik);
		$data["supir_kernet"] = $this->Model_dashboard->count_supir_kernet($id_pemilik);
		$data["aktifitas"] = $this->Model_dashboard->count_aktifitas($id_pemilik);
		$data["sim"] = $this->Model_dashboard->count_sim($id_pemilik);
		$data["stnk"] = $this->Model_dashboard->count_stnk($id_pemilik);
		$data["skt"] = $this->Model_dashboard->count_skt($id_pemilik);
		$data["kim"] = $this->Model_dashboard->count_kim($id_pemilik);
		$data["kir"] = $this->Model_dashboard->count_kir($id_pemilik);
		$data["metrologi"] = $this->Model_dashboard->count_metrologi($id_pemilik);
		$data["newidcard"] = $this->Model_dashboard->count_newidcard($id_pemilik);
		$data["oldidcard"] = $this->Model_dashboard->count_oldidcard($id_pemilik);
		$data["gantiawak"] = $this->Model_dashboard->count_gantiawak($id_pemilik);
		$data["urgent_temuan"] = $this->Model_dashboard->count_urgent_temuan($id_pemilik);
		$this->template->load("template", "dashboard/data-dashboard", $data);
	}
	
	public function kalender_masa_aktif(){
		$id_pemilik = ($this->session->userdata("level_user") == "PEMILIK" ? $this->session->userdata("id_pemilik") : "");
		$truck = $this->Model_truck->getTruck(null, $id_pemilik);
		$awak = $this->Model_awak_kernet->getAwak(null, $id_pemilik);
		foreach ($truck as $t_key => $t_val) {
			if($t_val->masa_berlaku_stnk != null && $t_val->masa_berlaku_stnk != "0000-00-00"){
				$data["title"] = "STNK - ".$t_val->no_polisi;
				$data["start"] = $t_val->masa_berlaku_stnk;
			}

			if($t_val->masa_berlaku_skt != null && $t_val->masa_berlaku_skt != "0000-00-00"){
				$data["title"] = "SKT - ".$t_val->no_polisi;
				$data["start"] = $t_val->masa_berlaku_skt;
			}

			if($t_val->metrologi_tera != null && $t_val->metrologi_tera != "0000-00-00"){
				$data["title"] = "TERA - ".$t_val->no_polisi;
				$data["start"] = $t_val->metrologi_tera;
			}

			if($t_val->keur != null && $t_val->keur != "0000-00-00"){
				$data["title"] = "KEUR - ".$t_val->no_polisi;
				$data["start"] = $t_val->keur;
			}

			if($t_val->masa_berlaku_kim != null && $t_val->masa_berlaku_kim != "0000-00-00"){
				$data["title"] = "KIM AWAK - ".$t_val->no_polisi;
				$data["start"] = $t_val->masa_berlaku_kim;
			}

			$json[] = $data;
		}

		foreach ($awak as $a_key => $a_val) {
			if($a_val->masa_berlaku_sim_awak != null && $a_val->masa_berlaku_sim_awak != "0000-00-00"){
				$data["title"] = "SIM AWAK - ".$a_val->nama_awak;
				$data["start"] = $a_val->masa_berlaku_sim_awak;
			}

			if($a_val->masa_berlaku_idcard_awak != null && $a_val->masa_berlaku_idcard_awak != "0000-00-00"){
				$data["title"] = "IDCARD AWAK - ".$a_val->nama_awak;
				$data["start"] = $a_val->masa_berlaku_idcard_awak;
			}

			$json[] = $data;
		}

		echo json_encode(array("data" => $json));
	}

	public function notifikasi(){
		$level_user = $this->session->userdata("level_user");
		$notif = $this->Model_dashboard->count_notifikasi($level_user, "count");
		$data = $this->Model_dashboard->count_notifikasi($level_user, "data");
		echo json_encode(array("jumlah" => $notif->jumlah, "data" => $data));
	}

	public function rekap_pengajuan(){
		$bulan = array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des");
		$id_pemilik = ($this->session->userdata("level_user") == "PEMILIK" ? $this->session->userdata("id_pemilik") : null);
		$data = $this->Model_dashboard->get_rekap_pengajuan($id_pemilik);
		$amt = array();
		$truck = array();
		for($i = 1; $i<= 12;$i++){
			$dataTruck = 0;
			$dataAmt = 0;

			foreach ($data as $key => $value) {
				if($value->type_keperluan == "truck"){
					if($i == $value->bulan){
						$dataTruck = (int)$value->jml;
					}
				}

				if($value->type_keperluan == "amt"){
					if($i == $value->bulan){
						$dataAmt = (int)$value->jml;
					}
				}
			}
			array_push($truck, $dataTruck);
			array_push($amt, $dataAmt);

		}

		echo json_encode(array("categories" =>  $bulan, "truck" => $truck, "amt" => $amt));

	}
}