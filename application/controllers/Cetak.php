<?php
Class Cetak extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdfcetak');
		
		$this->load->model("Model_temuan");
    }
    // function Header(){
    // 	$this->SetAutoPageBreak(False, 0);
    // 	$img_file = './asset/img/a4.jpg/';
    // 	$this->image($img_file, 0, 0 ,210, 297, '','','', false, 300, '', false, false, 0);
    // }

    function tanggal_indo($tanggal){
    	$bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
    	$split = explode('-', $tanggal);
    	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }
    
    function index(){
		$id = $this->input->get("id");
		
		$data = $this->Model_temuan->get_temuan($id);
		

        $pdf = new FPDFCETAK('L','mm',array(120,80));
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->image('asset/img/back.png', -17, -9, 235, 360);//kiri-kanan, atas, ,
        // setting jenis font yang akan digunakan
 		//$pdf->Image('asset/img/a4.jpg', 12, 6, 0, 17);
		$pdf->SetTextColor(230,230,230);
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,''.$data[0]->jenis_temuan,0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,''.$data[0]->deskripsi_temuan,0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,'Supir S. 9878 UR',0,1,'L');
		$pdf->ln(5);
		$pdf->SetTextColor(0);
		$pdf->SetFont('Arial','',10);
		// $pdf->Image('dokumen/foto-temuan/'.$data[0]->foto_pertama, 12, 22, 25, 30);
		//$pdf->Cell(0,0,''.$data[0]->foto_pertama,0,1,'L');
		$pdf->ln(18);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,'PT PERTAMINA (PERSERO)',0,1,'R');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,0,'MOR VIII Area Maluku - Papua',0,1,'R');
		$pdf->ln(10);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,'12/20',0,1,'L');
		$pdf->ln(5);
		
		//bawah
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Nomor 																:',0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Alamat Kantor  	 :',0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Gol. Darah 										:',0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,0,'Jayapura, 29 Januari 2020',0,1,'R');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,0,'Sr. Spv. Geosecurity Maluku - Papua',0,1,'R');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,0,'TTD',0,1,'R');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,0,'Untung Tarsono',0,1,'R');
		$pdf->ln(4);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Security MOR VIII',0,1,'L');
		$pdf->ln(4);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Telp 											:086-',0,1,'L');
			
        $pdf->Output();
    }


	
}
