<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
        parent::__construct();
        checkSessionUser();
        $this->load->model("Model_user");
	}

	public function index(){
		$this->template->load("template", "user/data-user");
    }

    public function get_data_user($iduser = null){
        $data = $this->Model_user->data_user($iduser);
        echo json_encode(array("status" => "success", "data" => $data));
    }

    public function add_data_user(){
        $fullname = $this->input->post("fullname");
        $email = $this->input->post("email");
        $no_telfon = $this->input->post("no_telfon");
        $no_telfon_kedua = $this->input->post("no_telfon_kedua");
        $level_user = $this->input->post("level_user");
        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $data = array(
            "fullname" => $fullname,
            "email" => $email,
            "no_telfon" => $no_telfon,
            "no_telfon_kedua" => $no_telfon_kedua,
            "level_user" => $level_user,
            "username" => $username,
            "password" => md5($password)
        );

        $check_when_double_email = $this->Model_user->check_existing_user("email", $email);
        $check_when_double_phone = $this->Model_user->check_existing_user("no_telfon", $no_telfon);
        $check_when_double_username = $this->Model_user->check_existing_user("username", $username);
        if($check_when_double_email){
            echo json_encode(array("status" => "error", "message" => "Email Tersebut sudah pernah di daftarkan, silahkan gunakan email lain"));
        } else if($check_when_double_phone){
            echo json_encode(array("status" => "error", "message" => "Nomor Telfon Tersebut sudah pernah di daftarkan, silahkan gunakan nomor telfon lain"));
        } else if($check_when_double_username){
            echo json_encode(array("status" => "error", "message" => "Username Tersebut sudah pernah di daftarkan, silahkan gunakan username lain"));
        } else {
            $add = $this->Model_user->add_user($data);
            if($add){
                echo json_encode(array("status" => "success", "message" => "Tambah data user berhasil", "data" => $data));
            } else {
                echo json_encode(array("status" => "error", "message" => "Gagal menambahkan data user"));
            }
        }
    }

    public function update_data_user(){
        $id_user = $this->input->post("id_user");
        $fullname = $this->input->post("fullname");
        $email = $this->input->post("email");
        $no_telfon = $this->input->post("no_telfon");
        $no_telfon_kedua = $this->input->post("no_telfon_kedua");
        $level_user = $this->input->post("level_user");
        $username = $this->input->post("username");
        $password = $this->input->post("password");

        $data = array(
            "fullname" => $fullname,
            "email" => $email,
            "no_telfon" => $no_telfon,
            "no_telfon_kedua" => $no_telfon,
            "level_user" => $level_user,
            "username" => $username
        );

        if($password != ""){
            $data["password"] = md5($password);
        }

        $update = $this->Model_user->update_user($data, $id_user);
        if($update){
            echo json_encode(array("status" => "success", "message" => "Ubah data user berhasil", "data" => $data));
        } else {
            echo json_encode(array("status" => "error", "message" => "Gagal mengubah data user"));
        }
    }

    public function delete_data_user(){
        $id_user = $this->input->post("id_user");
        $delete = $this->Model_user->delete_user($id_user);
		if($delete){
			echo json_encode(array("status" => "success", "data" => $id_user, "message" => "Berhasil Menghapus Data User"));
		} else {
			echo json_encode(array("status" => "error", "message" => "Gagal Menghapus Data User"));
		}
    }

    public function change_password(){
        $id_user = $this->session->userdata('id_user');
        $password = $this->input->post("password");
        $data = array(
            "password" => md5($password)
        );

        $update = $this->Model_user->update_user($data, $id_user);
        if($update){
            echo json_encode(array("status" => "success", "message" => "Berhasil Mengubah Password"));
        } else {
            echo json_encode(array("status" => "error", "message" => "Gagal Mengubah Password"));
        }

    }
}