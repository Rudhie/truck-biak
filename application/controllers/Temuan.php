<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temuan extends CI_Controller {

	public function __construct(){
		parent::__construct();
        checkSessionUser();
        $this->load->model("Model_temuan");
        $this->load->model("Model_keperluan");
        $this->load->model("Model_truck");
        $this->load->model("Model_pemilik");
        $this->load->model("Model_perpanjangan");
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index(){

        $transportir = $this->input->get("transportir");
        $bulan = $this->input->get("bulan");
        $tahun = $this->input->get("tahun");
        $data["transportir"] = $this->Model_pemilik->getPemilik();
		$data["temuan"] = $this->Model_temuan->get_temuan(null, $transportir, $bulan, $tahun);
		$this->template->load("template", "temuan/data-temuan", $data);
    }

    // public function nopol_pemilik(){
    //     $nopol = $this->Model_temuan->viewNopol($idPemilik);
    //     echo json_encode(array("status" => "success", "data" => $nopol));
    // }

    public function nopol_pemilik(){
        if($this->input->post("id_pemilik")){
            echo $this->Model_temuan->view_nopol($this->input->post('id_pemilik'), $this->input->post("id_truck"));
        }
    }

    public function tambah(){
        $data["temuan"] = $this->Model_temuan->get_temuan();
        $data["pemilik"] = $this->Model_temuan->get_pemilik();
        $this->template->load("template", "temuan/form-temuan", $data);
    }

    public function edit(){
        $id = $this->input->get("id");
        $data["temuan"] = $this->Model_temuan->get_temuan($id);
        $data["pemilik"] = $this->Model_temuan->get_pemilik();
        $this->template->load("template", "temuan/edit-temuan", $data);
    }

    public function detail(){
        $id = $this->input->get("id");
        $data["temuan"] = $this->Model_temuan->get_temuan($id);
        $this->template->load("template", "temuan/detail-temuan", $data);
    }

    public function followup(){
        $id = $this->input->get("id");
        $data["temuan"] = $this->Model_temuan->get_temuan($id);
        $this->template->load("template", "temuan/follow-up", $data);
    }

    public function action_tambah(){
        $temuan = $this->input->post("id_temuan");
        $truck = $this->input->post("id_truck");
        $pemilik = $this->input->post("id_pemilik");
        $jenis_temuan = $this->input->post("jenis_temuan");
        $desc = $this->input->post("deskripsi_temuan");
        $tgl_temuan = $this->input->post("tgl_temuan");
        $status = $this->input->post("status");
        $status_tindak_lanjut = $this->input->post("status_tindak_lanjut");
       
        // $foto_pertama = $this->input->post("foto_pertama");
        // $foto_kedua = $this->input->post("foto_kedua");
        // $foto_ketiga = $this->input->post("foto_ketiga");
        $data = array(
        "id_temuan" => $temuan,
        "id_truck" => $truck,
        "id_pemilik" => $pemilik,
        "jenis_temuan" => $jenis_temuan,
        "deskripsi_temuan" => $desc,
        "tgl_temuan" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_temuan))),
        "status" => $status,
        "status_tindak_lanjut" => $status_tindak_lanjut
       
        // "foto_pertama" => $foto_pertama,
        // "foto_kedua" => $foto_kedua,
        // "foto_ketiga" => $foto_ketiga
        );
        // $config['image_library']='gd2';
        // $config['create_thumb']= FALSE;
        // $config['maintain_ratio']= FALSE;
        // $config['width'] = 200;  
        // $config['height'] = 200; 
        // $config['quality']= '50%';
        
        if(isset($_FILES['foto_pertama']) || isset($_FILES['foto_kedua']) || isset($_FILES['foto_ketiga'])){
                $configFoto['upload_path']= './dokumen/foto-temuan/';
                $configFoto['allowed_types'] = 'jpg|png|jpeg';
                $configFoto['max_size'] = 2048;
                $this->load->library('upload', $configFoto);
                $this->upload->initialize($configFoto);
                if(isset($_FILES['foto_pertama'])){
                    if ($this->upload->do_upload('foto_pertama')){
                        $foto_pertama = $this->upload->data();
                        $config['source_image']='./dokumen/foto-temuan/'.$foto_pertama['file_name'];
                        $config['new_image']= './dokumen/foto-temuan/'.$foto_pertama['file_name'];
                        // $this->load->library('image_lib', $config);
                        // $this->image_lib->resize();

                        $data["foto_pertama"] = $foto_pertama["file_name"];
                    }
                }
    
                if(isset($_FILES['foto_kedua'])){
                    if($this->upload->do_upload('foto_kedua')){
                        $foto_kedua = $this->upload->data();
                        $config['source_image']='./dokumen/foto-temuan/'.$foto_kedua['file_name'];
                        $config['new_image']= './dokumen/foto-temuan/'.$foto_kedua['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();
                        $data["foto_kedua"] = $foto_kedua["file_name"];
                    }
                    
                }
    
                if(isset($_FILES['foto_ketiga'])){
                    if($this->upload->do_upload('foto_ketiga')){
                        $foto_ketiga = $this->upload->data();
                        $config['source_image']='./dokumen/foto-temuan/'.$foto_ketiga['file_name'];
                        $config['new_image']= './dokumen/foto-temuan/'.$foto_ketiga['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();
                        $data["foto_ketiga"] = $foto_ketiga["file_name"];
                    }
                    
                }
            }
        $tambah = $this->Model_temuan->tambah_temuan($data);
            if($tambah){
                $this->session->set_flashdata("success", "Berhasil Menambahkan Temuan");
                $this->send_email_temuan($tambah);
                $this->send_sms($tambah);
            } else {
                $this->session->set_flashdata("error", "Gagal Melakukan Temuan");
            }
            redirect("temuan");
        
        }
    public function action_hapus($id){
        $hapus = $this->Model_temuan->hapus_temuan($id);
        if($hapus){
            $this->session->set_flashdata("success", "Berhasil Menghapus Temuan");
        } else {
            $this->session->set_flashdata("error", "Gagal Menghapus Temuan");
        }
        redirect("temuan");
    }

    public function action_edit($id){
        
        $truck = $this->input->post("id_truck");
        $pemilik = $this->input->post("id_pemilik");
        $jenis_temuan = $this->input->post("jenis_temuan");
        $desc = $this->input->post("deskripsi_temuan");
        $tgl_temuan = $this->input->post("tgl_temuan");
        $status = $this->input->post("status");
        $status_tindak_lanjut = $this->input->post("status_tindak_lanjut");
        
        $data = array(
        "id_truck" => $truck,
        "id_pemilik" => $pemilik,
        "jenis_temuan" => $jenis_temuan,
        "deskripsi_temuan" => $desc,
        "tgl_temuan" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_temuan))),
        "status" => $status,
        "status_tindak_lanjut" => $status_tindak_lanjut
       
        );
         if($_FILES['foto_pertama']['name'] != "" || $_FILES['foto_kedua']['name'] != "" || $_FILES['foto_ketiga']['name'] != ""){
            $configFoto['upload_path']= './dokumen/foto-temuan/';
            $configFoto['allowed_types'] = 'jpg|png|jpeg';
            $configFoto['max_size'] = 1024;
            $this->load->library('upload', $configFoto);
            $this->upload->initialize($configFoto);
            if($_FILES['foto_pertama']['name'] != ""){
                if ($this->upload->do_upload('foto_pertama')){
                    $foto_pertama = $this->upload->data();
                    $data["foto_pertama"] = $foto_pertama["file_name"];
                }                    
            }

            if($_FILES['foto_kedua']['name'] != ""){
                if($this->upload->do_upload('foto_kedua')){
                    $foto_kedua = $this->upload->data();
                    $data["foto_kedua"] = $foto_kedua["file_name"];
                }
                
            }

            if($_FILES['foto_ketiga']['name'] != ""){
                if($this->upload->do_upload('foto_ketiga')){
                    $foto_ketiga = $this->upload->data();
                    $data["foto_ketiga"] = $foto_ketiga["file_name"];
                }
                
            }
        }

        $edit = $this->Model_temuan->edit_temuan($data, $id);
        if($edit){
            $this->session->set_flashdata("success", "Berhasil Mengubah Temuan");
        } else {
            $this->session->set_flashdata("error", "Gagal Mengubah Temuan");
        }
        redirect("temuan");
    }

    public function action_followup($id){
        $komentar_tindak_lanjut = $this->input->post("komentar_tindak_lanjut");
        $tgl_tindak_lanjut = $this->input->post("tgl_tindak_lanjut");

        $data = array(
        "komentar_tindak_lanjut" => $komentar_tindak_lanjut,
        "tgl_tindak_lanjut" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_tindak_lanjut)))
        );
        
        if(isset($_FILES['tindak_lanjut_pertama']) || isset($_FILES['tindak_lanjut_kedua']) || isset($_FILES['tindak_lanjut_ketiga'])){
                $configFoto['upload_path']= './dokumen/foto-tindak-lanjut-temuan/';
                $configFoto['allowed_types'] = 'jpg|png|jpeg';
                $configFoto['max_size'] = 2048;
                $this->load->library('upload', $configFoto);
                $this->upload->initialize($configFoto);
                if(isset($_FILES['tindak_lanjut_pertama'])){
                    if ($this->upload->do_upload('tindak_lanjut_pertama')){
                        $tindak_lanjut_pertama = $this->upload->data();
                        $config['source_image']='./dokumen/foto-tindak-lanjut-temuan/'.$tindak_lanjut_pertama['file_name'];
                        $config['new_image']= './dokumen/foto-tindak-lanjut-temuan/'.$tindak_lanjut_pertama['file_name'];
                        $data["tindak_lanjut_pertama"] = $tindak_lanjut_pertama["file_name"];
                    }
                }
    
                if(isset($_FILES['tindak_lanjut_kedua'])){
                    if($this->upload->do_upload('tindak_lanjut_kedua')){
                        $tindak_lanjut_kedua = $this->upload->data();
                        $config['source_image']='./dokumen/foto-tindak-lanjut-temuan/'.$tindak_lanjut_kedua['file_name'];
                        $config['new_image']= './dokumen/foto-tindak-lanjut-temuan/'.$tindak_lanjut_kedua['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();
                        $data["tindak_lanjut_kedua"] = $tindak_lanjut_kedua["file_name"];
                    }
                    
                }
    
                if(isset($_FILES['tindak_lanjut_ketiga'])){
                    if($this->upload->do_upload('tindak_lanjut_ketiga')){
                        $tindak_lanjut_ketiga = $this->upload->data();
                        $config['source_image']='./dokumen/foto-tindak-lanjut-temuan/'.$tindak_lanjut_ketiga['file_name'];
                        $config['new_image']= './dokumen/foto-tindak-lanjut-temuan/'.$tindak_lanjut_ketiga['file_name'];
                        $this->load->library('image_lib', $config);
                        $this->image_lib->resize();
                        $data["tindak_lanjut_ketiga"] = $tindak_lanjut_ketiga["file_name"];
                    }
                    
                }
            }
        $followup = $this->Model_temuan->edit_temuan($data, $id);
            if($followup){
                $this->session->set_flashdata("success", "Berhasil Melakukan Tindak Lanjut");
            } else {
                $this->session->set_flashdata("error", "Gagal Melakukan Tindak Lanjut");
            }
            redirect("temuan");
        
        }

    public function tanggal_indo($tanggal){
        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $split = explode('-', $tanggal);
        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

    public function send_email_temuan($id_temuan){
        $temuan = $this->Model_temuan->get_temuan($id_temuan);
        
        $to = $this->Model_perpanjangan->get_email("PEMILIK", $temuan[0]->id_pemilik);
        $general = $this->Model_perpanjangan->get_email_general();

        $email_to = array();
        $email_general = array();
        foreach($to as $tos){
            array_push($email_to, $tos['email']);
        }

        foreach($general as $gen){
            array_push($email_general, $gen['email']);
        }

        $message = '<html><body><div style="background-color:#e8e8e8;padding: 5%;">
                        <div style="width: 100%;height: 80px;background: -webkit-linear-gradient(45deg, #008cc9,#009ea5);
                            background: -moz-linear-gradient(45deg, #008cc9,#009ea5);
                            background: -o-linear-gradient(45deg, #008cc9,#009ea5);
                            background: linear-gradient(45deg, #008cc9,#009ea5);
                            border-radius: 10px 10px 0px 0px;">
                            <center><img src="http://pertaminabiak.com/asset/img/logo-pertamina.png" width="200px;" style="margin-top:15px"/></center>
                        </div>
                        <div style="background-color:#ffffff;padding: 2%;">
                            <center>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;"><p>Kepada Yth. Bapak/Ibu Transportir '.$temuan[0]->nama_perusahaan.'</p>
                                    <p>Pada tanggal '.$this->tanggal_indo($temuan[0]->tgl_temuan).' Admin HSSE mendapatkan temuan pada kendaraan truck sebagai berikut :</p>
                                </span>
                                <table width="50%" border="1">
                                    <tr>
                                        <th>Jenis Temuan</th>
                                        <td>'.$temuan[0]->jenis_temuan.'</td>
                                    </tr>
                                    <tr>
                                        <th>Deskripsi Temuan</th>
                                        <td>'.$temuan[0]->deskripsi_temuan.'</td>
                                    </tr>
                                    <tr>
                                        <th>No Polisi</th>
                                        <td>'.$temuan[0]->no_polisi.'</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Temuan</th>
                                        <td>'.$this->tanggal_indo($temuan[0]->tgl_temuan).'</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>'.$temuan[0]->status.'</td>
                                    </tr>';
            $message .= '</table>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;">
                                    <p>Mohon untuk Bapak/Ibu Transportir '.$temuan[0]->nama_perusahaan.' untuk segera menindaklanjuti dan memperbaiki temuan ini dan segera melakukan peroses perbaikan.</p>
                                </span>
                                <span style="font-family: sans-serif;font-size: 13px;color: #7b7b7b;">
                                    <p>Untuk melihat detail temuan bisa di klik link dibawah ini <br/><br/><a target="_blank" href="'.base_url('temuan/detail?id=').$id_temuan.'">https:'.base_url('temuan/detail?id=').$id_temuan.'</a></p>
                                </span>
                                <span style="font-size: 13px;font-style: normal;font-family: sans-serif;color: #7b7b7b;">Terima Kasih</span>
                                </center>
                            </div>
                            <div style="color: #a6a6a6;font-family: sans-serif;font-size: 12px;">
                                <center>
                                    <p>&copy;'.date('Y').' Sistem Informasi dan Monitoring Truck Tangki</p>
                                    <p>PT. Pertamina (persero) Fuel Terminal Biak</p>
                                </center>
                            </div>
                        </div></body></html>';


                        $config = array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'mail.pertaminabiak.com',
                            'smtp_port' => 587,
                            'smtp_user' => 'noreply@pertaminabiak.com',
                            'smtp_pass' => 'culametanmetmet',
                            'mailtype'  => 'html', 
                            'charset'  => 'iso-8859-1'
                        );
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->set_mailtype("html");
                        $this->email->from('noreply@pertaminabiak.com', 'PT. Pertamina Fuel Terminal Biak');
                        $this->email->to($email_to);
                        $this->email->cc($email_general);
                        $this->email->subject('Notifikasi Temuan Mobil Tangki - '.strtoupper($temuan[0]->nama_perusahaan).' - '.strtoupper($temuan[0]->jenis_temuan)); 
                        $this->email->message($message);
                        $this->email->send();
                        return true;
    }

    public function send_sms($id_temuan){
        $temuan = $this->Model_temuan->get_temuan($id_temuan);

        $message = "Mobil Tangki ".$temuan[0]->no_polisi.", terdapat temuan aspek safety ".strtoupper($temuan[0]->jenis_temuan)."' pada Mobil Tangki anda . Segera lakukan  tindaklanjut perbaikan dengan cara klik tautan di bawah atau segera koordinasi dengan HSSE Fuel Terminal Biak. Untuk lebih detail dapat klik tautan berikut : https:".base_url('temuan/detail')."?id=".$id_temuan;
        if($temuan[0]->no_telfon != null){
            $telepon = $temuan[0]->no_telfon;
            $this->sms_config($telepon, $message);
        }

        if($temuan[0]->no_telfon_kedua != null){
            $telepon = $temuan[0]->no_telfon_kedua;
            $this->sms_config($telepon, $message);
        }
    }

    public function sms_config($telepon, $message){
        $userkey = 'c0027701b6e4';
        $passkey = '7be0a5670850b9b3ac359761';
        
        $url = 'https://masking.zenziva.net/api/sendsms/';
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => $userkey,
            'passkey' => $passkey,
            'nohp' => $telepon,
            'pesan' => $message
        ));
        $results = json_decode(curl_exec($curlHandle), true);
        curl_close($curlHandle);
    }

    public function export(){
        $transportir = $this->input->get("transportir");

    $bulan = $this->input->get("bulan");
    $tahun = $this->input->get("tahun");
   
        //$data["transportir"] = $this->Model_pemilik->getPemilik();
        //$data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan("truck", null, $transportir, $bulan, $tahun, $status);
        //$this->template->load("template", "perpanjangan/data-perpanjangan", $data);
    include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
    
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
   
    // Settingan awal fil excel
    $excel->getProperties()->setCreator('Team Programmer')
                 ->setLastModifiedBy('Team Programmer')
                 ->setTitle("PT PERTAMINA")
                 ->setSubject("Laporan Data Temuan ")
                 ->setDescription("Laporan Semua Data Temuan")
                 ->setKeywords("PT PERTAMINA");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      //'color' => array('argb'=> 'FF808080'),
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      ),
      'fill' => array(
        'type' => PHPExcel_Style_Fill :: FILL_SOLID,
        'startcolor' => array(
              'argb' => 'C0C0C0C0'
            )
      )
    );
   $gdImage = imagecreatefrompng('asset/img/logo-pertamina.png');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');
$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setOffsetX(15);//samping kanan kiri
$objDrawing->setOffsetY(7);
$objDrawing->setHeight(50);
$objDrawing->setCoordinates('D1');
$objDrawing->setWorksheet($excel->getActiveSheet());

$gdImage = imagecreatefrompng('asset/img/truck.png');
// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
$objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
$objDrawing->setName('Sample image');
$objDrawing->setDescription('Sample image');
$objDrawing->setImageResource($gdImage);
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setOffsetX(75);
$objDrawing->setOffsetY(7);
$objDrawing->setHeight(50);
//$objDrawing->setWidth(20);
$objDrawing->setCoordinates('G1');
$objDrawing->setWorksheet($excel->getActiveSheet());

    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      ),
      // 'fill' => array(
      //   'type' => PHPExcel_Style_Fill :: FILL_GRADIENT_LINEAR,
      //   'startcolor' => array(
      //         'argb' => 'FFA0A0A0'
      //       ),
      //   'endcolor' => array(
      //         'argb' => 'FFFFFFFF'

   
      // ))
    );

        function tanggal_indo($tanggal){
                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                        $split = explode('-', $tanggal);
                        return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                   }
                   function bulan_indo($tanggal){
                        $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                        $split = explode('-', $tanggal);
                        return $bulan[ (int)$split[1] ] ;
                   }
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "PT PERTAMINA"); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A1:I1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20); 
    $excel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1

    $excel->setActiveSheetIndex(0)->setCellValue('A2', "DATA LAPORAN TEMUAN ".$tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
    $excel->getActiveSheet()->mergeCells('A2:I2'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); 
    $excel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    // Buat header tabel nya pada baris ke 3
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "Bulan");
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "No");
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "No Polisi"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "Jenis Temuan"); // Set kolom B3 dengan tulisan"
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "Deskripsi"); // Set kolom C3 dengan tulisan "NAMA"
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "Tanggal Temuan"); // Set kolom A3 dengan tulisan "NO"
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "Tindak Lanjut"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "Tanggal Tindak Lanjut"); // Set kolom B3 dengan tulisan "NIS"
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "Status"); // Set kolom C3 dengan tulisan "NAMA"

    // Apply style header yang telah kita buat tadi ke masing-masing kolom header
    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);


    $numrow = 4;
    //$no = 0;
    //$y = 0;
    $t = 0;
    
    $temuan = $this->Model_temuan->get_temuan(null, $transportir, $bulan, $tahun);
    $no = 1;
    
    if($temuan){
      $total_data = count($temuan);
      foreach($temuan as $data){ // Lakukan looping pada variabel siswa
        //$excel->setActiveSheetIndex(0)->setCellValue('A2', "Laporan Po Tahun ".$tahun);
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, bulan_indo($data->tgl_temuan));
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $no);
        $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->no_polisi);
        $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->jenis_temuan);
        $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->deskripsi_temuan);
        $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, tanggal_indo($data->tgl_temuan));
        $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->tindak_lanjut);
        $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, tanggal_indo($data->tgl_tindaklanjut));
        $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->status);
 
        // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
  
        $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
        // $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        // $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
  
        $no++; // Tambah 1 setiap kali looping
        $numrow++; // Tambah 1 setiap kali looping
        $t++;
       
      }

      $excel->setActiveSheetIndex(0)->mergeCells('A'.($numrow - ($total_data)).':A'.($numrow + $t - $total_data - 1));
      $t = 0;
    
    
    
  }

    
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(10); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(35); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(45); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(35); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(35); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(35); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(25); // Set width kolom C
   
    // $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    // $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
  
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);



    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Laporan Data Temuan");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Laporan Data Temuan.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
  }

}