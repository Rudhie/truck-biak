<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Idcard extends CI_Controller {

	public function __construct(){
		parent::__construct();
        checkSessionUser();
		$this->load->model("Model_idcard");
        $this->load->model("Model_perpanjangan");
        $this->load->model("Model_pemilik");
        $this->load->model("Model_awak_kernet");
        $this->load->library('pdfcetak');
        date_default_timezone_set('Asia/Jakarta');
	}

	public function index(){
        $transportir = $this->input->get("transportir");
        $bulan = $this->input->get("bulan");
        $tahun = $this->input->get("tahun");
        $status = $this->input->get("status");
        $data["transportir"] = $this->Model_pemilik->getPemilik();
        $data["idcard"] = $this->Model_perpanjangan->get_perpanjangan("idcard", null, $transportir, $bulan, $tahun, $status, TRUE);
		$this->template->load("template", "idcard/data-idcard", $data);
	}
	
	public function tambah(){
		$data["supir"] = $this->Model_perpanjangan->get_supir();
		$data["keperluan"] = $this->Model_perpanjangan->get_keperluan_by_name("ID CARD");
		$this->template->load("template", "idcard/form-idcard", $data);
    }
    
    public function edit(){
        $id = $this->input->get("id");
        $page = $this->input->get("page");
        $data["supir"] = $this->Model_perpanjangan->get_supir();
        $data["keperluan"] = $this->Model_perpanjangan->get_keperluan_by_name("ID CARD");
        $data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan($page, $id);
        $data["dok_perpanjangan"] = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);
		$this->template->load("template", "idcard/edit-idcard", $data);
    }

    function cetak_idcard(){
		$id = $this->input->get("id");
        $data = $this->Model_perpanjangan->get_perpanjangan("idcard", $id);
        $dokumen = $this->Model_perpanjangan->get_dokumen_perpanjangan($id);

        $pdf = new FPDFCETAK('L','mm',array(120,66));
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->image('asset/img/bg-idcard-front.jpg', 7, 3, 90, 60);//kiri-kanan, atas, ,
     
		$pdf->SetTextColor(230,230,230);
        $pdf->SetLineWidth(0);
        $pdf->Line(5,2,113,2);
        $pdf->Line(5,64,113,64);
        $pdf->Line(5,64,5,2);
        $pdf->Line(113,64,113,2);
       
        $pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,''.$data[0]->nama_awak,0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,''.$data[0]->nama_perusahaan,0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,$data[0]->bagian.' '.$data[0]->no_polisi,0,1,'L');
		$pdf->ln(5);
		$pdf->SetTextColor(0);
		$pdf->SetFont('Arial','',10);
		$pdf->Image('dokumen/perpanjangan/'.$dokumen[1]->dokumen_syarat, 12, 23, 25, 30);
		// $pdf->Cell(0,0,''.$data[0]->foto_pertama,0,1,'L');
		$pdf->ln(22);
        $pdf->SetAutoPageBreak(false);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,'PT PERTAMINA (PERSERO)',0,1,'R');

		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,0,'MOR VIII Area Maluku - Papua',0,1,'R');
		$pdf->ln(8);
        $pdf->SetAutoPageBreak(false, 15);
        $pdf->SetAutoPageBreak(true);
		$pdf->SetFont('Arial','',10);
		$pdf->Cell(0,0,date('m')."/".(date('y') + 1),0,1,'L');
		$pdf->ln(10);
		
		//bawah
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Nomor 																: 579 / IDC-Q28060/2020-S0',0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Alamat Kantor  	 : Jl. Nimboran No 2-4 Jayapura',0,1,'L');
		$pdf->ln(5);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Gol. Darah 										:',0,1,'L');
		$pdf->ln(10);
        $pdf->SetAutoPageBreak(false, 15);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(150,0,'Jayapura, '.$this->tanggal_indo(date("Y-m-d")),0,1,'C');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,0,'Sr. Spv. Geosecurity Maluku - Papua',0,1,'R');
		$pdf->ln(5);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(150,0,'TTD',0,1,'C');
		$pdf->ln(11);
        $pdf->SetAutoPageBreak(false, 15);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(150,0,'Cevi Firmansyah',0,1,'C');
		$pdf->ln(4);
        $pdf->SetAutoPageBreak(false, 15);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Security MOR VIII',0,1,'L');
		$pdf->ln(4);
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(0,0,'Telp 											:0967-541369',0,1,'L');
         $pdf->SetLineWidth(0);
        $pdf->Line(5,2,113,2);
        $pdf->Line(5,64,113,64);
        $pdf->Line(5,64,5,2);
        $pdf->Line(113,64,113,2);
       
			
        $pdf->Output();
    }

    public function tanggal_indo($tanggal){
    	$bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
    	$split = explode('-', $tanggal);
    	return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    }

    public function export(){
    
        $transportir = $this->input->get("transportir");
        $bulan = $this->input->get("bulan");
        $tahun = $this->input->get("tahun");
        $status = $this->input->get("status");
            //$data["transportir"] = $this->Model_pemilik->getPemilik();
            //$data["perpanjangan"] = $this->Model_perpanjangan->get_perpanjangan("truck", null, $transportir, $bulan, $tahun, $status);
            //$this->template->load("template", "perpanjangan/data-perpanjangan", $data);
        include_once APPPATH. '/third_party/PHPExcel/PHPExcel.php';
        
        // Panggil class PHPExcel nya
        $excel = new PHPExcel();
       
        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Team Programmer')
                     ->setLastModifiedBy('Team Programmer')
                     ->setTitle("PT PERTAMINA")
                     ->setSubject("Laporan Data Idcard")
                     ->setDescription("Laporan Semua Data Idcard")
                     ->setKeywords("PT PERTAMINA");
        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
          'font' => array('bold' => true), // Set font nya jadi bold
          //'color' => array('argb'=> 'FF808080'),
          'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
          ),
          'borders' => array(
            'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
            'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
            'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
            'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
          ),
          'fill' => array(
            'type' => PHPExcel_Style_Fill :: FILL_SOLID,
            'startcolor' => array(
                  'argb' => 'C0C0C0C0'
                )
          )
        );
       $gdImage = imagecreatefrompng('asset/img/logo-pertamina.png');
    // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    $objDrawing->setName('Sample image');
    $objDrawing->setDescription('Sample image');
    $objDrawing->setImageResource($gdImage);
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setOffsetX(75);//samping kanan kiri
    $objDrawing->setOffsetY(7);
    $objDrawing->setHeight(50);
    $objDrawing->setCoordinates('E1');
    $objDrawing->setWorksheet($excel->getActiveSheet());
    
    $gdImage = imagecreatefrompng('asset/img/truck.png');
    // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
    $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
    $objDrawing->setName('Sample image');
    $objDrawing->setDescription('Sample image');
    $objDrawing->setImageResource($gdImage);
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setOffsetX(75);
    $objDrawing->setOffsetY(7);
    $objDrawing->setHeight(50);
    //$objDrawing->setWidth(20);
    $objDrawing->setCoordinates('H1');
    $objDrawing->setWorksheet($excel->getActiveSheet());
    
        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
          'alignment' => array(
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
          ),
          'borders' => array(
            'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
            'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
            'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
            'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
          ),
          // 'fill' => array(
          //   'type' => PHPExcel_Style_Fill :: FILL_GRADIENT_LINEAR,
          //   'startcolor' => array(
          //         'argb' => 'FFA0A0A0'
          //       ),
          //   'endcolor' => array(
          //         'argb' => 'FFFFFFFF'
    
       
          // ))
        );
    
            function tanggal_indo($tanggal){
                            $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            $split = explode('-', $tanggal);
                            return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                       }
                       function bulan_indo($tanggal){
                            $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                            $split = explode('-', $tanggal);
                            return $bulan[ (int)$split[1] ] ;
                       }
        $excel->setActiveSheetIndex(0)->setCellValue('A1', "PT PERTAMINA"); // Set kolom A1 dengan tulisan "DATA SISWA"
        $excel->getActiveSheet()->mergeCells('A1:J1'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20); 
        $excel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    
        $excel->setActiveSheetIndex(0)->setCellValue('A2', "DATA LAPORAN IDCARD ".$tahun); // Set kolom A1 dengan tulisan "DATA SISWA"
        $excel->getActiveSheet()->mergeCells('A2:J2'); // Set Merge Cell pada kolom A1 sampai E1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); 
        $excel->getActiveSheet()->getStyle('A2')->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);// Set font size 15 untuk kolom A1
        $excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
        // Buat header tabel nya pada baris ke 3
        $excel->setActiveSheetIndex(0)->setCellValue('A3', "Bulan");
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "No");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "Transportir"); // Set kolom A3 dengan tulisan "NO"
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "Awak"); // Set kolom B3 dengan tulisan"
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "Mobil Tangki"); // Set kolom C3 dengan tulisan "NAMA"
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "Bagian"); // Set kolom A3 dengan tulisan "NO"
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "Keperluan"); // Set kolom B3 dengan tulisan "NIS"
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "Status"); // Set kolom C3 dengan tulisan "NAMA"
        $excel->setActiveSheetIndex(0)->setCellValue('I3', "Tanggal Pengajuan"); // Set kolom A3 dengan tulisan "NO"
        $excel->setActiveSheetIndex(0)->setCellValue('J3', "Komentar");   
        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
    
        $numrow = 4;
        //$no = 0;
        //$y = 0;
        $t = 0;
        
        $perpanjangan = $this->Model_idcard->get_data_idcard(null, $transportir, $bulan, $tahun, $status);
     
        $no = 1;
        
        if($perpanjangan){
          $total_data = count($perpanjangan);
          foreach($perpanjangan as $data){ // Lakukan looping pada variabel siswa
            //$excel->setActiveSheetIndex(0)->setCellValue('A2', "Laporan Po Tahun ".$tahun);
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, bulan_indo($data->tgl_aktifitas));
            $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama_perusahaan);
            $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->nama_awak);
            $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->no_polisi);
            $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->bagian);
            $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->nm_keperluan);
            $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->status_aktifitas);
            $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, tanggal_indo($data->tgl_aktifitas));
            $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->komentar);
            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      
            $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
    
            // $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
            // $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      
            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
            $t++;
           
          }
    
          $excel->setActiveSheetIndex(0)->mergeCells('A'.($numrow - ($total_data)).':A'.($numrow + $t - $total_data - 1));
          $t = 0;
        
        
        
      }
    
        
        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(10); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(45); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(45); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(35); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(20); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(35); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('I')->setWidth(40); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('J')->setWidth(60); // Set width kolom C
        // $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
        // $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
      
        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    
    
    
        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle("Data Laporan Idcard");
        $excel->setActiveSheetIndex(0);
        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Data Laporan Idcard.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');
        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
      }
}